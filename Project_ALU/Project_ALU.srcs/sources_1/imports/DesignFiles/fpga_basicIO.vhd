----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/13/2016 07:01:44 PM
-- Design Name: 
-- Module Name: fpga_basicIO - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
--use IEEE.STD_LOGIC_ARITH.ALL;
--use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fpga_basicIO is
  port (
    clk: in std_logic;                            -- 100MHz clock
    btnC, btnU, btnL, btnR, btnD: in std_logic;   -- buttons
    sw: in std_logic_vector(15 downto 0);         -- switches
    led: out std_logic_vector(15 downto 0);       -- leds
    an: out std_logic_vector(3 downto 0);         -- display selectors
    seg: out std_logic_vector(6 downto 0);        -- display 7-segments
    dp: out std_logic                             -- display point
  );
end fpga_basicIO;

architecture Behavioral of fpga_basicIO is
  signal dd3, dd2, dd1, dd0 : std_logic_vector(6 downto 0);
  signal res : std_logic_vector(15 downto 0);
  signal dact : std_logic_vector(3 downto 0);
  signal btnRinstr : std_logic_vector(5 downto 0);
  signal clk10hz, clk_disp : std_logic;
  signal btnCreg, btnUreg, btnLreg, btnRreg, btnDreg: std_logic;   -- registered input buttons
  signal sw_reg : std_logic_vector(15 downto 0);  -- registered input switches
  signal negative : std_logic;
  signal real_value : std_logic_vector(12 downto 0);  -- registered input switches
  signal real_result : std_logic_vector(15 downto 0);  -- registered input switches
  signal neg : std_logic;
  component disp7
  port (
   disp3, disp2, disp1, disp0 : in std_logic_vector(6 downto 0);
   dp3, dp2, dp1, dp0 : in std_logic;
   dclk : in std_logic;
   dactive : in std_logic_vector(3 downto 0);
   en_disp_l : out std_logic_vector(3 downto 0);
   segm_l : out std_logic_vector(6 downto 0);
   dp_l : out std_logic);
  end component;
  component hex2disp
    port (
      sw : in std_logic_vector(3 downto 0);
      seg : out std_logic_vector(6 downto 0));
  end component;
  component clkdiv
    port(
      clk100M : in std_logic;          
      clk10Hz : out std_logic;
      clk_disp : out std_logic);
  end component;
  component alu is
      Port ( oper : in std_logic_vector(5 downto 0);
             reset : in STD_LOGIC;
             value : in std_logic_vector(12 downto 0);
             instr : in std_logic_vector(1 downto 0);
             clk : in std_logic;
             selection : in std_logic;
             result : out std_logic_vector(15 downto 0));
  end component;

begin
  led <= sw_reg;
--  led(15 downto 7) <= (others => '0');
--  led(6 downto 0) <= dd0;
    
  dact <= "1111";

  inst_disp7: disp7 port map(
      disp3 => dd3, disp2 => dd2, disp1 => dd1, disp0 => dd0,
      dp3 => neg, dp2 => neg, dp1 => neg, dp0 => neg,  
      dclk => clk_disp,
      dactive => dact,
      en_disp_l => an,
      segm_l => seg,
      dp_l => dp);
--7 segmentos 16 bits
   
  inst_hex0: hex2disp port map(sw => real_result(3 downto 0), seg => dd0);
  inst_hex1: hex2disp port map(sw => real_result(7 downto 4), seg => dd1);
  inst_hex2: hex2disp port map(sw => real_result(11 downto 8), seg => dd2);
  inst_hex3: hex2disp port map(sw => real_result(15 downto 12), seg => dd3);
  
  
  inst_clkdiv: clkdiv port map(
    clk100M => clk,
    clk10hz => clk10hz,
    clk_disp => clk_disp); 
 
 --ALU   
 -- Left Soma / Right Subtrai / Up And / Down OR / Sw12 Mul / Sw13 Shift
 -- Falta mostrar com o ponto (sign magnitude) e um botao pra converter o valor
 -- Do registro em numero negaitvo
 -- negative Up
 
  negative <= btnLreg;
  btnRinstr <= sw_reg(13 downto 12) & btnDreg & btnUreg & btnRreg & btnLreg;
  inst_alu: alu port map(
      oper => btnRinstr, -- Botoes cima, esq,direita
      clk => clk,
      reset => btnCreg, -- botao do meio
      instr => sw_reg(15 downto 14), -- ultimas 5 switches
      value => real_value, -- valor com o negativo
      --carry_out => dp,
      selection => sw_reg(0),
      result => res);
      
  process (clk10hz)
    begin
       if rising_edge(clk10hz) then
          btnCreg <= btnC; btnUreg <= btnU; btnLreg <= btnL; 
          btnRreg <= btnR; btnDreg <= btnD;
          sw_reg <= sw;
          
          if negative = '1' then
            real_value <= std_logic_vector('1' & (0 - signed(sw(11 downto 0))));
            neg <= '1';
          else 
            real_value <= '0' & sw(11 downto 0);
            neg <= '0';
          end if;
          
          if res(15) = '1' then
            real_result <= std_logic_vector(0-signed(res));
            neg <= '1';
          else
            real_result <= res;
            neg <= '0';
          end if;
         
      end if; 
    end process;    
end Behavioral;

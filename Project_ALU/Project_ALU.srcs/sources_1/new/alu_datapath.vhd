----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/09/2018 06:35:05 PM
-- Design Name: 
-- Module Name: alu_datapath - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
--use IEEE.STD_LOGIC_SIGNED.ALL


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity alu_datapath is
    Port ( ctrl : in std_logic_vector(2 downto 0);
           value : in std_logic_vector(12 downto 0);
           instr : in std_logic_vector(1 downto 0);
           clk : in std_logic;
           result : out std_logic_vector(15 downto 0);
           selection : in std_logic;
           rst,enable: in std_logic);
           
end alu_datapath;

architecture Behavioral of alu_datapath is
signal res_alu : std_logic_vector (15 downto 0);

signal r2 : std_logic_vector (15 downto 0) := (others => '0');
signal r1 : std_logic_vector (15 downto 0) := (others => '0');
signal alu : std_logic_vector (31 downto 0);
signal r2sg : signed (15 downto 0);
signal r1sg : signed (15 downto 0);
signal alusg: signed (31 downto 0);
signal valuesg : signed (12 downto 0);
signal aux_result : std_logic_vector (15 downto 0);
begin


valuesg <= signed(value);
r1sg <= signed(r1);
r2sg <= signed(r2);
alu <= std_logic_vector(alusg);



-- ALU --

alusg <= resize((r2sg + r1sg),32) when ctrl = "001" else 
resize((r2sg - r1sg),32) when ctrl ="010" else
resize((r2sg AND r1sg),32)when ctrl ="011" else  
resize((r2sg OR r1sg),32) when ctrl = "100" else
(r1sg * r2sg) when ctrl = "101" else 
resize((shift_right(r2sg, to_integer((r1sg(2 downto 0))))),32) when ctrl = "110" else
resize((r2sg),32);


--Registers
process (clk)
    begin
        if clk'event and clk='1' then  
            if rst='1' then   
            r1 <= X"0000";
            elsif instr = "10" then 
            r1 <= std_logic_vector(resize(valuesg,16));
            end if;
            
        end if;
    end process;
    
process (clk)
    begin
        if clk'event and clk='1' then  
            if rst='1' then   
            r2 <= X"0000";
            elsif instr = "11" or (instr = "00" and enable = '1') then
            r2 <= res_alu;       
            end if; 
        end if;
    end process;


--Multiplexer
res_alu <= std_logic_vector(resize(alusg,16)) when instr="00" else std_logic_vector(resize(valuesg,16));
    
--Result

result <= r1 when (instr = "10" or (selection = '1'  and  instr(1) = '0')) else r2;

--esult <= "0000000000000" & ctrl;
end Behavioral;

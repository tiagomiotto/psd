----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/09/2018 06:35:05 PM
-- Design Name: 
-- Module Name: alu_control - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity alu_control is
    Port ( oper : in std_logic_vector(5 downto 0);
           reset,clk : in std_logic;
           ctrl : out std_logic_vector(2 downto 0);
           enable : out std_logic);
end alu_control;

architecture Behavioral of alu_control is


 type fsm_states is ( s_initial, s_end, s_sub, s_add, s_and, s_or,s_mul, s_shft);
signal currstate, nextstate: fsm_states;

begin
state_reg: process (clk)
begin 
  if clk'event and clk = '1' then
    if reset = '1' then
      currstate <= s_initial ;
      
    else
      currstate <= nextstate ;
      
    end if ;
  end if ;
end process;

state_comb: process (currstate, oper)
begin  --  process
  nextstate <= currstate ;  
  -- by default, does not change the state.
  
  case currstate is
    when s_initial =>
      if oper="000001" then
        nextstate <= s_add ;
      elsif oper="000010" then
        nextstate <= s_sub ;
      elsif oper="000100" then
        nextstate <= s_and;
      elsif oper="001000" then
        nextstate <= s_or;
      elsif oper="010000" then
        nextstate <= s_mul;
      elsif oper="100000" then
        nextstate <= s_shft;
      end if;
      ctrl<="000";
      enable<='0';
      
      
    when s_add =>
      nextstate <= s_end;
      ctrl<="001";
      enable<='1';
            
    when s_sub =>
      nextstate <= s_end;
      ctrl<="010";
      enable<='1';     
    when s_and =>
      nextstate <= s_end;
      ctrl<="011";
      enable<='1';
    when s_or =>
      nextstate <= s_end;
      ctrl<="100";
      enable<='1';
    when s_mul =>
      nextstate <= s_end;
      ctrl<="101";
      enable<='1';
      
    when s_shft =>
      nextstate <= s_end;
      ctrl<="110";
      enable<='1';
      
    when s_end =>
      if oper="000000" then
        nextstate <= s_initial ;
      end if;
      ctrl<="000";
      enable<='1';

  end case;
end process;

end Behavioral;

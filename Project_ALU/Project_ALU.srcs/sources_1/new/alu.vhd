----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/09/2018 06:35:05 PM
-- Design Name: 
-- Module Name: alu - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity alu is
    Port ( oper : in std_logic_vector(5 downto 0);
           reset : in std_logic;
           value : in std_logic_vector(12 downto 0);
           instr : in std_logic_vector(1 downto 0);
           clk : in std_logic;
            selection : in std_logic;
           result : out std_logic_vector(15 downto 0));
end alu;

architecture Behavioral of alu is
    component alu_datapath
    Port ( ctrl : in std_logic_vector(2 downto 0);
           value : in std_logic_vector(12 downto 0);
           instr : in std_logic_vector(1 downto 0);
           clk : in std_logic;
           result : out std_logic_vector(15 downto 0);
           selection : in std_logic;
           rst,enable : in std_logic);
           
    end component;
    
    component alu_control
        Port ( oper : in std_logic_vector(5 downto 0);
               reset,clk : in std_logic;
               ctrl : out std_logic_vector(2 downto 0);
               enable : out std_logic);
    end component;
    
    signal ctrl : std_logic_vector(2 downto 0);
    signal rst : std_logic;
    signal enable : std_logic;
    
begin
    inst_alu_datapath: alu_datapath port map(
        clk => clk,
        result => result,
        ctrl => ctrl,
        value => value,
        instr => instr,
        rst => reset,
        selection=>  selection,
        enable => enable

        );
        
    inst_alu_control: alu_control port map(
        oper =>oper,
        reset =>reset,
        clk => clk,
        ctrl => ctrl,
        enable => enable
        );
    
--COlocar isso commo componente da fpga
end Behavioral;

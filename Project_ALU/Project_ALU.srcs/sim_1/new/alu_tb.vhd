----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/19/2018 12:07:56 PM
-- Design Name: 
-- Module Name: alu_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity alu_tb is
end alu_tb;

architecture Behavioral of alu_tb is
    COMPONENT alu
        Port ( oper : in std_logic_vector(5 downto 0);
           reset : in std_logic;
           value : in std_logic_vector (12 downto 0);
           instr : in std_logic_vector (1 downto 0);
           clk : in std_logic;
           selection : in std_logic;
           result : out std_logic_vector (15 downto 0)
           );
    END COMPONENT;
    
    
    --Inputs
    signal clk : std_logic := '0';
    signal rst : std_logic := '0';
    signal selection : std_logic := '0';
    signal instr : std_logic_vector(1 downto 0) := (others => '0');
    signal value : std_logic_vector(12 downto 0) := (others => '0');
    signal oper : std_logic_vector (5 downto 0) := (others => '0');
 
      --Outputs
    signal res: std_logic_vector(15 downto 0);
    --signal carry: std_logic;
 
    -- Clock period definitions
    constant clk_period : time := 10 ns;
           
BEGIN
    
    --UUT
       uut: alu PORT MAP (
       clk => clk,
       reset => rst,
       instr => instr,
       value => value,
       result => res,
       oper =>  oper,
        selection => selection
     );
   -- Clock definition
     clk <= not clk after clk_period/2;
  
      -- Stimulus process
     stim_proc: process
     begin        
        -- hold reset state for 100 ns.
        wait for 100 ns;    
  
        wait for clk_period*10;
  
        -- insert stimulus here 
        -- note that input signals should never change at the positive edge of the clock
        rst <= '1' after 20 ns,
               '0' after 40 ns;
        value <=   "0000000000010" after  40 ns,
                   "0000000001010" after 220 ns,
                   "0000000001110" after 300 ns;
        oper <=    "000001" after  300 ns,
                 "000000" after  380 ns,
                 "000010" after  460 ns, --sub
                 "000000" after  540 ns,
                 "000100" after 620 ns, --and
                 "000000" after  700 ns,
                 "001000" after 780 ns, -- or
                "000000" after  860 ns,
                 "010000" after 940 ns, -- mul
                "000000" after  1000 ns,
                 "100000" after 1080 ns, -- shft
                 "000000" after  1160 ns;
                 
        instr <= "11" after 40 ns, "00" after 160ns , "10" after 220 ns, "01" after 300 ns;
        
        selection <= '0' after 40ns, '1' after 1170 ns; 
  
        wait;
     end process;
END;
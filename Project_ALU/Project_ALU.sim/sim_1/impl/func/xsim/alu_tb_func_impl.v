// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Fri Oct 26 11:25:31 2018
// Host        : LAPTOP-TIAGO running 64-bit major release  (build 9200)
// Command     : write_verilog -mode funcsim -nolib -force -file
//               C:/Users/tiago/Desktop/Programing/git-repos/psd/Project_ALU/Project_ALU.sim/sim_1/impl/func/xsim/alu_tb_func_impl.v
// Design      : fpga_basicIO
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module alu
   (D,
    p_0_in,
    CLK,
    btnCreg,
    A,
    Q,
    oper,
    \sw_reg_reg[14] );
  output [15:0]D;
  output p_0_in;
  input CLK;
  input btnCreg;
  input [12:0]A;
  input [3:0]Q;
  input [3:0]oper;
  input \sw_reg_reg[14] ;

  wire [12:0]A;
  wire CLK;
  wire [2:0]COUNT;
  wire [15:0]D;
  wire [3:0]Q;
  wire btnCreg;
  wire [15:0]data0;
  wire [15:0]data1;
  wire inst_alu_control_n_0;
  wire inst_alu_control_n_1;
  wire inst_alu_control_n_15;
  wire inst_alu_control_n_16;
  wire inst_alu_control_n_17;
  wire inst_alu_control_n_18;
  wire inst_alu_control_n_19;
  wire inst_alu_control_n_2;
  wire inst_alu_control_n_20;
  wire inst_alu_control_n_21;
  wire inst_alu_control_n_22;
  wire inst_alu_control_n_23;
  wire inst_alu_control_n_24;
  wire inst_alu_control_n_25;
  wire inst_alu_control_n_26;
  wire inst_alu_control_n_27;
  wire inst_alu_control_n_28;
  wire inst_alu_control_n_29;
  wire inst_alu_control_n_31;
  wire inst_alu_control_n_32;
  wire inst_alu_control_n_33;
  wire inst_alu_control_n_34;
  wire inst_alu_control_n_35;
  wire inst_alu_control_n_36;
  wire inst_alu_control_n_37;
  wire inst_alu_control_n_38;
  wire inst_alu_datapath_n_0;
  wire inst_alu_datapath_n_1;
  wire inst_alu_datapath_n_10;
  wire inst_alu_datapath_n_104;
  wire inst_alu_datapath_n_11;
  wire inst_alu_datapath_n_12;
  wire inst_alu_datapath_n_13;
  wire inst_alu_datapath_n_14;
  wire inst_alu_datapath_n_2;
  wire inst_alu_datapath_n_27;
  wire inst_alu_datapath_n_28;
  wire inst_alu_datapath_n_29;
  wire inst_alu_datapath_n_3;
  wire inst_alu_datapath_n_30;
  wire inst_alu_datapath_n_31;
  wire inst_alu_datapath_n_32;
  wire inst_alu_datapath_n_33;
  wire inst_alu_datapath_n_34;
  wire inst_alu_datapath_n_35;
  wire inst_alu_datapath_n_36;
  wire inst_alu_datapath_n_37;
  wire inst_alu_datapath_n_38;
  wire inst_alu_datapath_n_39;
  wire inst_alu_datapath_n_4;
  wire inst_alu_datapath_n_40;
  wire inst_alu_datapath_n_41;
  wire inst_alu_datapath_n_42;
  wire inst_alu_datapath_n_43;
  wire inst_alu_datapath_n_44;
  wire inst_alu_datapath_n_5;
  wire inst_alu_datapath_n_6;
  wire inst_alu_datapath_n_62;
  wire inst_alu_datapath_n_63;
  wire inst_alu_datapath_n_64;
  wire inst_alu_datapath_n_65;
  wire inst_alu_datapath_n_66;
  wire inst_alu_datapath_n_67;
  wire inst_alu_datapath_n_68;
  wire inst_alu_datapath_n_69;
  wire inst_alu_datapath_n_7;
  wire inst_alu_datapath_n_70;
  wire inst_alu_datapath_n_71;
  wire inst_alu_datapath_n_72;
  wire inst_alu_datapath_n_73;
  wire inst_alu_datapath_n_74;
  wire inst_alu_datapath_n_75;
  wire inst_alu_datapath_n_76;
  wire inst_alu_datapath_n_77;
  wire inst_alu_datapath_n_78;
  wire inst_alu_datapath_n_79;
  wire inst_alu_datapath_n_8;
  wire inst_alu_datapath_n_80;
  wire inst_alu_datapath_n_81;
  wire inst_alu_datapath_n_82;
  wire inst_alu_datapath_n_83;
  wire inst_alu_datapath_n_84;
  wire inst_alu_datapath_n_85;
  wire inst_alu_datapath_n_86;
  wire inst_alu_datapath_n_9;
  wire [3:0]oper;
  wire p_0_in;
  wire r2;
  wire [14:0]res_alu;
  wire \sw_reg_reg[14] ;

  alu_control inst_alu_control
       (.A(A[12:1]),
        .CLK(CLK),
        .CO(inst_alu_datapath_n_62),
        .D({res_alu[14:5],res_alu[3],res_alu[0]}),
        .E(r2),
        .O(inst_alu_datapath_n_14),
        .P({inst_alu_datapath_n_0,inst_alu_datapath_n_1,inst_alu_datapath_n_2,inst_alu_datapath_n_3,inst_alu_datapath_n_4,inst_alu_datapath_n_5,inst_alu_datapath_n_6,inst_alu_datapath_n_7,inst_alu_datapath_n_8,inst_alu_datapath_n_9,inst_alu_datapath_n_10,inst_alu_datapath_n_11,inst_alu_datapath_n_12,inst_alu_datapath_n_13}),
        .Q({inst_alu_datapath_n_27,inst_alu_datapath_n_28,inst_alu_datapath_n_29,inst_alu_datapath_n_30,inst_alu_datapath_n_31,inst_alu_datapath_n_32,inst_alu_datapath_n_33,inst_alu_datapath_n_34,inst_alu_datapath_n_35,inst_alu_datapath_n_36,inst_alu_datapath_n_37,inst_alu_datapath_n_38,inst_alu_datapath_n_39,inst_alu_datapath_n_40,inst_alu_datapath_n_41,inst_alu_datapath_n_42}),
        .alusg0(inst_alu_control_n_20),
        .alusg0_0(inst_alu_control_n_24),
        .alusg0_1(inst_alu_control_n_25),
        .alusg0_10(inst_alu_control_n_38),
        .alusg0_2(inst_alu_control_n_27),
        .alusg0_3(inst_alu_control_n_31),
        .alusg0_4(inst_alu_control_n_32),
        .alusg0_5(inst_alu_control_n_33),
        .alusg0_6(inst_alu_control_n_34),
        .alusg0_7(inst_alu_control_n_35),
        .alusg0_8(inst_alu_control_n_36),
        .alusg0_9(inst_alu_control_n_37),
        .btnCreg(btnCreg),
        .data0({data0[15:5],data0[0]}),
        .data1({data1[15:4],data1[1:0]}),
        .oper(oper),
        .out({inst_alu_control_n_0,inst_alu_control_n_1,inst_alu_control_n_2}),
        .\r1_reg[0] (inst_alu_datapath_n_65),
        .\r1_reg[0]_0 (inst_alu_datapath_n_68),
        .\r1_reg[0]_1 (inst_alu_datapath_n_85),
        .\r1_reg[0]_2 (inst_alu_datapath_n_67),
        .\r1_reg[12] (inst_alu_datapath_n_63),
        .\r1_reg[12]_0 ({inst_alu_datapath_n_43,inst_alu_datapath_n_44,COUNT}),
        .\r1_reg[2] (inst_alu_datapath_n_104),
        .\r2_reg[0] (inst_alu_control_n_15),
        .\r2_reg[0]_0 (inst_alu_control_n_16),
        .\r2_reg[0]_1 (inst_alu_control_n_26),
        .\r2_reg[10] (inst_alu_datapath_n_83),
        .\r2_reg[11] (inst_alu_datapath_n_84),
        .\r2_reg[11]_0 (inst_alu_datapath_n_77),
        .\r2_reg[12] (inst_alu_control_n_19),
        .\r2_reg[13] (inst_alu_control_n_28),
        .\r2_reg[14] (inst_alu_control_n_17),
        .\r2_reg[14]_0 (inst_alu_control_n_29),
        .\r2_reg[2] (inst_alu_datapath_n_70),
        .\r2_reg[3] (inst_alu_control_n_21),
        .\r2_reg[3]_0 (inst_alu_control_n_22),
        .\r2_reg[3]_1 (inst_alu_control_n_23),
        .\r2_reg[3]_2 (inst_alu_datapath_n_71),
        .\r2_reg[3]_3 (inst_alu_datapath_n_86),
        .\r2_reg[4] (inst_alu_datapath_n_72),
        .\r2_reg[5] (inst_alu_control_n_18),
        .\r2_reg[5]_0 (inst_alu_datapath_n_78),
        .\r2_reg[5]_1 (inst_alu_datapath_n_73),
        .\r2_reg[5]_2 (inst_alu_datapath_n_69),
        .\r2_reg[6] (inst_alu_datapath_n_79),
        .\r2_reg[6]_0 (inst_alu_datapath_n_74),
        .\r2_reg[7] (inst_alu_datapath_n_80),
        .\r2_reg[7]_0 (inst_alu_datapath_n_75),
        .\r2_reg[8] (inst_alu_datapath_n_81),
        .\r2_reg[8]_0 (inst_alu_datapath_n_76),
        .\r2_reg[9] (inst_alu_datapath_n_82),
        .\real_value_reg[0] (inst_alu_datapath_n_66),
        .\sw_reg_reg[15] (Q),
        .\sw_reg_reg[15]_0 (inst_alu_datapath_n_64));
  alu_datapath inst_alu_datapath
       (.A(A),
        .CLK(CLK),
        .CO(inst_alu_datapath_n_62),
        .D({res_alu[14:5],res_alu[3],res_alu[0]}),
        .E(r2),
        .\FSM_sequential_currstate_reg[0] (inst_alu_control_n_38),
        .\FSM_sequential_currstate_reg[2] (inst_alu_control_n_29),
        .\FSM_sequential_currstate_reg[2]_0 (inst_alu_control_n_17),
        .\FSM_sequential_currstate_reg[2]_1 (inst_alu_control_n_27),
        .\FSM_sequential_currstate_reg[2]_2 (inst_alu_control_n_25),
        .\FSM_sequential_currstate_reg[2]_3 (inst_alu_control_n_26),
        .\FSM_sequential_currstate_reg[2]_4 (inst_alu_control_n_16),
        .\FSM_sequential_currstate_reg[2]_5 (inst_alu_control_n_15),
        .O(inst_alu_datapath_n_14),
        .P({inst_alu_datapath_n_0,inst_alu_datapath_n_1,inst_alu_datapath_n_2,inst_alu_datapath_n_3,inst_alu_datapath_n_4,inst_alu_datapath_n_5,inst_alu_datapath_n_6,inst_alu_datapath_n_7,inst_alu_datapath_n_8,inst_alu_datapath_n_9,inst_alu_datapath_n_10,inst_alu_datapath_n_11,inst_alu_datapath_n_12,inst_alu_datapath_n_13}),
        .Q({inst_alu_datapath_n_27,inst_alu_datapath_n_28,inst_alu_datapath_n_29,inst_alu_datapath_n_30,inst_alu_datapath_n_31,inst_alu_datapath_n_32,inst_alu_datapath_n_33,inst_alu_datapath_n_34,inst_alu_datapath_n_35,inst_alu_datapath_n_36,inst_alu_datapath_n_37,inst_alu_datapath_n_38,inst_alu_datapath_n_39,inst_alu_datapath_n_40,inst_alu_datapath_n_41,inst_alu_datapath_n_42}),
        .alusg0_0({data0[15:5],data0[0]}),
        .alusg0_1({data1[15:4],data1[1:0]}),
        .alusg0_2(inst_alu_datapath_n_67),
        .alusg0_3(inst_alu_datapath_n_68),
        .alusg0_4(inst_alu_datapath_n_85),
        .btnCreg(btnCreg),
        .out({inst_alu_control_n_0,inst_alu_control_n_1,inst_alu_control_n_2}),
        .p_0_in(p_0_in),
        .\r1_reg[12]_0 (inst_alu_control_n_33),
        .\r1_reg[2]_0 (inst_alu_control_n_23),
        .\r1_reg[2]_1 (inst_alu_control_n_18),
        .\r1_reg[2]_2 (inst_alu_control_n_20),
        .\r1_reg[2]_3 (inst_alu_control_n_22),
        .\r1_reg[2]_4 (inst_alu_control_n_21),
        .\r2_reg[0]_0 (inst_alu_datapath_n_65),
        .\r2_reg[0]_1 (inst_alu_datapath_n_66),
        .\r2_reg[0]_2 (inst_alu_control_n_36),
        .\r2_reg[10]_0 (inst_alu_datapath_n_75),
        .\r2_reg[10]_1 (inst_alu_datapath_n_83),
        .\r2_reg[11]_0 (inst_alu_datapath_n_76),
        .\r2_reg[11]_1 (inst_alu_datapath_n_84),
        .\r2_reg[12]_0 (inst_alu_datapath_n_77),
        .\r2_reg[14]_0 (inst_alu_datapath_n_63),
        .\r2_reg[14]_1 (inst_alu_datapath_n_64),
        .\r2_reg[14]_2 (inst_alu_datapath_n_104),
        .\r2_reg[15]_0 (inst_alu_control_n_24),
        .\r2_reg[3]_0 (inst_alu_datapath_n_69),
        .\r2_reg[3]_1 (inst_alu_datapath_n_86),
        .\r2_reg[5]_0 (inst_alu_datapath_n_70),
        .\r2_reg[5]_1 (inst_alu_datapath_n_78),
        .\r2_reg[6]_0 (inst_alu_datapath_n_71),
        .\r2_reg[6]_1 (inst_alu_datapath_n_79),
        .\r2_reg[7]_0 (inst_alu_datapath_n_72),
        .\r2_reg[7]_1 (inst_alu_datapath_n_80),
        .\r2_reg[8]_0 (inst_alu_datapath_n_73),
        .\r2_reg[8]_1 (inst_alu_datapath_n_81),
        .\r2_reg[9]_0 (inst_alu_datapath_n_74),
        .\r2_reg[9]_1 (inst_alu_datapath_n_82),
        .\real_result_reg[15] (D),
        .\real_result_reg[1] ({inst_alu_datapath_n_43,inst_alu_datapath_n_44,COUNT}),
        .\real_value_reg[12] (inst_alu_control_n_19),
        .\real_value_reg[1] (inst_alu_control_n_31),
        .\real_value_reg[2] (inst_alu_control_n_34),
        .\real_value_reg[4] (inst_alu_control_n_32),
        .\sw_reg_reg[14] (\sw_reg_reg[14] ),
        .\sw_reg_reg[15] (inst_alu_control_n_28),
        .\sw_reg_reg[15]_0 (inst_alu_control_n_35),
        .\sw_reg_reg[15]_1 (inst_alu_control_n_37),
        .\sw_reg_reg[15]_2 (Q[3:2]));
endmodule

module alu_control
   (out,
    D,
    \r2_reg[0] ,
    \r2_reg[0]_0 ,
    \r2_reg[14] ,
    \r2_reg[5] ,
    \r2_reg[12] ,
    alusg0,
    \r2_reg[3] ,
    \r2_reg[3]_0 ,
    \r2_reg[3]_1 ,
    alusg0_0,
    alusg0_1,
    \r2_reg[0]_1 ,
    alusg0_2,
    \r2_reg[13] ,
    \r2_reg[14]_0 ,
    E,
    alusg0_3,
    alusg0_4,
    alusg0_5,
    alusg0_6,
    alusg0_7,
    alusg0_8,
    alusg0_9,
    alusg0_10,
    \real_value_reg[0] ,
    data1,
    data0,
    Q,
    \sw_reg_reg[15] ,
    \r1_reg[0] ,
    \r1_reg[12] ,
    P,
    \r2_reg[5]_0 ,
    \r2_reg[2] ,
    \r2_reg[6] ,
    \r2_reg[3]_2 ,
    \r2_reg[7] ,
    \r2_reg[4] ,
    \r2_reg[8] ,
    \r2_reg[5]_1 ,
    \r2_reg[9] ,
    \r2_reg[6]_0 ,
    \r2_reg[10] ,
    \r2_reg[7]_0 ,
    \r2_reg[11] ,
    \r2_reg[8]_0 ,
    \r2_reg[11]_0 ,
    \r1_reg[12]_0 ,
    \r2_reg[3]_3 ,
    A,
    \r2_reg[5]_2 ,
    \r1_reg[0]_0 ,
    O,
    CO,
    \r1_reg[0]_1 ,
    \r1_reg[0]_2 ,
    \sw_reg_reg[15]_0 ,
    \r1_reg[2] ,
    oper,
    btnCreg,
    CLK);
  output [2:0]out;
  output [11:0]D;
  output \r2_reg[0] ;
  output \r2_reg[0]_0 ;
  output \r2_reg[14] ;
  output \r2_reg[5] ;
  output \r2_reg[12] ;
  output alusg0;
  output \r2_reg[3] ;
  output \r2_reg[3]_0 ;
  output \r2_reg[3]_1 ;
  output alusg0_0;
  output alusg0_1;
  output \r2_reg[0]_1 ;
  output alusg0_2;
  output \r2_reg[13] ;
  output \r2_reg[14]_0 ;
  output [0:0]E;
  output alusg0_3;
  output alusg0_4;
  output alusg0_5;
  output alusg0_6;
  output alusg0_7;
  output alusg0_8;
  output alusg0_9;
  output alusg0_10;
  input \real_value_reg[0] ;
  input [13:0]data1;
  input [11:0]data0;
  input [15:0]Q;
  input [3:0]\sw_reg_reg[15] ;
  input \r1_reg[0] ;
  input \r1_reg[12] ;
  input [13:0]P;
  input \r2_reg[5]_0 ;
  input \r2_reg[2] ;
  input \r2_reg[6] ;
  input \r2_reg[3]_2 ;
  input \r2_reg[7] ;
  input \r2_reg[4] ;
  input \r2_reg[8] ;
  input \r2_reg[5]_1 ;
  input \r2_reg[9] ;
  input \r2_reg[6]_0 ;
  input \r2_reg[10] ;
  input \r2_reg[7]_0 ;
  input \r2_reg[11] ;
  input \r2_reg[8]_0 ;
  input \r2_reg[11]_0 ;
  input [4:0]\r1_reg[12]_0 ;
  input \r2_reg[3]_3 ;
  input [11:0]A;
  input \r2_reg[5]_2 ;
  input \r1_reg[0]_0 ;
  input [0:0]O;
  input [0:0]CO;
  input \r1_reg[0]_1 ;
  input \r1_reg[0]_2 ;
  input \sw_reg_reg[15]_0 ;
  input \r1_reg[2] ;
  input [3:0]oper;
  input btnCreg;
  input CLK;

  wire [11:0]A;
  wire CLK;
  wire [0:0]CO;
  wire [11:0]D;
  wire [0:0]E;
  wire \FSM_sequential_currstate[0]_i_1_n_0 ;
  wire \FSM_sequential_currstate[0]_i_2_n_0 ;
  wire \FSM_sequential_currstate[0]_i_3_n_0 ;
  wire \FSM_sequential_currstate[0]_i_4_n_0 ;
  wire \FSM_sequential_currstate[1]_i_1_n_0 ;
  wire \FSM_sequential_currstate[1]_i_2_n_0 ;
  wire \FSM_sequential_currstate[1]_i_3_n_0 ;
  wire \FSM_sequential_currstate[2]_i_10_n_0 ;
  wire \FSM_sequential_currstate[2]_i_1_n_0 ;
  wire \FSM_sequential_currstate[2]_i_2_n_0 ;
  wire \FSM_sequential_currstate[2]_i_3_n_0 ;
  wire \FSM_sequential_currstate[2]_i_4_n_0 ;
  wire \FSM_sequential_currstate[2]_i_5_n_0 ;
  wire \FSM_sequential_currstate[2]_i_6_n_0 ;
  wire \FSM_sequential_currstate[2]_i_7_n_0 ;
  wire \FSM_sequential_currstate[2]_i_8_n_0 ;
  wire \FSM_sequential_currstate[2]_i_9_n_0 ;
  wire [0:0]O;
  wire [13:0]P;
  wire [15:0]Q;
  wire alusg0;
  wire alusg0_0;
  wire alusg0_1;
  wire alusg0_10;
  wire alusg0_2;
  wire alusg0_3;
  wire alusg0_4;
  wire alusg0_5;
  wire alusg0_6;
  wire alusg0_7;
  wire alusg0_8;
  wire alusg0_9;
  wire alusg0_i_114_n_0;
  wire alusg0_i_115_n_0;
  wire alusg0_i_116_n_0;
  wire alusg0_i_117_n_0;
  wire alusg0_i_121_n_0;
  wire alusg0_i_26_n_0;
  wire alusg0_i_27_n_0;
  wire alusg0_i_28_n_0;
  wire alusg0_i_29_n_0;
  wire alusg0_i_30_n_0;
  wire alusg0_i_32_n_0;
  wire alusg0_i_33_n_0;
  wire alusg0_i_35_n_0;
  wire alusg0_i_36_n_0;
  wire alusg0_i_37_n_0;
  wire alusg0_i_38_n_0;
  wire alusg0_i_40_n_0;
  wire alusg0_i_42_n_0;
  wire alusg0_i_43_n_0;
  wire alusg0_i_45_n_0;
  wire alusg0_i_47_n_0;
  wire alusg0_i_48_n_0;
  wire alusg0_i_50_n_0;
  wire alusg0_i_52_n_0;
  wire alusg0_i_53_n_0;
  wire alusg0_i_55_n_0;
  wire alusg0_i_57_n_0;
  wire alusg0_i_58_n_0;
  wire alusg0_i_60_n_0;
  wire alusg0_i_62_n_0;
  wire alusg0_i_63_n_0;
  wire alusg0_i_65_n_0;
  wire alusg0_i_67_n_0;
  wire alusg0_i_68_n_0;
  wire alusg0_i_70_n_0;
  wire alusg0_i_77_n_0;
  wire alusg0_i_79_n_0;
  wire alusg0_i_94_n_0;
  wire btnCreg;
  wire [11:0]data0;
  wire [13:0]data1;
  wire [3:0]oper;
  (* RTL_KEEP = "yes" *) wire [2:0]out;
  wire \r1_reg[0] ;
  wire \r1_reg[0]_0 ;
  wire \r1_reg[0]_1 ;
  wire \r1_reg[0]_2 ;
  wire \r1_reg[12] ;
  wire [4:0]\r1_reg[12]_0 ;
  wire \r1_reg[2] ;
  wire \r2_reg[0] ;
  wire \r2_reg[0]_0 ;
  wire \r2_reg[0]_1 ;
  wire \r2_reg[10] ;
  wire \r2_reg[11] ;
  wire \r2_reg[11]_0 ;
  wire \r2_reg[12] ;
  wire \r2_reg[13] ;
  wire \r2_reg[14] ;
  wire \r2_reg[14]_0 ;
  wire \r2_reg[2] ;
  wire \r2_reg[3] ;
  wire \r2_reg[3]_0 ;
  wire \r2_reg[3]_1 ;
  wire \r2_reg[3]_2 ;
  wire \r2_reg[3]_3 ;
  wire \r2_reg[4] ;
  wire \r2_reg[5] ;
  wire \r2_reg[5]_0 ;
  wire \r2_reg[5]_1 ;
  wire \r2_reg[5]_2 ;
  wire \r2_reg[6] ;
  wire \r2_reg[6]_0 ;
  wire \r2_reg[7] ;
  wire \r2_reg[7]_0 ;
  wire \r2_reg[8] ;
  wire \r2_reg[8]_0 ;
  wire \r2_reg[9] ;
  wire \real_value_reg[0] ;
  wire [3:0]\sw_reg_reg[15] ;
  wire \sw_reg_reg[15]_0 ;

  LUT5 #(
    .INIT(32'h0000EEE2)) 
    \FSM_sequential_currstate[0]_i_1 
       (.I0(out[0]),
        .I1(\FSM_sequential_currstate[2]_i_2_n_0 ),
        .I2(\FSM_sequential_currstate[0]_i_2_n_0 ),
        .I3(\FSM_sequential_currstate[0]_i_3_n_0 ),
        .I4(btnCreg),
        .O(\FSM_sequential_currstate[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h76)) 
    \FSM_sequential_currstate[0]_i_2 
       (.I0(out[0]),
        .I1(out[1]),
        .I2(out[2]),
        .O(\FSM_sequential_currstate[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h1600FFFF16001600)) 
    \FSM_sequential_currstate[0]_i_3 
       (.I0(\sw_reg_reg[15] [0]),
        .I1(oper[0]),
        .I2(oper[2]),
        .I3(\FSM_sequential_currstate[0]_i_4_n_0 ),
        .I4(out[2]),
        .I5(out[0]),
        .O(\FSM_sequential_currstate[0]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \FSM_sequential_currstate[0]_i_4 
       (.I0(out[0]),
        .I1(\sw_reg_reg[15] [1]),
        .I2(oper[3]),
        .I3(oper[1]),
        .O(\FSM_sequential_currstate[0]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h0000EEE2)) 
    \FSM_sequential_currstate[1]_i_1 
       (.I0(out[1]),
        .I1(\FSM_sequential_currstate[2]_i_2_n_0 ),
        .I2(\FSM_sequential_currstate[2]_i_3_n_0 ),
        .I3(\FSM_sequential_currstate[1]_i_2_n_0 ),
        .I4(btnCreg),
        .O(\FSM_sequential_currstate[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00FF00FE00FE00EF)) 
    \FSM_sequential_currstate[1]_i_2 
       (.I0(\FSM_sequential_currstate[1]_i_3_n_0 ),
        .I1(\sw_reg_reg[15] [1]),
        .I2(oper[0]),
        .I3(out[0]),
        .I4(oper[3]),
        .I5(\sw_reg_reg[15] [0]),
        .O(\FSM_sequential_currstate[1]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_currstate[1]_i_3 
       (.I0(oper[1]),
        .I1(oper[2]),
        .O(\FSM_sequential_currstate[1]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h0000EEE2)) 
    \FSM_sequential_currstate[2]_i_1 
       (.I0(out[2]),
        .I1(\FSM_sequential_currstate[2]_i_2_n_0 ),
        .I2(\FSM_sequential_currstate[2]_i_3_n_0 ),
        .I3(\FSM_sequential_currstate[2]_i_4_n_0 ),
        .I4(btnCreg),
        .O(\FSM_sequential_currstate[2]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_currstate[2]_i_10 
       (.I0(out[0]),
        .I1(oper[0]),
        .O(\FSM_sequential_currstate[2]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \FSM_sequential_currstate[2]_i_2 
       (.I0(\FSM_sequential_currstate[2]_i_5_n_0 ),
        .I1(\FSM_sequential_currstate[2]_i_6_n_0 ),
        .I2(\FSM_sequential_currstate[2]_i_7_n_0 ),
        .I3(\FSM_sequential_currstate[0]_i_2_n_0 ),
        .I4(\FSM_sequential_currstate[0]_i_3_n_0 ),
        .I5(\FSM_sequential_currstate[2]_i_8_n_0 ),
        .O(\FSM_sequential_currstate[2]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h7E)) 
    \FSM_sequential_currstate[2]_i_3 
       (.I0(out[1]),
        .I1(out[2]),
        .I2(out[0]),
        .O(\FSM_sequential_currstate[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00FF00FE00FE00EF)) 
    \FSM_sequential_currstate[2]_i_4 
       (.I0(\FSM_sequential_currstate[2]_i_9_n_0 ),
        .I1(\sw_reg_reg[15] [1]),
        .I2(oper[0]),
        .I3(out[0]),
        .I4(oper[2]),
        .I5(oper[1]),
        .O(\FSM_sequential_currstate[2]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \FSM_sequential_currstate[2]_i_5 
       (.I0(out[0]),
        .I1(oper[0]),
        .I2(oper[3]),
        .I3(\sw_reg_reg[15] [0]),
        .I4(\sw_reg_reg[15] [1]),
        .I5(\FSM_sequential_currstate[1]_i_3_n_0 ),
        .O(\FSM_sequential_currstate[2]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    \FSM_sequential_currstate[2]_i_6 
       (.I0(\FSM_sequential_currstate[1]_i_3_n_0 ),
        .I1(out[0]),
        .I2(oper[0]),
        .I3(\sw_reg_reg[15] [1]),
        .I4(\sw_reg_reg[15] [0]),
        .I5(oper[3]),
        .O(\FSM_sequential_currstate[2]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    \FSM_sequential_currstate[2]_i_7 
       (.I0(oper[1]),
        .I1(oper[2]),
        .I2(\sw_reg_reg[15] [0]),
        .I3(oper[3]),
        .I4(\sw_reg_reg[15] [1]),
        .I5(\FSM_sequential_currstate[2]_i_10_n_0 ),
        .O(\FSM_sequential_currstate[2]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000008)) 
    \FSM_sequential_currstate[2]_i_8 
       (.I0(\FSM_sequential_currstate[2]_i_10_n_0 ),
        .I1(oper[1]),
        .I2(oper[2]),
        .I3(\sw_reg_reg[15] [1]),
        .I4(\sw_reg_reg[15] [0]),
        .I5(oper[3]),
        .O(\FSM_sequential_currstate[2]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_currstate[2]_i_9 
       (.I0(\sw_reg_reg[15] [0]),
        .I1(oper[3]),
        .O(\FSM_sequential_currstate[2]_i_9_n_0 ));
  (* FSM_ENCODED_STATES = "s_initial:000,s_end:111,s_shft:110,s_mul:101,s_or:100,s_and:011,s_sub:010,s_add:001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_currstate_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\FSM_sequential_currstate[0]_i_1_n_0 ),
        .Q(out[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "s_initial:000,s_end:111,s_shft:110,s_mul:101,s_or:100,s_and:011,s_sub:010,s_add:001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_currstate_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\FSM_sequential_currstate[1]_i_1_n_0 ),
        .Q(out[1]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "s_initial:000,s_end:111,s_shft:110,s_mul:101,s_or:100,s_and:011,s_sub:010,s_add:001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_currstate_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\FSM_sequential_currstate[2]_i_1_n_0 ),
        .Q(out[2]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFE7E7E)) 
    alusg0_i_1
       (.I0(out[1]),
        .I1(out[0]),
        .I2(out[2]),
        .I3(\sw_reg_reg[15] [2]),
        .I4(\sw_reg_reg[15] [3]),
        .O(E));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    alusg0_i_10
       (.I0(alusg0_i_57_n_0),
        .I1(alusg0_i_58_n_0),
        .I2(\r2_reg[7] ),
        .I3(alusg0_i_60_n_0),
        .I4(\r2_reg[4] ),
        .O(D[4]));
  LUT4 #(
    .INIT(16'h0040)) 
    alusg0_i_100
       (.I0(out[2]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(\sw_reg_reg[15] [3]),
        .O(\r2_reg[14]_0 ));
  LUT5 #(
    .INIT(32'h00004045)) 
    alusg0_i_101
       (.I0(\sw_reg_reg[15] [3]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[0]),
        .I4(\r1_reg[12]_0 [2]),
        .O(alusg0_9));
  LUT6 #(
    .INIT(64'h0000000000008082)) 
    alusg0_i_102
       (.I0(\r1_reg[12]_0 [2]),
        .I1(out[1]),
        .I2(out[2]),
        .I3(out[0]),
        .I4(\sw_reg_reg[15] [3]),
        .I5(\r1_reg[0] ),
        .O(\r2_reg[5] ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    alusg0_i_11
       (.I0(alusg0_i_62_n_0),
        .I1(alusg0_i_63_n_0),
        .I2(\r2_reg[6] ),
        .I3(alusg0_i_65_n_0),
        .I4(\r2_reg[3]_2 ),
        .O(D[3]));
  LUT6 #(
    .INIT(64'h0000000080008008)) 
    alusg0_i_111
       (.I0(\r1_reg[0]_0 ),
        .I1(\r1_reg[12]_0 [2]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(out[0]),
        .I5(\sw_reg_reg[15] [3]),
        .O(\r2_reg[3]_0 ));
  LUT6 #(
    .INIT(64'h0000000000008082)) 
    alusg0_i_112
       (.I0(\r1_reg[12]_0 [2]),
        .I1(out[1]),
        .I2(out[2]),
        .I3(out[0]),
        .I4(\sw_reg_reg[15] [3]),
        .I5(\r1_reg[0]_2 ),
        .O(\r2_reg[3] ));
  LUT5 #(
    .INIT(32'h00005001)) 
    alusg0_i_113
       (.I0(\sw_reg_reg[15] [3]),
        .I1(out[0]),
        .I2(out[2]),
        .I3(out[1]),
        .I4(\r1_reg[12]_0 [2]),
        .O(\r2_reg[13] ));
  LUT6 #(
    .INIT(64'h0000FFFF00002000)) 
    alusg0_i_114
       (.I0(\r1_reg[12]_0 [4]),
        .I1(out[2]),
        .I2(out[0]),
        .I3(out[1]),
        .I4(\sw_reg_reg[15] [3]),
        .I5(\r1_reg[2] ),
        .O(alusg0_i_114_n_0));
  LUT6 #(
    .INIT(64'h0000000080008008)) 
    alusg0_i_115
       (.I0(\r1_reg[0]_1 ),
        .I1(\r1_reg[12]_0 [2]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(out[0]),
        .I5(\sw_reg_reg[15] [3]),
        .O(alusg0_i_115_n_0));
  LUT6 #(
    .INIT(64'h0000000000004041)) 
    alusg0_i_116
       (.I0(\r1_reg[12]_0 [2]),
        .I1(out[1]),
        .I2(out[2]),
        .I3(out[0]),
        .I4(\sw_reg_reg[15] [3]),
        .I5(\r1_reg[0]_2 ),
        .O(alusg0_i_116_n_0));
  LUT6 #(
    .INIT(64'h0000000020002002)) 
    alusg0_i_117
       (.I0(\r1_reg[0]_0 ),
        .I1(\r1_reg[12]_0 [2]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(out[0]),
        .I5(\sw_reg_reg[15] [3]),
        .O(alusg0_i_117_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    alusg0_i_12
       (.I0(alusg0_i_67_n_0),
        .I1(alusg0_i_68_n_0),
        .I2(\r2_reg[5]_0 ),
        .I3(alusg0_i_70_n_0),
        .I4(\r2_reg[2] ),
        .O(D[2]));
  LUT6 #(
    .INIT(64'h0042000000020000)) 
    alusg0_i_121
       (.I0(out[2]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(\sw_reg_reg[15] [3]),
        .I4(\r1_reg[12]_0 [3]),
        .I5(Q[3]),
        .O(alusg0_i_121_n_0));
  LUT6 #(
    .INIT(64'h0000A20200000000)) 
    alusg0_i_122
       (.I0(Q[0]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(\sw_reg_reg[15] [3]),
        .I5(\r1_reg[12]_0 [2]),
        .O(alusg0_8));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEFE)) 
    alusg0_i_14
       (.I0(alusg0_i_77_n_0),
        .I1(\r2_reg[3]_3 ),
        .I2(alusg0_i_79_n_0),
        .I3(\sw_reg_reg[15] [3]),
        .I4(A[2]),
        .I5(\r2_reg[5]_2 ),
        .O(D[1]));
  LUT4 #(
    .INIT(16'h00D1)) 
    alusg0_i_160
       (.I0(out[0]),
        .I1(out[1]),
        .I2(out[2]),
        .I3(\sw_reg_reg[15] [3]),
        .O(alusg0_10));
  LUT6 #(
    .INIT(64'h0042000200000000)) 
    alusg0_i_161
       (.I0(out[2]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(\sw_reg_reg[15] [3]),
        .I4(Q[0]),
        .I5(\r1_reg[12]_0 [0]),
        .O(\r2_reg[0]_1 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    alusg0_i_17
       (.I0(\real_value_reg[0] ),
        .I1(\r2_reg[0] ),
        .I2(data1[0]),
        .I3(data0[0]),
        .I4(\r2_reg[0]_0 ),
        .I5(alusg0_i_94_n_0),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    alusg0_i_19
       (.I0(\r2_reg[3]_1 ),
        .I1(CO),
        .I2(alusg0_1),
        .I3(Q[15]),
        .I4(data0[11]),
        .I5(\r2_reg[0]_0 ),
        .O(alusg0_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_20
       (.I0(data1[13]),
        .I1(\r2_reg[0] ),
        .I2(alusg0_1),
        .I3(\r1_reg[12]_0 [4]),
        .I4(A[11]),
        .I5(\sw_reg_reg[15] [3]),
        .O(alusg0_5));
  LUT4 #(
    .INIT(16'h0200)) 
    alusg0_i_21
       (.I0(out[2]),
        .I1(\sw_reg_reg[15] [3]),
        .I2(out[1]),
        .I3(out[0]),
        .O(\r2_reg[14] ));
  LUT4 #(
    .INIT(16'h0010)) 
    alusg0_i_25
       (.I0(out[2]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(\sw_reg_reg[15] [3]),
        .O(\r2_reg[0] ));
  LUT6 #(
    .INIT(64'hFFFF88F888F888F8)) 
    alusg0_i_26
       (.I0(\r2_reg[3]_0 ),
        .I1(Q[13]),
        .I2(\r2_reg[3]_1 ),
        .I3(CO),
        .I4(data0[10]),
        .I5(\r2_reg[0]_0 ),
        .O(alusg0_i_26_n_0));
  LUT6 #(
    .INIT(64'hF888FFFFF888F888)) 
    alusg0_i_27
       (.I0(\r2_reg[3] ),
        .I1(Q[11]),
        .I2(\r2_reg[3]_0 ),
        .I3(Q[12]),
        .I4(CO),
        .I5(\r2_reg[3]_1 ),
        .O(alusg0_i_27_n_0));
  LUT6 #(
    .INIT(64'h00000000000A0C00)) 
    alusg0_i_28
       (.I0(data1[11]),
        .I1(data0[9]),
        .I2(out[2]),
        .I3(out[0]),
        .I4(out[1]),
        .I5(\sw_reg_reg[15] [3]),
        .O(alusg0_i_28_n_0));
  LUT6 #(
    .INIT(64'hFFFFEAAAEAAAEAAA)) 
    alusg0_i_29
       (.I0(\r2_reg[12] ),
        .I1(\r1_reg[12]_0 [1]),
        .I2(Q[15]),
        .I3(\r2_reg[13] ),
        .I4(Q[13]),
        .I5(alusg0_i_114_n_0),
        .O(alusg0_i_29_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFEAEAEA)) 
    alusg0_i_3
       (.I0(\r1_reg[12] ),
        .I1(P[13]),
        .I2(\r2_reg[14] ),
        .I3(data1[12]),
        .I4(\r2_reg[0] ),
        .I5(alusg0_i_26_n_0),
        .O(D[11]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_30
       (.I0(alusg0),
        .I1(Q[14]),
        .I2(\r2_reg[5] ),
        .I3(Q[9]),
        .I4(Q[10]),
        .I5(alusg0_i_115_n_0),
        .O(alusg0_i_30_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    alusg0_i_32
       (.I0(\r2_reg[3]_1 ),
        .I1(CO),
        .O(alusg0_i_32_n_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_33
       (.I0(\r2_reg[0]_0 ),
        .I1(data0[8]),
        .I2(\r2_reg[0] ),
        .I3(data1[10]),
        .I4(P[11]),
        .I5(\r2_reg[14] ),
        .O(alusg0_i_33_n_0));
  LUT6 #(
    .INIT(64'hAA00AA00AA00AAC0)) 
    alusg0_i_34
       (.I0(A[11]),
        .I1(\r1_reg[12]_0 [4]),
        .I2(out[2]),
        .I3(\sw_reg_reg[15] [3]),
        .I4(out[1]),
        .I5(out[0]),
        .O(\r2_reg[12] ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_35
       (.I0(alusg0_i_114_n_0),
        .I1(Q[12]),
        .I2(alusg0),
        .I3(Q[13]),
        .I4(Q[14]),
        .I5(alusg0_i_116_n_0),
        .O(alusg0_i_35_n_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_36
       (.I0(alusg0_i_117_n_0),
        .I1(Q[15]),
        .I2(\r2_reg[5] ),
        .I3(Q[8]),
        .I4(Q[9]),
        .I5(alusg0_i_115_n_0),
        .O(alusg0_i_36_n_0));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    alusg0_i_37
       (.I0(\r2_reg[3]_1 ),
        .I1(CO),
        .I2(\sw_reg_reg[15]_0 ),
        .I3(Q[11]),
        .I4(data0[7]),
        .I5(\r2_reg[0]_0 ),
        .O(alusg0_i_37_n_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_38
       (.I0(\r2_reg[0] ),
        .I1(data1[9]),
        .I2(A[10]),
        .I3(\sw_reg_reg[15] [3]),
        .I4(P[10]),
        .I5(\r2_reg[14] ),
        .O(alusg0_i_38_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEEE)) 
    alusg0_i_4
       (.I0(alusg0_i_27_n_0),
        .I1(alusg0_i_28_n_0),
        .I2(P[12]),
        .I3(\r2_reg[14] ),
        .I4(alusg0_i_29_n_0),
        .I5(alusg0_i_30_n_0),
        .O(D[10]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_40
       (.I0(alusg0_i_116_n_0),
        .I1(Q[13]),
        .I2(alusg0_i_117_n_0),
        .I3(Q[14]),
        .I4(Q[7]),
        .I5(\r2_reg[5] ),
        .O(alusg0_i_40_n_0));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    alusg0_i_42
       (.I0(\r2_reg[3]_1 ),
        .I1(CO),
        .I2(\sw_reg_reg[15]_0 ),
        .I3(Q[10]),
        .I4(data0[6]),
        .I5(\r2_reg[0]_0 ),
        .O(alusg0_i_42_n_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_43
       (.I0(\r2_reg[0] ),
        .I1(data1[8]),
        .I2(A[9]),
        .I3(\sw_reg_reg[15] [3]),
        .I4(P[9]),
        .I5(\r2_reg[14] ),
        .O(alusg0_i_43_n_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_45
       (.I0(alusg0_i_116_n_0),
        .I1(Q[12]),
        .I2(alusg0_i_117_n_0),
        .I3(Q[13]),
        .I4(Q[6]),
        .I5(\r2_reg[5] ),
        .O(alusg0_i_45_n_0));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    alusg0_i_47
       (.I0(\r2_reg[3]_1 ),
        .I1(CO),
        .I2(\sw_reg_reg[15]_0 ),
        .I3(Q[9]),
        .I4(data0[5]),
        .I5(\r2_reg[0]_0 ),
        .O(alusg0_i_47_n_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_48
       (.I0(\r2_reg[0] ),
        .I1(data1[7]),
        .I2(A[8]),
        .I3(\sw_reg_reg[15] [3]),
        .I4(P[8]),
        .I5(\r2_reg[14] ),
        .O(alusg0_i_48_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    alusg0_i_5
       (.I0(\r2_reg[11]_0 ),
        .I1(alusg0_i_32_n_0),
        .I2(alusg0_i_33_n_0),
        .I3(\r2_reg[12] ),
        .I4(alusg0_i_35_n_0),
        .I5(alusg0_i_36_n_0),
        .O(D[9]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_50
       (.I0(alusg0_i_116_n_0),
        .I1(Q[11]),
        .I2(alusg0_i_117_n_0),
        .I3(Q[12]),
        .I4(Q[5]),
        .I5(\r2_reg[5] ),
        .O(alusg0_i_50_n_0));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    alusg0_i_52
       (.I0(\r2_reg[3]_1 ),
        .I1(CO),
        .I2(\sw_reg_reg[15]_0 ),
        .I3(Q[8]),
        .I4(data0[4]),
        .I5(\r2_reg[0]_0 ),
        .O(alusg0_i_52_n_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_53
       (.I0(\r2_reg[0] ),
        .I1(data1[6]),
        .I2(A[7]),
        .I3(\sw_reg_reg[15] [3]),
        .I4(P[7]),
        .I5(\r2_reg[14] ),
        .O(alusg0_i_53_n_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_55
       (.I0(alusg0_i_116_n_0),
        .I1(Q[10]),
        .I2(alusg0_i_117_n_0),
        .I3(Q[11]),
        .I4(Q[4]),
        .I5(\r2_reg[5] ),
        .O(alusg0_i_55_n_0));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    alusg0_i_57
       (.I0(\r2_reg[3]_1 ),
        .I1(CO),
        .I2(\sw_reg_reg[15]_0 ),
        .I3(Q[7]),
        .I4(data0[3]),
        .I5(\r2_reg[0]_0 ),
        .O(alusg0_i_57_n_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_58
       (.I0(\r2_reg[0] ),
        .I1(data1[5]),
        .I2(A[6]),
        .I3(\sw_reg_reg[15] [3]),
        .I4(P[6]),
        .I5(\r2_reg[14] ),
        .O(alusg0_i_58_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    alusg0_i_6
       (.I0(alusg0_i_37_n_0),
        .I1(alusg0_i_38_n_0),
        .I2(\r2_reg[11] ),
        .I3(alusg0_i_40_n_0),
        .I4(\r2_reg[8]_0 ),
        .O(D[8]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_60
       (.I0(alusg0_i_116_n_0),
        .I1(Q[9]),
        .I2(alusg0_i_117_n_0),
        .I3(Q[10]),
        .I4(Q[3]),
        .I5(\r2_reg[5] ),
        .O(alusg0_i_60_n_0));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    alusg0_i_62
       (.I0(\r2_reg[3]_1 ),
        .I1(CO),
        .I2(\sw_reg_reg[15]_0 ),
        .I3(Q[6]),
        .I4(data0[2]),
        .I5(\r2_reg[0]_0 ),
        .O(alusg0_i_62_n_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_63
       (.I0(\r2_reg[0] ),
        .I1(data1[4]),
        .I2(A[5]),
        .I3(\sw_reg_reg[15] [3]),
        .I4(P[5]),
        .I5(\r2_reg[14] ),
        .O(alusg0_i_63_n_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_65
       (.I0(alusg0_i_116_n_0),
        .I1(Q[8]),
        .I2(alusg0_i_117_n_0),
        .I3(Q[9]),
        .I4(Q[2]),
        .I5(\r2_reg[5] ),
        .O(alusg0_i_65_n_0));
  LUT6 #(
    .INIT(64'hFFFFF222F222F222)) 
    alusg0_i_67
       (.I0(\r2_reg[3]_1 ),
        .I1(CO),
        .I2(\sw_reg_reg[15]_0 ),
        .I3(Q[5]),
        .I4(data0[1]),
        .I5(\r2_reg[0]_0 ),
        .O(alusg0_i_67_n_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_68
       (.I0(\r2_reg[0] ),
        .I1(data1[3]),
        .I2(A[4]),
        .I3(\sw_reg_reg[15] [3]),
        .I4(P[4]),
        .I5(\r2_reg[14] ),
        .O(alusg0_i_68_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    alusg0_i_7
       (.I0(alusg0_i_42_n_0),
        .I1(alusg0_i_43_n_0),
        .I2(\r2_reg[10] ),
        .I3(alusg0_i_45_n_0),
        .I4(\r2_reg[7]_0 ),
        .O(D[7]));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_70
       (.I0(alusg0_i_116_n_0),
        .I1(Q[7]),
        .I2(alusg0_i_117_n_0),
        .I3(Q[8]),
        .I4(Q[1]),
        .I5(\r2_reg[5] ),
        .O(alusg0_i_70_n_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_73
       (.I0(\r2_reg[0] ),
        .I1(data1[2]),
        .I2(A[3]),
        .I3(\sw_reg_reg[15] [3]),
        .I4(P[3]),
        .I5(\r2_reg[14] ),
        .O(alusg0_4));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_77
       (.I0(\r2_reg[3] ),
        .I1(Q[1]),
        .I2(\r2_reg[3]_0 ),
        .I3(Q[2]),
        .I4(O),
        .I5(\r2_reg[3]_1 ),
        .O(alusg0_i_77_n_0));
  LUT5 #(
    .INIT(32'hFFFFF888)) 
    alusg0_i_79
       (.I0(P[2]),
        .I1(\r2_reg[14] ),
        .I2(alusg0),
        .I3(Q[4]),
        .I4(alusg0_i_121_n_0),
        .O(alusg0_i_79_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    alusg0_i_8
       (.I0(alusg0_i_47_n_0),
        .I1(alusg0_i_48_n_0),
        .I2(\r2_reg[9] ),
        .I3(alusg0_i_50_n_0),
        .I4(\r2_reg[6]_0 ),
        .O(D[6]));
  LUT6 #(
    .INIT(64'hAA00AAC0AA00AA00)) 
    alusg0_i_83
       (.I0(A[1]),
        .I1(P[1]),
        .I2(out[2]),
        .I3(\sw_reg_reg[15] [3]),
        .I4(out[1]),
        .I5(out[0]),
        .O(alusg0_6));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_86
       (.I0(\r2_reg[0] ),
        .I1(data1[1]),
        .I2(A[0]),
        .I3(\sw_reg_reg[15] [3]),
        .I4(P[0]),
        .I5(\r2_reg[14] ),
        .O(alusg0_3));
  LUT6 #(
    .INIT(64'h0042000000020000)) 
    alusg0_i_88
       (.I0(out[2]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(\sw_reg_reg[15] [3]),
        .I4(\r1_reg[12]_0 [1]),
        .I5(Q[1]),
        .O(alusg0_2));
  LUT6 #(
    .INIT(64'h0000000020002002)) 
    alusg0_i_89
       (.I0(\r1_reg[0]_1 ),
        .I1(\r1_reg[12]_0 [2]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(out[0]),
        .I5(\sw_reg_reg[15] [3]),
        .O(alusg0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    alusg0_i_9
       (.I0(alusg0_i_52_n_0),
        .I1(alusg0_i_53_n_0),
        .I2(\r2_reg[8] ),
        .I3(alusg0_i_55_n_0),
        .I4(\r2_reg[5]_1 ),
        .O(D[5]));
  LUT4 #(
    .INIT(16'h0004)) 
    alusg0_i_93
       (.I0(out[2]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(\sw_reg_reg[15] [3]),
        .O(\r2_reg[0]_0 ));
  LUT6 #(
    .INIT(64'h000000000000A202)) 
    alusg0_i_94
       (.I0(Q[0]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(\sw_reg_reg[15] [3]),
        .I5(\r1_reg[0] ),
        .O(alusg0_i_94_n_0));
  LUT5 #(
    .INIT(32'h50010000)) 
    alusg0_i_95
       (.I0(\sw_reg_reg[15] [3]),
        .I1(out[0]),
        .I2(out[2]),
        .I3(out[1]),
        .I4(\r1_reg[12]_0 [2]),
        .O(alusg0_7));
  LUT6 #(
    .INIT(64'h0000808200000000)) 
    alusg0_i_96
       (.I0(\r1_reg[12]_0 [2]),
        .I1(out[1]),
        .I2(out[2]),
        .I3(out[0]),
        .I4(\sw_reg_reg[15] [3]),
        .I5(Q[0]),
        .O(\r2_reg[3]_1 ));
  LUT4 #(
    .INIT(16'h0002)) 
    alusg0_i_98
       (.I0(out[2]),
        .I1(\sw_reg_reg[15] [3]),
        .I2(out[1]),
        .I3(out[0]),
        .O(alusg0_1));
endmodule

module alu_datapath
   (P,
    O,
    alusg0_0,
    Q,
    \real_result_reg[1] ,
    alusg0_1,
    CO,
    \r2_reg[14]_0 ,
    \r2_reg[14]_1 ,
    \r2_reg[0]_0 ,
    \r2_reg[0]_1 ,
    alusg0_2,
    alusg0_3,
    \r2_reg[3]_0 ,
    \r2_reg[5]_0 ,
    \r2_reg[6]_0 ,
    \r2_reg[7]_0 ,
    \r2_reg[8]_0 ,
    \r2_reg[9]_0 ,
    \r2_reg[10]_0 ,
    \r2_reg[11]_0 ,
    \r2_reg[12]_0 ,
    \r2_reg[5]_1 ,
    \r2_reg[6]_1 ,
    \r2_reg[7]_1 ,
    \r2_reg[8]_1 ,
    \r2_reg[9]_1 ,
    \r2_reg[10]_1 ,
    \r2_reg[11]_1 ,
    alusg0_4,
    \r2_reg[3]_1 ,
    \real_result_reg[15] ,
    p_0_in,
    \r2_reg[14]_2 ,
    E,
    CLK,
    btnCreg,
    D,
    A,
    \real_value_reg[4] ,
    \sw_reg_reg[15] ,
    \r1_reg[2]_0 ,
    \FSM_sequential_currstate_reg[2] ,
    \real_value_reg[12] ,
    \sw_reg_reg[15]_0 ,
    \r2_reg[15]_0 ,
    \r1_reg[12]_0 ,
    \FSM_sequential_currstate_reg[2]_0 ,
    \sw_reg_reg[15]_1 ,
    \r1_reg[2]_1 ,
    \sw_reg_reg[15]_2 ,
    \r2_reg[0]_2 ,
    \real_value_reg[1] ,
    \FSM_sequential_currstate_reg[2]_1 ,
    \r1_reg[2]_2 ,
    \real_value_reg[2] ,
    \FSM_sequential_currstate_reg[2]_2 ,
    \FSM_sequential_currstate_reg[0] ,
    \r1_reg[2]_3 ,
    \r1_reg[2]_4 ,
    \FSM_sequential_currstate_reg[2]_3 ,
    \FSM_sequential_currstate_reg[2]_4 ,
    \FSM_sequential_currstate_reg[2]_5 ,
    \sw_reg_reg[14] ,
    out);
  output [13:0]P;
  output [0:0]O;
  output [11:0]alusg0_0;
  output [15:0]Q;
  output [4:0]\real_result_reg[1] ;
  output [13:0]alusg0_1;
  output [0:0]CO;
  output \r2_reg[14]_0 ;
  output \r2_reg[14]_1 ;
  output \r2_reg[0]_0 ;
  output \r2_reg[0]_1 ;
  output alusg0_2;
  output alusg0_3;
  output \r2_reg[3]_0 ;
  output \r2_reg[5]_0 ;
  output \r2_reg[6]_0 ;
  output \r2_reg[7]_0 ;
  output \r2_reg[8]_0 ;
  output \r2_reg[9]_0 ;
  output \r2_reg[10]_0 ;
  output \r2_reg[11]_0 ;
  output \r2_reg[12]_0 ;
  output \r2_reg[5]_1 ;
  output \r2_reg[6]_1 ;
  output \r2_reg[7]_1 ;
  output \r2_reg[8]_1 ;
  output \r2_reg[9]_1 ;
  output \r2_reg[10]_1 ;
  output \r2_reg[11]_1 ;
  output alusg0_4;
  output \r2_reg[3]_1 ;
  output [15:0]\real_result_reg[15] ;
  output p_0_in;
  output \r2_reg[14]_2 ;
  input [0:0]E;
  input CLK;
  input btnCreg;
  input [11:0]D;
  input [12:0]A;
  input \real_value_reg[4] ;
  input \sw_reg_reg[15] ;
  input \r1_reg[2]_0 ;
  input \FSM_sequential_currstate_reg[2] ;
  input \real_value_reg[12] ;
  input \sw_reg_reg[15]_0 ;
  input \r2_reg[15]_0 ;
  input \r1_reg[12]_0 ;
  input \FSM_sequential_currstate_reg[2]_0 ;
  input \sw_reg_reg[15]_1 ;
  input \r1_reg[2]_1 ;
  input [1:0]\sw_reg_reg[15]_2 ;
  input \r2_reg[0]_2 ;
  input \real_value_reg[1] ;
  input \FSM_sequential_currstate_reg[2]_1 ;
  input \r1_reg[2]_2 ;
  input \real_value_reg[2] ;
  input \FSM_sequential_currstate_reg[2]_2 ;
  input \FSM_sequential_currstate_reg[0] ;
  input \r1_reg[2]_3 ;
  input \r1_reg[2]_4 ;
  input \FSM_sequential_currstate_reg[2]_3 ;
  input \FSM_sequential_currstate_reg[2]_4 ;
  input \FSM_sequential_currstate_reg[2]_5 ;
  input \sw_reg_reg[14] ;
  input [2:0]out;

  wire [12:0]A;
  wire CLK;
  wire [0:0]CO;
  wire [11:0]D;
  wire [0:0]E;
  wire \FSM_sequential_currstate_reg[0] ;
  wire \FSM_sequential_currstate_reg[2] ;
  wire \FSM_sequential_currstate_reg[2]_0 ;
  wire \FSM_sequential_currstate_reg[2]_1 ;
  wire \FSM_sequential_currstate_reg[2]_2 ;
  wire \FSM_sequential_currstate_reg[2]_3 ;
  wire \FSM_sequential_currstate_reg[2]_4 ;
  wire \FSM_sequential_currstate_reg[2]_5 ;
  wire [0:0]O;
  wire [13:0]P;
  wire [15:0]Q;
  wire \SHIFT_RIGHT2_inferred__0/i__carry_n_0 ;
  wire \SHIFT_RIGHT2_inferred__0/i__carry_n_4 ;
  wire \SHIFT_RIGHT2_inferred__0/i__carry_n_6 ;
  wire \SHIFT_RIGHT2_inferred__0/i__carry_n_7 ;
  wire [11:0]alusg0_0;
  wire [13:0]alusg0_1;
  wire alusg0_2;
  wire alusg0_3;
  wire alusg0_4;
  wire alusg0_i_103_n_0;
  wire alusg0_i_105_n_0;
  wire alusg0_i_106_n_0;
  wire alusg0_i_107_n_0;
  wire alusg0_i_108_n_0;
  wire alusg0_i_109_n_0;
  wire alusg0_i_110_n_0;
  wire alusg0_i_118_n_0;
  wire alusg0_i_119_n_0;
  wire alusg0_i_120_n_0;
  wire alusg0_i_124_n_0;
  wire alusg0_i_126_n_0;
  wire alusg0_i_127_n_0;
  wire alusg0_i_128_n_0;
  wire alusg0_i_129_n_0;
  wire alusg0_i_130_n_0;
  wire alusg0_i_131_n_0;
  wire alusg0_i_132_n_0;
  wire alusg0_i_133_n_0;
  wire alusg0_i_134_n_0;
  wire alusg0_i_135_n_0;
  wire alusg0_i_137_n_0;
  wire alusg0_i_138_n_0;
  wire alusg0_i_139_n_0;
  wire alusg0_i_140_n_0;
  wire alusg0_i_141_n_0;
  wire alusg0_i_143_n_0;
  wire alusg0_i_144_n_0;
  wire alusg0_i_145_n_0;
  wire alusg0_i_146_n_0;
  wire alusg0_i_148_n_0;
  wire alusg0_i_149_n_0;
  wire alusg0_i_150_n_0;
  wire alusg0_i_151_n_0;
  wire alusg0_i_152_n_0;
  wire alusg0_i_153_n_0;
  wire alusg0_i_154_n_0;
  wire alusg0_i_155_n_0;
  wire alusg0_i_156_n_0;
  wire alusg0_i_157_n_0;
  wire alusg0_i_158_n_0;
  wire alusg0_i_159_n_0;
  wire alusg0_i_18_n_0;
  wire alusg0_i_22_n_0;
  wire alusg0_i_72_n_0;
  wire alusg0_i_74_n_0;
  wire alusg0_i_75_n_0;
  wire alusg0_i_76_n_0;
  wire alusg0_i_81_n_0;
  wire alusg0_i_82_n_0;
  wire alusg0_i_84_n_0;
  wire alusg0_i_85_n_0;
  wire alusg0_i_87_n_0;
  wire alusg0_i_91_n_0;
  wire alusg0_i_92_n_0;
  wire alusg0_n_105;
  wire alusg0_n_90;
  wire btnCreg;
  wire [4:1]data0;
  wire [3:2]data1;
  wire i__carry_i_1_n_0;
  wire i__carry_i_2_n_0;
  wire i__carry_i_3_n_0;
  wire i__carry_i_4_n_0;
  wire i__carry_i_5_n_0;
  wire i__carry_i_6_n_0;
  wire i__carry_i_7_n_0;
  wire i__carry_i_8_n_0;
  wire i__carry_i_9_n_0;
  wire [2:0]out;
  wire p_0_in;
  wire \r1[12]_i_1_n_0 ;
  wire \r1_reg[12]_0 ;
  wire \r1_reg[2]_0 ;
  wire \r1_reg[2]_1 ;
  wire \r1_reg[2]_2 ;
  wire \r1_reg[2]_3 ;
  wire \r1_reg[2]_4 ;
  wire \r1_reg_n_0_[10] ;
  wire \r1_reg_n_0_[11] ;
  wire \r1_reg_n_0_[4] ;
  wire \r1_reg_n_0_[5] ;
  wire \r1_reg_n_0_[6] ;
  wire \r1_reg_n_0_[7] ;
  wire \r1_reg_n_0_[8] ;
  wire \r1_reg_n_0_[9] ;
  wire \r2_reg[0]_0 ;
  wire \r2_reg[0]_1 ;
  wire \r2_reg[0]_2 ;
  wire \r2_reg[10]_0 ;
  wire \r2_reg[10]_1 ;
  wire \r2_reg[11]_0 ;
  wire \r2_reg[11]_1 ;
  wire \r2_reg[12]_0 ;
  wire \r2_reg[14]_0 ;
  wire \r2_reg[14]_1 ;
  wire \r2_reg[14]_2 ;
  wire \r2_reg[15]_0 ;
  wire \r2_reg[3]_0 ;
  wire \r2_reg[3]_1 ;
  wire \r2_reg[5]_0 ;
  wire \r2_reg[5]_1 ;
  wire \r2_reg[6]_0 ;
  wire \r2_reg[6]_1 ;
  wire \r2_reg[7]_0 ;
  wire \r2_reg[7]_1 ;
  wire \r2_reg[8]_0 ;
  wire \r2_reg[8]_1 ;
  wire \r2_reg[9]_0 ;
  wire \r2_reg[9]_1 ;
  wire \real_result[12]_i_3_n_0 ;
  wire \real_result[12]_i_4_n_0 ;
  wire \real_result[12]_i_5_n_0 ;
  wire \real_result[12]_i_6_n_0 ;
  wire \real_result[15]_i_3_n_0 ;
  wire \real_result[15]_i_4_n_0 ;
  wire \real_result[15]_i_5_n_0 ;
  wire \real_result[4]_i_3_n_0 ;
  wire \real_result[4]_i_4_n_0 ;
  wire \real_result[4]_i_5_n_0 ;
  wire \real_result[4]_i_6_n_0 ;
  wire \real_result[4]_i_7_n_0 ;
  wire \real_result[8]_i_3_n_0 ;
  wire \real_result[8]_i_4_n_0 ;
  wire \real_result[8]_i_5_n_0 ;
  wire \real_result[8]_i_6_n_0 ;
  wire \real_result_reg[12]_i_2_n_0 ;
  wire \real_result_reg[12]_i_2_n_4 ;
  wire \real_result_reg[12]_i_2_n_5 ;
  wire \real_result_reg[12]_i_2_n_6 ;
  wire \real_result_reg[12]_i_2_n_7 ;
  wire [15:0]\real_result_reg[15] ;
  wire \real_result_reg[15]_i_2_n_5 ;
  wire \real_result_reg[15]_i_2_n_6 ;
  wire \real_result_reg[15]_i_2_n_7 ;
  wire [4:0]\real_result_reg[1] ;
  wire \real_result_reg[4]_i_2_n_0 ;
  wire \real_result_reg[4]_i_2_n_4 ;
  wire \real_result_reg[4]_i_2_n_5 ;
  wire \real_result_reg[4]_i_2_n_6 ;
  wire \real_result_reg[4]_i_2_n_7 ;
  wire \real_result_reg[8]_i_2_n_0 ;
  wire \real_result_reg[8]_i_2_n_4 ;
  wire \real_result_reg[8]_i_2_n_5 ;
  wire \real_result_reg[8]_i_2_n_6 ;
  wire \real_result_reg[8]_i_2_n_7 ;
  wire \real_value_reg[12] ;
  wire \real_value_reg[1] ;
  wire \real_value_reg[2] ;
  wire \real_value_reg[4] ;
  wire [15:1]res_alu;
  wire \sw_reg_reg[14] ;
  wire \sw_reg_reg[15] ;
  wire \sw_reg_reg[15]_0 ;
  wire \sw_reg_reg[15]_1 ;
  wire [1:0]\sw_reg_reg[15]_2 ;
  wire [2:0]\NLW_SHIFT_RIGHT2_inferred__0/i__carry_CO_UNCONNECTED ;
  wire NLW_alusg0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_alusg0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_alusg0_OVERFLOW_UNCONNECTED;
  wire NLW_alusg0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_alusg0_PATTERNDETECT_UNCONNECTED;
  wire NLW_alusg0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_alusg0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_alusg0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_alusg0_CARRYOUT_UNCONNECTED;
  wire [47:16]NLW_alusg0_P_UNCONNECTED;
  wire [47:0]NLW_alusg0_PCOUT_UNCONNECTED;
  wire [2:0]NLW_alusg0_i_106_CO_UNCONNECTED;
  wire [2:0]NLW_alusg0_i_118_CO_UNCONNECTED;
  wire [2:0]NLW_alusg0_i_119_CO_UNCONNECTED;
  wire [2:0]NLW_alusg0_i_120_CO_UNCONNECTED;
  wire [3:0]NLW_alusg0_i_24_CO_UNCONNECTED;
  wire [2:0]NLW_alusg0_i_91_CO_UNCONNECTED;
  wire [2:0]NLW_alusg0_i_92_CO_UNCONNECTED;
  wire [3:1]NLW_alusg0_i_97_CO_UNCONNECTED;
  wire [3:0]NLW_alusg0_i_97_O_UNCONNECTED;
  wire [3:0]NLW_alusg0_i_99_CO_UNCONNECTED;
  wire [2:0]\NLW_real_result_reg[12]_i_2_CO_UNCONNECTED ;
  wire [3:0]\NLW_real_result_reg[15]_i_2_CO_UNCONNECTED ;
  wire [3:3]\NLW_real_result_reg[15]_i_2_O_UNCONNECTED ;
  wire [2:0]\NLW_real_result_reg[4]_i_2_CO_UNCONNECTED ;
  wire [2:0]\NLW_real_result_reg[8]_i_2_CO_UNCONNECTED ;

  CARRY4 \SHIFT_RIGHT2_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\SHIFT_RIGHT2_inferred__0/i__carry_n_0 ,\NLW_SHIFT_RIGHT2_inferred__0/i__carry_CO_UNCONNECTED [2:0]}),
        .CYINIT(i__carry_i_1_n_0),
        .DI({i__carry_i_2_n_0,i__carry_i_3_n_0,i__carry_i_4_n_0,i__carry_i_5_n_0}),
        .O({\SHIFT_RIGHT2_inferred__0/i__carry_n_4 ,O,\SHIFT_RIGHT2_inferred__0/i__carry_n_6 ,\SHIFT_RIGHT2_inferred__0/i__carry_n_7 }),
        .S({i__carry_i_6_n_0,i__carry_i_7_n_0,i__carry_i_8_n_0,i__carry_i_9_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-11 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(1),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(1),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(1),
    .BREG(1),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    alusg0
       (.A({A[12],A[12],A[12],A[12],A[12],A[12],A[12],A[12],A[12],A[12],A[12],A[12],A[12],A[12],A[12],A[12],A[12],A}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_alusg0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({res_alu[15],res_alu[15],res_alu[15],D[11:2],res_alu[4],D[1],res_alu[2:1],D[0]}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_alusg0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_alusg0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_alusg0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(\r1[12]_i_1_n_0 ),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(E),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(CLK),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_alusg0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_alusg0_OVERFLOW_UNCONNECTED),
        .P({NLW_alusg0_P_UNCONNECTED[47:16],alusg0_n_90,P,alusg0_n_105}),
        .PATTERNBDETECT(NLW_alusg0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_alusg0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_alusg0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(btnCreg),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(btnCreg),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_alusg0_UNDERFLOW_UNCONNECTED));
  LUT6 #(
    .INIT(64'h0000CC00F000AA00)) 
    alusg0_i_103
       (.I0(Q[10]),
        .I1(Q[11]),
        .I2(Q[12]),
        .I3(\sw_reg_reg[15]_0 ),
        .I4(\real_result_reg[1] [1]),
        .I5(\real_result_reg[1] [0]),
        .O(alusg0_i_103_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    alusg0_i_104
       (.I0(\r2_reg[14]_2 ),
        .I1(\sw_reg_reg[15]_2 [1]),
        .O(\r2_reg[14]_1 ));
  LUT5 #(
    .INIT(32'hFFFFA800)) 
    alusg0_i_105
       (.I0(\sw_reg_reg[15] ),
        .I1(\real_result_reg[1] [1]),
        .I2(\real_result_reg[1] [0]),
        .I3(Q[15]),
        .I4(\real_value_reg[12] ),
        .O(alusg0_i_105_n_0));
  CARRY4 alusg0_i_106
       (.CI(alusg0_i_120_n_0),
        .CO({alusg0_i_106_n_0,NLW_alusg0_i_106_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[11:8]),
        .O(alusg0_1[9:6]),
        .S({alusg0_i_143_n_0,alusg0_i_144_n_0,alusg0_i_145_n_0,alusg0_i_146_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    alusg0_i_107
       (.I0(Q[14]),
        .I1(Q[15]),
        .O(alusg0_i_107_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    alusg0_i_108
       (.I0(Q[13]),
        .I1(Q[14]),
        .O(alusg0_i_108_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    alusg0_i_109
       (.I0(\real_result_reg[1] [4]),
        .I1(Q[13]),
        .O(alusg0_i_109_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    alusg0_i_110
       (.I0(\real_result_reg[1] [4]),
        .I1(Q[12]),
        .O(alusg0_i_110_n_0));
  CARRY4 alusg0_i_118
       (.CI(alusg0_i_119_n_0),
        .CO({alusg0_i_118_n_0,NLW_alusg0_i_118_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[11:8]),
        .O(alusg0_0[7:4]),
        .S({alusg0_i_148_n_0,alusg0_i_149_n_0,alusg0_i_150_n_0,alusg0_i_151_n_0}));
  CARRY4 alusg0_i_119
       (.CI(alusg0_i_92_n_0),
        .CO({alusg0_i_119_n_0,NLW_alusg0_i_119_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[7:4]),
        .O({alusg0_0[3:1],data0[4]}),
        .S({alusg0_i_152_n_0,alusg0_i_153_n_0,alusg0_i_154_n_0,alusg0_i_155_n_0}));
  CARRY4 alusg0_i_120
       (.CI(alusg0_i_91_n_0),
        .CO({alusg0_i_120_n_0,NLW_alusg0_i_120_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[7:4]),
        .O(alusg0_1[5:2]),
        .S({alusg0_i_156_n_0,alusg0_i_157_n_0,alusg0_i_158_n_0,alusg0_i_159_n_0}));
  LUT2 #(
    .INIT(4'hB)) 
    alusg0_i_123
       (.I0(\real_result_reg[1] [0]),
        .I1(\real_result_reg[1] [1]),
        .O(alusg0_2));
  LUT6 #(
    .INIT(64'hF888000088880000)) 
    alusg0_i_124
       (.I0(\FSM_sequential_currstate_reg[2] ),
        .I1(Q[2]),
        .I2(\FSM_sequential_currstate_reg[0] ),
        .I3(Q[1]),
        .I4(\real_result_reg[1] [2]),
        .I5(alusg0_3),
        .O(alusg0_i_124_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    alusg0_i_125
       (.I0(\real_result_reg[1] [0]),
        .I1(\real_result_reg[1] [1]),
        .O(alusg0_4));
  LUT6 #(
    .INIT(64'hFFFFFFFFFF404040)) 
    alusg0_i_126
       (.I0(alusg0_2),
        .I1(\sw_reg_reg[15] ),
        .I2(Q[2]),
        .I3(\FSM_sequential_currstate_reg[2]_2 ),
        .I4(Q[0]),
        .I5(\FSM_sequential_currstate_reg[2]_3 ),
        .O(alusg0_i_126_n_0));
  LUT6 #(
    .INIT(64'hFAEAAAAAAAEAAAAA)) 
    alusg0_i_127
       (.I0(\r2_reg[0]_2 ),
        .I1(Q[1]),
        .I2(\sw_reg_reg[15]_1 ),
        .I3(\real_result_reg[1] [1]),
        .I4(\real_result_reg[1] [0]),
        .I5(Q[3]),
        .O(alusg0_i_127_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    alusg0_i_128
       (.I0(Q[3]),
        .I1(\real_result_reg[1] [3]),
        .O(alusg0_i_128_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    alusg0_i_129
       (.I0(Q[2]),
        .I1(\real_result_reg[1] [2]),
        .O(alusg0_i_129_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    alusg0_i_13
       (.I0(alusg0_i_72_n_0),
        .I1(\real_value_reg[4] ),
        .I2(alusg0_i_74_n_0),
        .I3(alusg0_i_75_n_0),
        .I4(alusg0_i_76_n_0),
        .O(res_alu[4]));
  LUT2 #(
    .INIT(4'h9)) 
    alusg0_i_130
       (.I0(Q[1]),
        .I1(\real_result_reg[1] [1]),
        .O(alusg0_i_130_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    alusg0_i_131
       (.I0(Q[0]),
        .I1(\real_result_reg[1] [0]),
        .O(alusg0_i_131_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    alusg0_i_132
       (.I0(\real_result_reg[1] [3]),
        .I1(Q[3]),
        .O(alusg0_i_132_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    alusg0_i_133
       (.I0(\real_result_reg[1] [2]),
        .I1(Q[2]),
        .O(alusg0_i_133_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    alusg0_i_134
       (.I0(\real_result_reg[1] [1]),
        .I1(Q[1]),
        .O(alusg0_i_134_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    alusg0_i_135
       (.I0(\real_result_reg[1] [0]),
        .I1(Q[0]),
        .O(alusg0_i_135_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    alusg0_i_136
       (.I0(\real_result_reg[1] [0]),
        .I1(\real_result_reg[1] [1]),
        .O(\r2_reg[0]_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    alusg0_i_137
       (.I0(\real_result_reg[1] [4]),
        .O(alusg0_i_137_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    alusg0_i_138
       (.I0(Q[14]),
        .I1(Q[15]),
        .O(alusg0_i_138_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    alusg0_i_139
       (.I0(Q[13]),
        .I1(Q[14]),
        .O(alusg0_i_139_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    alusg0_i_140
       (.I0(Q[13]),
        .I1(\real_result_reg[1] [4]),
        .O(alusg0_i_140_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    alusg0_i_141
       (.I0(Q[12]),
        .I1(\real_result_reg[1] [4]),
        .O(alusg0_i_141_n_0));
  LUT6 #(
    .INIT(64'h0101000000FF0001)) 
    alusg0_i_142
       (.I0(\real_result_reg[1] [2]),
        .I1(\real_result_reg[1] [1]),
        .I2(\real_result_reg[1] [0]),
        .I3(out[0]),
        .I4(out[2]),
        .I5(out[1]),
        .O(\r2_reg[14]_2 ));
  LUT2 #(
    .INIT(4'h9)) 
    alusg0_i_143
       (.I0(\r1_reg_n_0_[11] ),
        .I1(Q[11]),
        .O(alusg0_i_143_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    alusg0_i_144
       (.I0(\r1_reg_n_0_[10] ),
        .I1(Q[10]),
        .O(alusg0_i_144_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    alusg0_i_145
       (.I0(\r1_reg_n_0_[9] ),
        .I1(Q[9]),
        .O(alusg0_i_145_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    alusg0_i_146
       (.I0(\r1_reg_n_0_[8] ),
        .I1(Q[8]),
        .O(alusg0_i_146_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    alusg0_i_147
       (.I0(\real_result_reg[1] [0]),
        .I1(\real_result_reg[1] [1]),
        .O(alusg0_3));
  LUT2 #(
    .INIT(4'h6)) 
    alusg0_i_148
       (.I0(Q[11]),
        .I1(\r1_reg_n_0_[11] ),
        .O(alusg0_i_148_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    alusg0_i_149
       (.I0(Q[10]),
        .I1(\r1_reg_n_0_[10] ),
        .O(alusg0_i_149_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    alusg0_i_15
       (.I0(alusg0_i_81_n_0),
        .I1(alusg0_i_82_n_0),
        .I2(\real_value_reg[2] ),
        .I3(alusg0_i_84_n_0),
        .O(res_alu[2]));
  LUT2 #(
    .INIT(4'h6)) 
    alusg0_i_150
       (.I0(Q[9]),
        .I1(\r1_reg_n_0_[9] ),
        .O(alusg0_i_150_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    alusg0_i_151
       (.I0(Q[8]),
        .I1(\r1_reg_n_0_[8] ),
        .O(alusg0_i_151_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    alusg0_i_152
       (.I0(Q[7]),
        .I1(\r1_reg_n_0_[7] ),
        .O(alusg0_i_152_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    alusg0_i_153
       (.I0(Q[6]),
        .I1(\r1_reg_n_0_[6] ),
        .O(alusg0_i_153_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    alusg0_i_154
       (.I0(Q[5]),
        .I1(\r1_reg_n_0_[5] ),
        .O(alusg0_i_154_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    alusg0_i_155
       (.I0(Q[4]),
        .I1(\r1_reg_n_0_[4] ),
        .O(alusg0_i_155_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    alusg0_i_156
       (.I0(\r1_reg_n_0_[7] ),
        .I1(Q[7]),
        .O(alusg0_i_156_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    alusg0_i_157
       (.I0(\r1_reg_n_0_[6] ),
        .I1(Q[6]),
        .O(alusg0_i_157_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    alusg0_i_158
       (.I0(\r1_reg_n_0_[5] ),
        .I1(Q[5]),
        .O(alusg0_i_158_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    alusg0_i_159
       (.I0(\r1_reg_n_0_[4] ),
        .I1(Q[4]),
        .O(alusg0_i_159_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFEFFFE)) 
    alusg0_i_16
       (.I0(alusg0_i_85_n_0),
        .I1(\real_value_reg[1] ),
        .I2(alusg0_i_87_n_0),
        .I3(\FSM_sequential_currstate_reg[2]_1 ),
        .I4(Q[2]),
        .I5(\r1_reg[2]_2 ),
        .O(res_alu[1]));
  LUT6 #(
    .INIT(64'hF0AACC0000000000)) 
    alusg0_i_18
       (.I0(Q[12]),
        .I1(Q[13]),
        .I2(Q[14]),
        .I3(\real_result_reg[1] [1]),
        .I4(\real_result_reg[1] [0]),
        .I5(\sw_reg_reg[15]_0 ),
        .O(alusg0_i_18_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEFE)) 
    alusg0_i_2
       (.I0(alusg0_i_18_n_0),
        .I1(\r2_reg[15]_0 ),
        .I2(\r1_reg[12]_0 ),
        .I3(alusg0_n_90),
        .I4(\FSM_sequential_currstate_reg[2]_0 ),
        .I5(alusg0_i_22_n_0),
        .O(res_alu[15]));
  LUT6 #(
    .INIT(64'hFFFFCC80CC80CC80)) 
    alusg0_i_22
       (.I0(\real_result_reg[1] [4]),
        .I1(Q[15]),
        .I2(\FSM_sequential_currstate_reg[2] ),
        .I3(\sw_reg_reg[15]_1 ),
        .I4(Q[11]),
        .I5(\r1_reg[2]_1 ),
        .O(alusg0_i_22_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEEEAAAA)) 
    alusg0_i_23
       (.I0(alusg0_i_103_n_0),
        .I1(\r2_reg[14]_1 ),
        .I2(\FSM_sequential_currstate_reg[2] ),
        .I3(\real_result_reg[1] [4]),
        .I4(Q[14]),
        .I5(alusg0_i_105_n_0),
        .O(\r2_reg[14]_0 ));
  CARRY4 alusg0_i_24
       (.CI(alusg0_i_106_n_0),
        .CO(NLW_alusg0_i_24_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,Q[13],\real_result_reg[1] [4],Q[12]}),
        .O(alusg0_1[13:10]),
        .S({alusg0_i_107_n_0,alusg0_i_108_n_0,alusg0_i_109_n_0,alusg0_i_110_n_0}));
  LUT4 #(
    .INIT(16'hF888)) 
    alusg0_i_31
       (.I0(Q[11]),
        .I1(\r1_reg[2]_3 ),
        .I2(Q[10]),
        .I3(\r1_reg[2]_4 ),
        .O(\r2_reg[12]_0 ));
  LUT6 #(
    .INIT(64'hFFFFCC80CC80CC80)) 
    alusg0_i_39
       (.I0(Q[11]),
        .I1(\r1_reg_n_0_[11] ),
        .I2(\FSM_sequential_currstate_reg[2] ),
        .I3(\FSM_sequential_currstate_reg[2]_2 ),
        .I4(Q[12]),
        .I5(\r1_reg[2]_2 ),
        .O(\r2_reg[11]_1 ));
  LUT6 #(
    .INIT(64'hF0AACC0000000000)) 
    alusg0_i_41
       (.I0(Q[8]),
        .I1(Q[9]),
        .I2(Q[10]),
        .I3(\real_result_reg[1] [1]),
        .I4(\real_result_reg[1] [0]),
        .I5(\sw_reg_reg[15]_0 ),
        .O(\r2_reg[11]_0 ));
  LUT6 #(
    .INIT(64'hFFFFCC80CC80CC80)) 
    alusg0_i_44
       (.I0(Q[10]),
        .I1(\r1_reg_n_0_[10] ),
        .I2(\FSM_sequential_currstate_reg[2] ),
        .I3(\FSM_sequential_currstate_reg[2]_2 ),
        .I4(Q[11]),
        .I5(\r1_reg[2]_2 ),
        .O(\r2_reg[10]_1 ));
  LUT6 #(
    .INIT(64'hF0AACC0000000000)) 
    alusg0_i_46
       (.I0(Q[7]),
        .I1(Q[8]),
        .I2(Q[9]),
        .I3(\real_result_reg[1] [1]),
        .I4(\real_result_reg[1] [0]),
        .I5(\sw_reg_reg[15]_0 ),
        .O(\r2_reg[10]_0 ));
  LUT6 #(
    .INIT(64'hFFFFCC80CC80CC80)) 
    alusg0_i_49
       (.I0(Q[9]),
        .I1(\r1_reg_n_0_[9] ),
        .I2(\FSM_sequential_currstate_reg[2] ),
        .I3(\FSM_sequential_currstate_reg[2]_2 ),
        .I4(Q[10]),
        .I5(\r1_reg[2]_2 ),
        .O(\r2_reg[9]_1 ));
  LUT6 #(
    .INIT(64'hF0AACC0000000000)) 
    alusg0_i_51
       (.I0(Q[6]),
        .I1(Q[7]),
        .I2(Q[8]),
        .I3(\real_result_reg[1] [1]),
        .I4(\real_result_reg[1] [0]),
        .I5(\sw_reg_reg[15]_0 ),
        .O(\r2_reg[9]_0 ));
  LUT6 #(
    .INIT(64'hFFFFCC80CC80CC80)) 
    alusg0_i_54
       (.I0(Q[8]),
        .I1(\r1_reg_n_0_[8] ),
        .I2(\FSM_sequential_currstate_reg[2] ),
        .I3(\FSM_sequential_currstate_reg[2]_2 ),
        .I4(Q[9]),
        .I5(\r1_reg[2]_2 ),
        .O(\r2_reg[8]_1 ));
  LUT6 #(
    .INIT(64'hF0AACC0000000000)) 
    alusg0_i_56
       (.I0(Q[5]),
        .I1(Q[6]),
        .I2(Q[7]),
        .I3(\real_result_reg[1] [1]),
        .I4(\real_result_reg[1] [0]),
        .I5(\sw_reg_reg[15]_0 ),
        .O(\r2_reg[8]_0 ));
  LUT6 #(
    .INIT(64'hFFFFCC80CC80CC80)) 
    alusg0_i_59
       (.I0(Q[7]),
        .I1(\r1_reg_n_0_[7] ),
        .I2(\FSM_sequential_currstate_reg[2] ),
        .I3(\FSM_sequential_currstate_reg[2]_2 ),
        .I4(Q[8]),
        .I5(\r1_reg[2]_2 ),
        .O(\r2_reg[7]_1 ));
  LUT6 #(
    .INIT(64'hF0AACC0000000000)) 
    alusg0_i_61
       (.I0(Q[4]),
        .I1(Q[5]),
        .I2(Q[6]),
        .I3(\real_result_reg[1] [1]),
        .I4(\real_result_reg[1] [0]),
        .I5(\sw_reg_reg[15]_0 ),
        .O(\r2_reg[7]_0 ));
  LUT6 #(
    .INIT(64'hFFFFCC80CC80CC80)) 
    alusg0_i_64
       (.I0(Q[6]),
        .I1(\r1_reg_n_0_[6] ),
        .I2(\FSM_sequential_currstate_reg[2] ),
        .I3(\FSM_sequential_currstate_reg[2]_2 ),
        .I4(Q[7]),
        .I5(\r1_reg[2]_2 ),
        .O(\r2_reg[6]_1 ));
  LUT6 #(
    .INIT(64'hF0AACC0000000000)) 
    alusg0_i_66
       (.I0(Q[3]),
        .I1(Q[4]),
        .I2(Q[5]),
        .I3(\real_result_reg[1] [1]),
        .I4(\real_result_reg[1] [0]),
        .I5(\sw_reg_reg[15]_0 ),
        .O(\r2_reg[6]_0 ));
  LUT6 #(
    .INIT(64'hFFFFCC80CC80CC80)) 
    alusg0_i_69
       (.I0(Q[5]),
        .I1(\r1_reg_n_0_[5] ),
        .I2(\FSM_sequential_currstate_reg[2] ),
        .I3(\FSM_sequential_currstate_reg[2]_2 ),
        .I4(Q[6]),
        .I5(\r1_reg[2]_2 ),
        .O(\r2_reg[5]_1 ));
  LUT6 #(
    .INIT(64'hF0AACC0000000000)) 
    alusg0_i_71
       (.I0(Q[2]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(\real_result_reg[1] [1]),
        .I4(\real_result_reg[1] [0]),
        .I5(\sw_reg_reg[15]_0 ),
        .O(\r2_reg[5]_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_72
       (.I0(\r1_reg[2]_0 ),
        .I1(\SHIFT_RIGHT2_inferred__0/i__carry_n_4 ),
        .I2(\r2_reg[14]_1 ),
        .I3(Q[4]),
        .I4(data0[4]),
        .I5(\FSM_sequential_currstate_reg[2]_4 ),
        .O(alusg0_i_72_n_0));
  LUT6 #(
    .INIT(64'hFFFFCC80CC80CC80)) 
    alusg0_i_74
       (.I0(Q[4]),
        .I1(\r1_reg_n_0_[4] ),
        .I2(\FSM_sequential_currstate_reg[2] ),
        .I3(\FSM_sequential_currstate_reg[2]_2 ),
        .I4(Q[5]),
        .I5(\r1_reg[2]_2 ),
        .O(alusg0_i_74_n_0));
  LUT6 #(
    .INIT(64'hA08800FFA0880000)) 
    alusg0_i_75
       (.I0(\sw_reg_reg[15] ),
        .I1(Q[6]),
        .I2(Q[7]),
        .I3(\real_result_reg[1] [0]),
        .I4(\real_result_reg[1] [1]),
        .I5(\r1_reg[2]_0 ),
        .O(alusg0_i_75_n_0));
  LUT6 #(
    .INIT(64'hF0AACC0000000000)) 
    alusg0_i_76
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(\real_result_reg[1] [1]),
        .I4(\real_result_reg[1] [0]),
        .I5(\sw_reg_reg[15]_0 ),
        .O(alusg0_i_76_n_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_78
       (.I0(\r2_reg[14]_1 ),
        .I1(Q[3]),
        .I2(\FSM_sequential_currstate_reg[2]_4 ),
        .I3(data0[3]),
        .I4(data1[3]),
        .I5(\FSM_sequential_currstate_reg[2]_5 ),
        .O(\r2_reg[3]_1 ));
  LUT6 #(
    .INIT(64'hA088FF00A0880000)) 
    alusg0_i_80
       (.I0(\sw_reg_reg[15] ),
        .I1(Q[5]),
        .I2(Q[6]),
        .I3(\real_result_reg[1] [0]),
        .I4(\real_result_reg[1] [1]),
        .I5(\r1_reg[2]_0 ),
        .O(\r2_reg[3]_0 ));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_81
       (.I0(\r2_reg[14]_1 ),
        .I1(Q[2]),
        .I2(\FSM_sequential_currstate_reg[2]_4 ),
        .I3(data0[2]),
        .I4(data1[2]),
        .I5(\FSM_sequential_currstate_reg[2]_5 ),
        .O(alusg0_i_81_n_0));
  LUT6 #(
    .INIT(64'hF0AACC0000000000)) 
    alusg0_i_82
       (.I0(Q[3]),
        .I1(Q[4]),
        .I2(Q[5]),
        .I3(\real_result_reg[1] [1]),
        .I4(\real_result_reg[1] [0]),
        .I5(\sw_reg_reg[15] ),
        .O(alusg0_i_82_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFA2FFA2FFA2)) 
    alusg0_i_84
       (.I0(\r2_reg[0]_2 ),
        .I1(alusg0_2),
        .I2(\SHIFT_RIGHT2_inferred__0/i__carry_n_6 ),
        .I3(alusg0_i_124_n_0),
        .I4(\FSM_sequential_currstate_reg[2]_2 ),
        .I5(\real_result_reg[1] [2]),
        .O(alusg0_i_84_n_0));
  LUT6 #(
    .INIT(64'hFFFFF888F888F888)) 
    alusg0_i_85
       (.I0(\r1_reg[2]_0 ),
        .I1(\SHIFT_RIGHT2_inferred__0/i__carry_n_7 ),
        .I2(\r2_reg[14]_1 ),
        .I3(Q[1]),
        .I4(data0[1]),
        .I5(\FSM_sequential_currstate_reg[2]_4 ),
        .O(alusg0_i_85_n_0));
  LUT6 #(
    .INIT(64'hFF880000C0880000)) 
    alusg0_i_87
       (.I0(Q[3]),
        .I1(\sw_reg_reg[15]_1 ),
        .I2(Q[4]),
        .I3(\real_result_reg[1] [0]),
        .I4(\real_result_reg[1] [1]),
        .I5(\r2_reg[0]_2 ),
        .O(alusg0_i_87_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFEAFFEAFFEA)) 
    alusg0_i_90
       (.I0(alusg0_i_126_n_0),
        .I1(A[0]),
        .I2(\sw_reg_reg[15]_2 [1]),
        .I3(alusg0_i_127_n_0),
        .I4(\FSM_sequential_currstate_reg[2]_0 ),
        .I5(alusg0_n_105),
        .O(\r2_reg[0]_1 ));
  CARRY4 alusg0_i_91
       (.CI(1'b0),
        .CO({alusg0_i_91_n_0,NLW_alusg0_i_91_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b1),
        .DI(Q[3:0]),
        .O({data1,alusg0_1[1:0]}),
        .S({alusg0_i_128_n_0,alusg0_i_129_n_0,alusg0_i_130_n_0,alusg0_i_131_n_0}));
  CARRY4 alusg0_i_92
       (.CI(1'b0),
        .CO({alusg0_i_92_n_0,NLW_alusg0_i_92_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(Q[3:0]),
        .O({data0[3:1],alusg0_0[0]}),
        .S({alusg0_i_132_n_0,alusg0_i_133_n_0,alusg0_i_134_n_0,alusg0_i_135_n_0}));
  CARRY4 alusg0_i_97
       (.CI(\SHIFT_RIGHT2_inferred__0/i__carry_n_0 ),
        .CO({NLW_alusg0_i_97_CO_UNCONNECTED[3:1],CO}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_alusg0_i_97_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  CARRY4 alusg0_i_99
       (.CI(alusg0_i_118_n_0),
        .CO(NLW_alusg0_i_99_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,Q[13],alusg0_i_137_n_0,\real_result_reg[1] [4]}),
        .O(alusg0_0[11:8]),
        .S({alusg0_i_138_n_0,alusg0_i_139_n_0,alusg0_i_140_n_0,alusg0_i_141_n_0}));
  LUT3 #(
    .INIT(8'h01)) 
    i__carry_i_1
       (.I0(\real_result_reg[1] [1]),
        .I1(\real_result_reg[1] [0]),
        .I2(\real_result_reg[1] [2]),
        .O(i__carry_i_1_n_0));
  LUT3 #(
    .INIT(8'h02)) 
    i__carry_i_2
       (.I0(\real_result_reg[1] [2]),
        .I1(\real_result_reg[1] [0]),
        .I2(\real_result_reg[1] [1]),
        .O(i__carry_i_2_n_0));
  LUT3 #(
    .INIT(8'h48)) 
    i__carry_i_3
       (.I0(\real_result_reg[1] [2]),
        .I1(\real_result_reg[1] [0]),
        .I2(\real_result_reg[1] [1]),
        .O(i__carry_i_3_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry_i_4
       (.I0(\real_result_reg[1] [1]),
        .I1(\real_result_reg[1] [0]),
        .O(i__carry_i_4_n_0));
  LUT3 #(
    .INIT(8'h84)) 
    i__carry_i_5
       (.I0(\real_result_reg[1] [2]),
        .I1(\real_result_reg[1] [0]),
        .I2(\real_result_reg[1] [1]),
        .O(i__carry_i_5_n_0));
  LUT3 #(
    .INIT(8'hEF)) 
    i__carry_i_6
       (.I0(\real_result_reg[1] [1]),
        .I1(\real_result_reg[1] [0]),
        .I2(\real_result_reg[1] [2]),
        .O(i__carry_i_6_n_0));
  LUT3 #(
    .INIT(8'h9F)) 
    i__carry_i_7
       (.I0(\real_result_reg[1] [2]),
        .I1(\real_result_reg[1] [1]),
        .I2(\real_result_reg[1] [0]),
        .O(i__carry_i_7_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    i__carry_i_8
       (.I0(\real_result_reg[1] [0]),
        .I1(\real_result_reg[1] [1]),
        .O(i__carry_i_8_n_0));
  LUT3 #(
    .INIT(8'h6F)) 
    i__carry_i_9
       (.I0(\real_result_reg[1] [2]),
        .I1(\real_result_reg[1] [1]),
        .I2(\real_result_reg[1] [0]),
        .O(i__carry_i_9_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hBA8A)) 
    neg_i_1
       (.I0(Q[15]),
        .I1(\sw_reg_reg[15]_2 [0]),
        .I2(\sw_reg_reg[15]_2 [1]),
        .I3(\real_result_reg[1] [4]),
        .O(p_0_in));
  LUT2 #(
    .INIT(4'h2)) 
    \r1[12]_i_1 
       (.I0(\sw_reg_reg[15]_2 [1]),
        .I1(\sw_reg_reg[15]_2 [0]),
        .O(\r1[12]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[0] 
       (.C(CLK),
        .CE(\r1[12]_i_1_n_0 ),
        .D(A[0]),
        .Q(\real_result_reg[1] [0]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[10] 
       (.C(CLK),
        .CE(\r1[12]_i_1_n_0 ),
        .D(A[10]),
        .Q(\r1_reg_n_0_[10] ),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[11] 
       (.C(CLK),
        .CE(\r1[12]_i_1_n_0 ),
        .D(A[11]),
        .Q(\r1_reg_n_0_[11] ),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[12] 
       (.C(CLK),
        .CE(\r1[12]_i_1_n_0 ),
        .D(A[12]),
        .Q(\real_result_reg[1] [4]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[1] 
       (.C(CLK),
        .CE(\r1[12]_i_1_n_0 ),
        .D(A[1]),
        .Q(\real_result_reg[1] [1]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[2] 
       (.C(CLK),
        .CE(\r1[12]_i_1_n_0 ),
        .D(A[2]),
        .Q(\real_result_reg[1] [2]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[3] 
       (.C(CLK),
        .CE(\r1[12]_i_1_n_0 ),
        .D(A[3]),
        .Q(\real_result_reg[1] [3]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[4] 
       (.C(CLK),
        .CE(\r1[12]_i_1_n_0 ),
        .D(A[4]),
        .Q(\r1_reg_n_0_[4] ),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[5] 
       (.C(CLK),
        .CE(\r1[12]_i_1_n_0 ),
        .D(A[5]),
        .Q(\r1_reg_n_0_[5] ),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[6] 
       (.C(CLK),
        .CE(\r1[12]_i_1_n_0 ),
        .D(A[6]),
        .Q(\r1_reg_n_0_[6] ),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[7] 
       (.C(CLK),
        .CE(\r1[12]_i_1_n_0 ),
        .D(A[7]),
        .Q(\r1_reg_n_0_[7] ),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[8] 
       (.C(CLK),
        .CE(\r1[12]_i_1_n_0 ),
        .D(A[8]),
        .Q(\r1_reg_n_0_[8] ),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[9] 
       (.C(CLK),
        .CE(\r1[12]_i_1_n_0 ),
        .D(A[9]),
        .Q(\r1_reg_n_0_[9] ),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[0] 
       (.C(CLK),
        .CE(E),
        .D(D[0]),
        .Q(Q[0]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[10] 
       (.C(CLK),
        .CE(E),
        .D(D[7]),
        .Q(Q[10]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[11] 
       (.C(CLK),
        .CE(E),
        .D(D[8]),
        .Q(Q[11]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[12] 
       (.C(CLK),
        .CE(E),
        .D(D[9]),
        .Q(Q[12]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[13] 
       (.C(CLK),
        .CE(E),
        .D(D[10]),
        .Q(Q[13]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[14] 
       (.C(CLK),
        .CE(E),
        .D(D[11]),
        .Q(Q[14]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[15] 
       (.C(CLK),
        .CE(E),
        .D(res_alu[15]),
        .Q(Q[15]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[1] 
       (.C(CLK),
        .CE(E),
        .D(res_alu[1]),
        .Q(Q[1]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[2] 
       (.C(CLK),
        .CE(E),
        .D(res_alu[2]),
        .Q(Q[2]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[3] 
       (.C(CLK),
        .CE(E),
        .D(D[1]),
        .Q(Q[3]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[4] 
       (.C(CLK),
        .CE(E),
        .D(res_alu[4]),
        .Q(Q[4]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[5] 
       (.C(CLK),
        .CE(E),
        .D(D[2]),
        .Q(Q[5]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[6] 
       (.C(CLK),
        .CE(E),
        .D(D[3]),
        .Q(Q[6]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[7] 
       (.C(CLK),
        .CE(E),
        .D(D[4]),
        .Q(Q[7]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[8] 
       (.C(CLK),
        .CE(E),
        .D(D[5]),
        .Q(Q[8]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[9] 
       (.C(CLK),
        .CE(E),
        .D(D[6]),
        .Q(Q[9]),
        .R(btnCreg));
  LUT4 #(
    .INIT(16'hBA8A)) 
    \real_result[0]_i_1 
       (.I0(Q[0]),
        .I1(\sw_reg_reg[15]_2 [0]),
        .I2(\sw_reg_reg[15]_2 [1]),
        .I3(\real_result_reg[1] [0]),
        .O(\real_result_reg[15] [0]));
  LUT6 #(
    .INIT(64'hF0CCF0F0F0CCAAAA)) 
    \real_result[10]_i_1 
       (.I0(\r1_reg_n_0_[10] ),
        .I1(Q[10]),
        .I2(\real_result_reg[12]_i_2_n_6 ),
        .I3(Q[15]),
        .I4(\sw_reg_reg[14] ),
        .I5(\real_result_reg[1] [4]),
        .O(\real_result_reg[15] [10]));
  LUT6 #(
    .INIT(64'hF0CCF0F0F0CCAAAA)) 
    \real_result[11]_i_1 
       (.I0(\r1_reg_n_0_[11] ),
        .I1(Q[11]),
        .I2(\real_result_reg[12]_i_2_n_5 ),
        .I3(Q[15]),
        .I4(\sw_reg_reg[14] ),
        .I5(\real_result_reg[1] [4]),
        .O(\real_result_reg[15] [11]));
  LUT6 #(
    .INIT(64'hAAAA88AAF0F088F0)) 
    \real_result[12]_i_1 
       (.I0(\real_result_reg[12]_i_2_n_4 ),
        .I1(\real_result_reg[1] [4]),
        .I2(Q[12]),
        .I3(\sw_reg_reg[15]_2 [1]),
        .I4(\sw_reg_reg[15]_2 [0]),
        .I5(Q[15]),
        .O(\real_result_reg[15] [12]));
  LUT4 #(
    .INIT(16'h5355)) 
    \real_result[12]_i_3 
       (.I0(Q[12]),
        .I1(\real_result_reg[1] [4]),
        .I2(\sw_reg_reg[15]_2 [0]),
        .I3(\sw_reg_reg[15]_2 [1]),
        .O(\real_result[12]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h4575)) 
    \real_result[12]_i_4 
       (.I0(Q[11]),
        .I1(\sw_reg_reg[15]_2 [0]),
        .I2(\sw_reg_reg[15]_2 [1]),
        .I3(\r1_reg_n_0_[11] ),
        .O(\real_result[12]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h4575)) 
    \real_result[12]_i_5 
       (.I0(Q[10]),
        .I1(\sw_reg_reg[15]_2 [0]),
        .I2(\sw_reg_reg[15]_2 [1]),
        .I3(\r1_reg_n_0_[10] ),
        .O(\real_result[12]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h4575)) 
    \real_result[12]_i_6 
       (.I0(Q[9]),
        .I1(\sw_reg_reg[15]_2 [0]),
        .I2(\sw_reg_reg[15]_2 [1]),
        .I3(\r1_reg_n_0_[9] ),
        .O(\real_result[12]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAAAA88AAF0F088F0)) 
    \real_result[13]_i_1 
       (.I0(\real_result_reg[15]_i_2_n_7 ),
        .I1(\real_result_reg[1] [4]),
        .I2(Q[13]),
        .I3(\sw_reg_reg[15]_2 [1]),
        .I4(\sw_reg_reg[15]_2 [0]),
        .I5(Q[15]),
        .O(\real_result_reg[15] [13]));
  LUT6 #(
    .INIT(64'hAAAA88AAF0F088F0)) 
    \real_result[14]_i_1 
       (.I0(\real_result_reg[15]_i_2_n_6 ),
        .I1(\real_result_reg[1] [4]),
        .I2(Q[14]),
        .I3(\sw_reg_reg[15]_2 [1]),
        .I4(\sw_reg_reg[15]_2 [0]),
        .I5(Q[15]),
        .O(\real_result_reg[15] [14]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFB080000)) 
    \real_result[15]_i_1 
       (.I0(\real_result_reg[1] [4]),
        .I1(\sw_reg_reg[15]_2 [1]),
        .I2(\sw_reg_reg[15]_2 [0]),
        .I3(Q[15]),
        .I4(\real_result_reg[15]_i_2_n_5 ),
        .O(\real_result_reg[15] [15]));
  LUT4 #(
    .INIT(16'h04F7)) 
    \real_result[15]_i_3 
       (.I0(\real_result_reg[1] [4]),
        .I1(\sw_reg_reg[15]_2 [1]),
        .I2(\sw_reg_reg[15]_2 [0]),
        .I3(Q[15]),
        .O(\real_result[15]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h5355)) 
    \real_result[15]_i_4 
       (.I0(Q[14]),
        .I1(\real_result_reg[1] [4]),
        .I2(\sw_reg_reg[15]_2 [0]),
        .I3(\sw_reg_reg[15]_2 [1]),
        .O(\real_result[15]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h5355)) 
    \real_result[15]_i_5 
       (.I0(Q[13]),
        .I1(\real_result_reg[1] [4]),
        .I2(\sw_reg_reg[15]_2 [0]),
        .I3(\sw_reg_reg[15]_2 [1]),
        .O(\real_result[15]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hF0CCF0F0F0CCAAAA)) 
    \real_result[1]_i_1 
       (.I0(\real_result_reg[1] [1]),
        .I1(Q[1]),
        .I2(\real_result_reg[4]_i_2_n_7 ),
        .I3(Q[15]),
        .I4(\sw_reg_reg[14] ),
        .I5(\real_result_reg[1] [4]),
        .O(\real_result_reg[15] [1]));
  LUT6 #(
    .INIT(64'hF0CCF0F0F0CCAAAA)) 
    \real_result[2]_i_1 
       (.I0(\real_result_reg[1] [2]),
        .I1(Q[2]),
        .I2(\real_result_reg[4]_i_2_n_6 ),
        .I3(Q[15]),
        .I4(\sw_reg_reg[14] ),
        .I5(\real_result_reg[1] [4]),
        .O(\real_result_reg[15] [2]));
  LUT6 #(
    .INIT(64'hF0CCF0F0F0CCAAAA)) 
    \real_result[3]_i_1 
       (.I0(\real_result_reg[1] [3]),
        .I1(Q[3]),
        .I2(\real_result_reg[4]_i_2_n_5 ),
        .I3(Q[15]),
        .I4(\sw_reg_reg[14] ),
        .I5(\real_result_reg[1] [4]),
        .O(\real_result_reg[15] [3]));
  LUT6 #(
    .INIT(64'hF0CCF0F0F0CCAAAA)) 
    \real_result[4]_i_1 
       (.I0(\r1_reg_n_0_[4] ),
        .I1(Q[4]),
        .I2(\real_result_reg[4]_i_2_n_4 ),
        .I3(Q[15]),
        .I4(\sw_reg_reg[14] ),
        .I5(\real_result_reg[1] [4]),
        .O(\real_result_reg[15] [4]));
  LUT4 #(
    .INIT(16'h04F7)) 
    \real_result[4]_i_3 
       (.I0(\real_result_reg[1] [0]),
        .I1(\sw_reg_reg[15]_2 [1]),
        .I2(\sw_reg_reg[15]_2 [0]),
        .I3(Q[0]),
        .O(\real_result[4]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h4575)) 
    \real_result[4]_i_4 
       (.I0(Q[4]),
        .I1(\sw_reg_reg[15]_2 [0]),
        .I2(\sw_reg_reg[15]_2 [1]),
        .I3(\r1_reg_n_0_[4] ),
        .O(\real_result[4]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h4575)) 
    \real_result[4]_i_5 
       (.I0(Q[3]),
        .I1(\sw_reg_reg[15]_2 [0]),
        .I2(\sw_reg_reg[15]_2 [1]),
        .I3(\real_result_reg[1] [3]),
        .O(\real_result[4]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h4575)) 
    \real_result[4]_i_6 
       (.I0(Q[2]),
        .I1(\sw_reg_reg[15]_2 [0]),
        .I2(\sw_reg_reg[15]_2 [1]),
        .I3(\real_result_reg[1] [2]),
        .O(\real_result[4]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h4575)) 
    \real_result[4]_i_7 
       (.I0(Q[1]),
        .I1(\sw_reg_reg[15]_2 [0]),
        .I2(\sw_reg_reg[15]_2 [1]),
        .I3(\real_result_reg[1] [1]),
        .O(\real_result[4]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hF0CCF0F0F0CCAAAA)) 
    \real_result[5]_i_1 
       (.I0(\r1_reg_n_0_[5] ),
        .I1(Q[5]),
        .I2(\real_result_reg[8]_i_2_n_7 ),
        .I3(Q[15]),
        .I4(\sw_reg_reg[14] ),
        .I5(\real_result_reg[1] [4]),
        .O(\real_result_reg[15] [5]));
  LUT6 #(
    .INIT(64'hF0CCF0F0F0CCAAAA)) 
    \real_result[6]_i_1 
       (.I0(\r1_reg_n_0_[6] ),
        .I1(Q[6]),
        .I2(\real_result_reg[8]_i_2_n_6 ),
        .I3(Q[15]),
        .I4(\sw_reg_reg[14] ),
        .I5(\real_result_reg[1] [4]),
        .O(\real_result_reg[15] [6]));
  LUT6 #(
    .INIT(64'hF0CCF0F0F0CCAAAA)) 
    \real_result[7]_i_1 
       (.I0(\r1_reg_n_0_[7] ),
        .I1(Q[7]),
        .I2(\real_result_reg[8]_i_2_n_5 ),
        .I3(Q[15]),
        .I4(\sw_reg_reg[14] ),
        .I5(\real_result_reg[1] [4]),
        .O(\real_result_reg[15] [7]));
  LUT6 #(
    .INIT(64'hF0CCF0F0F0CCAAAA)) 
    \real_result[8]_i_1 
       (.I0(\r1_reg_n_0_[8] ),
        .I1(Q[8]),
        .I2(\real_result_reg[8]_i_2_n_4 ),
        .I3(Q[15]),
        .I4(\sw_reg_reg[14] ),
        .I5(\real_result_reg[1] [4]),
        .O(\real_result_reg[15] [8]));
  LUT4 #(
    .INIT(16'h4575)) 
    \real_result[8]_i_3 
       (.I0(Q[8]),
        .I1(\sw_reg_reg[15]_2 [0]),
        .I2(\sw_reg_reg[15]_2 [1]),
        .I3(\r1_reg_n_0_[8] ),
        .O(\real_result[8]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h4575)) 
    \real_result[8]_i_4 
       (.I0(Q[7]),
        .I1(\sw_reg_reg[15]_2 [0]),
        .I2(\sw_reg_reg[15]_2 [1]),
        .I3(\r1_reg_n_0_[7] ),
        .O(\real_result[8]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h4575)) 
    \real_result[8]_i_5 
       (.I0(Q[6]),
        .I1(\sw_reg_reg[15]_2 [0]),
        .I2(\sw_reg_reg[15]_2 [1]),
        .I3(\r1_reg_n_0_[6] ),
        .O(\real_result[8]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h4575)) 
    \real_result[8]_i_6 
       (.I0(Q[5]),
        .I1(\sw_reg_reg[15]_2 [0]),
        .I2(\sw_reg_reg[15]_2 [1]),
        .I3(\r1_reg_n_0_[5] ),
        .O(\real_result[8]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hF0CCF0F0F0CCAAAA)) 
    \real_result[9]_i_1 
       (.I0(\r1_reg_n_0_[9] ),
        .I1(Q[9]),
        .I2(\real_result_reg[12]_i_2_n_7 ),
        .I3(Q[15]),
        .I4(\sw_reg_reg[14] ),
        .I5(\real_result_reg[1] [4]),
        .O(\real_result_reg[15] [9]));
  CARRY4 \real_result_reg[12]_i_2 
       (.CI(\real_result_reg[8]_i_2_n_0 ),
        .CO({\real_result_reg[12]_i_2_n_0 ,\NLW_real_result_reg[12]_i_2_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\real_result_reg[12]_i_2_n_4 ,\real_result_reg[12]_i_2_n_5 ,\real_result_reg[12]_i_2_n_6 ,\real_result_reg[12]_i_2_n_7 }),
        .S({\real_result[12]_i_3_n_0 ,\real_result[12]_i_4_n_0 ,\real_result[12]_i_5_n_0 ,\real_result[12]_i_6_n_0 }));
  CARRY4 \real_result_reg[15]_i_2 
       (.CI(\real_result_reg[12]_i_2_n_0 ),
        .CO(\NLW_real_result_reg[15]_i_2_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_real_result_reg[15]_i_2_O_UNCONNECTED [3],\real_result_reg[15]_i_2_n_5 ,\real_result_reg[15]_i_2_n_6 ,\real_result_reg[15]_i_2_n_7 }),
        .S({1'b0,\real_result[15]_i_3_n_0 ,\real_result[15]_i_4_n_0 ,\real_result[15]_i_5_n_0 }));
  CARRY4 \real_result_reg[4]_i_2 
       (.CI(1'b0),
        .CO({\real_result_reg[4]_i_2_n_0 ,\NLW_real_result_reg[4]_i_2_CO_UNCONNECTED [2:0]}),
        .CYINIT(\real_result[4]_i_3_n_0 ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\real_result_reg[4]_i_2_n_4 ,\real_result_reg[4]_i_2_n_5 ,\real_result_reg[4]_i_2_n_6 ,\real_result_reg[4]_i_2_n_7 }),
        .S({\real_result[4]_i_4_n_0 ,\real_result[4]_i_5_n_0 ,\real_result[4]_i_6_n_0 ,\real_result[4]_i_7_n_0 }));
  CARRY4 \real_result_reg[8]_i_2 
       (.CI(\real_result_reg[4]_i_2_n_0 ),
        .CO({\real_result_reg[8]_i_2_n_0 ,\NLW_real_result_reg[8]_i_2_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\real_result_reg[8]_i_2_n_4 ,\real_result_reg[8]_i_2_n_5 ,\real_result_reg[8]_i_2_n_6 ,\real_result_reg[8]_i_2_n_7 }),
        .S({\real_result[8]_i_3_n_0 ,\real_result[8]_i_4_n_0 ,\real_result[8]_i_5_n_0 ,\real_result[8]_i_6_n_0 }));
endmodule

module clkdiv
   (clk10hz,
    CLK,
    clk);
  output clk10hz;
  output CLK;
  input clk;

  wire CLK;
  wire I;
  wire clear;
  wire clk;
  wire clk10hz;
  wire \cnt[0]_i_3_n_0 ;
  wire \cnt[0]_i_4_n_0 ;
  wire \cnt[0]_i_5_n_0 ;
  wire \cnt[0]_i_6_n_0 ;
  wire \cnt[0]_i_7_n_0 ;
  wire [22:0]cnt_reg;
  wire \cnt_reg[0]_i_2_n_0 ;
  wire \cnt_reg[0]_i_2_n_4 ;
  wire \cnt_reg[0]_i_2_n_5 ;
  wire \cnt_reg[0]_i_2_n_6 ;
  wire \cnt_reg[0]_i_2_n_7 ;
  wire \cnt_reg[12]_i_1_n_0 ;
  wire \cnt_reg[12]_i_1_n_4 ;
  wire \cnt_reg[12]_i_1_n_5 ;
  wire \cnt_reg[12]_i_1_n_6 ;
  wire \cnt_reg[12]_i_1_n_7 ;
  wire \cnt_reg[16]_i_1_n_0 ;
  wire \cnt_reg[16]_i_1_n_4 ;
  wire \cnt_reg[16]_i_1_n_5 ;
  wire \cnt_reg[16]_i_1_n_6 ;
  wire \cnt_reg[16]_i_1_n_7 ;
  wire \cnt_reg[20]_i_1_n_4 ;
  wire \cnt_reg[20]_i_1_n_5 ;
  wire \cnt_reg[20]_i_1_n_6 ;
  wire \cnt_reg[20]_i_1_n_7 ;
  wire \cnt_reg[4]_i_1_n_0 ;
  wire \cnt_reg[4]_i_1_n_4 ;
  wire \cnt_reg[4]_i_1_n_5 ;
  wire \cnt_reg[4]_i_1_n_6 ;
  wire \cnt_reg[4]_i_1_n_7 ;
  wire \cnt_reg[8]_i_1_n_0 ;
  wire \cnt_reg[8]_i_1_n_4 ;
  wire \cnt_reg[8]_i_1_n_5 ;
  wire \cnt_reg[8]_i_1_n_6 ;
  wire \cnt_reg[8]_i_1_n_7 ;
  wire \cnt_reg_n_0_[16] ;
  wire [2:0]\NLW_cnt_reg[0]_i_2_CO_UNCONNECTED ;
  wire [2:0]\NLW_cnt_reg[12]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_cnt_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:0]\NLW_cnt_reg[20]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_cnt_reg[4]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_cnt_reg[8]_i_1_CO_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  BUFG BUFG_INST2
       (.I(I),
        .O(clk10hz));
  (* box_type = "PRIMITIVE" *) 
  BUFG BUFG_INST3
       (.I(\cnt_reg_n_0_[16] ),
        .O(CLK));
  LUT4 #(
    .INIT(16'h4000)) 
    \cnt[0]_i_1 
       (.I0(\cnt[0]_i_3_n_0 ),
        .I1(\cnt[0]_i_4_n_0 ),
        .I2(\cnt[0]_i_5_n_0 ),
        .I3(\cnt[0]_i_6_n_0 ),
        .O(clear));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFBF)) 
    \cnt[0]_i_3 
       (.I0(cnt_reg[7]),
        .I1(cnt_reg[20]),
        .I2(I),
        .I3(cnt_reg[13]),
        .I4(cnt_reg[8]),
        .I5(cnt_reg[11]),
        .O(\cnt[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \cnt[0]_i_4 
       (.I0(cnt_reg[22]),
        .I1(cnt_reg[18]),
        .I2(cnt_reg[21]),
        .I3(cnt_reg[14]),
        .I4(\cnt_reg_n_0_[16] ),
        .I5(cnt_reg[17]),
        .O(\cnt[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \cnt[0]_i_5 
       (.I0(cnt_reg[12]),
        .I1(cnt_reg[15]),
        .I2(cnt_reg[19]),
        .I3(cnt_reg[10]),
        .I4(cnt_reg[6]),
        .I5(cnt_reg[9]),
        .O(\cnt[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \cnt[0]_i_6 
       (.I0(cnt_reg[3]),
        .I1(cnt_reg[4]),
        .I2(cnt_reg[5]),
        .I3(cnt_reg[2]),
        .I4(cnt_reg[0]),
        .I5(cnt_reg[1]),
        .O(\cnt[0]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt[0]_i_7 
       (.I0(cnt_reg[0]),
        .O(\cnt[0]_i_7_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_7 ),
        .Q(cnt_reg[0]),
        .R(clear));
  CARRY4 \cnt_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\cnt_reg[0]_i_2_n_0 ,\NLW_cnt_reg[0]_i_2_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_reg[0]_i_2_n_4 ,\cnt_reg[0]_i_2_n_5 ,\cnt_reg[0]_i_2_n_6 ,\cnt_reg[0]_i_2_n_7 }),
        .S({cnt_reg[3:1],\cnt[0]_i_7_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_5 ),
        .Q(cnt_reg[10]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_4 ),
        .Q(cnt_reg[11]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_7 ),
        .Q(cnt_reg[12]),
        .R(clear));
  CARRY4 \cnt_reg[12]_i_1 
       (.CI(\cnt_reg[8]_i_1_n_0 ),
        .CO({\cnt_reg[12]_i_1_n_0 ,\NLW_cnt_reg[12]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[12]_i_1_n_4 ,\cnt_reg[12]_i_1_n_5 ,\cnt_reg[12]_i_1_n_6 ,\cnt_reg[12]_i_1_n_7 }),
        .S(cnt_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_6 ),
        .Q(cnt_reg[13]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_5 ),
        .Q(cnt_reg[14]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_4 ),
        .Q(cnt_reg[15]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[16] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[16]_i_1_n_7 ),
        .Q(\cnt_reg_n_0_[16] ),
        .R(clear));
  CARRY4 \cnt_reg[16]_i_1 
       (.CI(\cnt_reg[12]_i_1_n_0 ),
        .CO({\cnt_reg[16]_i_1_n_0 ,\NLW_cnt_reg[16]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[16]_i_1_n_4 ,\cnt_reg[16]_i_1_n_5 ,\cnt_reg[16]_i_1_n_6 ,\cnt_reg[16]_i_1_n_7 }),
        .S({cnt_reg[19:17],\cnt_reg_n_0_[16] }));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[17] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[16]_i_1_n_6 ),
        .Q(cnt_reg[17]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[18] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[16]_i_1_n_5 ),
        .Q(cnt_reg[18]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[19] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[16]_i_1_n_4 ),
        .Q(cnt_reg[19]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_6 ),
        .Q(cnt_reg[1]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[20] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[20]_i_1_n_7 ),
        .Q(cnt_reg[20]),
        .R(clear));
  CARRY4 \cnt_reg[20]_i_1 
       (.CI(\cnt_reg[16]_i_1_n_0 ),
        .CO(\NLW_cnt_reg[20]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[20]_i_1_n_4 ,\cnt_reg[20]_i_1_n_5 ,\cnt_reg[20]_i_1_n_6 ,\cnt_reg[20]_i_1_n_7 }),
        .S({I,cnt_reg[22:20]}));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[21] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[20]_i_1_n_6 ),
        .Q(cnt_reg[21]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[22] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[20]_i_1_n_5 ),
        .Q(cnt_reg[22]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[23] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[20]_i_1_n_4 ),
        .Q(I),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_5 ),
        .Q(cnt_reg[2]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_4 ),
        .Q(cnt_reg[3]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_7 ),
        .Q(cnt_reg[4]),
        .R(clear));
  CARRY4 \cnt_reg[4]_i_1 
       (.CI(\cnt_reg[0]_i_2_n_0 ),
        .CO({\cnt_reg[4]_i_1_n_0 ,\NLW_cnt_reg[4]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[4]_i_1_n_4 ,\cnt_reg[4]_i_1_n_5 ,\cnt_reg[4]_i_1_n_6 ,\cnt_reg[4]_i_1_n_7 }),
        .S(cnt_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_6 ),
        .Q(cnt_reg[5]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_5 ),
        .Q(cnt_reg[6]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_4 ),
        .Q(cnt_reg[7]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_7 ),
        .Q(cnt_reg[8]),
        .R(clear));
  CARRY4 \cnt_reg[8]_i_1 
       (.CI(\cnt_reg[4]_i_1_n_0 ),
        .CO({\cnt_reg[8]_i_1_n_0 ,\NLW_cnt_reg[8]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[8]_i_1_n_4 ,\cnt_reg[8]_i_1_n_5 ,\cnt_reg[8]_i_1_n_6 ,\cnt_reg[8]_i_1_n_7 }),
        .S(cnt_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_6 ),
        .Q(cnt_reg[9]),
        .R(clear));
endmodule

module disp7
   (an_OBUF,
    seg_OBUF,
    Q,
    \real_result_reg[11] ,
    \real_result_reg[11]_0 ,
    \real_result_reg[11]_1 ,
    \real_result_reg[11]_2 ,
    \real_result_reg[11]_3 ,
    \real_result_reg[3] ,
    \real_result_reg[3]_0 ,
    \real_result_reg[3]_1 ,
    \real_result_reg[3]_2 ,
    \real_result_reg[3]_3 ,
    \real_result_reg[11]_4 ,
    \real_result_reg[11]_5 ,
    \real_result_reg[3]_4 ,
    \real_result_reg[3]_5 ,
    CLK);
  output [3:0]an_OBUF;
  output [6:0]seg_OBUF;
  input [7:0]Q;
  input \real_result_reg[11] ;
  input \real_result_reg[11]_0 ;
  input \real_result_reg[11]_1 ;
  input \real_result_reg[11]_2 ;
  input \real_result_reg[11]_3 ;
  input \real_result_reg[3] ;
  input \real_result_reg[3]_0 ;
  input \real_result_reg[3]_1 ;
  input \real_result_reg[3]_2 ;
  input \real_result_reg[3]_3 ;
  input \real_result_reg[11]_4 ;
  input \real_result_reg[11]_5 ;
  input \real_result_reg[3]_4 ;
  input \real_result_reg[3]_5 ;
  input CLK;

  wire CLK;
  wire [7:0]Q;
  wire [3:0]an_OBUF;
  wire [1:0]ndisp;
  wire [1:0]plusOp;
  wire \real_result_reg[11] ;
  wire \real_result_reg[11]_0 ;
  wire \real_result_reg[11]_1 ;
  wire \real_result_reg[11]_2 ;
  wire \real_result_reg[11]_3 ;
  wire \real_result_reg[11]_4 ;
  wire \real_result_reg[11]_5 ;
  wire \real_result_reg[3] ;
  wire \real_result_reg[3]_0 ;
  wire \real_result_reg[3]_1 ;
  wire \real_result_reg[3]_2 ;
  wire \real_result_reg[3]_3 ;
  wire \real_result_reg[3]_4 ;
  wire \real_result_reg[3]_5 ;
  wire [6:0]seg_OBUF;
  wire \seg_OBUF[0]_inst_i_2_n_0 ;
  wire \seg_OBUF[0]_inst_i_3_n_0 ;
  wire \seg_OBUF[1]_inst_i_2_n_0 ;
  wire \seg_OBUF[1]_inst_i_3_n_0 ;
  wire \seg_OBUF[2]_inst_i_2_n_0 ;
  wire \seg_OBUF[2]_inst_i_3_n_0 ;
  wire \seg_OBUF[3]_inst_i_2_n_0 ;
  wire \seg_OBUF[3]_inst_i_3_n_0 ;
  wire \seg_OBUF[4]_inst_i_2_n_0 ;
  wire \seg_OBUF[4]_inst_i_3_n_0 ;
  wire \seg_OBUF[5]_inst_i_2_n_0 ;
  wire \seg_OBUF[5]_inst_i_3_n_0 ;
  wire \seg_OBUF[6]_inst_i_2_n_0 ;
  wire \seg_OBUF[6]_inst_i_3_n_0 ;

  LUT2 #(
    .INIT(4'hE)) 
    \an_OBUF[0]_inst_i_1 
       (.I0(ndisp[0]),
        .I1(ndisp[1]),
        .O(an_OBUF[0]));
  LUT2 #(
    .INIT(4'hB)) 
    \an_OBUF[1]_inst_i_1 
       (.I0(ndisp[1]),
        .I1(ndisp[0]),
        .O(an_OBUF[1]));
  LUT2 #(
    .INIT(4'hB)) 
    \an_OBUF[2]_inst_i_1 
       (.I0(ndisp[0]),
        .I1(ndisp[1]),
        .O(an_OBUF[2]));
  LUT2 #(
    .INIT(4'h7)) 
    \an_OBUF[3]_inst_i_1 
       (.I0(ndisp[0]),
        .I1(ndisp[1]),
        .O(an_OBUF[3]));
  LUT1 #(
    .INIT(2'h1)) 
    \ndisp[0]_i_1 
       (.I0(ndisp[0]),
        .O(plusOp[0]));
  LUT2 #(
    .INIT(4'h6)) 
    \ndisp[1]_i_1 
       (.I0(ndisp[1]),
        .I1(ndisp[0]),
        .O(plusOp[1]));
  FDRE #(
    .INIT(1'b0)) 
    \ndisp_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(plusOp[0]),
        .Q(ndisp[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ndisp_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(plusOp[1]),
        .Q(ndisp[1]),
        .R(1'b0));
  MUXF7 \seg_OBUF[0]_inst_i_1 
       (.I0(\seg_OBUF[0]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[0]_inst_i_3_n_0 ),
        .O(seg_OBUF[0]),
        .S(ndisp[1]));
  LUT6 #(
    .INIT(64'h2094FFFF20940000)) 
    \seg_OBUF[0]_inst_i_2 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(ndisp[0]),
        .I5(\real_result_reg[3]_1 ),
        .O(\seg_OBUF[0]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h2094FFFF20940000)) 
    \seg_OBUF[0]_inst_i_3 
       (.I0(Q[7]),
        .I1(Q[6]),
        .I2(Q[4]),
        .I3(Q[5]),
        .I4(ndisp[0]),
        .I5(\real_result_reg[11]_1 ),
        .O(\seg_OBUF[0]_inst_i_3_n_0 ));
  MUXF7 \seg_OBUF[1]_inst_i_1 
       (.I0(\seg_OBUF[1]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[1]_inst_i_3_n_0 ),
        .O(seg_OBUF[1]),
        .S(ndisp[1]));
  LUT6 #(
    .INIT(64'h9E80FFFF9E800000)) 
    \seg_OBUF[1]_inst_i_2 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(ndisp[0]),
        .I5(\real_result_reg[3]_2 ),
        .O(\seg_OBUF[1]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB680FFFFB6800000)) 
    \seg_OBUF[1]_inst_i_3 
       (.I0(Q[7]),
        .I1(Q[4]),
        .I2(Q[5]),
        .I3(Q[6]),
        .I4(ndisp[0]),
        .I5(\real_result_reg[11]_2 ),
        .O(\seg_OBUF[1]_inst_i_3_n_0 ));
  MUXF7 \seg_OBUF[2]_inst_i_1 
       (.I0(\seg_OBUF[2]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[2]_inst_i_3_n_0 ),
        .O(seg_OBUF[2]),
        .S(ndisp[1]));
  LUT6 #(
    .INIT(64'hA210FFFFA2100000)) 
    \seg_OBUF[2]_inst_i_2 
       (.I0(Q[3]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(ndisp[0]),
        .I5(\real_result_reg[3]_5 ),
        .O(\seg_OBUF[2]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hA210FFFFA2100000)) 
    \seg_OBUF[2]_inst_i_3 
       (.I0(Q[7]),
        .I1(Q[4]),
        .I2(Q[5]),
        .I3(Q[6]),
        .I4(ndisp[0]),
        .I5(\real_result_reg[11]_4 ),
        .O(\seg_OBUF[2]_inst_i_3_n_0 ));
  MUXF7 \seg_OBUF[3]_inst_i_1 
       (.I0(\seg_OBUF[3]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[3]_inst_i_3_n_0 ),
        .O(seg_OBUF[3]),
        .S(ndisp[1]));
  LUT6 #(
    .INIT(64'hC214FFFFC2140000)) 
    \seg_OBUF[3]_inst_i_2 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(ndisp[0]),
        .I5(\real_result_reg[3] ),
        .O(\seg_OBUF[3]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hC214FFFFC2140000)) 
    \seg_OBUF[3]_inst_i_3 
       (.I0(Q[7]),
        .I1(Q[6]),
        .I2(Q[4]),
        .I3(Q[5]),
        .I4(ndisp[0]),
        .I5(\real_result_reg[11] ),
        .O(\seg_OBUF[3]_inst_i_3_n_0 ));
  MUXF7 \seg_OBUF[4]_inst_i_1 
       (.I0(\seg_OBUF[4]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[4]_inst_i_3_n_0 ),
        .O(seg_OBUF[4]),
        .S(ndisp[1]));
  LUT6 #(
    .INIT(64'h5710FFFF57100000)) 
    \seg_OBUF[4]_inst_i_2 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(ndisp[0]),
        .I5(\real_result_reg[3]_4 ),
        .O(\seg_OBUF[4]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5710FFFF57100000)) 
    \seg_OBUF[4]_inst_i_3 
       (.I0(Q[7]),
        .I1(Q[5]),
        .I2(Q[6]),
        .I3(Q[4]),
        .I4(ndisp[0]),
        .I5(\real_result_reg[11]_5 ),
        .O(\seg_OBUF[4]_inst_i_3_n_0 ));
  MUXF7 \seg_OBUF[5]_inst_i_1 
       (.I0(\seg_OBUF[5]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[5]_inst_i_3_n_0 ),
        .O(seg_OBUF[5]),
        .S(ndisp[1]));
  LUT6 #(
    .INIT(64'h5190FFFF51900000)) 
    \seg_OBUF[5]_inst_i_2 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(ndisp[0]),
        .I5(\real_result_reg[3]_3 ),
        .O(\seg_OBUF[5]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5910FFFF59100000)) 
    \seg_OBUF[5]_inst_i_3 
       (.I0(Q[7]),
        .I1(Q[6]),
        .I2(Q[5]),
        .I3(Q[4]),
        .I4(ndisp[0]),
        .I5(\real_result_reg[11]_3 ),
        .O(\seg_OBUF[5]_inst_i_3_n_0 ));
  MUXF7 \seg_OBUF[6]_inst_i_1 
       (.I0(\seg_OBUF[6]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[6]_inst_i_3_n_0 ),
        .O(seg_OBUF[6]),
        .S(ndisp[1]));
  LUT6 #(
    .INIT(64'h4025FFFF40250000)) 
    \seg_OBUF[6]_inst_i_2 
       (.I0(Q[3]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(ndisp[0]),
        .I5(\real_result_reg[3]_0 ),
        .O(\seg_OBUF[6]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h4025FFFF40250000)) 
    \seg_OBUF[6]_inst_i_3 
       (.I0(Q[7]),
        .I1(Q[4]),
        .I2(Q[6]),
        .I3(Q[5]),
        .I4(ndisp[0]),
        .I5(\real_result_reg[11]_0 ),
        .O(\seg_OBUF[6]_inst_i_3_n_0 ));
endmodule

(* ECO_CHECKSUM = "24a8089b" *) 
(* NotValidForBitStream *)
module fpga_basicIO
   (clk,
    btnC,
    btnU,
    btnL,
    btnR,
    btnD,
    sw,
    led,
    an,
    seg,
    dp);
  input clk;
  input btnC;
  input btnU;
  input btnL;
  input btnR;
  input btnD;
  input [15:0]sw;
  output [15:0]led;
  output [3:0]an;
  output [6:0]seg;
  output dp;

  wire [3:0]an;
  wire [3:0]an_OBUF;
  wire btnC;
  wire btnC_IBUF;
  wire btnCreg;
  wire btnD;
  wire btnD_IBUF;
  wire btnL;
  wire btnL_IBUF;
  wire btnR;
  wire btnR_IBUF;
  wire btnU;
  wire btnU_IBUF;
  wire clk;
  wire clk10hz;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire dclk;
  wire dp;
  wire dp_OBUF;
  wire inst_alu_n_0;
  wire inst_alu_n_1;
  wire inst_alu_n_10;
  wire inst_alu_n_11;
  wire inst_alu_n_12;
  wire inst_alu_n_13;
  wire inst_alu_n_14;
  wire inst_alu_n_15;
  wire inst_alu_n_2;
  wire inst_alu_n_3;
  wire inst_alu_n_4;
  wire inst_alu_n_5;
  wire inst_alu_n_6;
  wire inst_alu_n_7;
  wire inst_alu_n_8;
  wire inst_alu_n_9;
  wire [15:0]led;
  wire [15:0]led_OBUF;
  wire [11:0]minusOp;
  wire [3:0]oper;
  wire p_0_in;
  wire \real_result[11]_i_2_n_0 ;
  wire \real_result_reg_n_0_[0] ;
  wire \real_result_reg_n_0_[10] ;
  wire \real_result_reg_n_0_[11] ;
  wire \real_result_reg_n_0_[12] ;
  wire \real_result_reg_n_0_[13] ;
  wire \real_result_reg_n_0_[14] ;
  wire \real_result_reg_n_0_[15] ;
  wire \real_result_reg_n_0_[1] ;
  wire \real_result_reg_n_0_[2] ;
  wire \real_result_reg_n_0_[3] ;
  wire \real_result_reg_n_0_[8] ;
  wire \real_result_reg_n_0_[9] ;
  wire \real_value[0]_i_1_n_0 ;
  wire \real_value[0]_i_3_n_0 ;
  wire \real_value[0]_i_4_n_0 ;
  wire \real_value[0]_i_5_n_0 ;
  wire \real_value[10]_i_1_n_0 ;
  wire \real_value[11]_i_1_n_0 ;
  wire \real_value[12]_i_1_n_0 ;
  wire \real_value[1]_i_1_n_0 ;
  wire \real_value[2]_i_1_n_0 ;
  wire \real_value[3]_i_1_n_0 ;
  wire \real_value[4]_i_1_n_0 ;
  wire \real_value[4]_i_3_n_0 ;
  wire \real_value[4]_i_4_n_0 ;
  wire \real_value[4]_i_5_n_0 ;
  wire \real_value[4]_i_6_n_0 ;
  wire \real_value[5]_i_1_n_0 ;
  wire \real_value[6]_i_1_n_0 ;
  wire \real_value[7]_i_1_n_0 ;
  wire \real_value[8]_i_1_n_0 ;
  wire \real_value[8]_i_3_n_0 ;
  wire \real_value[8]_i_4_n_0 ;
  wire \real_value[8]_i_5_n_0 ;
  wire \real_value[8]_i_6_n_0 ;
  wire \real_value[9]_i_1_n_0 ;
  wire \real_value_reg[0]_i_2_n_0 ;
  wire \real_value_reg[4]_i_2_n_0 ;
  wire [6:0]seg;
  wire [6:0]seg_OBUF;
  wire \seg_OBUF[0]_inst_i_4_n_0 ;
  wire \seg_OBUF[0]_inst_i_5_n_0 ;
  wire \seg_OBUF[1]_inst_i_4_n_0 ;
  wire \seg_OBUF[1]_inst_i_5_n_0 ;
  wire \seg_OBUF[2]_inst_i_4_n_0 ;
  wire \seg_OBUF[2]_inst_i_5_n_0 ;
  wire \seg_OBUF[3]_inst_i_4_n_0 ;
  wire \seg_OBUF[3]_inst_i_5_n_0 ;
  wire \seg_OBUF[4]_inst_i_4_n_0 ;
  wire \seg_OBUF[4]_inst_i_5_n_0 ;
  wire \seg_OBUF[5]_inst_i_4_n_0 ;
  wire \seg_OBUF[5]_inst_i_5_n_0 ;
  wire \seg_OBUF[6]_inst_i_4_n_0 ;
  wire \seg_OBUF[6]_inst_i_5_n_0 ;
  wire [3:0]sel0;
  wire [15:0]sw;
  wire [15:0]sw_IBUF;
  wire [12:0]value;
  wire [2:0]\NLW_real_value_reg[0]_i_2_CO_UNCONNECTED ;
  wire [2:0]\NLW_real_value_reg[4]_i_2_CO_UNCONNECTED ;
  wire [3:0]\NLW_real_value_reg[8]_i_2_CO_UNCONNECTED ;

  OBUF \an_OBUF[0]_inst 
       (.I(an_OBUF[0]),
        .O(an[0]));
  OBUF \an_OBUF[1]_inst 
       (.I(an_OBUF[1]),
        .O(an[1]));
  OBUF \an_OBUF[2]_inst 
       (.I(an_OBUF[2]),
        .O(an[2]));
  OBUF \an_OBUF[3]_inst 
       (.I(an_OBUF[3]),
        .O(an[3]));
  IBUF btnC_IBUF_inst
       (.I(btnC),
        .O(btnC_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    btnCreg_reg
       (.C(clk10hz),
        .CE(1'b1),
        .D(btnC_IBUF),
        .Q(btnCreg),
        .R(1'b0));
  IBUF btnD_IBUF_inst
       (.I(btnD),
        .O(btnD_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    btnDreg_reg
       (.C(clk10hz),
        .CE(1'b1),
        .D(btnD_IBUF),
        .Q(oper[3]),
        .R(1'b0));
  IBUF btnL_IBUF_inst
       (.I(btnL),
        .O(btnL_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    btnLreg_reg
       (.C(clk10hz),
        .CE(1'b1),
        .D(btnL_IBUF),
        .Q(oper[0]),
        .R(1'b0));
  IBUF btnR_IBUF_inst
       (.I(btnR),
        .O(btnR_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    btnRreg_reg
       (.C(clk10hz),
        .CE(1'b1),
        .D(btnR_IBUF),
        .Q(oper[1]),
        .R(1'b0));
  IBUF btnU_IBUF_inst
       (.I(btnU),
        .O(btnU_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    btnUreg_reg
       (.C(clk10hz),
        .CE(1'b1),
        .D(btnU_IBUF),
        .Q(oper[2]),
        .R(1'b0));
  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  OBUF dp_OBUF_inst
       (.I(dp_OBUF),
        .O(dp));
  alu inst_alu
       (.A(value),
        .CLK(clk_IBUF_BUFG),
        .D({inst_alu_n_0,inst_alu_n_1,inst_alu_n_2,inst_alu_n_3,inst_alu_n_4,inst_alu_n_5,inst_alu_n_6,inst_alu_n_7,inst_alu_n_8,inst_alu_n_9,inst_alu_n_10,inst_alu_n_11,inst_alu_n_12,inst_alu_n_13,inst_alu_n_14,inst_alu_n_15}),
        .Q(led_OBUF[15:12]),
        .btnCreg(btnCreg),
        .oper(oper),
        .p_0_in(p_0_in),
        .\sw_reg_reg[14] (\real_result[11]_i_2_n_0 ));
  clkdiv inst_clkdiv
       (.CLK(dclk),
        .clk(clk_IBUF_BUFG),
        .clk10hz(clk10hz));
  disp7 inst_disp7
       (.CLK(dclk),
        .Q({\real_result_reg_n_0_[15] ,\real_result_reg_n_0_[14] ,\real_result_reg_n_0_[13] ,\real_result_reg_n_0_[12] ,sel0}),
        .an_OBUF(an_OBUF),
        .\real_result_reg[11] (\seg_OBUF[3]_inst_i_5_n_0 ),
        .\real_result_reg[11]_0 (\seg_OBUF[6]_inst_i_5_n_0 ),
        .\real_result_reg[11]_1 (\seg_OBUF[0]_inst_i_5_n_0 ),
        .\real_result_reg[11]_2 (\seg_OBUF[1]_inst_i_5_n_0 ),
        .\real_result_reg[11]_3 (\seg_OBUF[5]_inst_i_5_n_0 ),
        .\real_result_reg[11]_4 (\seg_OBUF[2]_inst_i_5_n_0 ),
        .\real_result_reg[11]_5 (\seg_OBUF[4]_inst_i_5_n_0 ),
        .\real_result_reg[3] (\seg_OBUF[3]_inst_i_4_n_0 ),
        .\real_result_reg[3]_0 (\seg_OBUF[6]_inst_i_4_n_0 ),
        .\real_result_reg[3]_1 (\seg_OBUF[0]_inst_i_4_n_0 ),
        .\real_result_reg[3]_2 (\seg_OBUF[1]_inst_i_4_n_0 ),
        .\real_result_reg[3]_3 (\seg_OBUF[5]_inst_i_4_n_0 ),
        .\real_result_reg[3]_4 (\seg_OBUF[4]_inst_i_4_n_0 ),
        .\real_result_reg[3]_5 (\seg_OBUF[2]_inst_i_4_n_0 ),
        .seg_OBUF(seg_OBUF));
  OBUF \led_OBUF[0]_inst 
       (.I(led_OBUF[0]),
        .O(led[0]));
  OBUF \led_OBUF[10]_inst 
       (.I(led_OBUF[10]),
        .O(led[10]));
  OBUF \led_OBUF[11]_inst 
       (.I(led_OBUF[11]),
        .O(led[11]));
  OBUF \led_OBUF[12]_inst 
       (.I(led_OBUF[12]),
        .O(led[12]));
  OBUF \led_OBUF[13]_inst 
       (.I(led_OBUF[13]),
        .O(led[13]));
  OBUF \led_OBUF[14]_inst 
       (.I(led_OBUF[14]),
        .O(led[14]));
  OBUF \led_OBUF[15]_inst 
       (.I(led_OBUF[15]),
        .O(led[15]));
  OBUF \led_OBUF[1]_inst 
       (.I(led_OBUF[1]),
        .O(led[1]));
  OBUF \led_OBUF[2]_inst 
       (.I(led_OBUF[2]),
        .O(led[2]));
  OBUF \led_OBUF[3]_inst 
       (.I(led_OBUF[3]),
        .O(led[3]));
  OBUF \led_OBUF[4]_inst 
       (.I(led_OBUF[4]),
        .O(led[4]));
  OBUF \led_OBUF[5]_inst 
       (.I(led_OBUF[5]),
        .O(led[5]));
  OBUF \led_OBUF[6]_inst 
       (.I(led_OBUF[6]),
        .O(led[6]));
  OBUF \led_OBUF[7]_inst 
       (.I(led_OBUF[7]),
        .O(led[7]));
  OBUF \led_OBUF[8]_inst 
       (.I(led_OBUF[8]),
        .O(led[8]));
  OBUF \led_OBUF[9]_inst 
       (.I(led_OBUF[9]),
        .O(led[9]));
  FDRE #(
    .INIT(1'b0)) 
    neg_reg
       (.C(clk10hz),
        .CE(1'b1),
        .D(p_0_in),
        .Q(dp_OBUF),
        .R(1'b0));
  LUT2 #(
    .INIT(4'hB)) 
    \real_result[11]_i_2 
       (.I0(led_OBUF[14]),
        .I1(led_OBUF[15]),
        .O(\real_result[11]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \real_result_reg[0] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(inst_alu_n_15),
        .Q(\real_result_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_result_reg[10] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(inst_alu_n_5),
        .Q(\real_result_reg_n_0_[10] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_result_reg[11] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(inst_alu_n_4),
        .Q(\real_result_reg_n_0_[11] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_result_reg[12] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(inst_alu_n_3),
        .Q(\real_result_reg_n_0_[12] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_result_reg[13] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(inst_alu_n_2),
        .Q(\real_result_reg_n_0_[13] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_result_reg[14] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(inst_alu_n_1),
        .Q(\real_result_reg_n_0_[14] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_result_reg[15] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(inst_alu_n_0),
        .Q(\real_result_reg_n_0_[15] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_result_reg[1] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(inst_alu_n_14),
        .Q(\real_result_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_result_reg[2] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(inst_alu_n_13),
        .Q(\real_result_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_result_reg[3] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(inst_alu_n_12),
        .Q(\real_result_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_result_reg[4] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(inst_alu_n_11),
        .Q(sel0[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_result_reg[5] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(inst_alu_n_10),
        .Q(sel0[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_result_reg[6] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(inst_alu_n_9),
        .Q(sel0[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_result_reg[7] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(inst_alu_n_8),
        .Q(sel0[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_result_reg[8] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(inst_alu_n_7),
        .Q(\real_result_reg_n_0_[8] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_result_reg[9] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(inst_alu_n_6),
        .Q(\real_result_reg_n_0_[9] ),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \real_value[0]_i_1 
       (.I0(sw_IBUF[0]),
        .I1(oper[2]),
        .I2(minusOp[0]),
        .O(\real_value[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \real_value[0]_i_3 
       (.I0(sw_IBUF[3]),
        .O(\real_value[0]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \real_value[0]_i_4 
       (.I0(sw_IBUF[2]),
        .O(\real_value[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \real_value[0]_i_5 
       (.I0(sw_IBUF[1]),
        .O(\real_value[0]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \real_value[10]_i_1 
       (.I0(sw_IBUF[10]),
        .I1(oper[2]),
        .I2(minusOp[10]),
        .O(\real_value[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \real_value[11]_i_1 
       (.I0(sw_IBUF[11]),
        .I1(oper[2]),
        .I2(minusOp[11]),
        .O(\real_value[11]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \real_value[12]_i_1 
       (.I0(oper[2]),
        .O(\real_value[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \real_value[1]_i_1 
       (.I0(sw_IBUF[1]),
        .I1(oper[2]),
        .I2(minusOp[1]),
        .O(\real_value[1]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \real_value[2]_i_1 
       (.I0(sw_IBUF[2]),
        .I1(oper[2]),
        .I2(minusOp[2]),
        .O(\real_value[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \real_value[3]_i_1 
       (.I0(sw_IBUF[3]),
        .I1(oper[2]),
        .I2(minusOp[3]),
        .O(\real_value[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \real_value[4]_i_1 
       (.I0(sw_IBUF[4]),
        .I1(oper[2]),
        .I2(minusOp[4]),
        .O(\real_value[4]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \real_value[4]_i_3 
       (.I0(sw_IBUF[7]),
        .O(\real_value[4]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \real_value[4]_i_4 
       (.I0(sw_IBUF[6]),
        .O(\real_value[4]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \real_value[4]_i_5 
       (.I0(sw_IBUF[5]),
        .O(\real_value[4]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \real_value[4]_i_6 
       (.I0(sw_IBUF[4]),
        .O(\real_value[4]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \real_value[5]_i_1 
       (.I0(sw_IBUF[5]),
        .I1(oper[2]),
        .I2(minusOp[5]),
        .O(\real_value[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \real_value[6]_i_1 
       (.I0(sw_IBUF[6]),
        .I1(oper[2]),
        .I2(minusOp[6]),
        .O(\real_value[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \real_value[7]_i_1 
       (.I0(sw_IBUF[7]),
        .I1(oper[2]),
        .I2(minusOp[7]),
        .O(\real_value[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \real_value[8]_i_1 
       (.I0(sw_IBUF[8]),
        .I1(oper[2]),
        .I2(minusOp[8]),
        .O(\real_value[8]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \real_value[8]_i_3 
       (.I0(sw_IBUF[11]),
        .O(\real_value[8]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \real_value[8]_i_4 
       (.I0(sw_IBUF[10]),
        .O(\real_value[8]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \real_value[8]_i_5 
       (.I0(sw_IBUF[9]),
        .O(\real_value[8]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \real_value[8]_i_6 
       (.I0(sw_IBUF[8]),
        .O(\real_value[8]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \real_value[9]_i_1 
       (.I0(sw_IBUF[9]),
        .I1(oper[2]),
        .I2(minusOp[9]),
        .O(\real_value[9]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \real_value_reg[0] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(\real_value[0]_i_1_n_0 ),
        .Q(value[0]),
        .R(1'b0));
  CARRY4 \real_value_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\real_value_reg[0]_i_2_n_0 ,\NLW_real_value_reg[0]_i_2_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O(minusOp[3:0]),
        .S({\real_value[0]_i_3_n_0 ,\real_value[0]_i_4_n_0 ,\real_value[0]_i_5_n_0 ,sw_IBUF[0]}));
  FDRE #(
    .INIT(1'b0)) 
    \real_value_reg[10] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(\real_value[10]_i_1_n_0 ),
        .Q(value[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_value_reg[11] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(\real_value[11]_i_1_n_0 ),
        .Q(value[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_value_reg[12] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(\real_value[12]_i_1_n_0 ),
        .Q(value[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_value_reg[1] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(\real_value[1]_i_1_n_0 ),
        .Q(value[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_value_reg[2] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(\real_value[2]_i_1_n_0 ),
        .Q(value[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_value_reg[3] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(\real_value[3]_i_1_n_0 ),
        .Q(value[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_value_reg[4] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(\real_value[4]_i_1_n_0 ),
        .Q(value[4]),
        .R(1'b0));
  CARRY4 \real_value_reg[4]_i_2 
       (.CI(\real_value_reg[0]_i_2_n_0 ),
        .CO({\real_value_reg[4]_i_2_n_0 ,\NLW_real_value_reg[4]_i_2_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(minusOp[7:4]),
        .S({\real_value[4]_i_3_n_0 ,\real_value[4]_i_4_n_0 ,\real_value[4]_i_5_n_0 ,\real_value[4]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \real_value_reg[5] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(\real_value[5]_i_1_n_0 ),
        .Q(value[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_value_reg[6] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(\real_value[6]_i_1_n_0 ),
        .Q(value[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_value_reg[7] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(\real_value[7]_i_1_n_0 ),
        .Q(value[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \real_value_reg[8] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(\real_value[8]_i_1_n_0 ),
        .Q(value[8]),
        .R(1'b0));
  CARRY4 \real_value_reg[8]_i_2 
       (.CI(\real_value_reg[4]_i_2_n_0 ),
        .CO(\NLW_real_value_reg[8]_i_2_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(minusOp[11:8]),
        .S({\real_value[8]_i_3_n_0 ,\real_value[8]_i_4_n_0 ,\real_value[8]_i_5_n_0 ,\real_value[8]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \real_value_reg[9] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(\real_value[9]_i_1_n_0 ),
        .Q(value[9]),
        .R(1'b0));
  OBUF \seg_OBUF[0]_inst 
       (.I(seg_OBUF[0]),
        .O(seg[0]));
  LUT4 #(
    .INIT(16'h2094)) 
    \seg_OBUF[0]_inst_i_4 
       (.I0(\real_result_reg_n_0_[3] ),
        .I1(\real_result_reg_n_0_[2] ),
        .I2(\real_result_reg_n_0_[0] ),
        .I3(\real_result_reg_n_0_[1] ),
        .O(\seg_OBUF[0]_inst_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h2094)) 
    \seg_OBUF[0]_inst_i_5 
       (.I0(\real_result_reg_n_0_[11] ),
        .I1(\real_result_reg_n_0_[10] ),
        .I2(\real_result_reg_n_0_[8] ),
        .I3(\real_result_reg_n_0_[9] ),
        .O(\seg_OBUF[0]_inst_i_5_n_0 ));
  OBUF \seg_OBUF[1]_inst 
       (.I(seg_OBUF[1]),
        .O(seg[1]));
  LUT4 #(
    .INIT(16'h9E80)) 
    \seg_OBUF[1]_inst_i_4 
       (.I0(\real_result_reg_n_0_[3] ),
        .I1(\real_result_reg_n_0_[1] ),
        .I2(\real_result_reg_n_0_[0] ),
        .I3(\real_result_reg_n_0_[2] ),
        .O(\seg_OBUF[1]_inst_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h9E80)) 
    \seg_OBUF[1]_inst_i_5 
       (.I0(\real_result_reg_n_0_[11] ),
        .I1(\real_result_reg_n_0_[9] ),
        .I2(\real_result_reg_n_0_[8] ),
        .I3(\real_result_reg_n_0_[10] ),
        .O(\seg_OBUF[1]_inst_i_5_n_0 ));
  OBUF \seg_OBUF[2]_inst 
       (.I(seg_OBUF[2]),
        .O(seg[2]));
  LUT4 #(
    .INIT(16'hA210)) 
    \seg_OBUF[2]_inst_i_4 
       (.I0(\real_result_reg_n_0_[3] ),
        .I1(\real_result_reg_n_0_[0] ),
        .I2(\real_result_reg_n_0_[1] ),
        .I3(\real_result_reg_n_0_[2] ),
        .O(\seg_OBUF[2]_inst_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hA210)) 
    \seg_OBUF[2]_inst_i_5 
       (.I0(\real_result_reg_n_0_[11] ),
        .I1(\real_result_reg_n_0_[8] ),
        .I2(\real_result_reg_n_0_[9] ),
        .I3(\real_result_reg_n_0_[10] ),
        .O(\seg_OBUF[2]_inst_i_5_n_0 ));
  OBUF \seg_OBUF[3]_inst 
       (.I(seg_OBUF[3]),
        .O(seg[3]));
  LUT4 #(
    .INIT(16'hC214)) 
    \seg_OBUF[3]_inst_i_4 
       (.I0(\real_result_reg_n_0_[3] ),
        .I1(\real_result_reg_n_0_[2] ),
        .I2(\real_result_reg_n_0_[0] ),
        .I3(\real_result_reg_n_0_[1] ),
        .O(\seg_OBUF[3]_inst_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hC214)) 
    \seg_OBUF[3]_inst_i_5 
       (.I0(\real_result_reg_n_0_[11] ),
        .I1(\real_result_reg_n_0_[10] ),
        .I2(\real_result_reg_n_0_[8] ),
        .I3(\real_result_reg_n_0_[9] ),
        .O(\seg_OBUF[3]_inst_i_5_n_0 ));
  OBUF \seg_OBUF[4]_inst 
       (.I(seg_OBUF[4]),
        .O(seg[4]));
  LUT4 #(
    .INIT(16'h5710)) 
    \seg_OBUF[4]_inst_i_4 
       (.I0(\real_result_reg_n_0_[3] ),
        .I1(\real_result_reg_n_0_[1] ),
        .I2(\real_result_reg_n_0_[2] ),
        .I3(\real_result_reg_n_0_[0] ),
        .O(\seg_OBUF[4]_inst_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h5710)) 
    \seg_OBUF[4]_inst_i_5 
       (.I0(\real_result_reg_n_0_[11] ),
        .I1(\real_result_reg_n_0_[9] ),
        .I2(\real_result_reg_n_0_[10] ),
        .I3(\real_result_reg_n_0_[8] ),
        .O(\seg_OBUF[4]_inst_i_5_n_0 ));
  OBUF \seg_OBUF[5]_inst 
       (.I(seg_OBUF[5]),
        .O(seg[5]));
  LUT4 #(
    .INIT(16'h5190)) 
    \seg_OBUF[5]_inst_i_4 
       (.I0(\real_result_reg_n_0_[3] ),
        .I1(\real_result_reg_n_0_[2] ),
        .I2(\real_result_reg_n_0_[0] ),
        .I3(\real_result_reg_n_0_[1] ),
        .O(\seg_OBUF[5]_inst_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h5190)) 
    \seg_OBUF[5]_inst_i_5 
       (.I0(\real_result_reg_n_0_[11] ),
        .I1(\real_result_reg_n_0_[10] ),
        .I2(\real_result_reg_n_0_[8] ),
        .I3(\real_result_reg_n_0_[9] ),
        .O(\seg_OBUF[5]_inst_i_5_n_0 ));
  OBUF \seg_OBUF[6]_inst 
       (.I(seg_OBUF[6]),
        .O(seg[6]));
  LUT4 #(
    .INIT(16'h4025)) 
    \seg_OBUF[6]_inst_i_4 
       (.I0(\real_result_reg_n_0_[3] ),
        .I1(\real_result_reg_n_0_[0] ),
        .I2(\real_result_reg_n_0_[2] ),
        .I3(\real_result_reg_n_0_[1] ),
        .O(\seg_OBUF[6]_inst_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h4025)) 
    \seg_OBUF[6]_inst_i_5 
       (.I0(\real_result_reg_n_0_[11] ),
        .I1(\real_result_reg_n_0_[8] ),
        .I2(\real_result_reg_n_0_[10] ),
        .I3(\real_result_reg_n_0_[9] ),
        .O(\seg_OBUF[6]_inst_i_5_n_0 ));
  IBUF \sw_IBUF[0]_inst 
       (.I(sw[0]),
        .O(sw_IBUF[0]));
  IBUF \sw_IBUF[10]_inst 
       (.I(sw[10]),
        .O(sw_IBUF[10]));
  IBUF \sw_IBUF[11]_inst 
       (.I(sw[11]),
        .O(sw_IBUF[11]));
  IBUF \sw_IBUF[12]_inst 
       (.I(sw[12]),
        .O(sw_IBUF[12]));
  IBUF \sw_IBUF[13]_inst 
       (.I(sw[13]),
        .O(sw_IBUF[13]));
  IBUF \sw_IBUF[14]_inst 
       (.I(sw[14]),
        .O(sw_IBUF[14]));
  IBUF \sw_IBUF[15]_inst 
       (.I(sw[15]),
        .O(sw_IBUF[15]));
  IBUF \sw_IBUF[1]_inst 
       (.I(sw[1]),
        .O(sw_IBUF[1]));
  IBUF \sw_IBUF[2]_inst 
       (.I(sw[2]),
        .O(sw_IBUF[2]));
  IBUF \sw_IBUF[3]_inst 
       (.I(sw[3]),
        .O(sw_IBUF[3]));
  IBUF \sw_IBUF[4]_inst 
       (.I(sw[4]),
        .O(sw_IBUF[4]));
  IBUF \sw_IBUF[5]_inst 
       (.I(sw[5]),
        .O(sw_IBUF[5]));
  IBUF \sw_IBUF[6]_inst 
       (.I(sw[6]),
        .O(sw_IBUF[6]));
  IBUF \sw_IBUF[7]_inst 
       (.I(sw[7]),
        .O(sw_IBUF[7]));
  IBUF \sw_IBUF[8]_inst 
       (.I(sw[8]),
        .O(sw_IBUF[8]));
  IBUF \sw_IBUF[9]_inst 
       (.I(sw[9]),
        .O(sw_IBUF[9]));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[0] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[0]),
        .Q(led_OBUF[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[10] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[10]),
        .Q(led_OBUF[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[11] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[11]),
        .Q(led_OBUF[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[12] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[12]),
        .Q(led_OBUF[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[13] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[13]),
        .Q(led_OBUF[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[14] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[14]),
        .Q(led_OBUF[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[15] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[15]),
        .Q(led_OBUF[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[1] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[1]),
        .Q(led_OBUF[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[2] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[2]),
        .Q(led_OBUF[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[3] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[3]),
        .Q(led_OBUF[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[4] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[4]),
        .Q(led_OBUF[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[5] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[5]),
        .Q(led_OBUF[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[6] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[6]),
        .Q(led_OBUF[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[7] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[7]),
        .Q(led_OBUF[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[8] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[8]),
        .Q(led_OBUF[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[9] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[9]),
        .Q(led_OBUF[9]),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

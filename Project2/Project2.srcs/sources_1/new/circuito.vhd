----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/09/2018 06:35:05 PM
-- Design Name: 
-- Module Name: alu - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity circuito is
    Port ( q00,q01,q10,q11: in std_logic_vector(9 downto 0);
           x,x0,y,y0: in std_logic_vector(7 downto 0);
           reset : in std_logic;
           clock : in std_logic;
           oper: in std_logic;
                      
           result : out std_logic_vector(19 downto 0));

end circuito;

architecture Behavioral of circuito is
component datapath is
    Port ( ctrl : in std_logic_vector(5 downto 0);
           instr : in std_logic_vector(1 downto 0);
           clk : in std_logic;
           rst,enable: in std_logic;
           x,x0,y,y0: in std_logic_vector(7 downto 0);
           q00,q01,q10,q11: in std_logic_vector(9 downto 0);
           p: out std_logic_vector(19 downto 0);
           done: out std_logic);
           
end component;
    
component control is
    Port ( oper : in std_logic;
           reset,clk : in std_logic;
           ctrl : out std_logic_vector(5 downto 0);
           instr: out std_logic_vector(1 downto 0);
           enable : out std_logic;
           done: in std_logic);
end component;
    
    signal ctrl : std_logic_vector(5 downto 0);
    signal instr : std_logic_vector(1 downto 0);
    signal rst : std_logic;
    signal enable : std_logic;
    signal done: std_logic;
   
begin

    inst_datapath: datapath port map(
        clk => clock,
        p => result,
        ctrl => ctrl,
        instr => instr,
        rst => reset,
        done=>  done,
        enable => enable,
        x0 => x0,
        x => x,

        y0 => y0,
        y => y,

        q00 => q00,
        q01 => q01,
        q10 => q10,
        q11 => q11
       
        );
        
    inst_control: control port map(
        oper =>oper,
        reset =>reset,
        clk => clock,
        ctrl => ctrl,
        enable => enable,
        instr => instr,
        done => done
        );
    

end Behavioral;

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/09/2018 06:35:05 PM
-- Design Name: 
-- Module Name: alu_datapath - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
--use IEEE.STD_LOGIC_SIGNED.ALL


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity datapath is
    Port ( ctrl : in std_logic_vector(5 downto 0);
           instr : in std_logic_vector(1 downto 0);
           clk : in std_logic;
           rst,enable: in std_logic;
           x,x0,y,y0: in std_logic_vector(7 downto 0);
           q00,q01,q10,q11: in std_logic_vector(9 downto 0);
           p: out std_logic_vector(19 downto 0);
           done: out std_logic);
           
end datapath;

architecture Behavioral of datapath is

signal q00_sg,q01_sg,q10_sg,q11_sg: signed(9 downto 0);
 
signal mux1, mux2, mux3, mux4, mux5, mux6, muxA1: signed (19 downto 0); 
signal muxA2, muxA3, muxA4, muxB1, muxB2, muxB3, muxB4, muxM1: signed (19 downto 0);
signal muxM2, muxM3, muxM4, muxM5, muxM6: signed (19 downto 0);

signal aluA, aluB: signed(19 downto 0); 
signal mult: signed(19 downto 0);

signal r1_5_10, r29, r38, r47, r6, rP: std_logic_vector (19 downto 0) := (others => '0');
signal r1_5_10sg, r29sg, r38sg, r47sg, r6sg, rPsg: signed (19 downto 0) := (others => '0');
signal result: std_logic_vector (19 downto 0) := (others => '0');

begin
q00_sg <= signed(q00);
q01_sg <= signed(q01);
q10_sg <= signed(q10);
q11_sg <= signed(q11);

r1_5_10sg <= signed(r1_5_10);
r29sg <= signed(r29);
r38sg <= signed(r38);
r47sg <= signed(r47);
r6sg <= signed(r6);
rPsg <= signed(rP);



muxA1 <= resize(q10_sg,20) when ctrl(5) ='0' else resize(q11_sg,20);
muxA2 <= resize(shift_right(r1_5_10sg,5),20) when ctrl(5) ='0' else resize(signed(y),20); 
mux1 <= muxA1 when ctrl(4) ='0' else muxA2;

muxA3 <= resize(q00_sg,20) when ctrl(5) ='0' else resize(q01_sg,20);
muxA4 <= resize(q01_sg,20) when ctrl(5) ='0' else resize(signed(y0),20);
mux2 <= muxA3 when ctrl(4) ='0' else muxA4;


muxB1 <= resize(signed(x),20) when ctrl(3) ='0' else resize(shift_right(r38sg,5),20);
muxB2 <= r47sg when ctrl(3) ='0' else r1_5_10sg  when enable = '1' else resize(shift_right(r1_5_10sg,5),20); 
muxB3 <= resize(signed(x0),20) when ctrl(3) ='0' else resize(q00_sg,20);
muxB4 <= r6sg when ctrl(3) ='0' else r6sg;
mux3 <= muxB1 when ctrl(2) ='0' else muxB2;
mux4 <= muxB3 when ctrl(2) ='0' else muxB4;

muxM1 <= r1_5_10sg when ctrl(1) ='0' else r29sg;
muxM2 <= r29sg when ctrl(1)='0' else r47sg;

mux5 <= muxM1 when ctrl(0) ='0' else r38sg;
mux6 <= muxM2 when ctrl(0) = '0' else r29sg;

aluA <= mux1 - mux2 when instr(1) = '0' else mux1 + mux2;
alub <= mux3 - mux4 when instr(0) = '0' else mux3 + mux4;
mult <= resize(mux5 * mux6, 20);



p <= std_logic_vector(shift_right(rPsg,5));


--Registers
process (clk)
    begin
        if clk'event and clk='1' then  
            if rst='1' then   
            rP <= X"00000";
            done <='0';
            elsif enable = '1' then 
            rP <= std_logic_vector(aluB);
            done <= '1';
            end if;
            
        end if;
    end process;
    

process (clk)
    begin
        if clk'event and clk='1' then  
            if rst='1' then   
            r1_5_10 <= X"00000";
            else if ctrl="000000" then
            r1_5_10 <= std_logic_vector(aluA);
            else if ctrl="001010" then
            r1_5_10 <= std_logic_vector(mult);
            else if ctrl="000001" then
            r1_5_10 <= std_logic_vector(mult);
            end if;
            end if;
            end if;
            end if;       
        end if;
    end process;


process (clk)
    begin
        if clk'event and clk='1' then  
            if rst='1' then   
            r29 <= X"00000";
            else if ctrl="000000" then
            r29 <= std_logic_vector(aluB);
            else if ctrl="110100" then
            r29 <= std_logic_vector(aluA);
            end if; 
            end if;       
            end if; 
        end if;
    end process;
    
process (clk)
    begin
        if clk'event and clk='1' then  
            if rst='1' then   
            r38 <= X"00000";
            else if ctrl="100000" then
            r38 <= std_logic_vector(mult);
            else if ctrl="110100" then
            r38 <= std_logic_vector(aluB);
            end if;
            end if;       
            end if; 
        end if;
    end process;
    
process (clk)
        begin
            if clk'event and clk='1' then  
                if rst='1' then   
                r47 <= X"00000";
                else if ctrl="100000" then
                r47 <= std_logic_vector(aluA);
                else if ctrl="010000" then
                r47 <= std_logic_vector(aluA);
                end if;
                end if;       
                end if; 
            end if;
        end process;
 

process (clk)
        begin
            if clk'event and clk='1' then  
                if rst='1' then   
                r6 <= X"00000";
                else if ctrl="001010" then
                r6 <= std_logic_vector(aluB);
                end if;       
                end if; 
            end if;
        end process;


end Behavioral;

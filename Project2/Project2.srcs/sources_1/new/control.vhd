----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/09/2018 06:35:05 PM
-- Design Name: 
-- Module Name: alu_control - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity control is
    Port ( oper : in std_logic;
           reset,clk : in std_logic;
           ctrl : out std_logic_vector(5 downto 0);
           instr: out std_logic_vector(1 downto 0);
           enable : out std_logic;
           done: in std_logic);
end control;

architecture Behavioral of control is


type fsm_states is ( s_initial, s_end, s_cycle1, s_cycle2, s_cycle3, s_cycle4, s_cycle5, s_cycle6, s_cycle7);
signal currstate, nextstate: fsm_states;

begin
state_reg: process (clk)
begin 
  if clk'event and clk = '1' then
    if reset = '1' then
      currstate <= s_initial ;
      
    else
      currstate <= nextstate ;
      
    end if ;
  end if ;
end process;

state_comb: process (currstate, oper, done)
begin  --  process
  nextstate <= currstate ;  
  -- by default, does not change the state.
  
  case currstate is
    when s_initial =>
        if oper='1' then
        nextstate <= s_cycle1 ;
        end if;
        ctrl<="111111";
        enable<='0';
        instr<="00";
      
    when s_cycle1 =>
        nextstate <= s_cycle2;
        ctrl<="000000";
        instr<="00";
        enable<='0';
            
    when s_cycle2 =>
        nextstate <= s_cycle3;
        ctrl<="100000";
        instr<="00";
        enable<='0';
        
    when s_cycle3 =>
        nextstate <= s_cycle4;
        ctrl<="001010";
        instr<="01";
        enable<='0';
        
    when s_cycle4 =>
        nextstate <= s_cycle5;
        ctrl<="010000";
        instr<="10";
        enable<='0';
        
    when s_cycle5 =>
        nextstate <= s_cycle6;
        ctrl<="110100";
        instr<="00";
        enable<='0';
        
    when s_cycle6 =>
        nextstate <= s_cycle7;
        ctrl<="000001";
        instr<="00";
        enable<='0';
        
    when s_cycle7 =>
        nextstate <= s_end;
        ctrl<="001100";
        instr<="01";
        enable<='1';
        
    when s_end =>
        if done='1' then
        nextstate <= s_initial ;
        end if;
        ctrl<="111111";
        enable<='0';
        instr<="00";

  end case;
end process;

end Behavioral;

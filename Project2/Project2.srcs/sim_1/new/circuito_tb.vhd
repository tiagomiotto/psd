----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10/19/2018 12:07:56 PM
-- Design Name: 
-- Module Name: alu_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity circuito_tb is
end circuito_tb;

architecture Behavioral of circuito_tb is
    COMPONENT circuito
    Port ( q00,q01,q10,q11: in std_logic_vector(9 downto 0);
           x,x0,y,y0: in std_logic_vector(7 downto 0);
           reset : in std_logic;
           clock : in std_logic;
           oper: in std_logic;
           result : out std_logic_vector(19 downto 0));
    END COMPONENT;
    
    
    --Inputs
    signal clock : std_logic := '0';
    signal rst : std_logic := '0';
    signal selection : std_logic := '0';
    signal x,x0,y,y0: std_logic_vector(7 downto 0) :=(others =>'0');
    signal q00,q01,q10,q11: std_logic_vector(9 downto 0):=(others =>'0');
    signal oper : std_logic :='0';
 
    --Outputs
    signal res: std_logic_vector(19 downto 0);
    --signal carry: std_logic;
 
    -- Clock period definitions
    constant clk_period : time := 10 ns;
           
BEGIN
    
    --UUT
       uut: circuito PORT MAP (
       clock => clock,
       reset => rst,
       result => res,
       oper =>  oper,
        x0 => x0,
        x => x,

        y0 => y0,
        y => y,

        q00 => q00,
        q01 => q01,
        q10 => q10,
        q11 => q11
     );
   -- Clock definition
     clock <= not clock after clk_period/2;
  
      -- Stimulus process
     stim_proc: process
     begin        
        -- hold reset state for 100 ns.
        --wait for 100 ns;    
  
        --wait for clk_period*10;
  
        -- insert stimulus here 
        -- note that input signals should never change at the positive edge of the clock
        rst <= '1' after 20 ns,
               '0' after 40 ns;
        x <= X"05" after 40 ns, X"00" after 1000ns;
        x0 <= X"02" after 40 ns;

        y <= X"0c" after 40 ns;
        y0 <= X"01" after 40 ns;

        q00 <= "1111111000" after 40 ns;
        q01 <= "0000001101" after 40 ns;
        q10 <= "0010101101" after 40 ns;
        q11 <= "0010110110" after 40 ns;
        oper <= '1' after 40ns, '0' after 50ns;      
        
        
                 
  
        wait;
     end process;
END;
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2.2 (lin64) Build 2348494 Mon Oct  1 18:25:39 MDT 2018
// Date        : Thu Nov 22 20:07:03 2018
// Host        : sebastiaobeirao running 64-bit Ubuntu 18.04.1 LTS
// Command     : write_verilog -mode funcsim -nolib -force -file
//               /home/sebastiaobeirao/Desktop/psd/Project2/Project2.sim/sim_1/synth/func/xsim/circuito_tb_func_synth.v
// Design      : circuito
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* NotValidForBitStream *)
module circuito
   (q00,
    q01,
    q10,
    q11,
    x,
    x0,
    x1,
    y,
    y0,
    y1,
    reset,
    clock,
    oper,
    result);
  input [9:0]q00;
  input [9:0]q01;
  input [9:0]q10;
  input [9:0]q11;
  input [7:0]x;
  input [7:0]x0;
  input [7:0]x1;
  input [7:0]y;
  input [7:0]y0;
  input [7:0]y1;
  input reset;
  input clock;
  input oper;
  output [15:0]result;

  wire [8:0]A;
  wire RESIZE0_in0;
  wire RESIZE1_in0;
  wire clock;
  wire clock_IBUF;
  wire clock_IBUF_BUFG;
  wire [3:3]ctrl;
  wire [5:0]ctrl__0;
  wire [15:0]data0;
  wire [15:0]data1;
  wire done;
  wire enable;
  wire inst_control_n_0;
  wire inst_control_n_1;
  wire inst_control_n_10;
  wire inst_control_n_115;
  wire inst_control_n_116;
  wire inst_control_n_15;
  wire inst_control_n_16;
  wire inst_control_n_17;
  wire inst_control_n_18;
  wire inst_control_n_19;
  wire inst_control_n_2;
  wire inst_control_n_20;
  wire inst_control_n_21;
  wire inst_control_n_22;
  wire inst_control_n_3;
  wire inst_control_n_35;
  wire inst_control_n_36;
  wire inst_control_n_37;
  wire inst_control_n_38;
  wire inst_control_n_39;
  wire inst_control_n_4;
  wire inst_control_n_40;
  wire inst_control_n_41;
  wire inst_control_n_42;
  wire inst_control_n_43;
  wire inst_control_n_44;
  wire inst_control_n_45;
  wire inst_control_n_46;
  wire inst_control_n_47;
  wire inst_control_n_48;
  wire inst_control_n_49;
  wire inst_control_n_5;
  wire inst_control_n_50;
  wire inst_control_n_51;
  wire inst_control_n_52;
  wire inst_control_n_53;
  wire inst_control_n_54;
  wire inst_control_n_55;
  wire inst_control_n_56;
  wire inst_control_n_57;
  wire inst_control_n_58;
  wire inst_control_n_59;
  wire inst_control_n_60;
  wire inst_control_n_61;
  wire inst_control_n_62;
  wire inst_control_n_63;
  wire inst_control_n_64;
  wire inst_control_n_65;
  wire inst_control_n_66;
  wire inst_control_n_67;
  wire inst_control_n_68;
  wire inst_control_n_69;
  wire inst_control_n_70;
  wire inst_control_n_71;
  wire inst_control_n_72;
  wire inst_control_n_73;
  wire inst_control_n_74;
  wire inst_control_n_75;
  wire inst_control_n_76;
  wire inst_control_n_77;
  wire inst_control_n_78;
  wire inst_control_n_79;
  wire inst_control_n_80;
  wire inst_control_n_81;
  wire inst_control_n_82;
  wire inst_datapath_n_100;
  wire inst_datapath_n_27;
  wire inst_datapath_n_28;
  wire inst_datapath_n_29;
  wire inst_datapath_n_30;
  wire inst_datapath_n_31;
  wire inst_datapath_n_32;
  wire inst_datapath_n_33;
  wire inst_datapath_n_34;
  wire inst_datapath_n_35;
  wire inst_datapath_n_36;
  wire inst_datapath_n_52;
  wire inst_datapath_n_67;
  wire inst_datapath_n_68;
  wire inst_datapath_n_85;
  wire inst_datapath_n_86;
  wire inst_datapath_n_87;
  wire inst_datapath_n_88;
  wire inst_datapath_n_89;
  wire inst_datapath_n_90;
  wire inst_datapath_n_91;
  wire inst_datapath_n_92;
  wire inst_datapath_n_93;
  wire inst_datapath_n_94;
  wire inst_datapath_n_95;
  wire inst_datapath_n_96;
  wire inst_datapath_n_97;
  wire inst_datapath_n_98;
  wire inst_datapath_n_99;
  wire [15:0]mux1;
  wire [15:15]muxB3;
  wire oper;
  wire oper_IBUF;
  wire [9:0]q00;
  wire [9:0]q00_IBUF;
  wire [9:0]q01;
  wire [9:0]q01_IBUF;
  wire [9:0]q10;
  wire [9:0]q10_IBUF;
  wire [9:0]q11;
  wire [9:0]q11_IBUF;
  wire [15:0]r1_5_10;
  wire r29;
  wire [14:1]r47;
  wire r47_0;
  wire [14:1]r6;
  wire reset;
  wire reset_IBUF;
  wire [15:0]result;
  wire [15:0]result_OBUF;
  wire [7:0]x;
  wire [7:0]x0;
  wire [7:0]x0_IBUF;
  wire [7:0]x_IBUF;
  wire [7:0]y;
  wire [7:0]y0;
  wire [7:0]y0_IBUF;
  wire [7:0]y_IBUF;

  BUFG clock_IBUF_BUFG_inst
       (.I(clock_IBUF),
        .O(clock_IBUF_BUFG));
  IBUF clock_IBUF_inst
       (.I(clock),
        .O(clock_IBUF));
  control inst_control
       (.A(A),
        .B(data0),
        .CLK(clock_IBUF_BUFG),
        .D({inst_control_n_35,inst_control_n_36,inst_control_n_37,inst_control_n_38,inst_control_n_39,inst_control_n_40,inst_control_n_41,inst_control_n_42,inst_control_n_43,inst_control_n_44,inst_control_n_45,inst_control_n_46,inst_control_n_47,inst_control_n_48,inst_control_n_49,inst_control_n_50}),
        .E(inst_control_n_4),
        .\FSM_sequential_currstate_reg[3]_0 ({inst_datapath_n_85,inst_datapath_n_86,inst_datapath_n_87,inst_datapath_n_88}),
        .\FSM_sequential_currstate_reg[3]_1 ({inst_datapath_n_89,inst_datapath_n_90,inst_datapath_n_91,inst_datapath_n_92}),
        .P(data1),
        .Q(RESIZE1_in0),
        .S(inst_datapath_n_68),
        .SR(reset_IBUF),
        .ctrl(ctrl),
        .done(done),
        .done_reg(inst_control_n_116),
        .mux1({mux1[15],mux1[9:0]}),
        .muxB3(muxB3),
        .oper_IBUF(oper_IBUF),
        .out({inst_control_n_0,inst_control_n_1,inst_control_n_2,inst_control_n_3}),
        .q00_IBUF(q00_IBUF),
        .q01_IBUF(q01_IBUF),
        .q10_IBUF(q10_IBUF),
        .q11_IBUF(q11_IBUF),
        .\r1_5_10_reg[15] (r1_5_10),
        .\r1_5_10_reg[15]_0 ({RESIZE0_in0,inst_datapath_n_27,inst_datapath_n_28,inst_datapath_n_29,inst_datapath_n_30,inst_datapath_n_31,inst_datapath_n_32,inst_datapath_n_33,inst_datapath_n_34,inst_datapath_n_35,inst_datapath_n_36}),
        .\r1_5_10_reg[15]_1 ({inst_datapath_n_93,inst_datapath_n_94,inst_datapath_n_95,inst_datapath_n_96}),
        .\r1_5_10_reg[15]_2 ({inst_datapath_n_97,inst_datapath_n_98,inst_datapath_n_99,inst_datapath_n_100}),
        .\r29_reg[15] (r29),
        .\r29_reg[15]_0 ({inst_control_n_67,inst_control_n_68,inst_control_n_69,inst_control_n_70,inst_control_n_71,inst_control_n_72,inst_control_n_73,inst_control_n_74,inst_control_n_75,inst_control_n_76,inst_control_n_77,inst_control_n_78,inst_control_n_79,inst_control_n_80,inst_control_n_81,inst_control_n_82}),
        .\r38_reg[0] ({ctrl__0[5],ctrl__0[2:0]}),
        .\r47_reg[11] (inst_control_n_22),
        .\r47_reg[11]_0 (inst_control_n_115),
        .\r47_reg[14] (r47),
        .\r47_reg[15] (r47_0),
        .\r47_reg[3] (inst_control_n_10),
        .\r47_reg[3]_0 (inst_control_n_15),
        .\r47_reg[3]_1 (inst_control_n_16),
        .\r47_reg[3]_2 (inst_control_n_17),
        .\r47_reg[7] (inst_control_n_18),
        .\r47_reg[7]_0 (inst_control_n_19),
        .\r47_reg[7]_1 (inst_control_n_20),
        .\r47_reg[7]_2 (inst_control_n_21),
        .\r6_reg[14] (r6),
        .\r6_reg[15] (inst_control_n_5),
        .\r6_reg[15]_0 ({inst_control_n_51,inst_control_n_52,inst_control_n_53,inst_control_n_54,inst_control_n_55,inst_control_n_56,inst_control_n_57,inst_control_n_58,inst_control_n_59,inst_control_n_60,inst_control_n_61,inst_control_n_62,inst_control_n_63,inst_control_n_64,inst_control_n_65,inst_control_n_66}),
        .\r6_reg[15]_1 (inst_datapath_n_52),
        .\r6_reg[9] (inst_datapath_n_67),
        .\rP_reg[15] (enable),
        .x0_IBUF(x0_IBUF[7:1]),
        .x_IBUF(x_IBUF[7]),
        .y0_IBUF(y0_IBUF),
        .y_IBUF(y_IBUF));
  datapath inst_datapath
       (.ARG_0(RESIZE1_in0),
        .ARG_1({inst_control_n_35,inst_control_n_36,inst_control_n_37,inst_control_n_38,inst_control_n_39,inst_control_n_40,inst_control_n_41,inst_control_n_42,inst_control_n_43,inst_control_n_44,inst_control_n_45,inst_control_n_46,inst_control_n_47,inst_control_n_48,inst_control_n_49,inst_control_n_50}),
        .B(data0),
        .CLK(clock_IBUF_BUFG),
        .D({inst_control_n_51,inst_control_n_52,inst_control_n_53,inst_control_n_54,inst_control_n_55,inst_control_n_56,inst_control_n_57,inst_control_n_58,inst_control_n_59,inst_control_n_60,inst_control_n_61,inst_control_n_62,inst_control_n_63,inst_control_n_64,inst_control_n_65,inst_control_n_66}),
        .E(inst_control_n_5),
        .\FSM_sequential_currstate_reg[1] (r47_0),
        .\FSM_sequential_currstate_reg[2] ({inst_control_n_67,inst_control_n_68,inst_control_n_69,inst_control_n_70,inst_control_n_71,inst_control_n_72,inst_control_n_73,inst_control_n_74,inst_control_n_75,inst_control_n_76,inst_control_n_77,inst_control_n_78,inst_control_n_79,inst_control_n_80,inst_control_n_81,inst_control_n_82}),
        .\FSM_sequential_currstate_reg[2]_0 (inst_control_n_4),
        .\FSM_sequential_currstate_reg[2]_1 (r1_5_10),
        .\FSM_sequential_currstate_reg[2]_10 (inst_control_n_21),
        .\FSM_sequential_currstate_reg[2]_11 (inst_control_n_22),
        .\FSM_sequential_currstate_reg[2]_2 (enable),
        .\FSM_sequential_currstate_reg[2]_3 (inst_control_n_10),
        .\FSM_sequential_currstate_reg[2]_4 (inst_control_n_15),
        .\FSM_sequential_currstate_reg[2]_5 (inst_control_n_16),
        .\FSM_sequential_currstate_reg[2]_6 (inst_control_n_17),
        .\FSM_sequential_currstate_reg[2]_7 (inst_control_n_18),
        .\FSM_sequential_currstate_reg[2]_8 (inst_control_n_19),
        .\FSM_sequential_currstate_reg[2]_9 (inst_control_n_20),
        .\FSM_sequential_currstate_reg[3] ({ctrl__0[5],ctrl__0[2:0]}),
        .\FSM_sequential_currstate_reg[3]_0 (r29),
        .\FSM_sequential_currstate_reg[3]_1 (inst_control_n_115),
        .P(data1),
        .Q({RESIZE0_in0,inst_datapath_n_27,inst_datapath_n_28,inst_datapath_n_29,inst_datapath_n_30,inst_datapath_n_31,inst_datapath_n_32,inst_datapath_n_33,inst_datapath_n_34,inst_datapath_n_35,inst_datapath_n_36}),
        .S(inst_datapath_n_68),
        .SR(reset_IBUF),
        .ctrl(ctrl),
        .done(done),
        .done_reg_0(inst_control_n_116),
        .mux1({mux1[15],mux1[9:0]}),
        .muxB3(muxB3),
        .out({inst_control_n_0,inst_control_n_1,inst_control_n_2,inst_control_n_3}),
        .q00_IBUF(q00_IBUF[0]),
        .\r47_reg[11]_0 ({inst_datapath_n_93,inst_datapath_n_94,inst_datapath_n_95,inst_datapath_n_96}),
        .\r47_reg[15]_0 ({inst_datapath_n_97,inst_datapath_n_98,inst_datapath_n_99,inst_datapath_n_100}),
        .\r47_reg[3]_0 ({inst_datapath_n_85,inst_datapath_n_86,inst_datapath_n_87,inst_datapath_n_88}),
        .\r47_reg[7]_0 ({inst_datapath_n_89,inst_datapath_n_90,inst_datapath_n_91,inst_datapath_n_92}),
        .\r6_reg[11]_0 (A),
        .\r6_reg[11]_1 (inst_datapath_n_67),
        .\r6_reg[15]_0 (r6),
        .\r6_reg[15]_1 (inst_datapath_n_52),
        .\r6_reg[15]_2 (r47),
        .\result[15] (result_OBUF),
        .x0_IBUF(x0_IBUF[0]),
        .x_IBUF(x_IBUF));
  IBUF oper_IBUF_inst
       (.I(oper),
        .O(oper_IBUF));
  IBUF \q00_IBUF[0]_inst 
       (.I(q00[0]),
        .O(q00_IBUF[0]));
  IBUF \q00_IBUF[1]_inst 
       (.I(q00[1]),
        .O(q00_IBUF[1]));
  IBUF \q00_IBUF[2]_inst 
       (.I(q00[2]),
        .O(q00_IBUF[2]));
  IBUF \q00_IBUF[3]_inst 
       (.I(q00[3]),
        .O(q00_IBUF[3]));
  IBUF \q00_IBUF[4]_inst 
       (.I(q00[4]),
        .O(q00_IBUF[4]));
  IBUF \q00_IBUF[5]_inst 
       (.I(q00[5]),
        .O(q00_IBUF[5]));
  IBUF \q00_IBUF[6]_inst 
       (.I(q00[6]),
        .O(q00_IBUF[6]));
  IBUF \q00_IBUF[7]_inst 
       (.I(q00[7]),
        .O(q00_IBUF[7]));
  IBUF \q00_IBUF[8]_inst 
       (.I(q00[8]),
        .O(q00_IBUF[8]));
  IBUF \q00_IBUF[9]_inst 
       (.I(q00[9]),
        .O(q00_IBUF[9]));
  IBUF \q01_IBUF[0]_inst 
       (.I(q01[0]),
        .O(q01_IBUF[0]));
  IBUF \q01_IBUF[1]_inst 
       (.I(q01[1]),
        .O(q01_IBUF[1]));
  IBUF \q01_IBUF[2]_inst 
       (.I(q01[2]),
        .O(q01_IBUF[2]));
  IBUF \q01_IBUF[3]_inst 
       (.I(q01[3]),
        .O(q01_IBUF[3]));
  IBUF \q01_IBUF[4]_inst 
       (.I(q01[4]),
        .O(q01_IBUF[4]));
  IBUF \q01_IBUF[5]_inst 
       (.I(q01[5]),
        .O(q01_IBUF[5]));
  IBUF \q01_IBUF[6]_inst 
       (.I(q01[6]),
        .O(q01_IBUF[6]));
  IBUF \q01_IBUF[7]_inst 
       (.I(q01[7]),
        .O(q01_IBUF[7]));
  IBUF \q01_IBUF[8]_inst 
       (.I(q01[8]),
        .O(q01_IBUF[8]));
  IBUF \q01_IBUF[9]_inst 
       (.I(q01[9]),
        .O(q01_IBUF[9]));
  IBUF \q10_IBUF[0]_inst 
       (.I(q10[0]),
        .O(q10_IBUF[0]));
  IBUF \q10_IBUF[1]_inst 
       (.I(q10[1]),
        .O(q10_IBUF[1]));
  IBUF \q10_IBUF[2]_inst 
       (.I(q10[2]),
        .O(q10_IBUF[2]));
  IBUF \q10_IBUF[3]_inst 
       (.I(q10[3]),
        .O(q10_IBUF[3]));
  IBUF \q10_IBUF[4]_inst 
       (.I(q10[4]),
        .O(q10_IBUF[4]));
  IBUF \q10_IBUF[5]_inst 
       (.I(q10[5]),
        .O(q10_IBUF[5]));
  IBUF \q10_IBUF[6]_inst 
       (.I(q10[6]),
        .O(q10_IBUF[6]));
  IBUF \q10_IBUF[7]_inst 
       (.I(q10[7]),
        .O(q10_IBUF[7]));
  IBUF \q10_IBUF[8]_inst 
       (.I(q10[8]),
        .O(q10_IBUF[8]));
  IBUF \q10_IBUF[9]_inst 
       (.I(q10[9]),
        .O(q10_IBUF[9]));
  IBUF \q11_IBUF[0]_inst 
       (.I(q11[0]),
        .O(q11_IBUF[0]));
  IBUF \q11_IBUF[1]_inst 
       (.I(q11[1]),
        .O(q11_IBUF[1]));
  IBUF \q11_IBUF[2]_inst 
       (.I(q11[2]),
        .O(q11_IBUF[2]));
  IBUF \q11_IBUF[3]_inst 
       (.I(q11[3]),
        .O(q11_IBUF[3]));
  IBUF \q11_IBUF[4]_inst 
       (.I(q11[4]),
        .O(q11_IBUF[4]));
  IBUF \q11_IBUF[5]_inst 
       (.I(q11[5]),
        .O(q11_IBUF[5]));
  IBUF \q11_IBUF[6]_inst 
       (.I(q11[6]),
        .O(q11_IBUF[6]));
  IBUF \q11_IBUF[7]_inst 
       (.I(q11[7]),
        .O(q11_IBUF[7]));
  IBUF \q11_IBUF[8]_inst 
       (.I(q11[8]),
        .O(q11_IBUF[8]));
  IBUF \q11_IBUF[9]_inst 
       (.I(q11[9]),
        .O(q11_IBUF[9]));
  IBUF reset_IBUF_inst
       (.I(reset),
        .O(reset_IBUF));
  OBUF \result_OBUF[0]_inst 
       (.I(result_OBUF[0]),
        .O(result[0]));
  OBUF \result_OBUF[10]_inst 
       (.I(result_OBUF[10]),
        .O(result[10]));
  OBUF \result_OBUF[11]_inst 
       (.I(result_OBUF[11]),
        .O(result[11]));
  OBUF \result_OBUF[12]_inst 
       (.I(result_OBUF[12]),
        .O(result[12]));
  OBUF \result_OBUF[13]_inst 
       (.I(result_OBUF[13]),
        .O(result[13]));
  OBUF \result_OBUF[14]_inst 
       (.I(result_OBUF[14]),
        .O(result[14]));
  OBUF \result_OBUF[15]_inst 
       (.I(result_OBUF[15]),
        .O(result[15]));
  OBUF \result_OBUF[1]_inst 
       (.I(result_OBUF[1]),
        .O(result[1]));
  OBUF \result_OBUF[2]_inst 
       (.I(result_OBUF[2]),
        .O(result[2]));
  OBUF \result_OBUF[3]_inst 
       (.I(result_OBUF[3]),
        .O(result[3]));
  OBUF \result_OBUF[4]_inst 
       (.I(result_OBUF[4]),
        .O(result[4]));
  OBUF \result_OBUF[5]_inst 
       (.I(result_OBUF[5]),
        .O(result[5]));
  OBUF \result_OBUF[6]_inst 
       (.I(result_OBUF[6]),
        .O(result[6]));
  OBUF \result_OBUF[7]_inst 
       (.I(result_OBUF[7]),
        .O(result[7]));
  OBUF \result_OBUF[8]_inst 
       (.I(result_OBUF[8]),
        .O(result[8]));
  OBUF \result_OBUF[9]_inst 
       (.I(result_OBUF[9]),
        .O(result[9]));
  IBUF \x0_IBUF[0]_inst 
       (.I(x0[0]),
        .O(x0_IBUF[0]));
  IBUF \x0_IBUF[1]_inst 
       (.I(x0[1]),
        .O(x0_IBUF[1]));
  IBUF \x0_IBUF[2]_inst 
       (.I(x0[2]),
        .O(x0_IBUF[2]));
  IBUF \x0_IBUF[3]_inst 
       (.I(x0[3]),
        .O(x0_IBUF[3]));
  IBUF \x0_IBUF[4]_inst 
       (.I(x0[4]),
        .O(x0_IBUF[4]));
  IBUF \x0_IBUF[5]_inst 
       (.I(x0[5]),
        .O(x0_IBUF[5]));
  IBUF \x0_IBUF[6]_inst 
       (.I(x0[6]),
        .O(x0_IBUF[6]));
  IBUF \x0_IBUF[7]_inst 
       (.I(x0[7]),
        .O(x0_IBUF[7]));
  IBUF \x_IBUF[0]_inst 
       (.I(x[0]),
        .O(x_IBUF[0]));
  IBUF \x_IBUF[1]_inst 
       (.I(x[1]),
        .O(x_IBUF[1]));
  IBUF \x_IBUF[2]_inst 
       (.I(x[2]),
        .O(x_IBUF[2]));
  IBUF \x_IBUF[3]_inst 
       (.I(x[3]),
        .O(x_IBUF[3]));
  IBUF \x_IBUF[4]_inst 
       (.I(x[4]),
        .O(x_IBUF[4]));
  IBUF \x_IBUF[5]_inst 
       (.I(x[5]),
        .O(x_IBUF[5]));
  IBUF \x_IBUF[6]_inst 
       (.I(x[6]),
        .O(x_IBUF[6]));
  IBUF \x_IBUF[7]_inst 
       (.I(x[7]),
        .O(x_IBUF[7]));
  IBUF \y0_IBUF[0]_inst 
       (.I(y0[0]),
        .O(y0_IBUF[0]));
  IBUF \y0_IBUF[1]_inst 
       (.I(y0[1]),
        .O(y0_IBUF[1]));
  IBUF \y0_IBUF[2]_inst 
       (.I(y0[2]),
        .O(y0_IBUF[2]));
  IBUF \y0_IBUF[3]_inst 
       (.I(y0[3]),
        .O(y0_IBUF[3]));
  IBUF \y0_IBUF[4]_inst 
       (.I(y0[4]),
        .O(y0_IBUF[4]));
  IBUF \y0_IBUF[5]_inst 
       (.I(y0[5]),
        .O(y0_IBUF[5]));
  IBUF \y0_IBUF[6]_inst 
       (.I(y0[6]),
        .O(y0_IBUF[6]));
  IBUF \y0_IBUF[7]_inst 
       (.I(y0[7]),
        .O(y0_IBUF[7]));
  IBUF \y_IBUF[0]_inst 
       (.I(y[0]),
        .O(y_IBUF[0]));
  IBUF \y_IBUF[1]_inst 
       (.I(y[1]),
        .O(y_IBUF[1]));
  IBUF \y_IBUF[2]_inst 
       (.I(y[2]),
        .O(y_IBUF[2]));
  IBUF \y_IBUF[3]_inst 
       (.I(y[3]),
        .O(y_IBUF[3]));
  IBUF \y_IBUF[4]_inst 
       (.I(y[4]),
        .O(y_IBUF[4]));
  IBUF \y_IBUF[5]_inst 
       (.I(y[5]),
        .O(y_IBUF[5]));
  IBUF \y_IBUF[6]_inst 
       (.I(y[6]),
        .O(y_IBUF[6]));
  IBUF \y_IBUF[7]_inst 
       (.I(y[7]),
        .O(y_IBUF[7]));
endmodule

module control
   (out,
    E,
    \r6_reg[15] ,
    \r29_reg[15] ,
    muxB3,
    \r47_reg[15] ,
    ctrl,
    \r47_reg[3] ,
    \r38_reg[0] ,
    \r47_reg[3]_0 ,
    \r47_reg[3]_1 ,
    \r47_reg[3]_2 ,
    \r47_reg[7] ,
    \r47_reg[7]_0 ,
    \r47_reg[7]_1 ,
    \r47_reg[7]_2 ,
    \r47_reg[11] ,
    mux1,
    \rP_reg[15] ,
    D,
    \r6_reg[15]_0 ,
    \r29_reg[15]_0 ,
    B,
    \r1_5_10_reg[15] ,
    \r47_reg[11]_0 ,
    done_reg,
    q00_IBUF,
    x0_IBUF,
    Q,
    x_IBUF,
    y0_IBUF,
    q01_IBUF,
    y_IBUF,
    \r1_5_10_reg[15]_0 ,
    q11_IBUF,
    q10_IBUF,
    done,
    oper_IBUF,
    P,
    A,
    S,
    \r6_reg[9] ,
    \r6_reg[15]_1 ,
    \FSM_sequential_currstate_reg[3]_0 ,
    \FSM_sequential_currstate_reg[3]_1 ,
    \r1_5_10_reg[15]_1 ,
    \r1_5_10_reg[15]_2 ,
    \r6_reg[14] ,
    \r47_reg[14] ,
    SR,
    CLK);
  output [3:0]out;
  output [0:0]E;
  output [0:0]\r6_reg[15] ;
  output [0:0]\r29_reg[15] ;
  output [0:0]muxB3;
  output [0:0]\r47_reg[15] ;
  output [0:0]ctrl;
  output \r47_reg[3] ;
  output [3:0]\r38_reg[0] ;
  output \r47_reg[3]_0 ;
  output \r47_reg[3]_1 ;
  output \r47_reg[3]_2 ;
  output \r47_reg[7] ;
  output \r47_reg[7]_0 ;
  output \r47_reg[7]_1 ;
  output \r47_reg[7]_2 ;
  output \r47_reg[11] ;
  output [10:0]mux1;
  output [0:0]\rP_reg[15] ;
  output [15:0]D;
  output [15:0]\r6_reg[15]_0 ;
  output [15:0]\r29_reg[15]_0 ;
  output [15:0]B;
  output [15:0]\r1_5_10_reg[15] ;
  output \r47_reg[11]_0 ;
  output done_reg;
  input [9:0]q00_IBUF;
  input [6:0]x0_IBUF;
  input [0:0]Q;
  input [0:0]x_IBUF;
  input [7:0]y0_IBUF;
  input [9:0]q01_IBUF;
  input [7:0]y_IBUF;
  input [10:0]\r1_5_10_reg[15]_0 ;
  input [9:0]q11_IBUF;
  input [9:0]q10_IBUF;
  input done;
  input oper_IBUF;
  input [15:0]P;
  input [8:0]A;
  input [0:0]S;
  input [0:0]\r6_reg[9] ;
  input [0:0]\r6_reg[15]_1 ;
  input [3:0]\FSM_sequential_currstate_reg[3]_0 ;
  input [3:0]\FSM_sequential_currstate_reg[3]_1 ;
  input [3:0]\r1_5_10_reg[15]_1 ;
  input [3:0]\r1_5_10_reg[15]_2 ;
  input [13:0]\r6_reg[14] ;
  input [13:0]\r47_reg[14] ;
  input [0:0]SR;
  input CLK;

  wire [8:0]A;
  wire [15:0]B;
  wire CLK;
  wire [15:0]D;
  wire [0:0]E;
  wire \FSM_sequential_currstate[1]_i_1_n_0 ;
  wire \FSM_sequential_currstate[2]_i_1_n_0 ;
  wire \FSM_sequential_currstate[3]_i_1_n_0 ;
  wire \FSM_sequential_currstate[3]_i_2_n_0 ;
  wire [3:0]\FSM_sequential_currstate_reg[3]_0 ;
  wire [3:0]\FSM_sequential_currstate_reg[3]_1 ;
  wire [15:0]P;
  wire [0:0]Q;
  wire [0:0]S;
  wire [0:0]SR;
  wire [0:0]ctrl;
  wire [4:4]ctrl__0;
  wire done;
  wire done_reg;
  wire [1:1]instr;
  wire [10:0]mux1;
  wire [15:15]muxB1;
  wire [0:0]muxB3;
  wire [0:0]nextstate;
  wire oper_IBUF;
  (* RTL_KEEP = "yes" *) wire [3:0]out;
  wire [9:0]q00_IBUF;
  wire [9:0]q01_IBUF;
  wire [9:0]q10_IBUF;
  wire [9:0]q11_IBUF;
  wire [15:0]\r1_5_10_reg[15] ;
  wire [10:0]\r1_5_10_reg[15]_0 ;
  wire [3:0]\r1_5_10_reg[15]_1 ;
  wire [3:0]\r1_5_10_reg[15]_2 ;
  wire [0:0]\r29_reg[15] ;
  wire [15:0]\r29_reg[15]_0 ;
  wire [3:0]\r38_reg[0] ;
  wire \r47[3]_i_2_n_0 ;
  wire \r47_reg[11] ;
  wire \r47_reg[11]_0 ;
  wire \r47_reg[11]_i_1_n_0 ;
  wire \r47_reg[11]_i_1_n_1 ;
  wire \r47_reg[11]_i_1_n_2 ;
  wire \r47_reg[11]_i_1_n_3 ;
  wire [13:0]\r47_reg[14] ;
  wire [0:0]\r47_reg[15] ;
  wire \r47_reg[15]_i_2_n_1 ;
  wire \r47_reg[15]_i_2_n_2 ;
  wire \r47_reg[15]_i_2_n_3 ;
  wire \r47_reg[3] ;
  wire \r47_reg[3]_0 ;
  wire \r47_reg[3]_1 ;
  wire \r47_reg[3]_2 ;
  wire \r47_reg[3]_i_1_n_0 ;
  wire \r47_reg[3]_i_1_n_1 ;
  wire \r47_reg[3]_i_1_n_2 ;
  wire \r47_reg[3]_i_1_n_3 ;
  wire \r47_reg[7] ;
  wire \r47_reg[7]_0 ;
  wire \r47_reg[7]_1 ;
  wire \r47_reg[7]_2 ;
  wire \r47_reg[7]_i_1_n_0 ;
  wire \r47_reg[7]_i_1_n_1 ;
  wire \r47_reg[7]_i_1_n_2 ;
  wire \r47_reg[7]_i_1_n_3 ;
  wire \r6[11]_i_2_n_0 ;
  wire \r6[11]_i_3_n_0 ;
  wire \r6[11]_i_4_n_0 ;
  wire \r6[11]_i_5_n_0 ;
  wire \r6[11]_i_6_n_0 ;
  wire \r6[11]_i_7_n_0 ;
  wire \r6[11]_i_9_n_0 ;
  wire \r6[15]_i_3_n_0 ;
  wire \r6[15]_i_4_n_0 ;
  wire \r6[15]_i_5_n_0 ;
  wire \r6[15]_i_7_n_0 ;
  wire \r6[15]_i_8_n_0 ;
  wire \r6[15]_i_9_n_0 ;
  wire \r6[3]_i_3_n_0 ;
  wire \r6[3]_i_4_n_0 ;
  wire \r6[3]_i_5_n_0 ;
  wire \r6[3]_i_6_n_0 ;
  wire \r6[3]_i_7_n_0 ;
  wire \r6[3]_i_8_n_0 ;
  wire \r6[3]_i_9_n_0 ;
  wire \r6[7]_i_2_n_0 ;
  wire \r6[7]_i_3_n_0 ;
  wire \r6[7]_i_4_n_0 ;
  wire \r6[7]_i_5_n_0 ;
  wire \r6[7]_i_6_n_0 ;
  wire \r6[7]_i_7_n_0 ;
  wire \r6[7]_i_8_n_0 ;
  wire \r6[7]_i_9_n_0 ;
  wire \r6_reg[11]_i_1_n_0 ;
  wire \r6_reg[11]_i_1_n_1 ;
  wire \r6_reg[11]_i_1_n_2 ;
  wire \r6_reg[11]_i_1_n_3 ;
  wire [13:0]\r6_reg[14] ;
  wire [0:0]\r6_reg[15] ;
  wire [15:0]\r6_reg[15]_0 ;
  wire [0:0]\r6_reg[15]_1 ;
  wire \r6_reg[15]_i_2_n_1 ;
  wire \r6_reg[15]_i_2_n_2 ;
  wire \r6_reg[15]_i_2_n_3 ;
  wire \r6_reg[3]_i_1_n_0 ;
  wire \r6_reg[3]_i_1_n_1 ;
  wire \r6_reg[3]_i_1_n_2 ;
  wire \r6_reg[3]_i_1_n_3 ;
  wire \r6_reg[7]_i_1_n_0 ;
  wire \r6_reg[7]_i_1_n_1 ;
  wire \r6_reg[7]_i_1_n_2 ;
  wire \r6_reg[7]_i_1_n_3 ;
  wire [0:0]\r6_reg[9] ;
  wire [0:0]\rP_reg[15] ;
  wire [6:0]x0_IBUF;
  wire [0:0]x_IBUF;
  wire [7:0]y0_IBUF;
  wire [7:0]y_IBUF;
  wire [3:3]\NLW_r47_reg[15]_i_2_CO_UNCONNECTED ;
  wire [3:3]\NLW_r6_reg[15]_i_2_CO_UNCONNECTED ;

  LUT4 #(
    .INIT(16'h1000)) 
    ARG_i_17
       (.I0(out[3]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(out[2]),
        .O(\r38_reg[0] [0]));
  LUT4 #(
    .INIT(16'h1000)) 
    ARG_i_18
       (.I0(out[3]),
        .I1(out[2]),
        .I2(out[0]),
        .I3(out[1]),
        .O(\r38_reg[0] [1]));
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_currstate[0]_i_1 
       (.I0(out[3]),
        .I1(out[0]),
        .O(nextstate));
  LUT3 #(
    .INIT(8'h06)) 
    \FSM_sequential_currstate[1]_i_1 
       (.I0(out[0]),
        .I1(out[1]),
        .I2(out[3]),
        .O(\FSM_sequential_currstate[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0078)) 
    \FSM_sequential_currstate[2]_i_1 
       (.I0(out[0]),
        .I1(out[1]),
        .I2(out[2]),
        .I3(out[3]),
        .O(\FSM_sequential_currstate[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h33333333333333B8)) 
    \FSM_sequential_currstate[3]_i_1 
       (.I0(done),
        .I1(out[3]),
        .I2(oper_IBUF),
        .I3(out[2]),
        .I4(out[0]),
        .I5(out[1]),
        .O(\FSM_sequential_currstate[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \FSM_sequential_currstate[3]_i_2 
       (.I0(out[1]),
        .I1(out[0]),
        .I2(out[2]),
        .I3(out[3]),
        .O(\FSM_sequential_currstate[3]_i_2_n_0 ));
  (* FSM_ENCODED_STATES = "s_cycle2:0010,s_cycle3:0011,s_cycle1:0001,s_end:1000,s_initial:0000,s_cycle6:0110,s_cycle5:0101,s_cycle7:0111,s_cycle4:0100" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_currstate_reg[0] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate[3]_i_1_n_0 ),
        .D(nextstate),
        .Q(out[0]),
        .R(SR));
  (* FSM_ENCODED_STATES = "s_cycle2:0010,s_cycle3:0011,s_cycle1:0001,s_end:1000,s_initial:0000,s_cycle6:0110,s_cycle5:0101,s_cycle7:0111,s_cycle4:0100" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_currstate_reg[1] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate[3]_i_1_n_0 ),
        .D(\FSM_sequential_currstate[1]_i_1_n_0 ),
        .Q(out[1]),
        .R(SR));
  (* FSM_ENCODED_STATES = "s_cycle2:0010,s_cycle3:0011,s_cycle1:0001,s_end:1000,s_initial:0000,s_cycle6:0110,s_cycle5:0101,s_cycle7:0111,s_cycle4:0100" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_currstate_reg[2] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate[3]_i_1_n_0 ),
        .D(\FSM_sequential_currstate[2]_i_1_n_0 ),
        .Q(out[2]),
        .R(SR));
  (* FSM_ENCODED_STATES = "s_cycle2:0010,s_cycle3:0011,s_cycle1:0001,s_end:1000,s_initial:0000,s_cycle6:0110,s_cycle5:0101,s_cycle7:0111,s_cycle4:0100" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_currstate_reg[3] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate[3]_i_1_n_0 ),
        .D(\FSM_sequential_currstate[3]_i_2_n_0 ),
        .Q(out[3]),
        .R(SR));
  LUT6 #(
    .INIT(64'h00000000AEAAAAAA)) 
    done_i_1
       (.I0(done),
        .I1(out[2]),
        .I2(out[3]),
        .I3(out[0]),
        .I4(out[1]),
        .I5(SR),
        .O(done_reg));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r1_5_10[0]_i_1 
       (.I0(B[0]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(P[0]),
        .O(\r1_5_10_reg[15] [0]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r1_5_10[10]_i_1 
       (.I0(B[10]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(P[10]),
        .O(\r1_5_10_reg[15] [10]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r1_5_10[11]_i_1 
       (.I0(B[11]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(P[11]),
        .O(\r1_5_10_reg[15] [11]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r1_5_10[12]_i_1 
       (.I0(B[12]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(P[12]),
        .O(\r1_5_10_reg[15] [12]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r1_5_10[13]_i_1 
       (.I0(B[13]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(P[13]),
        .O(\r1_5_10_reg[15] [13]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r1_5_10[14]_i_1 
       (.I0(B[14]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(P[14]),
        .O(\r1_5_10_reg[15] [14]));
  LUT4 #(
    .INIT(16'hDDED)) 
    \r1_5_10[15]_i_1 
       (.I0(out[2]),
        .I1(out[3]),
        .I2(out[1]),
        .I3(out[0]),
        .O(E));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r1_5_10[15]_i_2 
       (.I0(B[15]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(P[15]),
        .O(\r1_5_10_reg[15] [15]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r1_5_10[1]_i_1 
       (.I0(B[1]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(P[1]),
        .O(\r1_5_10_reg[15] [1]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r1_5_10[2]_i_1 
       (.I0(B[2]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(P[2]),
        .O(\r1_5_10_reg[15] [2]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r1_5_10[3]_i_1 
       (.I0(B[3]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(P[3]),
        .O(\r1_5_10_reg[15] [3]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r1_5_10[4]_i_1 
       (.I0(B[4]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(P[4]),
        .O(\r1_5_10_reg[15] [4]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r1_5_10[5]_i_1 
       (.I0(B[5]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(P[5]),
        .O(\r1_5_10_reg[15] [5]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r1_5_10[6]_i_1 
       (.I0(B[6]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(P[6]),
        .O(\r1_5_10_reg[15] [6]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r1_5_10[7]_i_1 
       (.I0(B[7]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(P[7]),
        .O(\r1_5_10_reg[15] [7]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r1_5_10[8]_i_1 
       (.I0(B[8]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(P[8]),
        .O(\r1_5_10_reg[15] [8]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r1_5_10[9]_i_1 
       (.I0(B[9]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(P[9]),
        .O(\r1_5_10_reg[15] [9]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r29[0]_i_1 
       (.I0(\r6_reg[15]_0 [0]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(B[0]),
        .O(\r29_reg[15]_0 [0]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r29[10]_i_1 
       (.I0(\r6_reg[15]_0 [10]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(B[10]),
        .O(\r29_reg[15]_0 [10]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r29[11]_i_1 
       (.I0(\r6_reg[15]_0 [11]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(B[11]),
        .O(\r29_reg[15]_0 [11]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r29[12]_i_1 
       (.I0(\r6_reg[15]_0 [12]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(B[12]),
        .O(\r29_reg[15]_0 [12]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r29[13]_i_1 
       (.I0(\r6_reg[15]_0 [13]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(B[13]),
        .O(\r29_reg[15]_0 [13]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r29[14]_i_1 
       (.I0(\r6_reg[15]_0 [14]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(B[14]),
        .O(\r29_reg[15]_0 [14]));
  LUT4 #(
    .INIT(16'hAEAF)) 
    \r29[15]_i_1 
       (.I0(out[3]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(out[2]),
        .O(\r29_reg[15] ));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r29[15]_i_2 
       (.I0(\r6_reg[15]_0 [15]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(B[15]),
        .O(\r29_reg[15]_0 [15]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r29[1]_i_1 
       (.I0(\r6_reg[15]_0 [1]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(B[1]),
        .O(\r29_reg[15]_0 [1]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r29[2]_i_1 
       (.I0(\r6_reg[15]_0 [2]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(B[2]),
        .O(\r29_reg[15]_0 [2]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r29[3]_i_1 
       (.I0(\r6_reg[15]_0 [3]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(B[3]),
        .O(\r29_reg[15]_0 [3]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r29[4]_i_1 
       (.I0(\r6_reg[15]_0 [4]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(B[4]),
        .O(\r29_reg[15]_0 [4]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r29[5]_i_1 
       (.I0(\r6_reg[15]_0 [5]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(B[5]),
        .O(\r29_reg[15]_0 [5]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r29[6]_i_1 
       (.I0(\r6_reg[15]_0 [6]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(B[6]),
        .O(\r29_reg[15]_0 [6]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r29[7]_i_1 
       (.I0(\r6_reg[15]_0 [7]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(B[7]),
        .O(\r29_reg[15]_0 [7]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r29[8]_i_1 
       (.I0(\r6_reg[15]_0 [8]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(B[8]),
        .O(\r29_reg[15]_0 [8]));
  LUT5 #(
    .INIT(32'hAAFEAA02)) 
    \r29[9]_i_1 
       (.I0(\r6_reg[15]_0 [9]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(out[3]),
        .I4(B[9]),
        .O(\r29_reg[15]_0 [9]));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \r38[0]_i_1 
       (.I0(P[0]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(\r6_reg[15]_0 [0]),
        .O(D[0]));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \r38[10]_i_1 
       (.I0(P[10]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(\r6_reg[15]_0 [10]),
        .O(D[10]));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \r38[11]_i_1 
       (.I0(P[11]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(\r6_reg[15]_0 [11]),
        .O(D[11]));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \r38[12]_i_1 
       (.I0(P[12]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(\r6_reg[15]_0 [12]),
        .O(D[12]));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \r38[13]_i_1 
       (.I0(P[13]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(\r6_reg[15]_0 [13]),
        .O(D[13]));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \r38[14]_i_1 
       (.I0(P[14]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(\r6_reg[15]_0 [14]),
        .O(D[14]));
  LUT4 #(
    .INIT(16'h1004)) 
    \r38[15]_i_1 
       (.I0(out[3]),
        .I1(out[1]),
        .I2(out[2]),
        .I3(out[0]),
        .O(\r38_reg[0] [3]));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \r38[15]_i_2 
       (.I0(P[15]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(\r6_reg[15]_0 [15]),
        .O(D[15]));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \r38[1]_i_1 
       (.I0(P[1]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(\r6_reg[15]_0 [1]),
        .O(D[1]));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \r38[2]_i_1 
       (.I0(P[2]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(\r6_reg[15]_0 [2]),
        .O(D[2]));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \r38[3]_i_1 
       (.I0(P[3]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(\r6_reg[15]_0 [3]),
        .O(D[3]));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \r38[4]_i_1 
       (.I0(P[4]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(\r6_reg[15]_0 [4]),
        .O(D[4]));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \r38[5]_i_1 
       (.I0(P[5]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(\r6_reg[15]_0 [5]),
        .O(D[5]));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \r38[6]_i_1 
       (.I0(P[6]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(\r6_reg[15]_0 [6]),
        .O(D[6]));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \r38[7]_i_1 
       (.I0(P[7]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(\r6_reg[15]_0 [7]),
        .O(D[7]));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \r38[8]_i_1 
       (.I0(P[8]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(\r6_reg[15]_0 [8]),
        .O(D[8]));
  LUT5 #(
    .INIT(32'hFFEF0020)) 
    \r38[9]_i_1 
       (.I0(P[9]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(out[2]),
        .I4(\r6_reg[15]_0 [9]),
        .O(D[9]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r47[11]_i_2 
       (.I0(y_IBUF[7]),
        .I1(\r1_5_10_reg[15]_0 [9]),
        .I2(ctrl__0),
        .I3(q11_IBUF[9]),
        .I4(\r38_reg[0] [3]),
        .I5(q10_IBUF[9]),
        .O(mux1[9]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r47[11]_i_3 
       (.I0(y_IBUF[7]),
        .I1(\r1_5_10_reg[15]_0 [8]),
        .I2(ctrl__0),
        .I3(q11_IBUF[8]),
        .I4(\r38_reg[0] [3]),
        .I5(q10_IBUF[8]),
        .O(mux1[8]));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \r47[11]_i_8 
       (.I0(y0_IBUF[7]),
        .I1(ctrl__0),
        .I2(q01_IBUF[8]),
        .I3(\r38_reg[0] [3]),
        .I4(q00_IBUF[8]),
        .O(\r47_reg[11] ));
  LUT4 #(
    .INIT(16'h0012)) 
    \r47[15]_i_1 
       (.I0(out[1]),
        .I1(out[0]),
        .I2(out[2]),
        .I3(out[3]),
        .O(\r47_reg[15] ));
  LUT4 #(
    .INIT(16'h0010)) 
    \r47[15]_i_10 
       (.I0(out[3]),
        .I1(out[0]),
        .I2(out[2]),
        .I3(out[1]),
        .O(instr));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r47[15]_i_3 
       (.I0(y_IBUF[7]),
        .I1(\r1_5_10_reg[15]_0 [10]),
        .I2(ctrl__0),
        .I3(q11_IBUF[9]),
        .I4(\r38_reg[0] [3]),
        .I5(q10_IBUF[9]),
        .O(mux1[10]));
  LUT3 #(
    .INIT(8'h02)) 
    \r47[15]_i_8 
       (.I0(out[2]),
        .I1(out[3]),
        .I2(out[1]),
        .O(ctrl__0));
  LUT6 #(
    .INIT(64'hAAA5A9595A55A959)) 
    \r47[15]_i_9 
       (.I0(instr),
        .I1(q00_IBUF[9]),
        .I2(\r38_reg[0] [3]),
        .I3(q01_IBUF[9]),
        .I4(ctrl__0),
        .I5(y0_IBUF[7]),
        .O(\r47_reg[11]_0 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \r47[3]_i_11 
       (.I0(y0_IBUF[3]),
        .I1(ctrl__0),
        .I2(q01_IBUF[3]),
        .I3(\r38_reg[0] [3]),
        .I4(q00_IBUF[3]),
        .O(\r47_reg[3]_2 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \r47[3]_i_12 
       (.I0(y0_IBUF[2]),
        .I1(ctrl__0),
        .I2(q01_IBUF[2]),
        .I3(\r38_reg[0] [3]),
        .I4(q00_IBUF[2]),
        .O(\r47_reg[3]_1 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \r47[3]_i_13 
       (.I0(y0_IBUF[1]),
        .I1(ctrl__0),
        .I2(q01_IBUF[1]),
        .I3(\r38_reg[0] [3]),
        .I4(q00_IBUF[1]),
        .O(\r47_reg[3]_0 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \r47[3]_i_14 
       (.I0(y0_IBUF[0]),
        .I1(ctrl__0),
        .I2(q01_IBUF[0]),
        .I3(\r38_reg[0] [3]),
        .I4(q00_IBUF[0]),
        .O(\r47_reg[3] ));
  LUT4 #(
    .INIT(16'hFFFB)) 
    \r47[3]_i_2 
       (.I0(out[1]),
        .I1(out[2]),
        .I2(out[0]),
        .I3(out[3]),
        .O(\r47[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r47[3]_i_3 
       (.I0(y_IBUF[3]),
        .I1(\r1_5_10_reg[15]_0 [3]),
        .I2(ctrl__0),
        .I3(q11_IBUF[3]),
        .I4(\r38_reg[0] [3]),
        .I5(q10_IBUF[3]),
        .O(mux1[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r47[3]_i_4 
       (.I0(y_IBUF[2]),
        .I1(\r1_5_10_reg[15]_0 [2]),
        .I2(ctrl__0),
        .I3(q11_IBUF[2]),
        .I4(\r38_reg[0] [3]),
        .I5(q10_IBUF[2]),
        .O(mux1[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r47[3]_i_5 
       (.I0(y_IBUF[1]),
        .I1(\r1_5_10_reg[15]_0 [1]),
        .I2(ctrl__0),
        .I3(q11_IBUF[1]),
        .I4(\r38_reg[0] [3]),
        .I5(q10_IBUF[1]),
        .O(mux1[1]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r47[3]_i_6 
       (.I0(y_IBUF[0]),
        .I1(\r1_5_10_reg[15]_0 [0]),
        .I2(ctrl__0),
        .I3(q11_IBUF[0]),
        .I4(\r38_reg[0] [3]),
        .I5(q10_IBUF[0]),
        .O(mux1[0]));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \r47[7]_i_10 
       (.I0(y0_IBUF[7]),
        .I1(ctrl__0),
        .I2(q01_IBUF[7]),
        .I3(\r38_reg[0] [3]),
        .I4(q00_IBUF[7]),
        .O(\r47_reg[7]_2 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \r47[7]_i_11 
       (.I0(y0_IBUF[6]),
        .I1(ctrl__0),
        .I2(q01_IBUF[6]),
        .I3(\r38_reg[0] [3]),
        .I4(q00_IBUF[6]),
        .O(\r47_reg[7]_1 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \r47[7]_i_12 
       (.I0(y0_IBUF[5]),
        .I1(ctrl__0),
        .I2(q01_IBUF[5]),
        .I3(\r38_reg[0] [3]),
        .I4(q00_IBUF[5]),
        .O(\r47_reg[7]_0 ));
  LUT5 #(
    .INIT(32'hB8F3B8C0)) 
    \r47[7]_i_13 
       (.I0(y0_IBUF[4]),
        .I1(ctrl__0),
        .I2(q01_IBUF[4]),
        .I3(\r38_reg[0] [3]),
        .I4(q00_IBUF[4]),
        .O(\r47_reg[7] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r47[7]_i_2 
       (.I0(y_IBUF[7]),
        .I1(\r1_5_10_reg[15]_0 [7]),
        .I2(ctrl__0),
        .I3(q11_IBUF[7]),
        .I4(\r38_reg[0] [3]),
        .I5(q10_IBUF[7]),
        .O(mux1[7]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r47[7]_i_3 
       (.I0(y_IBUF[6]),
        .I1(\r1_5_10_reg[15]_0 [6]),
        .I2(ctrl__0),
        .I3(q11_IBUF[6]),
        .I4(\r38_reg[0] [3]),
        .I5(q10_IBUF[6]),
        .O(mux1[6]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r47[7]_i_4 
       (.I0(y_IBUF[5]),
        .I1(\r1_5_10_reg[15]_0 [5]),
        .I2(ctrl__0),
        .I3(q11_IBUF[5]),
        .I4(\r38_reg[0] [3]),
        .I5(q10_IBUF[5]),
        .O(mux1[5]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r47[7]_i_5 
       (.I0(y_IBUF[4]),
        .I1(\r1_5_10_reg[15]_0 [4]),
        .I2(ctrl__0),
        .I3(q11_IBUF[4]),
        .I4(\r38_reg[0] [3]),
        .I5(q10_IBUF[4]),
        .O(mux1[4]));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \r47_reg[11]_i_1 
       (.CI(\r47_reg[7]_i_1_n_0 ),
        .CO({\r47_reg[11]_i_1_n_0 ,\r47_reg[11]_i_1_n_1 ,\r47_reg[11]_i_1_n_2 ,\r47_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({mux1[10],mux1[10:8]}),
        .O(B[11:8]),
        .S(\r1_5_10_reg[15]_1 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \r47_reg[15]_i_2 
       (.CI(\r47_reg[11]_i_1_n_0 ),
        .CO({\NLW_r47_reg[15]_i_2_CO_UNCONNECTED [3],\r47_reg[15]_i_2_n_1 ,\r47_reg[15]_i_2_n_2 ,\r47_reg[15]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,mux1[10],mux1[10],mux1[10]}),
        .O(B[15:12]),
        .S(\r1_5_10_reg[15]_2 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \r47_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\r47_reg[3]_i_1_n_0 ,\r47_reg[3]_i_1_n_1 ,\r47_reg[3]_i_1_n_2 ,\r47_reg[3]_i_1_n_3 }),
        .CYINIT(\r47[3]_i_2_n_0 ),
        .DI(mux1[3:0]),
        .O(B[3:0]),
        .S(\FSM_sequential_currstate_reg[3]_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \r47_reg[7]_i_1 
       (.CI(\r47_reg[3]_i_1_n_0 ),
        .CO({\r47_reg[7]_i_1_n_0 ,\r47_reg[7]_i_1_n_1 ,\r47_reg[7]_i_1_n_2 ,\r47_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(mux1[7:4]),
        .O(B[7:4]),
        .S(\FSM_sequential_currstate_reg[3]_1 ));
  LUT6 #(
    .INIT(64'hCCD1FFD100D133D1)) 
    \r6[11]_i_2 
       (.I0(x0_IBUF[6]),
        .I1(ctrl),
        .I2(q00_IBUF[9]),
        .I3(\r38_reg[0] [2]),
        .I4(\r47_reg[14] [10]),
        .I5(\r6_reg[14] [10]),
        .O(\r6[11]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCCD1FFD100D133D1)) 
    \r6[11]_i_3 
       (.I0(x0_IBUF[6]),
        .I1(ctrl),
        .I2(q00_IBUF[9]),
        .I3(\r38_reg[0] [2]),
        .I4(\r47_reg[14] [9]),
        .I5(\r6_reg[14] [9]),
        .O(\r6[11]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCCD1FFD100D133D1)) 
    \r6[11]_i_4 
       (.I0(x0_IBUF[6]),
        .I1(ctrl),
        .I2(q00_IBUF[9]),
        .I3(\r38_reg[0] [2]),
        .I4(\r47_reg[14] [8]),
        .I5(\r6_reg[14] [8]),
        .O(\r6[11]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hCCD1FFD100D133D1)) 
    \r6[11]_i_5 
       (.I0(x0_IBUF[6]),
        .I1(ctrl),
        .I2(q00_IBUF[8]),
        .I3(\r38_reg[0] [2]),
        .I4(\r47_reg[14] [7]),
        .I5(\r6_reg[14] [7]),
        .O(\r6[11]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h565656A6A6A656A6)) 
    \r6[11]_i_6 
       (.I0(\r6[11]_i_2_n_0 ),
        .I1(muxB1),
        .I2(\r38_reg[0] [2]),
        .I3(\r6_reg[14] [10]),
        .I4(ctrl),
        .I5(\r1_5_10_reg[15]_0 [10]),
        .O(\r6[11]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h565656A6A6A656A6)) 
    \r6[11]_i_7 
       (.I0(\r6[11]_i_3_n_0 ),
        .I1(muxB1),
        .I2(\r38_reg[0] [2]),
        .I3(\r6_reg[14] [9]),
        .I4(ctrl),
        .I5(\r1_5_10_reg[15]_0 [10]),
        .O(\r6[11]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \r6[11]_i_9 
       (.I0(\r6[11]_i_5_n_0 ),
        .I1(A[8]),
        .O(\r6[11]_i_9_n_0 ));
  LUT4 #(
    .INIT(16'h0008)) 
    \r6[15]_i_1 
       (.I0(out[1]),
        .I1(out[0]),
        .I2(out[2]),
        .I3(out[3]),
        .O(\r6_reg[15] ));
  LUT3 #(
    .INIT(8'h40)) 
    \r6[15]_i_10 
       (.I0(out[3]),
        .I1(out[0]),
        .I2(out[1]),
        .O(ctrl));
  LUT3 #(
    .INIT(8'h40)) 
    \r6[15]_i_11 
       (.I0(out[3]),
        .I1(out[0]),
        .I2(out[2]),
        .O(\r38_reg[0] [2]));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \r6[15]_i_12 
       (.I0(q00_IBUF[9]),
        .I1(out[3]),
        .I2(out[0]),
        .I3(out[1]),
        .I4(x0_IBUF[6]),
        .O(muxB3));
  LUT5 #(
    .INIT(32'hEFFF2000)) 
    \r6[15]_i_14 
       (.I0(Q),
        .I1(out[3]),
        .I2(out[0]),
        .I3(out[1]),
        .I4(x_IBUF),
        .O(muxB1));
  LUT6 #(
    .INIT(64'hCCD1FFD100D133D1)) 
    \r6[15]_i_3 
       (.I0(x0_IBUF[6]),
        .I1(ctrl),
        .I2(q00_IBUF[9]),
        .I3(\r38_reg[0] [2]),
        .I4(\r47_reg[14] [13]),
        .I5(\r6_reg[14] [13]),
        .O(\r6[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCCD1FFD100D133D1)) 
    \r6[15]_i_4 
       (.I0(x0_IBUF[6]),
        .I1(ctrl),
        .I2(q00_IBUF[9]),
        .I3(\r38_reg[0] [2]),
        .I4(\r47_reg[14] [12]),
        .I5(\r6_reg[14] [12]),
        .O(\r6[15]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hCCD1FFD100D133D1)) 
    \r6[15]_i_5 
       (.I0(x0_IBUF[6]),
        .I1(ctrl),
        .I2(q00_IBUF[9]),
        .I3(\r38_reg[0] [2]),
        .I4(\r47_reg[14] [11]),
        .I5(\r6_reg[14] [11]),
        .O(\r6[15]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h565656A6A6A656A6)) 
    \r6[15]_i_7 
       (.I0(\r6[15]_i_3_n_0 ),
        .I1(muxB1),
        .I2(\r38_reg[0] [2]),
        .I3(\r6_reg[14] [13]),
        .I4(ctrl),
        .I5(\r1_5_10_reg[15]_0 [10]),
        .O(\r6[15]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h565656A6A6A656A6)) 
    \r6[15]_i_8 
       (.I0(\r6[15]_i_4_n_0 ),
        .I1(muxB1),
        .I2(\r38_reg[0] [2]),
        .I3(\r6_reg[14] [12]),
        .I4(ctrl),
        .I5(\r1_5_10_reg[15]_0 [10]),
        .O(\r6[15]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h565656A6A6A656A6)) 
    \r6[15]_i_9 
       (.I0(\r6[15]_i_5_n_0 ),
        .I1(muxB1),
        .I2(\r38_reg[0] [2]),
        .I3(\r6_reg[14] [11]),
        .I4(ctrl),
        .I5(\r1_5_10_reg[15]_0 [10]),
        .O(\r6[15]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hCCD1FFD100D133D1)) 
    \r6[3]_i_3 
       (.I0(x0_IBUF[2]),
        .I1(ctrl),
        .I2(q00_IBUF[3]),
        .I3(\r38_reg[0] [2]),
        .I4(\r47_reg[14] [2]),
        .I5(\r6_reg[14] [2]),
        .O(\r6[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCCD1FFD100D133D1)) 
    \r6[3]_i_4 
       (.I0(x0_IBUF[1]),
        .I1(ctrl),
        .I2(q00_IBUF[2]),
        .I3(\r38_reg[0] [2]),
        .I4(\r47_reg[14] [1]),
        .I5(\r6_reg[14] [1]),
        .O(\r6[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hCCD1FFD100D133D1)) 
    \r6[3]_i_5 
       (.I0(x0_IBUF[0]),
        .I1(ctrl),
        .I2(q00_IBUF[1]),
        .I3(\r38_reg[0] [2]),
        .I4(\r47_reg[14] [0]),
        .I5(\r6_reg[14] [0]),
        .O(\r6[3]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hF7)) 
    \r6[3]_i_6 
       (.I0(out[1]),
        .I1(out[0]),
        .I2(out[3]),
        .O(\r6[3]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \r6[3]_i_7 
       (.I0(\r6[3]_i_3_n_0 ),
        .I1(A[3]),
        .O(\r6[3]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \r6[3]_i_8 
       (.I0(\r6[3]_i_4_n_0 ),
        .I1(A[2]),
        .O(\r6[3]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \r6[3]_i_9 
       (.I0(\r6[3]_i_5_n_0 ),
        .I1(A[1]),
        .O(\r6[3]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hCCD1FFD100D133D1)) 
    \r6[7]_i_2 
       (.I0(x0_IBUF[6]),
        .I1(ctrl),
        .I2(q00_IBUF[7]),
        .I3(\r38_reg[0] [2]),
        .I4(\r47_reg[14] [6]),
        .I5(\r6_reg[14] [6]),
        .O(\r6[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCCD1FFD100D133D1)) 
    \r6[7]_i_3 
       (.I0(x0_IBUF[5]),
        .I1(ctrl),
        .I2(q00_IBUF[6]),
        .I3(\r38_reg[0] [2]),
        .I4(\r47_reg[14] [5]),
        .I5(\r6_reg[14] [5]),
        .O(\r6[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hCCD1FFD100D133D1)) 
    \r6[7]_i_4 
       (.I0(x0_IBUF[4]),
        .I1(ctrl),
        .I2(q00_IBUF[5]),
        .I3(\r38_reg[0] [2]),
        .I4(\r47_reg[14] [4]),
        .I5(\r6_reg[14] [4]),
        .O(\r6[7]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hCCD1FFD100D133D1)) 
    \r6[7]_i_5 
       (.I0(x0_IBUF[3]),
        .I1(ctrl),
        .I2(q00_IBUF[4]),
        .I3(\r38_reg[0] [2]),
        .I4(\r47_reg[14] [3]),
        .I5(\r6_reg[14] [3]),
        .O(\r6[7]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \r6[7]_i_6 
       (.I0(\r6[7]_i_2_n_0 ),
        .I1(A[7]),
        .O(\r6[7]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \r6[7]_i_7 
       (.I0(\r6[7]_i_3_n_0 ),
        .I1(A[6]),
        .O(\r6[7]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \r6[7]_i_8 
       (.I0(\r6[7]_i_4_n_0 ),
        .I1(A[5]),
        .O(\r6[7]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \r6[7]_i_9 
       (.I0(\r6[7]_i_5_n_0 ),
        .I1(A[4]),
        .O(\r6[7]_i_9_n_0 ));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \r6_reg[11]_i_1 
       (.CI(\r6_reg[7]_i_1_n_0 ),
        .CO({\r6_reg[11]_i_1_n_0 ,\r6_reg[11]_i_1_n_1 ,\r6_reg[11]_i_1_n_2 ,\r6_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\r6[11]_i_2_n_0 ,\r6[11]_i_3_n_0 ,\r6[11]_i_4_n_0 ,\r6[11]_i_5_n_0 }),
        .O(\r6_reg[15]_0 [11:8]),
        .S({\r6[11]_i_6_n_0 ,\r6[11]_i_7_n_0 ,\r6_reg[9] ,\r6[11]_i_9_n_0 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \r6_reg[15]_i_2 
       (.CI(\r6_reg[11]_i_1_n_0 ),
        .CO({\NLW_r6_reg[15]_i_2_CO_UNCONNECTED [3],\r6_reg[15]_i_2_n_1 ,\r6_reg[15]_i_2_n_2 ,\r6_reg[15]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\r6[15]_i_3_n_0 ,\r6[15]_i_4_n_0 ,\r6[15]_i_5_n_0 }),
        .O(\r6_reg[15]_0 [15:12]),
        .S({\r6_reg[15]_1 ,\r6[15]_i_7_n_0 ,\r6[15]_i_8_n_0 ,\r6[15]_i_9_n_0 }));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \r6_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\r6_reg[3]_i_1_n_0 ,\r6_reg[3]_i_1_n_1 ,\r6_reg[3]_i_1_n_2 ,\r6_reg[3]_i_1_n_3 }),
        .CYINIT(A[0]),
        .DI({\r6[3]_i_3_n_0 ,\r6[3]_i_4_n_0 ,\r6[3]_i_5_n_0 ,\r6[3]_i_6_n_0 }),
        .O(\r6_reg[15]_0 [3:0]),
        .S({\r6[3]_i_7_n_0 ,\r6[3]_i_8_n_0 ,\r6[3]_i_9_n_0 ,S}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \r6_reg[7]_i_1 
       (.CI(\r6_reg[3]_i_1_n_0 ),
        .CO({\r6_reg[7]_i_1_n_0 ,\r6_reg[7]_i_1_n_1 ,\r6_reg[7]_i_1_n_2 ,\r6_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\r6[7]_i_2_n_0 ,\r6[7]_i_3_n_0 ,\r6[7]_i_4_n_0 ,\r6[7]_i_5_n_0 }),
        .O(\r6_reg[15]_0 [7:4]),
        .S({\r6[7]_i_6_n_0 ,\r6[7]_i_7_n_0 ,\r6[7]_i_8_n_0 ,\r6[7]_i_9_n_0 }));
  LUT4 #(
    .INIT(16'h2000)) 
    \rP[15]_i_1 
       (.I0(out[2]),
        .I1(out[3]),
        .I2(out[0]),
        .I3(out[1]),
        .O(\rP_reg[15] ));
endmodule

module datapath
   (P,
    done,
    \r6_reg[11]_0 ,
    Q,
    \r6_reg[15]_0 ,
    ARG_0,
    \r6_reg[15]_1 ,
    \r6_reg[15]_2 ,
    \r6_reg[11]_1 ,
    S,
    \result[15] ,
    \r47_reg[3]_0 ,
    \r47_reg[7]_0 ,
    \r47_reg[11]_0 ,
    \r47_reg[15]_0 ,
    B,
    done_reg_0,
    CLK,
    \FSM_sequential_currstate_reg[3] ,
    ctrl,
    x_IBUF,
    muxB3,
    q00_IBUF,
    x0_IBUF,
    SR,
    E,
    D,
    ARG_1,
    \FSM_sequential_currstate_reg[1] ,
    \FSM_sequential_currstate_reg[3]_0 ,
    \FSM_sequential_currstate_reg[2] ,
    \FSM_sequential_currstate_reg[2]_0 ,
    \FSM_sequential_currstate_reg[2]_1 ,
    \FSM_sequential_currstate_reg[2]_2 ,
    mux1,
    \FSM_sequential_currstate_reg[2]_3 ,
    out,
    \FSM_sequential_currstate_reg[2]_4 ,
    \FSM_sequential_currstate_reg[2]_5 ,
    \FSM_sequential_currstate_reg[2]_6 ,
    \FSM_sequential_currstate_reg[2]_7 ,
    \FSM_sequential_currstate_reg[2]_8 ,
    \FSM_sequential_currstate_reg[2]_9 ,
    \FSM_sequential_currstate_reg[2]_10 ,
    \FSM_sequential_currstate_reg[2]_11 ,
    \FSM_sequential_currstate_reg[3]_1 );
  output [15:0]P;
  output done;
  output [8:0]\r6_reg[11]_0 ;
  output [10:0]Q;
  output [13:0]\r6_reg[15]_0 ;
  output [0:0]ARG_0;
  output [0:0]\r6_reg[15]_1 ;
  output [13:0]\r6_reg[15]_2 ;
  output [0:0]\r6_reg[11]_1 ;
  output [0:0]S;
  output [15:0]\result[15] ;
  output [3:0]\r47_reg[3]_0 ;
  output [3:0]\r47_reg[7]_0 ;
  output [3:0]\r47_reg[11]_0 ;
  output [3:0]\r47_reg[15]_0 ;
  input [15:0]B;
  input done_reg_0;
  input CLK;
  input [3:0]\FSM_sequential_currstate_reg[3] ;
  input [0:0]ctrl;
  input [7:0]x_IBUF;
  input [0:0]muxB3;
  input [0:0]q00_IBUF;
  input [0:0]x0_IBUF;
  input [0:0]SR;
  input [0:0]E;
  input [15:0]D;
  input [15:0]ARG_1;
  input [0:0]\FSM_sequential_currstate_reg[1] ;
  input [0:0]\FSM_sequential_currstate_reg[3]_0 ;
  input [15:0]\FSM_sequential_currstate_reg[2] ;
  input [0:0]\FSM_sequential_currstate_reg[2]_0 ;
  input [15:0]\FSM_sequential_currstate_reg[2]_1 ;
  input [0:0]\FSM_sequential_currstate_reg[2]_2 ;
  input [10:0]mux1;
  input \FSM_sequential_currstate_reg[2]_3 ;
  input [3:0]out;
  input \FSM_sequential_currstate_reg[2]_4 ;
  input \FSM_sequential_currstate_reg[2]_5 ;
  input \FSM_sequential_currstate_reg[2]_6 ;
  input \FSM_sequential_currstate_reg[2]_7 ;
  input \FSM_sequential_currstate_reg[2]_8 ;
  input \FSM_sequential_currstate_reg[2]_9 ;
  input \FSM_sequential_currstate_reg[2]_10 ;
  input \FSM_sequential_currstate_reg[2]_11 ;
  input \FSM_sequential_currstate_reg[3]_1 ;

  wire [9:9]A;
  wire [0:0]ARG_0;
  wire [15:0]ARG_1;
  wire ARG_i_10_n_0;
  wire ARG_i_11_n_0;
  wire ARG_i_12_n_0;
  wire ARG_i_13_n_0;
  wire ARG_i_14_n_0;
  wire ARG_i_15_n_0;
  wire ARG_i_16_n_0;
  wire ARG_i_1_n_0;
  wire ARG_i_2_n_0;
  wire ARG_i_3_n_0;
  wire ARG_i_4_n_0;
  wire ARG_i_5_n_0;
  wire ARG_i_6_n_0;
  wire ARG_i_7_n_0;
  wire ARG_i_8_n_0;
  wire ARG_i_9_n_0;
  wire ARG_n_75;
  wire ARG_n_76;
  wire ARG_n_77;
  wire ARG_n_78;
  wire ARG_n_79;
  wire ARG_n_80;
  wire ARG_n_81;
  wire ARG_n_82;
  wire ARG_n_83;
  wire ARG_n_84;
  wire ARG_n_85;
  wire ARG_n_86;
  wire ARG_n_87;
  wire ARG_n_88;
  wire ARG_n_89;
  wire ARG_n_90;
  wire [15:15]A__0;
  wire [15:0]B;
  wire CLK;
  wire [15:0]D;
  wire [0:0]E;
  wire [0:0]\FSM_sequential_currstate_reg[1] ;
  wire [15:0]\FSM_sequential_currstate_reg[2] ;
  wire [0:0]\FSM_sequential_currstate_reg[2]_0 ;
  wire [15:0]\FSM_sequential_currstate_reg[2]_1 ;
  wire \FSM_sequential_currstate_reg[2]_10 ;
  wire \FSM_sequential_currstate_reg[2]_11 ;
  wire [0:0]\FSM_sequential_currstate_reg[2]_2 ;
  wire \FSM_sequential_currstate_reg[2]_3 ;
  wire \FSM_sequential_currstate_reg[2]_4 ;
  wire \FSM_sequential_currstate_reg[2]_5 ;
  wire \FSM_sequential_currstate_reg[2]_6 ;
  wire \FSM_sequential_currstate_reg[2]_7 ;
  wire \FSM_sequential_currstate_reg[2]_8 ;
  wire \FSM_sequential_currstate_reg[2]_9 ;
  wire [3:0]\FSM_sequential_currstate_reg[3] ;
  wire [0:0]\FSM_sequential_currstate_reg[3]_0 ;
  wire \FSM_sequential_currstate_reg[3]_1 ;
  wire [15:0]P;
  wire [10:0]Q;
  wire [0:0]S;
  wire [0:0]SR;
  wire [0:0]ctrl;
  wire done;
  wire done_reg_0;
  wire [10:0]mux1;
  wire [0:0]muxB3;
  wire [3:0]out;
  wire [0:0]q00_IBUF;
  wire \r1_5_10_reg_n_0_[0] ;
  wire \r1_5_10_reg_n_0_[1] ;
  wire \r1_5_10_reg_n_0_[2] ;
  wire \r1_5_10_reg_n_0_[3] ;
  wire \r1_5_10_reg_n_0_[4] ;
  wire [15:0]r29;
  wire \r38_reg_n_0_[0] ;
  wire \r38_reg_n_0_[10] ;
  wire \r38_reg_n_0_[11] ;
  wire \r38_reg_n_0_[12] ;
  wire \r38_reg_n_0_[13] ;
  wire \r38_reg_n_0_[14] ;
  wire \r38_reg_n_0_[1] ;
  wire \r38_reg_n_0_[2] ;
  wire \r38_reg_n_0_[3] ;
  wire \r38_reg_n_0_[4] ;
  wire \r38_reg_n_0_[5] ;
  wire \r38_reg_n_0_[6] ;
  wire \r38_reg_n_0_[7] ;
  wire \r38_reg_n_0_[8] ;
  wire \r38_reg_n_0_[9] ;
  wire [15:0]r47;
  wire [3:0]\r47_reg[11]_0 ;
  wire [3:0]\r47_reg[15]_0 ;
  wire [3:0]\r47_reg[3]_0 ;
  wire [3:0]\r47_reg[7]_0 ;
  wire [15:0]r6;
  wire [8:0]\r6_reg[11]_0 ;
  wire [0:0]\r6_reg[11]_1 ;
  wire [13:0]\r6_reg[15]_0 ;
  wire [0:0]\r6_reg[15]_1 ;
  wire [13:0]\r6_reg[15]_2 ;
  wire [15:0]\result[15] ;
  wire [0:0]x0_IBUF;
  wire [7:0]x_IBUF;
  wire NLW_ARG_CARRYCASCOUT_UNCONNECTED;
  wire NLW_ARG_MULTSIGNOUT_UNCONNECTED;
  wire NLW_ARG_OVERFLOW_UNCONNECTED;
  wire NLW_ARG_PATTERNBDETECT_UNCONNECTED;
  wire NLW_ARG_PATTERNDETECT_UNCONNECTED;
  wire NLW_ARG_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_ARG_ACOUT_UNCONNECTED;
  wire [17:0]NLW_ARG_BCOUT_UNCONNECTED;
  wire [3:0]NLW_ARG_CARRYOUT_UNCONNECTED;
  wire [47:32]NLW_ARG_P_UNCONNECTED;
  wire [47:0]NLW_ARG_PCOUT_UNCONNECTED;

  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    ARG
       (.A({ARG_i_1_n_0,ARG_i_1_n_0,ARG_i_1_n_0,ARG_i_1_n_0,ARG_i_1_n_0,ARG_i_1_n_0,ARG_i_1_n_0,ARG_i_1_n_0,ARG_i_1_n_0,ARG_i_1_n_0,ARG_i_1_n_0,ARG_i_1_n_0,ARG_i_1_n_0,ARG_i_1_n_0,ARG_i_1_n_0,ARG_i_2_n_0,ARG_i_3_n_0,ARG_i_4_n_0,ARG_i_5_n_0,ARG_i_6_n_0,ARG_i_7_n_0,ARG_i_8_n_0,ARG_i_9_n_0,ARG_i_10_n_0,ARG_i_11_n_0,ARG_i_12_n_0,ARG_i_13_n_0,ARG_i_14_n_0,ARG_i_15_n_0,ARG_i_16_n_0}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_ARG_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({B[15],B[15],B}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_ARG_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_ARG_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_ARG_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_ARG_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_ARG_OVERFLOW_UNCONNECTED),
        .P({NLW_ARG_P_UNCONNECTED[47:32],P[15],ARG_n_75,ARG_n_76,ARG_n_77,ARG_n_78,ARG_n_79,ARG_n_80,ARG_n_81,ARG_n_82,ARG_n_83,ARG_n_84,ARG_n_85,ARG_n_86,ARG_n_87,ARG_n_88,ARG_n_89,ARG_n_90,P[14:0]}),
        .PATTERNBDETECT(NLW_ARG_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_ARG_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_ARG_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_ARG_UNDERFLOW_UNCONNECTED));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ARG_i_1
       (.I0(ARG_0),
        .I1(\FSM_sequential_currstate_reg[3] [0]),
        .I2(r29[15]),
        .I3(\FSM_sequential_currstate_reg[3] [1]),
        .I4(Q[10]),
        .O(ARG_i_1_n_0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ARG_i_10
       (.I0(\r38_reg_n_0_[6] ),
        .I1(\FSM_sequential_currstate_reg[3] [0]),
        .I2(r29[6]),
        .I3(\FSM_sequential_currstate_reg[3] [1]),
        .I4(Q[1]),
        .O(ARG_i_10_n_0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ARG_i_11
       (.I0(\r38_reg_n_0_[5] ),
        .I1(\FSM_sequential_currstate_reg[3] [0]),
        .I2(r29[5]),
        .I3(\FSM_sequential_currstate_reg[3] [1]),
        .I4(Q[0]),
        .O(ARG_i_11_n_0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ARG_i_12
       (.I0(\r38_reg_n_0_[4] ),
        .I1(\FSM_sequential_currstate_reg[3] [0]),
        .I2(r29[4]),
        .I3(\FSM_sequential_currstate_reg[3] [1]),
        .I4(\r1_5_10_reg_n_0_[4] ),
        .O(ARG_i_12_n_0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ARG_i_13
       (.I0(\r38_reg_n_0_[3] ),
        .I1(\FSM_sequential_currstate_reg[3] [0]),
        .I2(r29[3]),
        .I3(\FSM_sequential_currstate_reg[3] [1]),
        .I4(\r1_5_10_reg_n_0_[3] ),
        .O(ARG_i_13_n_0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ARG_i_14
       (.I0(\r38_reg_n_0_[2] ),
        .I1(\FSM_sequential_currstate_reg[3] [0]),
        .I2(r29[2]),
        .I3(\FSM_sequential_currstate_reg[3] [1]),
        .I4(\r1_5_10_reg_n_0_[2] ),
        .O(ARG_i_14_n_0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ARG_i_15
       (.I0(\r38_reg_n_0_[1] ),
        .I1(\FSM_sequential_currstate_reg[3] [0]),
        .I2(r29[1]),
        .I3(\FSM_sequential_currstate_reg[3] [1]),
        .I4(\r1_5_10_reg_n_0_[1] ),
        .O(ARG_i_15_n_0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ARG_i_16
       (.I0(\r38_reg_n_0_[0] ),
        .I1(\FSM_sequential_currstate_reg[3] [0]),
        .I2(r29[0]),
        .I3(\FSM_sequential_currstate_reg[3] [1]),
        .I4(\r1_5_10_reg_n_0_[0] ),
        .O(ARG_i_16_n_0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ARG_i_2
       (.I0(\r38_reg_n_0_[14] ),
        .I1(\FSM_sequential_currstate_reg[3] [0]),
        .I2(r29[14]),
        .I3(\FSM_sequential_currstate_reg[3] [1]),
        .I4(Q[9]),
        .O(ARG_i_2_n_0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ARG_i_3
       (.I0(\r38_reg_n_0_[13] ),
        .I1(\FSM_sequential_currstate_reg[3] [0]),
        .I2(r29[13]),
        .I3(\FSM_sequential_currstate_reg[3] [1]),
        .I4(Q[8]),
        .O(ARG_i_3_n_0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ARG_i_4
       (.I0(\r38_reg_n_0_[12] ),
        .I1(\FSM_sequential_currstate_reg[3] [0]),
        .I2(r29[12]),
        .I3(\FSM_sequential_currstate_reg[3] [1]),
        .I4(Q[7]),
        .O(ARG_i_4_n_0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ARG_i_5
       (.I0(\r38_reg_n_0_[11] ),
        .I1(\FSM_sequential_currstate_reg[3] [0]),
        .I2(r29[11]),
        .I3(\FSM_sequential_currstate_reg[3] [1]),
        .I4(Q[6]),
        .O(ARG_i_5_n_0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ARG_i_6
       (.I0(\r38_reg_n_0_[10] ),
        .I1(\FSM_sequential_currstate_reg[3] [0]),
        .I2(r29[10]),
        .I3(\FSM_sequential_currstate_reg[3] [1]),
        .I4(Q[5]),
        .O(ARG_i_6_n_0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ARG_i_7
       (.I0(\r38_reg_n_0_[9] ),
        .I1(\FSM_sequential_currstate_reg[3] [0]),
        .I2(r29[9]),
        .I3(\FSM_sequential_currstate_reg[3] [1]),
        .I4(Q[4]),
        .O(ARG_i_7_n_0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ARG_i_8
       (.I0(\r38_reg_n_0_[8] ),
        .I1(\FSM_sequential_currstate_reg[3] [0]),
        .I2(r29[8]),
        .I3(\FSM_sequential_currstate_reg[3] [1]),
        .I4(Q[3]),
        .O(ARG_i_8_n_0));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    ARG_i_9
       (.I0(\r38_reg_n_0_[7] ),
        .I1(\FSM_sequential_currstate_reg[3] [0]),
        .I2(r29[7]),
        .I3(\FSM_sequential_currstate_reg[3] [1]),
        .I4(Q[2]),
        .O(ARG_i_9_n_0));
  FDRE #(
    .INIT(1'b0)) 
    done_reg
       (.C(CLK),
        .CE(1'b1),
        .D(done_reg_0),
        .Q(done),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \r1_5_10_reg[0] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_0 ),
        .D(\FSM_sequential_currstate_reg[2]_1 [0]),
        .Q(\r1_5_10_reg_n_0_[0] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r1_5_10_reg[10] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_0 ),
        .D(\FSM_sequential_currstate_reg[2]_1 [10]),
        .Q(Q[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r1_5_10_reg[11] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_0 ),
        .D(\FSM_sequential_currstate_reg[2]_1 [11]),
        .Q(Q[6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r1_5_10_reg[12] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_0 ),
        .D(\FSM_sequential_currstate_reg[2]_1 [12]),
        .Q(Q[7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r1_5_10_reg[13] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_0 ),
        .D(\FSM_sequential_currstate_reg[2]_1 [13]),
        .Q(Q[8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r1_5_10_reg[14] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_0 ),
        .D(\FSM_sequential_currstate_reg[2]_1 [14]),
        .Q(Q[9]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r1_5_10_reg[15] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_0 ),
        .D(\FSM_sequential_currstate_reg[2]_1 [15]),
        .Q(Q[10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r1_5_10_reg[1] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_0 ),
        .D(\FSM_sequential_currstate_reg[2]_1 [1]),
        .Q(\r1_5_10_reg_n_0_[1] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r1_5_10_reg[2] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_0 ),
        .D(\FSM_sequential_currstate_reg[2]_1 [2]),
        .Q(\r1_5_10_reg_n_0_[2] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r1_5_10_reg[3] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_0 ),
        .D(\FSM_sequential_currstate_reg[2]_1 [3]),
        .Q(\r1_5_10_reg_n_0_[3] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r1_5_10_reg[4] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_0 ),
        .D(\FSM_sequential_currstate_reg[2]_1 [4]),
        .Q(\r1_5_10_reg_n_0_[4] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r1_5_10_reg[5] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_0 ),
        .D(\FSM_sequential_currstate_reg[2]_1 [5]),
        .Q(Q[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r1_5_10_reg[6] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_0 ),
        .D(\FSM_sequential_currstate_reg[2]_1 [6]),
        .Q(Q[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r1_5_10_reg[7] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_0 ),
        .D(\FSM_sequential_currstate_reg[2]_1 [7]),
        .Q(Q[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r1_5_10_reg[8] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_0 ),
        .D(\FSM_sequential_currstate_reg[2]_1 [8]),
        .Q(Q[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r1_5_10_reg[9] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_0 ),
        .D(\FSM_sequential_currstate_reg[2]_1 [9]),
        .Q(Q[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r29_reg[0] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3]_0 ),
        .D(\FSM_sequential_currstate_reg[2] [0]),
        .Q(r29[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r29_reg[10] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3]_0 ),
        .D(\FSM_sequential_currstate_reg[2] [10]),
        .Q(r29[10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r29_reg[11] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3]_0 ),
        .D(\FSM_sequential_currstate_reg[2] [11]),
        .Q(r29[11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r29_reg[12] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3]_0 ),
        .D(\FSM_sequential_currstate_reg[2] [12]),
        .Q(r29[12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r29_reg[13] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3]_0 ),
        .D(\FSM_sequential_currstate_reg[2] [13]),
        .Q(r29[13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r29_reg[14] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3]_0 ),
        .D(\FSM_sequential_currstate_reg[2] [14]),
        .Q(r29[14]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r29_reg[15] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3]_0 ),
        .D(\FSM_sequential_currstate_reg[2] [15]),
        .Q(r29[15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r29_reg[1] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3]_0 ),
        .D(\FSM_sequential_currstate_reg[2] [1]),
        .Q(r29[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r29_reg[2] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3]_0 ),
        .D(\FSM_sequential_currstate_reg[2] [2]),
        .Q(r29[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r29_reg[3] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3]_0 ),
        .D(\FSM_sequential_currstate_reg[2] [3]),
        .Q(r29[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r29_reg[4] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3]_0 ),
        .D(\FSM_sequential_currstate_reg[2] [4]),
        .Q(r29[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r29_reg[5] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3]_0 ),
        .D(\FSM_sequential_currstate_reg[2] [5]),
        .Q(r29[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r29_reg[6] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3]_0 ),
        .D(\FSM_sequential_currstate_reg[2] [6]),
        .Q(r29[6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r29_reg[7] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3]_0 ),
        .D(\FSM_sequential_currstate_reg[2] [7]),
        .Q(r29[7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r29_reg[8] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3]_0 ),
        .D(\FSM_sequential_currstate_reg[2] [8]),
        .Q(r29[8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r29_reg[9] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3]_0 ),
        .D(\FSM_sequential_currstate_reg[2] [9]),
        .Q(r29[9]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r38_reg[0] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3] [3]),
        .D(ARG_1[0]),
        .Q(\r38_reg_n_0_[0] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r38_reg[10] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3] [3]),
        .D(ARG_1[10]),
        .Q(\r38_reg_n_0_[10] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r38_reg[11] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3] [3]),
        .D(ARG_1[11]),
        .Q(\r38_reg_n_0_[11] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r38_reg[12] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3] [3]),
        .D(ARG_1[12]),
        .Q(\r38_reg_n_0_[12] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r38_reg[13] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3] [3]),
        .D(ARG_1[13]),
        .Q(\r38_reg_n_0_[13] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r38_reg[14] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3] [3]),
        .D(ARG_1[14]),
        .Q(\r38_reg_n_0_[14] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r38_reg[15] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3] [3]),
        .D(ARG_1[15]),
        .Q(ARG_0),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r38_reg[1] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3] [3]),
        .D(ARG_1[1]),
        .Q(\r38_reg_n_0_[1] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r38_reg[2] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3] [3]),
        .D(ARG_1[2]),
        .Q(\r38_reg_n_0_[2] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r38_reg[3] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3] [3]),
        .D(ARG_1[3]),
        .Q(\r38_reg_n_0_[3] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r38_reg[4] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3] [3]),
        .D(ARG_1[4]),
        .Q(\r38_reg_n_0_[4] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r38_reg[5] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3] [3]),
        .D(ARG_1[5]),
        .Q(\r38_reg_n_0_[5] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r38_reg[6] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3] [3]),
        .D(ARG_1[6]),
        .Q(\r38_reg_n_0_[6] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r38_reg[7] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3] [3]),
        .D(ARG_1[7]),
        .Q(\r38_reg_n_0_[7] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r38_reg[8] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3] [3]),
        .D(ARG_1[8]),
        .Q(\r38_reg_n_0_[8] ),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r38_reg[9] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[3] [3]),
        .D(ARG_1[9]),
        .Q(\r38_reg_n_0_[9] ),
        .R(SR));
  LUT2 #(
    .INIT(4'h6)) 
    \r47[11]_i_4 
       (.I0(mux1[10]),
        .I1(\FSM_sequential_currstate_reg[3]_1 ),
        .O(\r47_reg[11]_0 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    \r47[11]_i_5 
       (.I0(mux1[10]),
        .I1(\FSM_sequential_currstate_reg[3]_1 ),
        .O(\r47_reg[11]_0 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    \r47[11]_i_6 
       (.I0(mux1[9]),
        .I1(\FSM_sequential_currstate_reg[3]_1 ),
        .O(\r47_reg[11]_0 [1]));
  LUT6 #(
    .INIT(64'h9999999999969999)) 
    \r47[11]_i_7 
       (.I0(mux1[8]),
        .I1(\FSM_sequential_currstate_reg[2]_11 ),
        .I2(out[3]),
        .I3(out[0]),
        .I4(out[2]),
        .I5(out[1]),
        .O(\r47_reg[11]_0 [0]));
  LUT2 #(
    .INIT(4'h6)) 
    \r47[15]_i_4 
       (.I0(mux1[10]),
        .I1(\FSM_sequential_currstate_reg[3]_1 ),
        .O(\r47_reg[15]_0 [3]));
  LUT2 #(
    .INIT(4'h6)) 
    \r47[15]_i_5 
       (.I0(mux1[10]),
        .I1(\FSM_sequential_currstate_reg[3]_1 ),
        .O(\r47_reg[15]_0 [2]));
  LUT2 #(
    .INIT(4'h6)) 
    \r47[15]_i_6 
       (.I0(mux1[10]),
        .I1(\FSM_sequential_currstate_reg[3]_1 ),
        .O(\r47_reg[15]_0 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    \r47[15]_i_7 
       (.I0(mux1[10]),
        .I1(\FSM_sequential_currstate_reg[3]_1 ),
        .O(\r47_reg[15]_0 [0]));
  LUT6 #(
    .INIT(64'h9999999999969999)) 
    \r47[3]_i_10 
       (.I0(mux1[0]),
        .I1(\FSM_sequential_currstate_reg[2]_3 ),
        .I2(out[3]),
        .I3(out[0]),
        .I4(out[2]),
        .I5(out[1]),
        .O(\r47_reg[3]_0 [0]));
  LUT6 #(
    .INIT(64'h9999999999969999)) 
    \r47[3]_i_7 
       (.I0(mux1[3]),
        .I1(\FSM_sequential_currstate_reg[2]_6 ),
        .I2(out[3]),
        .I3(out[0]),
        .I4(out[2]),
        .I5(out[1]),
        .O(\r47_reg[3]_0 [3]));
  LUT6 #(
    .INIT(64'h9999999999969999)) 
    \r47[3]_i_8 
       (.I0(mux1[2]),
        .I1(\FSM_sequential_currstate_reg[2]_5 ),
        .I2(out[3]),
        .I3(out[0]),
        .I4(out[2]),
        .I5(out[1]),
        .O(\r47_reg[3]_0 [2]));
  LUT6 #(
    .INIT(64'h9999999999969999)) 
    \r47[3]_i_9 
       (.I0(mux1[1]),
        .I1(\FSM_sequential_currstate_reg[2]_4 ),
        .I2(out[3]),
        .I3(out[0]),
        .I4(out[2]),
        .I5(out[1]),
        .O(\r47_reg[3]_0 [1]));
  LUT6 #(
    .INIT(64'h9999999999969999)) 
    \r47[7]_i_6 
       (.I0(mux1[7]),
        .I1(\FSM_sequential_currstate_reg[2]_10 ),
        .I2(out[3]),
        .I3(out[0]),
        .I4(out[2]),
        .I5(out[1]),
        .O(\r47_reg[7]_0 [3]));
  LUT6 #(
    .INIT(64'h9999999999969999)) 
    \r47[7]_i_7 
       (.I0(mux1[6]),
        .I1(\FSM_sequential_currstate_reg[2]_9 ),
        .I2(out[3]),
        .I3(out[0]),
        .I4(out[2]),
        .I5(out[1]),
        .O(\r47_reg[7]_0 [2]));
  LUT6 #(
    .INIT(64'h9999999999969999)) 
    \r47[7]_i_8 
       (.I0(mux1[5]),
        .I1(\FSM_sequential_currstate_reg[2]_8 ),
        .I2(out[3]),
        .I3(out[0]),
        .I4(out[2]),
        .I5(out[1]),
        .O(\r47_reg[7]_0 [1]));
  LUT6 #(
    .INIT(64'h9999999999969999)) 
    \r47[7]_i_9 
       (.I0(mux1[4]),
        .I1(\FSM_sequential_currstate_reg[2]_7 ),
        .I2(out[3]),
        .I3(out[0]),
        .I4(out[2]),
        .I5(out[1]),
        .O(\r47_reg[7]_0 [0]));
  FDRE #(
    .INIT(1'b0)) 
    \r47_reg[0] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(B[0]),
        .Q(r47[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r47_reg[10] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(B[10]),
        .Q(\r6_reg[15]_2 [9]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r47_reg[11] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(B[11]),
        .Q(\r6_reg[15]_2 [10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r47_reg[12] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(B[12]),
        .Q(\r6_reg[15]_2 [11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r47_reg[13] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(B[13]),
        .Q(\r6_reg[15]_2 [12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r47_reg[14] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(B[14]),
        .Q(\r6_reg[15]_2 [13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r47_reg[15] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(B[15]),
        .Q(r47[15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r47_reg[1] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(B[1]),
        .Q(\r6_reg[15]_2 [0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r47_reg[2] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(B[2]),
        .Q(\r6_reg[15]_2 [1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r47_reg[3] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(B[3]),
        .Q(\r6_reg[15]_2 [2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r47_reg[4] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(B[4]),
        .Q(\r6_reg[15]_2 [3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r47_reg[5] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(B[5]),
        .Q(\r6_reg[15]_2 [4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r47_reg[6] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(B[6]),
        .Q(\r6_reg[15]_2 [5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r47_reg[7] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(B[7]),
        .Q(\r6_reg[15]_2 [6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r47_reg[8] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(B[8]),
        .Q(\r6_reg[15]_2 [7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r47_reg[9] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(B[9]),
        .Q(\r6_reg[15]_2 [8]),
        .R(SR));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r6[11]_i_10 
       (.I0(Q[9]),
        .I1(\r6_reg[15]_0 [8]),
        .I2(\FSM_sequential_currstate_reg[3] [2]),
        .I3(\r38_reg_n_0_[14] ),
        .I4(ctrl),
        .I5(x_IBUF[7]),
        .O(A));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r6[11]_i_11 
       (.I0(Q[8]),
        .I1(\r6_reg[15]_0 [7]),
        .I2(\FSM_sequential_currstate_reg[3] [2]),
        .I3(\r38_reg_n_0_[13] ),
        .I4(ctrl),
        .I5(x_IBUF[7]),
        .O(\r6_reg[11]_0 [8]));
  LUT6 #(
    .INIT(64'h743374CC8BCC8B33)) 
    \r6[11]_i_8 
       (.I0(\r6_reg[15]_0 [8]),
        .I1(ctrl),
        .I2(\r6_reg[15]_2 [8]),
        .I3(\FSM_sequential_currstate_reg[3] [2]),
        .I4(muxB3),
        .I5(A),
        .O(\r6_reg[11]_1 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r6[15]_i_13 
       (.I0(Q[10]),
        .I1(r6[15]),
        .I2(\FSM_sequential_currstate_reg[3] [2]),
        .I3(ARG_0),
        .I4(ctrl),
        .I5(x_IBUF[7]),
        .O(A__0));
  LUT6 #(
    .INIT(64'h743374CC8BCC8B33)) 
    \r6[15]_i_6 
       (.I0(r6[15]),
        .I1(ctrl),
        .I2(r47[15]),
        .I3(\FSM_sequential_currstate_reg[3] [2]),
        .I4(muxB3),
        .I5(A__0),
        .O(\r6_reg[15]_1 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r6[3]_i_10 
       (.I0(r6[0]),
        .I1(r47[0]),
        .I2(\FSM_sequential_currstate_reg[3] [2]),
        .I3(q00_IBUF),
        .I4(ctrl),
        .I5(x0_IBUF),
        .O(S));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r6[3]_i_11 
       (.I0(Q[3]),
        .I1(\r6_reg[15]_0 [2]),
        .I2(\FSM_sequential_currstate_reg[3] [2]),
        .I3(\r38_reg_n_0_[8] ),
        .I4(ctrl),
        .I5(x_IBUF[3]),
        .O(\r6_reg[11]_0 [3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r6[3]_i_12 
       (.I0(Q[2]),
        .I1(\r6_reg[15]_0 [1]),
        .I2(\FSM_sequential_currstate_reg[3] [2]),
        .I3(\r38_reg_n_0_[7] ),
        .I4(ctrl),
        .I5(x_IBUF[2]),
        .O(\r6_reg[11]_0 [2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r6[3]_i_13 
       (.I0(Q[1]),
        .I1(\r6_reg[15]_0 [0]),
        .I2(\FSM_sequential_currstate_reg[3] [2]),
        .I3(\r38_reg_n_0_[6] ),
        .I4(ctrl),
        .I5(x_IBUF[1]),
        .O(\r6_reg[11]_0 [1]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r6[3]_i_2 
       (.I0(Q[0]),
        .I1(r6[0]),
        .I2(\FSM_sequential_currstate_reg[3] [2]),
        .I3(\r38_reg_n_0_[5] ),
        .I4(ctrl),
        .I5(x_IBUF[0]),
        .O(\r6_reg[11]_0 [0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r6[7]_i_10 
       (.I0(Q[7]),
        .I1(\r6_reg[15]_0 [6]),
        .I2(\FSM_sequential_currstate_reg[3] [2]),
        .I3(\r38_reg_n_0_[12] ),
        .I4(ctrl),
        .I5(x_IBUF[7]),
        .O(\r6_reg[11]_0 [7]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r6[7]_i_11 
       (.I0(Q[6]),
        .I1(\r6_reg[15]_0 [5]),
        .I2(\FSM_sequential_currstate_reg[3] [2]),
        .I3(\r38_reg_n_0_[11] ),
        .I4(ctrl),
        .I5(x_IBUF[6]),
        .O(\r6_reg[11]_0 [6]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r6[7]_i_12 
       (.I0(Q[5]),
        .I1(\r6_reg[15]_0 [4]),
        .I2(\FSM_sequential_currstate_reg[3] [2]),
        .I3(\r38_reg_n_0_[10] ),
        .I4(ctrl),
        .I5(x_IBUF[5]),
        .O(\r6_reg[11]_0 [5]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \r6[7]_i_13 
       (.I0(Q[4]),
        .I1(\r6_reg[15]_0 [3]),
        .I2(\FSM_sequential_currstate_reg[3] [2]),
        .I3(\r38_reg_n_0_[9] ),
        .I4(ctrl),
        .I5(x_IBUF[4]),
        .O(\r6_reg[11]_0 [4]));
  FDRE #(
    .INIT(1'b0)) 
    \r6_reg[0] 
       (.C(CLK),
        .CE(E),
        .D(D[0]),
        .Q(r6[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r6_reg[10] 
       (.C(CLK),
        .CE(E),
        .D(D[10]),
        .Q(\r6_reg[15]_0 [9]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r6_reg[11] 
       (.C(CLK),
        .CE(E),
        .D(D[11]),
        .Q(\r6_reg[15]_0 [10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r6_reg[12] 
       (.C(CLK),
        .CE(E),
        .D(D[12]),
        .Q(\r6_reg[15]_0 [11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r6_reg[13] 
       (.C(CLK),
        .CE(E),
        .D(D[13]),
        .Q(\r6_reg[15]_0 [12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r6_reg[14] 
       (.C(CLK),
        .CE(E),
        .D(D[14]),
        .Q(\r6_reg[15]_0 [13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r6_reg[15] 
       (.C(CLK),
        .CE(E),
        .D(D[15]),
        .Q(r6[15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r6_reg[1] 
       (.C(CLK),
        .CE(E),
        .D(D[1]),
        .Q(\r6_reg[15]_0 [0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r6_reg[2] 
       (.C(CLK),
        .CE(E),
        .D(D[2]),
        .Q(\r6_reg[15]_0 [1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r6_reg[3] 
       (.C(CLK),
        .CE(E),
        .D(D[3]),
        .Q(\r6_reg[15]_0 [2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r6_reg[4] 
       (.C(CLK),
        .CE(E),
        .D(D[4]),
        .Q(\r6_reg[15]_0 [3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r6_reg[5] 
       (.C(CLK),
        .CE(E),
        .D(D[5]),
        .Q(\r6_reg[15]_0 [4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r6_reg[6] 
       (.C(CLK),
        .CE(E),
        .D(D[6]),
        .Q(\r6_reg[15]_0 [5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r6_reg[7] 
       (.C(CLK),
        .CE(E),
        .D(D[7]),
        .Q(\r6_reg[15]_0 [6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r6_reg[8] 
       (.C(CLK),
        .CE(E),
        .D(D[8]),
        .Q(\r6_reg[15]_0 [7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \r6_reg[9] 
       (.C(CLK),
        .CE(E),
        .D(D[9]),
        .Q(\r6_reg[15]_0 [8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \rP_reg[0] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_2 ),
        .D(B[0]),
        .Q(\result[15] [0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \rP_reg[10] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_2 ),
        .D(B[10]),
        .Q(\result[15] [10]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \rP_reg[11] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_2 ),
        .D(B[11]),
        .Q(\result[15] [11]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \rP_reg[12] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_2 ),
        .D(B[12]),
        .Q(\result[15] [12]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \rP_reg[13] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_2 ),
        .D(B[13]),
        .Q(\result[15] [13]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \rP_reg[14] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_2 ),
        .D(B[14]),
        .Q(\result[15] [14]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \rP_reg[15] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_2 ),
        .D(B[15]),
        .Q(\result[15] [15]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \rP_reg[1] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_2 ),
        .D(B[1]),
        .Q(\result[15] [1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \rP_reg[2] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_2 ),
        .D(B[2]),
        .Q(\result[15] [2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \rP_reg[3] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_2 ),
        .D(B[3]),
        .Q(\result[15] [3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \rP_reg[4] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_2 ),
        .D(B[4]),
        .Q(\result[15] [4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \rP_reg[5] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_2 ),
        .D(B[5]),
        .Q(\result[15] [5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \rP_reg[6] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_2 ),
        .D(B[6]),
        .Q(\result[15] [6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \rP_reg[7] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_2 ),
        .D(B[7]),
        .Q(\result[15] [7]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \rP_reg[8] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_2 ),
        .D(B[8]),
        .Q(\result[15] [8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \rP_reg[9] 
       (.C(CLK),
        .CE(\FSM_sequential_currstate_reg[2]_2 ),
        .D(B[9]),
        .Q(\result[15] [9]),
        .R(SR));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

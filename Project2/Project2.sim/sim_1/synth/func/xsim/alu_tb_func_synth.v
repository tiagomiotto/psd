// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Mon Oct 22 10:50:34 2018
// Host        : LAPTOP-TIAGO running 64-bit major release  (build 9200)
// Command     : write_verilog -mode funcsim -nolib -force -file
//               C:/Users/tiago/Desktop/Programing/git-repos/psd/Project_ALU/Project_ALU.sim/sim_1/synth/func/xsim/alu_tb_func_synth.v
// Design      : fpga_basicIO
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module alu
   (seg_OBUF,
    clk_IBUF_BUFG,
    btnCreg,
    Q,
    \ndisp_reg[1] ,
    oper);
  output [6:0]seg_OBUF;
  input clk_IBUF_BUFG;
  input btnCreg;
  input [15:0]Q;
  input [1:0]\ndisp_reg[1] ;
  input [3:0]oper;

  wire [15:0]Q;
  wire btnCreg;
  wire clk_IBUF_BUFG;
  wire inst_alu_control_n_0;
  wire inst_alu_control_n_1;
  wire inst_alu_control_n_2;
  wire inst_alu_control_n_3;
  wire inst_alu_control_n_4;
  wire inst_alu_control_n_5;
  wire [1:0]\ndisp_reg[1] ;
  wire [3:0]oper;
  wire [6:0]seg_OBUF;

  alu_control inst_alu_control
       (.E(inst_alu_control_n_4),
        .Q(Q[15:12]),
        .SR(inst_alu_control_n_3),
        .btnCreg(btnCreg),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .oper(oper),
        .out({inst_alu_control_n_0,inst_alu_control_n_1,inst_alu_control_n_2}),
        .\r2_reg[15] (inst_alu_control_n_5));
  alu_datapath inst_alu_datapath
       (.E(inst_alu_control_n_5),
        .\FSM_sequential_currstate_reg[1] (inst_alu_control_n_4),
        .Q({Q[15:14],Q[11:0]}),
        .SR({inst_alu_control_n_3,btnCreg}),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .\ndisp_reg[1] (\ndisp_reg[1] ),
        .out({inst_alu_control_n_0,inst_alu_control_n_1,inst_alu_control_n_2}),
        .seg_OBUF(seg_OBUF));
endmodule

module alu_control
   (out,
    SR,
    E,
    \r2_reg[15] ,
    Q,
    btnCreg,
    oper,
    clk_IBUF_BUFG);
  output [2:0]out;
  output [0:0]SR;
  output [0:0]E;
  output [0:0]\r2_reg[15] ;
  input [3:0]Q;
  input btnCreg;
  input [3:0]oper;
  input clk_IBUF_BUFG;

  wire [0:0]E;
  wire \FSM_sequential_currstate[0]_i_1_n_0 ;
  wire \FSM_sequential_currstate[0]_i_3_n_0 ;
  wire \FSM_sequential_currstate[1]_i_1_n_0 ;
  wire \FSM_sequential_currstate[1]_i_3_n_0 ;
  wire \FSM_sequential_currstate[2]_i_1_n_0 ;
  wire \FSM_sequential_currstate[2]_i_2_n_0 ;
  wire \FSM_sequential_currstate[2]_i_4_n_0 ;
  wire \FSM_sequential_currstate[2]_i_5_n_0 ;
  wire \FSM_sequential_currstate[2]_i_6_n_0 ;
  wire [3:0]Q;
  wire [0:0]SR;
  wire btnCreg;
  wire clk_IBUF_BUFG;
  wire [2:0]nextstate;
  wire [3:0]oper;
  (* RTL_KEEP = "yes" *) wire [2:0]out;
  wire [0:0]\r2_reg[15] ;

  LUT4 #(
    .INIT(16'h00E2)) 
    \FSM_sequential_currstate[0]_i_1 
       (.I0(out[0]),
        .I1(\FSM_sequential_currstate[2]_i_2_n_0 ),
        .I2(nextstate[0]),
        .I3(btnCreg),
        .O(\FSM_sequential_currstate[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h77FE)) 
    \FSM_sequential_currstate[0]_i_2 
       (.I0(out[2]),
        .I1(out[0]),
        .I2(\FSM_sequential_currstate[0]_i_3_n_0 ),
        .I3(out[1]),
        .O(nextstate[0]));
  LUT6 #(
    .INIT(64'h0000000000000112)) 
    \FSM_sequential_currstate[0]_i_3 
       (.I0(Q[0]),
        .I1(oper[1]),
        .I2(oper[0]),
        .I3(oper[2]),
        .I4(oper[3]),
        .I5(Q[1]),
        .O(\FSM_sequential_currstate[0]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \FSM_sequential_currstate[1]_i_1 
       (.I0(out[1]),
        .I1(\FSM_sequential_currstate[2]_i_2_n_0 ),
        .I2(nextstate[1]),
        .I3(btnCreg),
        .O(\FSM_sequential_currstate[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h77FE)) 
    \FSM_sequential_currstate[1]_i_2 
       (.I0(out[2]),
        .I1(out[0]),
        .I2(\FSM_sequential_currstate[1]_i_3_n_0 ),
        .I3(out[1]),
        .O(nextstate[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFEFD)) 
    \FSM_sequential_currstate[1]_i_3 
       (.I0(Q[0]),
        .I1(oper[3]),
        .I2(oper[0]),
        .I3(oper[2]),
        .I4(oper[1]),
        .I5(Q[1]),
        .O(\FSM_sequential_currstate[1]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \FSM_sequential_currstate[2]_i_1 
       (.I0(out[2]),
        .I1(\FSM_sequential_currstate[2]_i_2_n_0 ),
        .I2(nextstate[2]),
        .I3(btnCreg),
        .O(\FSM_sequential_currstate[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2FFF2FFFFFFFFFF0)) 
    \FSM_sequential_currstate[2]_i_2 
       (.I0(\FSM_sequential_currstate[2]_i_4_n_0 ),
        .I1(Q[0]),
        .I2(out[2]),
        .I3(out[0]),
        .I4(\FSM_sequential_currstate[2]_i_5_n_0 ),
        .I5(out[1]),
        .O(\FSM_sequential_currstate[2]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h77FE)) 
    \FSM_sequential_currstate[2]_i_3 
       (.I0(out[2]),
        .I1(out[0]),
        .I2(\FSM_sequential_currstate[2]_i_6_n_0 ),
        .I3(out[1]),
        .O(nextstate[2]));
  LUT5 #(
    .INIT(32'h00000001)) 
    \FSM_sequential_currstate[2]_i_4 
       (.I0(oper[1]),
        .I1(oper[2]),
        .I2(oper[0]),
        .I3(oper[3]),
        .I4(Q[1]),
        .O(\FSM_sequential_currstate[2]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h0000000100010116)) 
    \FSM_sequential_currstate[2]_i_5 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(oper[1]),
        .I3(oper[3]),
        .I4(oper[2]),
        .I5(oper[0]),
        .O(\FSM_sequential_currstate[2]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFF9)) 
    \FSM_sequential_currstate[2]_i_6 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(oper[3]),
        .I3(oper[2]),
        .I4(oper[0]),
        .I5(oper[1]),
        .O(\FSM_sequential_currstate[2]_i_6_n_0 ));
  (* FSM_ENCODED_STATES = "s_initial:000,s_end:111,s_shft:110,s_mul:101,s_or:100,s_and:011,s_sub:010,s_add:001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_currstate_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_sequential_currstate[0]_i_1_n_0 ),
        .Q(out[0]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "s_initial:000,s_end:111,s_shft:110,s_mul:101,s_or:100,s_and:011,s_sub:010,s_add:001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_currstate_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_sequential_currstate[1]_i_1_n_0 ),
        .Q(out[1]),
        .R(1'b0));
  (* FSM_ENCODED_STATES = "s_initial:000,s_end:111,s_shft:110,s_mul:101,s_or:100,s_and:011,s_sub:010,s_add:001" *) 
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_currstate_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\FSM_sequential_currstate[2]_i_1_n_0 ),
        .Q(out[2]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h7E)) 
    \alusg_reg[15]_i_2 
       (.I0(out[2]),
        .I1(out[0]),
        .I2(out[1]),
        .O(\r2_reg[15] ));
  LUT6 #(
    .INIT(64'hFFFFFFFFF0F0F0E0)) 
    \r2[15]_i_1 
       (.I0(out[2]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(out[0]),
        .I4(out[1]),
        .I5(btnCreg),
        .O(SR));
  LUT5 #(
    .INIT(32'hFFFFFFEA)) 
    \r2[15]_i_2 
       (.I0(out[1]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(out[0]),
        .I4(out[2]),
        .O(E));
endmodule

module alu_datapath
   (seg_OBUF,
    clk_IBUF_BUFG,
    SR,
    Q,
    \ndisp_reg[1] ,
    out,
    E,
    \FSM_sequential_currstate_reg[1] );
  output [6:0]seg_OBUF;
  input clk_IBUF_BUFG;
  input [1:0]SR;
  input [13:0]Q;
  input [1:0]\ndisp_reg[1] ;
  input [2:0]out;
  input [0:0]E;
  input [0:0]\FSM_sequential_currstate_reg[1] ;

  wire [2:0]COUNT;
  wire [0:0]E;
  wire [0:0]\FSM_sequential_currstate_reg[1] ;
  wire [13:0]Q;
  wire \SHIFT_RIGHT2_inferred__0/i__carry__0_n_7 ;
  wire \SHIFT_RIGHT2_inferred__0/i__carry_n_0 ;
  wire \SHIFT_RIGHT2_inferred__0/i__carry_n_1 ;
  wire \SHIFT_RIGHT2_inferred__0/i__carry_n_2 ;
  wire \SHIFT_RIGHT2_inferred__0/i__carry_n_3 ;
  wire \SHIFT_RIGHT2_inferred__0/i__carry_n_4 ;
  wire \SHIFT_RIGHT2_inferred__0/i__carry_n_5 ;
  wire \SHIFT_RIGHT2_inferred__0/i__carry_n_6 ;
  wire \SHIFT_RIGHT2_inferred__0/i__carry_n_7 ;
  wire [1:0]SR;
  wire [15:0]alusg;
  wire [15:0]alusg0__0;
  wire \alusg0_inferred__0/i__carry__0_n_0 ;
  wire \alusg0_inferred__0/i__carry__0_n_1 ;
  wire \alusg0_inferred__0/i__carry__0_n_2 ;
  wire \alusg0_inferred__0/i__carry__0_n_3 ;
  wire \alusg0_inferred__0/i__carry__1_n_0 ;
  wire \alusg0_inferred__0/i__carry__1_n_1 ;
  wire \alusg0_inferred__0/i__carry__1_n_2 ;
  wire \alusg0_inferred__0/i__carry__1_n_3 ;
  wire \alusg0_inferred__0/i__carry__2_n_1 ;
  wire \alusg0_inferred__0/i__carry__2_n_2 ;
  wire \alusg0_inferred__0/i__carry__2_n_3 ;
  wire \alusg0_inferred__0/i__carry_n_0 ;
  wire \alusg0_inferred__0/i__carry_n_1 ;
  wire \alusg0_inferred__0/i__carry_n_2 ;
  wire \alusg0_inferred__0/i__carry_n_3 ;
  wire alusg0_n_100;
  wire alusg0_n_101;
  wire alusg0_n_102;
  wire alusg0_n_103;
  wire alusg0_n_104;
  wire alusg0_n_105;
  wire alusg0_n_74;
  wire alusg0_n_75;
  wire alusg0_n_76;
  wire alusg0_n_77;
  wire alusg0_n_78;
  wire alusg0_n_79;
  wire alusg0_n_80;
  wire alusg0_n_81;
  wire alusg0_n_82;
  wire alusg0_n_83;
  wire alusg0_n_84;
  wire alusg0_n_85;
  wire alusg0_n_86;
  wire alusg0_n_87;
  wire alusg0_n_88;
  wire alusg0_n_89;
  wire alusg0_n_90;
  wire alusg0_n_91;
  wire alusg0_n_92;
  wire alusg0_n_93;
  wire alusg0_n_94;
  wire alusg0_n_95;
  wire alusg0_n_96;
  wire alusg0_n_97;
  wire alusg0_n_98;
  wire alusg0_n_99;
  wire \alusg_reg[0]_i_1_n_0 ;
  wire \alusg_reg[0]_i_2_n_0 ;
  wire \alusg_reg[0]_i_3_n_0 ;
  wire \alusg_reg[0]_i_4_n_0 ;
  wire \alusg_reg[0]_i_5_n_0 ;
  wire \alusg_reg[0]_i_6_n_0 ;
  wire \alusg_reg[10]_i_1_n_0 ;
  wire \alusg_reg[10]_i_2_n_0 ;
  wire \alusg_reg[10]_i_3_n_0 ;
  wire \alusg_reg[10]_i_4_n_0 ;
  wire \alusg_reg[10]_i_5_n_0 ;
  wire \alusg_reg[10]_i_6_n_0 ;
  wire \alusg_reg[10]_i_7_n_0 ;
  wire \alusg_reg[11]_i_10_n_0 ;
  wire \alusg_reg[11]_i_11_n_0 ;
  wire \alusg_reg[11]_i_12_n_0 ;
  wire \alusg_reg[11]_i_1_n_0 ;
  wire \alusg_reg[11]_i_2_n_0 ;
  wire \alusg_reg[11]_i_3_n_0 ;
  wire \alusg_reg[11]_i_4_n_0 ;
  wire \alusg_reg[11]_i_5_n_0 ;
  wire \alusg_reg[11]_i_5_n_1 ;
  wire \alusg_reg[11]_i_5_n_2 ;
  wire \alusg_reg[11]_i_5_n_3 ;
  wire \alusg_reg[11]_i_6_n_0 ;
  wire \alusg_reg[11]_i_7_n_0 ;
  wire \alusg_reg[11]_i_8_n_0 ;
  wire \alusg_reg[11]_i_9_n_0 ;
  wire \alusg_reg[12]_i_1_n_0 ;
  wire \alusg_reg[12]_i_2_n_0 ;
  wire \alusg_reg[12]_i_3_n_0 ;
  wire \alusg_reg[12]_i_4_n_0 ;
  wire \alusg_reg[12]_i_5_n_0 ;
  wire \alusg_reg[12]_i_6_n_0 ;
  wire \alusg_reg[13]_i_1_n_0 ;
  wire \alusg_reg[13]_i_2_n_0 ;
  wire \alusg_reg[13]_i_3_n_0 ;
  wire \alusg_reg[13]_i_4_n_0 ;
  wire \alusg_reg[13]_i_5_n_0 ;
  wire \alusg_reg[13]_i_6_n_0 ;
  wire \alusg_reg[14]_i_1_n_0 ;
  wire \alusg_reg[14]_i_2_n_0 ;
  wire \alusg_reg[14]_i_3_n_0 ;
  wire \alusg_reg[14]_i_4_n_0 ;
  wire \alusg_reg[14]_i_5_n_0 ;
  wire \alusg_reg[14]_i_6_n_0 ;
  wire \alusg_reg[15]_i_1_n_0 ;
  wire \alusg_reg[15]_i_3_n_0 ;
  wire \alusg_reg[15]_i_4_n_0 ;
  wire \alusg_reg[15]_i_5_n_1 ;
  wire \alusg_reg[15]_i_5_n_2 ;
  wire \alusg_reg[15]_i_5_n_3 ;
  wire \alusg_reg[15]_i_6_n_0 ;
  wire \alusg_reg[15]_i_7_n_0 ;
  wire \alusg_reg[15]_i_8_n_0 ;
  wire \alusg_reg[1]_i_1_n_0 ;
  wire \alusg_reg[1]_i_2_n_0 ;
  wire \alusg_reg[1]_i_3_n_0 ;
  wire \alusg_reg[1]_i_4_n_0 ;
  wire \alusg_reg[1]_i_5_n_0 ;
  wire \alusg_reg[1]_i_6_n_0 ;
  wire \alusg_reg[2]_i_1_n_0 ;
  wire \alusg_reg[2]_i_2_n_0 ;
  wire \alusg_reg[2]_i_3_n_0 ;
  wire \alusg_reg[2]_i_4_n_0 ;
  wire \alusg_reg[2]_i_5_n_0 ;
  wire \alusg_reg[2]_i_6_n_0 ;
  wire \alusg_reg[2]_i_7_n_0 ;
  wire \alusg_reg[3]_i_10_n_0 ;
  wire \alusg_reg[3]_i_11_n_0 ;
  wire \alusg_reg[3]_i_12_n_0 ;
  wire \alusg_reg[3]_i_1_n_0 ;
  wire \alusg_reg[3]_i_2_n_0 ;
  wire \alusg_reg[3]_i_3_n_0 ;
  wire \alusg_reg[3]_i_4_n_0 ;
  wire \alusg_reg[3]_i_5_n_0 ;
  wire \alusg_reg[3]_i_5_n_1 ;
  wire \alusg_reg[3]_i_5_n_2 ;
  wire \alusg_reg[3]_i_5_n_3 ;
  wire \alusg_reg[3]_i_6_n_0 ;
  wire \alusg_reg[3]_i_7_n_0 ;
  wire \alusg_reg[3]_i_8_n_0 ;
  wire \alusg_reg[3]_i_9_n_0 ;
  wire \alusg_reg[4]_i_1_n_0 ;
  wire \alusg_reg[4]_i_2_n_0 ;
  wire \alusg_reg[4]_i_3_n_0 ;
  wire \alusg_reg[4]_i_4_n_0 ;
  wire \alusg_reg[4]_i_5_n_0 ;
  wire \alusg_reg[4]_i_6_n_0 ;
  wire \alusg_reg[4]_i_7_n_0 ;
  wire \alusg_reg[4]_i_8_n_0 ;
  wire \alusg_reg[5]_i_1_n_0 ;
  wire \alusg_reg[5]_i_2_n_0 ;
  wire \alusg_reg[5]_i_3_n_0 ;
  wire \alusg_reg[5]_i_4_n_0 ;
  wire \alusg_reg[5]_i_5_n_0 ;
  wire \alusg_reg[5]_i_6_n_0 ;
  wire \alusg_reg[5]_i_7_n_0 ;
  wire \alusg_reg[6]_i_1_n_0 ;
  wire \alusg_reg[6]_i_2_n_0 ;
  wire \alusg_reg[6]_i_3_n_0 ;
  wire \alusg_reg[6]_i_4_n_0 ;
  wire \alusg_reg[6]_i_5_n_0 ;
  wire \alusg_reg[6]_i_6_n_0 ;
  wire \alusg_reg[6]_i_7_n_0 ;
  wire \alusg_reg[7]_i_10_n_0 ;
  wire \alusg_reg[7]_i_11_n_0 ;
  wire \alusg_reg[7]_i_12_n_0 ;
  wire \alusg_reg[7]_i_1_n_0 ;
  wire \alusg_reg[7]_i_2_n_0 ;
  wire \alusg_reg[7]_i_3_n_0 ;
  wire \alusg_reg[7]_i_4_n_0 ;
  wire \alusg_reg[7]_i_5_n_0 ;
  wire \alusg_reg[7]_i_5_n_1 ;
  wire \alusg_reg[7]_i_5_n_2 ;
  wire \alusg_reg[7]_i_5_n_3 ;
  wire \alusg_reg[7]_i_6_n_0 ;
  wire \alusg_reg[7]_i_7_n_0 ;
  wire \alusg_reg[7]_i_8_n_0 ;
  wire \alusg_reg[7]_i_9_n_0 ;
  wire \alusg_reg[8]_i_1_n_0 ;
  wire \alusg_reg[8]_i_2_n_0 ;
  wire \alusg_reg[8]_i_3_n_0 ;
  wire \alusg_reg[8]_i_4_n_0 ;
  wire \alusg_reg[8]_i_5_n_0 ;
  wire \alusg_reg[8]_i_6_n_0 ;
  wire \alusg_reg[8]_i_7_n_0 ;
  wire \alusg_reg[9]_i_1_n_0 ;
  wire \alusg_reg[9]_i_2_n_0 ;
  wire \alusg_reg[9]_i_3_n_0 ;
  wire \alusg_reg[9]_i_4_n_0 ;
  wire \alusg_reg[9]_i_5_n_0 ;
  wire \alusg_reg[9]_i_6_n_0 ;
  wire \alusg_reg[9]_i_7_n_0 ;
  wire clk_IBUF_BUFG;
  wire [15:0]data1;
  wire i__carry__0_i_1_n_0;
  wire i__carry__0_i_2_n_0;
  wire i__carry__0_i_3_n_0;
  wire i__carry__0_i_4_n_0;
  wire i__carry__1_i_1_n_0;
  wire i__carry__1_i_2_n_0;
  wire i__carry__1_i_3_n_0;
  wire i__carry__1_i_4_n_0;
  wire i__carry__2_i_1_n_0;
  wire i__carry__2_i_2_n_0;
  wire i__carry__2_i_3_n_0;
  wire i__carry__2_i_4_n_0;
  wire i__carry_i_1__0_n_0;
  wire i__carry_i_1_n_0;
  wire i__carry_i_2__0_n_0;
  wire i__carry_i_2_n_0;
  wire i__carry_i_3__0_n_0;
  wire i__carry_i_3_n_0;
  wire i__carry_i_4__0_n_0;
  wire i__carry_i_4_n_0;
  wire i__carry_i_5_n_0;
  wire i__carry_i_6_n_0;
  wire i__carry_i_7_n_0;
  wire i__carry_i_8_n_0;
  wire i__carry_i_9_n_0;
  wire [1:0]\ndisp_reg[1] ;
  wire [2:0]out;
  wire \r1[11]_i_1_n_0 ;
  wire \r1_reg_n_0_[10] ;
  wire \r1_reg_n_0_[11] ;
  wire \r1_reg_n_0_[3] ;
  wire \r1_reg_n_0_[4] ;
  wire \r1_reg_n_0_[5] ;
  wire \r1_reg_n_0_[6] ;
  wire \r1_reg_n_0_[7] ;
  wire \r1_reg_n_0_[8] ;
  wire \r1_reg_n_0_[9] ;
  wire [15:0]res;
  wire [15:0]res_alu;
  wire [6:0]seg_OBUF;
  wire \seg_OBUF[0]_inst_i_2_n_0 ;
  wire \seg_OBUF[0]_inst_i_3_n_0 ;
  wire \seg_OBUF[0]_inst_i_4_n_0 ;
  wire \seg_OBUF[0]_inst_i_5_n_0 ;
  wire \seg_OBUF[1]_inst_i_2_n_0 ;
  wire \seg_OBUF[1]_inst_i_3_n_0 ;
  wire \seg_OBUF[1]_inst_i_4_n_0 ;
  wire \seg_OBUF[1]_inst_i_5_n_0 ;
  wire \seg_OBUF[2]_inst_i_2_n_0 ;
  wire \seg_OBUF[2]_inst_i_3_n_0 ;
  wire \seg_OBUF[2]_inst_i_4_n_0 ;
  wire \seg_OBUF[2]_inst_i_5_n_0 ;
  wire \seg_OBUF[3]_inst_i_2_n_0 ;
  wire \seg_OBUF[3]_inst_i_3_n_0 ;
  wire \seg_OBUF[3]_inst_i_4_n_0 ;
  wire \seg_OBUF[3]_inst_i_5_n_0 ;
  wire \seg_OBUF[4]_inst_i_2_n_0 ;
  wire \seg_OBUF[4]_inst_i_3_n_0 ;
  wire \seg_OBUF[4]_inst_i_4_n_0 ;
  wire \seg_OBUF[4]_inst_i_5_n_0 ;
  wire \seg_OBUF[5]_inst_i_2_n_0 ;
  wire \seg_OBUF[5]_inst_i_3_n_0 ;
  wire \seg_OBUF[5]_inst_i_4_n_0 ;
  wire \seg_OBUF[5]_inst_i_5_n_0 ;
  wire \seg_OBUF[6]_inst_i_2_n_0 ;
  wire \seg_OBUF[6]_inst_i_3_n_0 ;
  wire \seg_OBUF[6]_inst_i_4_n_0 ;
  wire \seg_OBUF[6]_inst_i_5_n_0 ;
  wire [3:0]\NLW_SHIFT_RIGHT2_inferred__0/i__carry__0_CO_UNCONNECTED ;
  wire [3:1]\NLW_SHIFT_RIGHT2_inferred__0/i__carry__0_O_UNCONNECTED ;
  wire NLW_alusg0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_alusg0_MULTSIGNOUT_UNCONNECTED;
  wire NLW_alusg0_OVERFLOW_UNCONNECTED;
  wire NLW_alusg0_PATTERNBDETECT_UNCONNECTED;
  wire NLW_alusg0_PATTERNDETECT_UNCONNECTED;
  wire NLW_alusg0_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_alusg0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_alusg0_BCOUT_UNCONNECTED;
  wire [3:0]NLW_alusg0_CARRYOUT_UNCONNECTED;
  wire [47:32]NLW_alusg0_P_UNCONNECTED;
  wire [47:0]NLW_alusg0_PCOUT_UNCONNECTED;
  wire [3:3]\NLW_alusg0_inferred__0/i__carry__2_CO_UNCONNECTED ;
  wire [3:3]\NLW_alusg_reg[15]_i_5_CO_UNCONNECTED ;

  CARRY4 \SHIFT_RIGHT2_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\SHIFT_RIGHT2_inferred__0/i__carry_n_0 ,\SHIFT_RIGHT2_inferred__0/i__carry_n_1 ,\SHIFT_RIGHT2_inferred__0/i__carry_n_2 ,\SHIFT_RIGHT2_inferred__0/i__carry_n_3 }),
        .CYINIT(i__carry_i_1_n_0),
        .DI({i__carry_i_2_n_0,i__carry_i_3_n_0,i__carry_i_4_n_0,i__carry_i_5_n_0}),
        .O({\SHIFT_RIGHT2_inferred__0/i__carry_n_4 ,\SHIFT_RIGHT2_inferred__0/i__carry_n_5 ,\SHIFT_RIGHT2_inferred__0/i__carry_n_6 ,\SHIFT_RIGHT2_inferred__0/i__carry_n_7 }),
        .S({i__carry_i_6_n_0,i__carry_i_7_n_0,i__carry_i_8_n_0,i__carry_i_9_n_0}));
  CARRY4 \SHIFT_RIGHT2_inferred__0/i__carry__0 
       (.CI(\SHIFT_RIGHT2_inferred__0/i__carry_n_0 ),
        .CO(\NLW_SHIFT_RIGHT2_inferred__0/i__carry__0_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_SHIFT_RIGHT2_inferred__0/i__carry__0_O_UNCONNECTED [3:1],\SHIFT_RIGHT2_inferred__0/i__carry__0_n_7 }),
        .S({1'b0,1'b0,1'b0,1'b1}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-11 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(1),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(1),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    alusg0
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,Q[11:0]}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_alusg0_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({res[15],res[15],res}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_alusg0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_alusg0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_alusg0_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(\r1[11]_i_1_n_0 ),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(clk_IBUF_BUFG),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_alusg0_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_alusg0_OVERFLOW_UNCONNECTED),
        .P({NLW_alusg0_P_UNCONNECTED[47:32],alusg0_n_74,alusg0_n_75,alusg0_n_76,alusg0_n_77,alusg0_n_78,alusg0_n_79,alusg0_n_80,alusg0_n_81,alusg0_n_82,alusg0_n_83,alusg0_n_84,alusg0_n_85,alusg0_n_86,alusg0_n_87,alusg0_n_88,alusg0_n_89,alusg0_n_90,alusg0_n_91,alusg0_n_92,alusg0_n_93,alusg0_n_94,alusg0_n_95,alusg0_n_96,alusg0_n_97,alusg0_n_98,alusg0_n_99,alusg0_n_100,alusg0_n_101,alusg0_n_102,alusg0_n_103,alusg0_n_104,alusg0_n_105}),
        .PATTERNBDETECT(NLW_alusg0_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_alusg0_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_alusg0_PCOUT_UNCONNECTED[47:0]),
        .RSTA(SR[0]),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_alusg0_UNDERFLOW_UNCONNECTED));
  CARRY4 \alusg0_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\alusg0_inferred__0/i__carry_n_0 ,\alusg0_inferred__0/i__carry_n_1 ,\alusg0_inferred__0/i__carry_n_2 ,\alusg0_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI(res[3:0]),
        .O(data1[3:0]),
        .S({i__carry_i_1__0_n_0,i__carry_i_2__0_n_0,i__carry_i_3__0_n_0,i__carry_i_4__0_n_0}));
  CARRY4 \alusg0_inferred__0/i__carry__0 
       (.CI(\alusg0_inferred__0/i__carry_n_0 ),
        .CO({\alusg0_inferred__0/i__carry__0_n_0 ,\alusg0_inferred__0/i__carry__0_n_1 ,\alusg0_inferred__0/i__carry__0_n_2 ,\alusg0_inferred__0/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI(res[7:4]),
        .O(data1[7:4]),
        .S({i__carry__0_i_1_n_0,i__carry__0_i_2_n_0,i__carry__0_i_3_n_0,i__carry__0_i_4_n_0}));
  CARRY4 \alusg0_inferred__0/i__carry__1 
       (.CI(\alusg0_inferred__0/i__carry__0_n_0 ),
        .CO({\alusg0_inferred__0/i__carry__1_n_0 ,\alusg0_inferred__0/i__carry__1_n_1 ,\alusg0_inferred__0/i__carry__1_n_2 ,\alusg0_inferred__0/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI(res[11:8]),
        .O(data1[11:8]),
        .S({i__carry__1_i_1_n_0,i__carry__1_i_2_n_0,i__carry__1_i_3_n_0,i__carry__1_i_4_n_0}));
  CARRY4 \alusg0_inferred__0/i__carry__2 
       (.CI(\alusg0_inferred__0/i__carry__1_n_0 ),
        .CO({\NLW_alusg0_inferred__0/i__carry__2_CO_UNCONNECTED [3],\alusg0_inferred__0/i__carry__2_n_1 ,\alusg0_inferred__0/i__carry__2_n_2 ,\alusg0_inferred__0/i__carry__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,res[14:12]}),
        .O(data1[15:12]),
        .S({i__carry__2_i_1_n_0,i__carry__2_i_2_n_0,i__carry__2_i_3_n_0,i__carry__2_i_4_n_0}));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \alusg_reg[0] 
       (.CLR(1'b0),
        .D(\alusg_reg[0]_i_1_n_0 ),
        .G(E),
        .GE(1'b1),
        .Q(alusg[0]));
  MUXF7 \alusg_reg[0]_i_1 
       (.I0(\alusg_reg[0]_i_2_n_0 ),
        .I1(\alusg_reg[0]_i_3_n_0 ),
        .O(\alusg_reg[0]_i_1_n_0 ),
        .S(out[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \alusg_reg[0]_i_2 
       (.I0(\alusg_reg[0]_i_4_n_0 ),
        .I1(data1[0]),
        .I2(out[1]),
        .I3(alusg0__0[0]),
        .I4(out[0]),
        .I5(\alusg_reg[0]_i_5_n_0 ),
        .O(\alusg_reg[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB8BBB8BBB8BBB888)) 
    \alusg_reg[0]_i_3 
       (.I0(\alusg_reg[0]_i_5_n_0 ),
        .I1(out[1]),
        .I2(alusg0_n_105),
        .I3(out[0]),
        .I4(res[0]),
        .I5(COUNT[0]),
        .O(\alusg_reg[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \alusg_reg[0]_i_4 
       (.I0(res[0]),
        .I1(COUNT[0]),
        .O(\alusg_reg[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hEFEAEFEF45404040)) 
    \alusg_reg[0]_i_5 
       (.I0(COUNT[2]),
        .I1(\alusg_reg[0]_i_6_n_0 ),
        .I2(COUNT[0]),
        .I3(res[2]),
        .I4(COUNT[1]),
        .I5(res[0]),
        .O(\alusg_reg[0]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \alusg_reg[0]_i_6 
       (.I0(res[3]),
        .I1(COUNT[1]),
        .I2(res[1]),
        .O(\alusg_reg[0]_i_6_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \alusg_reg[10] 
       (.CLR(1'b0),
        .D(\alusg_reg[10]_i_1_n_0 ),
        .G(E),
        .GE(1'b1),
        .Q(alusg[10]));
  MUXF7 \alusg_reg[10]_i_1 
       (.I0(\alusg_reg[10]_i_2_n_0 ),
        .I1(\alusg_reg[10]_i_3_n_0 ),
        .O(\alusg_reg[10]_i_1_n_0 ),
        .S(out[2]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \alusg_reg[10]_i_2 
       (.I0(\alusg_reg[10]_i_4_n_0 ),
        .I1(out[1]),
        .I2(alusg0__0[10]),
        .I3(out[0]),
        .I4(\alusg_reg[10]_i_5_n_0 ),
        .O(\alusg_reg[10]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB8BBB8BBB8BBB888)) 
    \alusg_reg[10]_i_3 
       (.I0(\alusg_reg[10]_i_5_n_0 ),
        .I1(out[1]),
        .I2(alusg0_n_95),
        .I3(out[0]),
        .I4(\r1_reg_n_0_[10] ),
        .I5(res[10]),
        .O(\alusg_reg[10]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h8F80)) 
    \alusg_reg[10]_i_4 
       (.I0(\r1_reg_n_0_[10] ),
        .I1(res[10]),
        .I2(out[0]),
        .I3(data1[10]),
        .O(\alusg_reg[10]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \alusg_reg[10]_i_5 
       (.I0(\alusg_reg[11]_i_11_n_0 ),
        .I1(COUNT[0]),
        .I2(\alusg_reg[10]_i_6_n_0 ),
        .I3(COUNT[2]),
        .I4(\alusg_reg[10]_i_7_n_0 ),
        .O(\alusg_reg[10]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hFBBBF888)) 
    \alusg_reg[10]_i_6 
       (.I0(res[8]),
        .I1(COUNT[1]),
        .I2(res[0]),
        .I3(\SHIFT_RIGHT2_inferred__0/i__carry__0_n_7 ),
        .I4(res[6]),
        .O(\alusg_reg[10]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \alusg_reg[10]_i_7 
       (.I0(res[13]),
        .I1(res[11]),
        .I2(COUNT[0]),
        .I3(res[12]),
        .I4(COUNT[1]),
        .I5(res[10]),
        .O(\alusg_reg[10]_i_7_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \alusg_reg[11] 
       (.CLR(1'b0),
        .D(\alusg_reg[11]_i_1_n_0 ),
        .G(E),
        .GE(1'b1),
        .Q(alusg[11]));
  MUXF7 \alusg_reg[11]_i_1 
       (.I0(\alusg_reg[11]_i_2_n_0 ),
        .I1(\alusg_reg[11]_i_3_n_0 ),
        .O(\alusg_reg[11]_i_1_n_0 ),
        .S(out[2]));
  LUT2 #(
    .INIT(4'h6)) 
    \alusg_reg[11]_i_10 
       (.I0(res[8]),
        .I1(\r1_reg_n_0_[8] ),
        .O(\alusg_reg[11]_i_10_n_0 ));
  LUT5 #(
    .INIT(32'hFBBBF888)) 
    \alusg_reg[11]_i_11 
       (.I0(res[9]),
        .I1(COUNT[1]),
        .I2(res[0]),
        .I3(\SHIFT_RIGHT2_inferred__0/i__carry__0_n_7 ),
        .I4(res[7]),
        .O(\alusg_reg[11]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \alusg_reg[11]_i_12 
       (.I0(res[14]),
        .I1(res[12]),
        .I2(COUNT[0]),
        .I3(res[13]),
        .I4(COUNT[1]),
        .I5(res[11]),
        .O(\alusg_reg[11]_i_12_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \alusg_reg[11]_i_2 
       (.I0(\alusg_reg[11]_i_4_n_0 ),
        .I1(out[1]),
        .I2(alusg0__0[11]),
        .I3(out[0]),
        .I4(\alusg_reg[11]_i_6_n_0 ),
        .O(\alusg_reg[11]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB8BBB8BBB8BBB888)) 
    \alusg_reg[11]_i_3 
       (.I0(\alusg_reg[11]_i_6_n_0 ),
        .I1(out[1]),
        .I2(alusg0_n_94),
        .I3(out[0]),
        .I4(\r1_reg_n_0_[11] ),
        .I5(res[11]),
        .O(\alusg_reg[11]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h8F80)) 
    \alusg_reg[11]_i_4 
       (.I0(\r1_reg_n_0_[11] ),
        .I1(res[11]),
        .I2(out[0]),
        .I3(data1[11]),
        .O(\alusg_reg[11]_i_4_n_0 ));
  CARRY4 \alusg_reg[11]_i_5 
       (.CI(\alusg_reg[7]_i_5_n_0 ),
        .CO({\alusg_reg[11]_i_5_n_0 ,\alusg_reg[11]_i_5_n_1 ,\alusg_reg[11]_i_5_n_2 ,\alusg_reg[11]_i_5_n_3 }),
        .CYINIT(1'b0),
        .DI(res[11:8]),
        .O(alusg0__0[11:8]),
        .S({\alusg_reg[11]_i_7_n_0 ,\alusg_reg[11]_i_8_n_0 ,\alusg_reg[11]_i_9_n_0 ,\alusg_reg[11]_i_10_n_0 }));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \alusg_reg[11]_i_6 
       (.I0(\alusg_reg[12]_i_5_n_0 ),
        .I1(COUNT[0]),
        .I2(\alusg_reg[11]_i_11_n_0 ),
        .I3(COUNT[2]),
        .I4(\alusg_reg[11]_i_12_n_0 ),
        .O(\alusg_reg[11]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \alusg_reg[11]_i_7 
       (.I0(res[11]),
        .I1(\r1_reg_n_0_[11] ),
        .O(\alusg_reg[11]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \alusg_reg[11]_i_8 
       (.I0(res[10]),
        .I1(\r1_reg_n_0_[10] ),
        .O(\alusg_reg[11]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \alusg_reg[11]_i_9 
       (.I0(res[9]),
        .I1(\r1_reg_n_0_[9] ),
        .O(\alusg_reg[11]_i_9_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \alusg_reg[12] 
       (.CLR(1'b0),
        .D(\alusg_reg[12]_i_1_n_0 ),
        .G(E),
        .GE(1'b1),
        .Q(alusg[12]));
  MUXF7 \alusg_reg[12]_i_1 
       (.I0(\alusg_reg[12]_i_2_n_0 ),
        .I1(\alusg_reg[12]_i_3_n_0 ),
        .O(\alusg_reg[12]_i_1_n_0 ),
        .S(out[2]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \alusg_reg[12]_i_2 
       (.I0(data1[12]),
        .I1(out[1]),
        .I2(alusg0__0[12]),
        .I3(out[0]),
        .I4(\alusg_reg[12]_i_4_n_0 ),
        .O(\alusg_reg[12]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \alusg_reg[12]_i_3 
       (.I0(\alusg_reg[12]_i_4_n_0 ),
        .I1(out[1]),
        .I2(alusg0_n_93),
        .I3(out[0]),
        .I4(res[12]),
        .O(\alusg_reg[12]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \alusg_reg[12]_i_4 
       (.I0(\alusg_reg[13]_i_5_n_0 ),
        .I1(COUNT[0]),
        .I2(\alusg_reg[12]_i_5_n_0 ),
        .I3(COUNT[2]),
        .I4(\alusg_reg[12]_i_6_n_0 ),
        .O(\alusg_reg[12]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFBBBF888)) 
    \alusg_reg[12]_i_5 
       (.I0(res[10]),
        .I1(COUNT[1]),
        .I2(res[0]),
        .I3(\SHIFT_RIGHT2_inferred__0/i__carry__0_n_7 ),
        .I4(res[8]),
        .O(\alusg_reg[12]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \alusg_reg[12]_i_6 
       (.I0(res[15]),
        .I1(res[13]),
        .I2(COUNT[0]),
        .I3(res[14]),
        .I4(COUNT[1]),
        .I5(res[12]),
        .O(\alusg_reg[12]_i_6_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \alusg_reg[13] 
       (.CLR(1'b0),
        .D(\alusg_reg[13]_i_1_n_0 ),
        .G(E),
        .GE(1'b1),
        .Q(alusg[13]));
  MUXF7 \alusg_reg[13]_i_1 
       (.I0(\alusg_reg[13]_i_2_n_0 ),
        .I1(\alusg_reg[13]_i_3_n_0 ),
        .O(\alusg_reg[13]_i_1_n_0 ),
        .S(out[2]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \alusg_reg[13]_i_2 
       (.I0(data1[13]),
        .I1(out[1]),
        .I2(alusg0__0[13]),
        .I3(out[0]),
        .I4(\alusg_reg[13]_i_4_n_0 ),
        .O(\alusg_reg[13]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \alusg_reg[13]_i_3 
       (.I0(\alusg_reg[13]_i_4_n_0 ),
        .I1(out[1]),
        .I2(alusg0_n_92),
        .I3(out[0]),
        .I4(res[13]),
        .O(\alusg_reg[13]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \alusg_reg[13]_i_4 
       (.I0(\alusg_reg[14]_i_5_n_0 ),
        .I1(COUNT[0]),
        .I2(\alusg_reg[13]_i_5_n_0 ),
        .I3(COUNT[2]),
        .I4(\alusg_reg[13]_i_6_n_0 ),
        .O(\alusg_reg[13]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFBBBF888)) 
    \alusg_reg[13]_i_5 
       (.I0(res[11]),
        .I1(COUNT[1]),
        .I2(res[0]),
        .I3(\SHIFT_RIGHT2_inferred__0/i__carry__0_n_7 ),
        .I4(res[9]),
        .O(\alusg_reg[13]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hF0BBF088)) 
    \alusg_reg[13]_i_6 
       (.I0(res[14]),
        .I1(COUNT[0]),
        .I2(res[15]),
        .I3(COUNT[1]),
        .I4(res[13]),
        .O(\alusg_reg[13]_i_6_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \alusg_reg[14] 
       (.CLR(1'b0),
        .D(\alusg_reg[14]_i_1_n_0 ),
        .G(E),
        .GE(1'b1),
        .Q(alusg[14]));
  MUXF7 \alusg_reg[14]_i_1 
       (.I0(\alusg_reg[14]_i_2_n_0 ),
        .I1(\alusg_reg[14]_i_3_n_0 ),
        .O(\alusg_reg[14]_i_1_n_0 ),
        .S(out[2]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \alusg_reg[14]_i_2 
       (.I0(data1[14]),
        .I1(out[1]),
        .I2(alusg0__0[14]),
        .I3(out[0]),
        .I4(\alusg_reg[14]_i_4_n_0 ),
        .O(\alusg_reg[14]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \alusg_reg[14]_i_3 
       (.I0(\alusg_reg[14]_i_4_n_0 ),
        .I1(out[1]),
        .I2(alusg0_n_91),
        .I3(out[0]),
        .I4(res[14]),
        .O(\alusg_reg[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \alusg_reg[14]_i_4 
       (.I0(\alusg_reg[15]_i_8_n_0 ),
        .I1(\alusg_reg[14]_i_5_n_0 ),
        .I2(COUNT[2]),
        .I3(res[15]),
        .I4(COUNT[0]),
        .I5(\alusg_reg[14]_i_6_n_0 ),
        .O(\alusg_reg[14]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFBBBF888)) 
    \alusg_reg[14]_i_5 
       (.I0(res[12]),
        .I1(COUNT[1]),
        .I2(res[0]),
        .I3(\SHIFT_RIGHT2_inferred__0/i__carry__0_n_7 ),
        .I4(res[10]),
        .O(\alusg_reg[14]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \alusg_reg[14]_i_6 
       (.I0(res[15]),
        .I1(COUNT[1]),
        .I2(res[14]),
        .O(\alusg_reg[14]_i_6_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \alusg_reg[15] 
       (.CLR(1'b0),
        .D(\alusg_reg[15]_i_1_n_0 ),
        .G(E),
        .GE(1'b1),
        .Q(alusg[15]));
  MUXF7 \alusg_reg[15]_i_1 
       (.I0(\alusg_reg[15]_i_3_n_0 ),
        .I1(\alusg_reg[15]_i_4_n_0 ),
        .O(\alusg_reg[15]_i_1_n_0 ),
        .S(out[2]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \alusg_reg[15]_i_3 
       (.I0(data1[15]),
        .I1(out[1]),
        .I2(alusg0__0[15]),
        .I3(out[0]),
        .I4(\alusg_reg[15]_i_6_n_0 ),
        .O(\alusg_reg[15]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \alusg_reg[15]_i_4 
       (.I0(\alusg_reg[15]_i_6_n_0 ),
        .I1(out[1]),
        .I2(alusg0_n_90),
        .I3(out[0]),
        .I4(res[15]),
        .O(\alusg_reg[15]_i_4_n_0 ));
  CARRY4 \alusg_reg[15]_i_5 
       (.CI(\alusg_reg[11]_i_5_n_0 ),
        .CO({\NLW_alusg_reg[15]_i_5_CO_UNCONNECTED [3],\alusg_reg[15]_i_5_n_1 ,\alusg_reg[15]_i_5_n_2 ,\alusg_reg[15]_i_5_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,res[14:12]}),
        .O(alusg0__0[15:12]),
        .S(res[15:12]));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \alusg_reg[15]_i_6 
       (.I0(\alusg_reg[15]_i_7_n_0 ),
        .I1(COUNT[0]),
        .I2(\alusg_reg[15]_i_8_n_0 ),
        .I3(COUNT[2]),
        .I4(res[15]),
        .O(\alusg_reg[15]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFBBBF888)) 
    \alusg_reg[15]_i_7 
       (.I0(res[14]),
        .I1(COUNT[1]),
        .I2(res[0]),
        .I3(\SHIFT_RIGHT2_inferred__0/i__carry__0_n_7 ),
        .I4(res[12]),
        .O(\alusg_reg[15]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hFBBBF888)) 
    \alusg_reg[15]_i_8 
       (.I0(res[13]),
        .I1(COUNT[1]),
        .I2(res[0]),
        .I3(\SHIFT_RIGHT2_inferred__0/i__carry__0_n_7 ),
        .I4(res[11]),
        .O(\alusg_reg[15]_i_8_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \alusg_reg[1] 
       (.CLR(1'b0),
        .D(\alusg_reg[1]_i_1_n_0 ),
        .G(E),
        .GE(1'b1),
        .Q(alusg[1]));
  MUXF7 \alusg_reg[1]_i_1 
       (.I0(\alusg_reg[1]_i_2_n_0 ),
        .I1(\alusg_reg[1]_i_3_n_0 ),
        .O(\alusg_reg[1]_i_1_n_0 ),
        .S(out[2]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \alusg_reg[1]_i_2 
       (.I0(\alusg_reg[1]_i_4_n_0 ),
        .I1(out[1]),
        .I2(alusg0__0[1]),
        .I3(out[0]),
        .I4(\alusg_reg[1]_i_5_n_0 ),
        .O(\alusg_reg[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB8BBB8BBB8BBB888)) 
    \alusg_reg[1]_i_3 
       (.I0(\alusg_reg[1]_i_5_n_0 ),
        .I1(out[1]),
        .I2(alusg0_n_104),
        .I3(out[0]),
        .I4(res[1]),
        .I5(COUNT[1]),
        .O(\alusg_reg[1]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h8F80)) 
    \alusg_reg[1]_i_4 
       (.I0(res[1]),
        .I1(COUNT[1]),
        .I2(out[0]),
        .I3(data1[1]),
        .O(\alusg_reg[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hE0A0FFFFE0A00000)) 
    \alusg_reg[1]_i_5 
       (.I0(\SHIFT_RIGHT2_inferred__0/i__carry_n_7 ),
        .I1(COUNT[1]),
        .I2(res[0]),
        .I3(COUNT[0]),
        .I4(COUNT[2]),
        .I5(\alusg_reg[1]_i_6_n_0 ),
        .O(\alusg_reg[1]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \alusg_reg[1]_i_6 
       (.I0(res[4]),
        .I1(res[2]),
        .I2(COUNT[0]),
        .I3(res[3]),
        .I4(COUNT[1]),
        .I5(res[1]),
        .O(\alusg_reg[1]_i_6_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \alusg_reg[2] 
       (.CLR(1'b0),
        .D(\alusg_reg[2]_i_1_n_0 ),
        .G(E),
        .GE(1'b1),
        .Q(alusg[2]));
  MUXF7 \alusg_reg[2]_i_1 
       (.I0(\alusg_reg[2]_i_2_n_0 ),
        .I1(\alusg_reg[2]_i_3_n_0 ),
        .O(\alusg_reg[2]_i_1_n_0 ),
        .S(out[2]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \alusg_reg[2]_i_2 
       (.I0(\alusg_reg[2]_i_4_n_0 ),
        .I1(out[1]),
        .I2(alusg0__0[2]),
        .I3(out[0]),
        .I4(\alusg_reg[2]_i_5_n_0 ),
        .O(\alusg_reg[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB8BBB8BBB8BBB888)) 
    \alusg_reg[2]_i_3 
       (.I0(\alusg_reg[2]_i_5_n_0 ),
        .I1(out[1]),
        .I2(alusg0_n_103),
        .I3(out[0]),
        .I4(res[2]),
        .I5(COUNT[2]),
        .O(\alusg_reg[2]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h8F80)) 
    \alusg_reg[2]_i_4 
       (.I0(res[2]),
        .I1(COUNT[2]),
        .I2(out[0]),
        .I3(data1[2]),
        .O(\alusg_reg[2]_i_4_n_0 ));
  MUXF7 \alusg_reg[2]_i_5 
       (.I0(\alusg_reg[2]_i_6_n_0 ),
        .I1(\alusg_reg[2]_i_7_n_0 ),
        .O(\alusg_reg[2]_i_5_n_0 ),
        .S(COUNT[2]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \alusg_reg[2]_i_6 
       (.I0(res[5]),
        .I1(res[3]),
        .I2(COUNT[0]),
        .I3(res[4]),
        .I4(COUNT[1]),
        .I5(res[2]),
        .O(\alusg_reg[2]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFFB08080)) 
    \alusg_reg[2]_i_7 
       (.I0(res[1]),
        .I1(COUNT[0]),
        .I2(COUNT[1]),
        .I3(\SHIFT_RIGHT2_inferred__0/i__carry_n_6 ),
        .I4(res[0]),
        .O(\alusg_reg[2]_i_7_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \alusg_reg[3] 
       (.CLR(1'b0),
        .D(\alusg_reg[3]_i_1_n_0 ),
        .G(E),
        .GE(1'b1),
        .Q(alusg[3]));
  MUXF7 \alusg_reg[3]_i_1 
       (.I0(\alusg_reg[3]_i_2_n_0 ),
        .I1(\alusg_reg[3]_i_3_n_0 ),
        .O(\alusg_reg[3]_i_1_n_0 ),
        .S(out[2]));
  LUT2 #(
    .INIT(4'h6)) 
    \alusg_reg[3]_i_10 
       (.I0(res[0]),
        .I1(COUNT[0]),
        .O(\alusg_reg[3]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \alusg_reg[3]_i_11 
       (.I0(res[6]),
        .I1(res[4]),
        .I2(COUNT[0]),
        .I3(res[5]),
        .I4(COUNT[1]),
        .I5(res[3]),
        .O(\alusg_reg[3]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFB8CCB800B800)) 
    \alusg_reg[3]_i_12 
       (.I0(res[2]),
        .I1(COUNT[0]),
        .I2(res[1]),
        .I3(COUNT[1]),
        .I4(\SHIFT_RIGHT2_inferred__0/i__carry_n_5 ),
        .I5(res[0]),
        .O(\alusg_reg[3]_i_12_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \alusg_reg[3]_i_2 
       (.I0(\alusg_reg[3]_i_4_n_0 ),
        .I1(out[1]),
        .I2(alusg0__0[3]),
        .I3(out[0]),
        .I4(\alusg_reg[3]_i_6_n_0 ),
        .O(\alusg_reg[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB8BBB8BBB8BBB888)) 
    \alusg_reg[3]_i_3 
       (.I0(\alusg_reg[3]_i_6_n_0 ),
        .I1(out[1]),
        .I2(alusg0_n_102),
        .I3(out[0]),
        .I4(\r1_reg_n_0_[3] ),
        .I5(res[3]),
        .O(\alusg_reg[3]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h8F80)) 
    \alusg_reg[3]_i_4 
       (.I0(\r1_reg_n_0_[3] ),
        .I1(res[3]),
        .I2(out[0]),
        .I3(data1[3]),
        .O(\alusg_reg[3]_i_4_n_0 ));
  CARRY4 \alusg_reg[3]_i_5 
       (.CI(1'b0),
        .CO({\alusg_reg[3]_i_5_n_0 ,\alusg_reg[3]_i_5_n_1 ,\alusg_reg[3]_i_5_n_2 ,\alusg_reg[3]_i_5_n_3 }),
        .CYINIT(1'b0),
        .DI(res[3:0]),
        .O(alusg0__0[3:0]),
        .S({\alusg_reg[3]_i_7_n_0 ,\alusg_reg[3]_i_8_n_0 ,\alusg_reg[3]_i_9_n_0 ,\alusg_reg[3]_i_10_n_0 }));
  MUXF7 \alusg_reg[3]_i_6 
       (.I0(\alusg_reg[3]_i_11_n_0 ),
        .I1(\alusg_reg[3]_i_12_n_0 ),
        .O(\alusg_reg[3]_i_6_n_0 ),
        .S(COUNT[2]));
  LUT2 #(
    .INIT(4'h6)) 
    \alusg_reg[3]_i_7 
       (.I0(res[3]),
        .I1(\r1_reg_n_0_[3] ),
        .O(\alusg_reg[3]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \alusg_reg[3]_i_8 
       (.I0(res[2]),
        .I1(COUNT[2]),
        .O(\alusg_reg[3]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \alusg_reg[3]_i_9 
       (.I0(res[1]),
        .I1(COUNT[1]),
        .O(\alusg_reg[3]_i_9_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \alusg_reg[4] 
       (.CLR(1'b0),
        .D(\alusg_reg[4]_i_1_n_0 ),
        .G(E),
        .GE(1'b1),
        .Q(alusg[4]));
  MUXF7 \alusg_reg[4]_i_1 
       (.I0(\alusg_reg[4]_i_2_n_0 ),
        .I1(\alusg_reg[4]_i_3_n_0 ),
        .O(\alusg_reg[4]_i_1_n_0 ),
        .S(out[2]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \alusg_reg[4]_i_2 
       (.I0(\alusg_reg[4]_i_4_n_0 ),
        .I1(out[1]),
        .I2(alusg0__0[4]),
        .I3(out[0]),
        .I4(\alusg_reg[4]_i_5_n_0 ),
        .O(\alusg_reg[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB8BBB8BBB8BBB888)) 
    \alusg_reg[4]_i_3 
       (.I0(\alusg_reg[4]_i_5_n_0 ),
        .I1(out[1]),
        .I2(alusg0_n_101),
        .I3(out[0]),
        .I4(\r1_reg_n_0_[4] ),
        .I5(res[4]),
        .O(\alusg_reg[4]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h8F80)) 
    \alusg_reg[4]_i_4 
       (.I0(\r1_reg_n_0_[4] ),
        .I1(res[4]),
        .I2(out[0]),
        .I3(data1[4]),
        .O(\alusg_reg[4]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \alusg_reg[4]_i_5 
       (.I0(\alusg_reg[4]_i_6_n_0 ),
        .I1(COUNT[0]),
        .I2(\alusg_reg[4]_i_7_n_0 ),
        .I3(COUNT[2]),
        .I4(\alusg_reg[4]_i_8_n_0 ),
        .O(\alusg_reg[4]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hFBBBF888)) 
    \alusg_reg[4]_i_6 
       (.I0(res[3]),
        .I1(COUNT[1]),
        .I2(res[0]),
        .I3(\SHIFT_RIGHT2_inferred__0/i__carry_n_4 ),
        .I4(res[1]),
        .O(\alusg_reg[4]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'hEFC0)) 
    \alusg_reg[4]_i_7 
       (.I0(\SHIFT_RIGHT2_inferred__0/i__carry_n_4 ),
        .I1(res[2]),
        .I2(COUNT[1]),
        .I3(res[0]),
        .O(\alusg_reg[4]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \alusg_reg[4]_i_8 
       (.I0(res[7]),
        .I1(res[5]),
        .I2(COUNT[0]),
        .I3(res[6]),
        .I4(COUNT[1]),
        .I5(res[4]),
        .O(\alusg_reg[4]_i_8_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \alusg_reg[5] 
       (.CLR(1'b0),
        .D(\alusg_reg[5]_i_1_n_0 ),
        .G(E),
        .GE(1'b1),
        .Q(alusg[5]));
  MUXF7 \alusg_reg[5]_i_1 
       (.I0(\alusg_reg[5]_i_2_n_0 ),
        .I1(\alusg_reg[5]_i_3_n_0 ),
        .O(\alusg_reg[5]_i_1_n_0 ),
        .S(out[2]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \alusg_reg[5]_i_2 
       (.I0(\alusg_reg[5]_i_4_n_0 ),
        .I1(out[1]),
        .I2(alusg0__0[5]),
        .I3(out[0]),
        .I4(\alusg_reg[5]_i_5_n_0 ),
        .O(\alusg_reg[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB8BBB8BBB8BBB888)) 
    \alusg_reg[5]_i_3 
       (.I0(\alusg_reg[5]_i_5_n_0 ),
        .I1(out[1]),
        .I2(alusg0_n_100),
        .I3(out[0]),
        .I4(\r1_reg_n_0_[5] ),
        .I5(res[5]),
        .O(\alusg_reg[5]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h8F80)) 
    \alusg_reg[5]_i_4 
       (.I0(\r1_reg_n_0_[5] ),
        .I1(res[5]),
        .I2(out[0]),
        .I3(data1[5]),
        .O(\alusg_reg[5]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \alusg_reg[5]_i_5 
       (.I0(\alusg_reg[6]_i_6_n_0 ),
        .I1(COUNT[0]),
        .I2(\alusg_reg[5]_i_6_n_0 ),
        .I3(COUNT[2]),
        .I4(\alusg_reg[5]_i_7_n_0 ),
        .O(\alusg_reg[5]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFBBBF888)) 
    \alusg_reg[5]_i_6 
       (.I0(res[3]),
        .I1(COUNT[1]),
        .I2(res[0]),
        .I3(\SHIFT_RIGHT2_inferred__0/i__carry__0_n_7 ),
        .I4(res[1]),
        .O(\alusg_reg[5]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \alusg_reg[5]_i_7 
       (.I0(res[8]),
        .I1(res[6]),
        .I2(COUNT[0]),
        .I3(res[7]),
        .I4(COUNT[1]),
        .I5(res[5]),
        .O(\alusg_reg[5]_i_7_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \alusg_reg[6] 
       (.CLR(1'b0),
        .D(\alusg_reg[6]_i_1_n_0 ),
        .G(E),
        .GE(1'b1),
        .Q(alusg[6]));
  MUXF7 \alusg_reg[6]_i_1 
       (.I0(\alusg_reg[6]_i_2_n_0 ),
        .I1(\alusg_reg[6]_i_3_n_0 ),
        .O(\alusg_reg[6]_i_1_n_0 ),
        .S(out[2]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \alusg_reg[6]_i_2 
       (.I0(\alusg_reg[6]_i_4_n_0 ),
        .I1(out[1]),
        .I2(alusg0__0[6]),
        .I3(out[0]),
        .I4(\alusg_reg[6]_i_5_n_0 ),
        .O(\alusg_reg[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB8BBB8BBB8BBB888)) 
    \alusg_reg[6]_i_3 
       (.I0(\alusg_reg[6]_i_5_n_0 ),
        .I1(out[1]),
        .I2(alusg0_n_99),
        .I3(out[0]),
        .I4(\r1_reg_n_0_[6] ),
        .I5(res[6]),
        .O(\alusg_reg[6]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h8F80)) 
    \alusg_reg[6]_i_4 
       (.I0(\r1_reg_n_0_[6] ),
        .I1(res[6]),
        .I2(out[0]),
        .I3(data1[6]),
        .O(\alusg_reg[6]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \alusg_reg[6]_i_5 
       (.I0(\alusg_reg[7]_i_11_n_0 ),
        .I1(COUNT[0]),
        .I2(\alusg_reg[6]_i_6_n_0 ),
        .I3(COUNT[2]),
        .I4(\alusg_reg[6]_i_7_n_0 ),
        .O(\alusg_reg[6]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hFBBBF888)) 
    \alusg_reg[6]_i_6 
       (.I0(res[4]),
        .I1(COUNT[1]),
        .I2(res[0]),
        .I3(\SHIFT_RIGHT2_inferred__0/i__carry__0_n_7 ),
        .I4(res[2]),
        .O(\alusg_reg[6]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \alusg_reg[6]_i_7 
       (.I0(res[9]),
        .I1(res[7]),
        .I2(COUNT[0]),
        .I3(res[8]),
        .I4(COUNT[1]),
        .I5(res[6]),
        .O(\alusg_reg[6]_i_7_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \alusg_reg[7] 
       (.CLR(1'b0),
        .D(\alusg_reg[7]_i_1_n_0 ),
        .G(E),
        .GE(1'b1),
        .Q(alusg[7]));
  MUXF7 \alusg_reg[7]_i_1 
       (.I0(\alusg_reg[7]_i_2_n_0 ),
        .I1(\alusg_reg[7]_i_3_n_0 ),
        .O(\alusg_reg[7]_i_1_n_0 ),
        .S(out[2]));
  LUT2 #(
    .INIT(4'h6)) 
    \alusg_reg[7]_i_10 
       (.I0(res[4]),
        .I1(\r1_reg_n_0_[4] ),
        .O(\alusg_reg[7]_i_10_n_0 ));
  LUT5 #(
    .INIT(32'hFBBBF888)) 
    \alusg_reg[7]_i_11 
       (.I0(res[5]),
        .I1(COUNT[1]),
        .I2(res[0]),
        .I3(\SHIFT_RIGHT2_inferred__0/i__carry__0_n_7 ),
        .I4(res[3]),
        .O(\alusg_reg[7]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \alusg_reg[7]_i_12 
       (.I0(res[10]),
        .I1(res[8]),
        .I2(COUNT[0]),
        .I3(res[9]),
        .I4(COUNT[1]),
        .I5(res[7]),
        .O(\alusg_reg[7]_i_12_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \alusg_reg[7]_i_2 
       (.I0(\alusg_reg[7]_i_4_n_0 ),
        .I1(out[1]),
        .I2(alusg0__0[7]),
        .I3(out[0]),
        .I4(\alusg_reg[7]_i_6_n_0 ),
        .O(\alusg_reg[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB8BBB8BBB8BBB888)) 
    \alusg_reg[7]_i_3 
       (.I0(\alusg_reg[7]_i_6_n_0 ),
        .I1(out[1]),
        .I2(alusg0_n_98),
        .I3(out[0]),
        .I4(\r1_reg_n_0_[7] ),
        .I5(res[7]),
        .O(\alusg_reg[7]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h8F80)) 
    \alusg_reg[7]_i_4 
       (.I0(\r1_reg_n_0_[7] ),
        .I1(res[7]),
        .I2(out[0]),
        .I3(data1[7]),
        .O(\alusg_reg[7]_i_4_n_0 ));
  CARRY4 \alusg_reg[7]_i_5 
       (.CI(\alusg_reg[3]_i_5_n_0 ),
        .CO({\alusg_reg[7]_i_5_n_0 ,\alusg_reg[7]_i_5_n_1 ,\alusg_reg[7]_i_5_n_2 ,\alusg_reg[7]_i_5_n_3 }),
        .CYINIT(1'b0),
        .DI(res[7:4]),
        .O(alusg0__0[7:4]),
        .S({\alusg_reg[7]_i_7_n_0 ,\alusg_reg[7]_i_8_n_0 ,\alusg_reg[7]_i_9_n_0 ,\alusg_reg[7]_i_10_n_0 }));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \alusg_reg[7]_i_6 
       (.I0(\alusg_reg[8]_i_6_n_0 ),
        .I1(COUNT[0]),
        .I2(\alusg_reg[7]_i_11_n_0 ),
        .I3(COUNT[2]),
        .I4(\alusg_reg[7]_i_12_n_0 ),
        .O(\alusg_reg[7]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \alusg_reg[7]_i_7 
       (.I0(res[7]),
        .I1(\r1_reg_n_0_[7] ),
        .O(\alusg_reg[7]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \alusg_reg[7]_i_8 
       (.I0(res[6]),
        .I1(\r1_reg_n_0_[6] ),
        .O(\alusg_reg[7]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \alusg_reg[7]_i_9 
       (.I0(res[5]),
        .I1(\r1_reg_n_0_[5] ),
        .O(\alusg_reg[7]_i_9_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \alusg_reg[8] 
       (.CLR(1'b0),
        .D(\alusg_reg[8]_i_1_n_0 ),
        .G(E),
        .GE(1'b1),
        .Q(alusg[8]));
  MUXF7 \alusg_reg[8]_i_1 
       (.I0(\alusg_reg[8]_i_2_n_0 ),
        .I1(\alusg_reg[8]_i_3_n_0 ),
        .O(\alusg_reg[8]_i_1_n_0 ),
        .S(out[2]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \alusg_reg[8]_i_2 
       (.I0(\alusg_reg[8]_i_4_n_0 ),
        .I1(out[1]),
        .I2(alusg0__0[8]),
        .I3(out[0]),
        .I4(\alusg_reg[8]_i_5_n_0 ),
        .O(\alusg_reg[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB8BBB8BBB8BBB888)) 
    \alusg_reg[8]_i_3 
       (.I0(\alusg_reg[8]_i_5_n_0 ),
        .I1(out[1]),
        .I2(alusg0_n_97),
        .I3(out[0]),
        .I4(\r1_reg_n_0_[8] ),
        .I5(res[8]),
        .O(\alusg_reg[8]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h8F80)) 
    \alusg_reg[8]_i_4 
       (.I0(\r1_reg_n_0_[8] ),
        .I1(res[8]),
        .I2(out[0]),
        .I3(data1[8]),
        .O(\alusg_reg[8]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \alusg_reg[8]_i_5 
       (.I0(\alusg_reg[9]_i_6_n_0 ),
        .I1(COUNT[0]),
        .I2(\alusg_reg[8]_i_6_n_0 ),
        .I3(COUNT[2]),
        .I4(\alusg_reg[8]_i_7_n_0 ),
        .O(\alusg_reg[8]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hFBBBF888)) 
    \alusg_reg[8]_i_6 
       (.I0(res[6]),
        .I1(COUNT[1]),
        .I2(res[0]),
        .I3(\SHIFT_RIGHT2_inferred__0/i__carry__0_n_7 ),
        .I4(res[4]),
        .O(\alusg_reg[8]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \alusg_reg[8]_i_7 
       (.I0(res[11]),
        .I1(res[9]),
        .I2(COUNT[0]),
        .I3(res[10]),
        .I4(COUNT[1]),
        .I5(res[8]),
        .O(\alusg_reg[8]_i_7_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \alusg_reg[9] 
       (.CLR(1'b0),
        .D(\alusg_reg[9]_i_1_n_0 ),
        .G(E),
        .GE(1'b1),
        .Q(alusg[9]));
  MUXF7 \alusg_reg[9]_i_1 
       (.I0(\alusg_reg[9]_i_2_n_0 ),
        .I1(\alusg_reg[9]_i_3_n_0 ),
        .O(\alusg_reg[9]_i_1_n_0 ),
        .S(out[2]));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \alusg_reg[9]_i_2 
       (.I0(\alusg_reg[9]_i_4_n_0 ),
        .I1(out[1]),
        .I2(alusg0__0[9]),
        .I3(out[0]),
        .I4(\alusg_reg[9]_i_5_n_0 ),
        .O(\alusg_reg[9]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB8BBB8BBB8BBB888)) 
    \alusg_reg[9]_i_3 
       (.I0(\alusg_reg[9]_i_5_n_0 ),
        .I1(out[1]),
        .I2(alusg0_n_96),
        .I3(out[0]),
        .I4(\r1_reg_n_0_[9] ),
        .I5(res[9]),
        .O(\alusg_reg[9]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h8F80)) 
    \alusg_reg[9]_i_4 
       (.I0(\r1_reg_n_0_[9] ),
        .I1(res[9]),
        .I2(out[0]),
        .I3(data1[9]),
        .O(\alusg_reg[9]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    \alusg_reg[9]_i_5 
       (.I0(\alusg_reg[10]_i_6_n_0 ),
        .I1(COUNT[0]),
        .I2(\alusg_reg[9]_i_6_n_0 ),
        .I3(COUNT[2]),
        .I4(\alusg_reg[9]_i_7_n_0 ),
        .O(\alusg_reg[9]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hFBBBF888)) 
    \alusg_reg[9]_i_6 
       (.I0(res[7]),
        .I1(COUNT[1]),
        .I2(res[0]),
        .I3(\SHIFT_RIGHT2_inferred__0/i__carry__0_n_7 ),
        .I4(res[5]),
        .O(\alusg_reg[9]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \alusg_reg[9]_i_7 
       (.I0(res[12]),
        .I1(res[10]),
        .I2(COUNT[0]),
        .I3(res[11]),
        .I4(COUNT[1]),
        .I5(res[9]),
        .O(\alusg_reg[9]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_1
       (.I0(\r1_reg_n_0_[7] ),
        .I1(res[7]),
        .O(i__carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_2
       (.I0(\r1_reg_n_0_[6] ),
        .I1(res[6]),
        .O(i__carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_3
       (.I0(\r1_reg_n_0_[5] ),
        .I1(res[5]),
        .O(i__carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_4
       (.I0(\r1_reg_n_0_[4] ),
        .I1(res[4]),
        .O(i__carry__0_i_4_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__1_i_1
       (.I0(\r1_reg_n_0_[11] ),
        .I1(res[11]),
        .O(i__carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__1_i_2
       (.I0(\r1_reg_n_0_[10] ),
        .I1(res[10]),
        .O(i__carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__1_i_3
       (.I0(\r1_reg_n_0_[9] ),
        .I1(res[9]),
        .O(i__carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__1_i_4
       (.I0(\r1_reg_n_0_[8] ),
        .I1(res[8]),
        .O(i__carry__1_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__2_i_1
       (.I0(res[15]),
        .O(i__carry__2_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__2_i_2
       (.I0(res[14]),
        .O(i__carry__2_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__2_i_3
       (.I0(res[13]),
        .O(i__carry__2_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__2_i_4
       (.I0(res[12]),
        .O(i__carry__2_i_4_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    i__carry_i_1
       (.I0(COUNT[0]),
        .I1(COUNT[1]),
        .I2(COUNT[2]),
        .O(i__carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry_i_1__0
       (.I0(\r1_reg_n_0_[3] ),
        .I1(res[3]),
        .O(i__carry_i_1__0_n_0));
  LUT3 #(
    .INIT(8'h10)) 
    i__carry_i_2
       (.I0(COUNT[0]),
        .I1(COUNT[1]),
        .I2(COUNT[2]),
        .O(i__carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry_i_2__0
       (.I0(COUNT[2]),
        .I1(res[2]),
        .O(i__carry_i_2__0_n_0));
  LUT3 #(
    .INIT(8'h60)) 
    i__carry_i_3
       (.I0(COUNT[2]),
        .I1(COUNT[1]),
        .I2(COUNT[0]),
        .O(i__carry_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry_i_3__0
       (.I0(COUNT[1]),
        .I1(res[1]),
        .O(i__carry_i_3__0_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    i__carry_i_4
       (.I0(COUNT[1]),
        .I1(COUNT[0]),
        .O(i__carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry_i_4__0
       (.I0(COUNT[0]),
        .I1(res[0]),
        .O(i__carry_i_4__0_n_0));
  LUT3 #(
    .INIT(8'h84)) 
    i__carry_i_5
       (.I0(COUNT[2]),
        .I1(COUNT[0]),
        .I2(COUNT[1]),
        .O(i__carry_i_5_n_0));
  LUT3 #(
    .INIT(8'hEF)) 
    i__carry_i_6
       (.I0(COUNT[1]),
        .I1(COUNT[0]),
        .I2(COUNT[2]),
        .O(i__carry_i_6_n_0));
  LUT3 #(
    .INIT(8'hB7)) 
    i__carry_i_7
       (.I0(COUNT[2]),
        .I1(COUNT[0]),
        .I2(COUNT[1]),
        .O(i__carry_i_7_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    i__carry_i_8
       (.I0(COUNT[0]),
        .I1(COUNT[1]),
        .O(i__carry_i_8_n_0));
  LUT3 #(
    .INIT(8'h6F)) 
    i__carry_i_9
       (.I0(COUNT[2]),
        .I1(COUNT[1]),
        .I2(COUNT[0]),
        .O(i__carry_i_9_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    \r1[11]_i_1 
       (.I0(Q[13]),
        .I1(Q[12]),
        .O(\r1[11]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(\r1[11]_i_1_n_0 ),
        .D(Q[0]),
        .Q(COUNT[0]),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(\r1[11]_i_1_n_0 ),
        .D(Q[10]),
        .Q(\r1_reg_n_0_[10] ),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(\r1[11]_i_1_n_0 ),
        .D(Q[11]),
        .Q(\r1_reg_n_0_[11] ),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(\r1[11]_i_1_n_0 ),
        .D(Q[1]),
        .Q(COUNT[1]),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(\r1[11]_i_1_n_0 ),
        .D(Q[2]),
        .Q(COUNT[2]),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(\r1[11]_i_1_n_0 ),
        .D(Q[3]),
        .Q(\r1_reg_n_0_[3] ),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(\r1[11]_i_1_n_0 ),
        .D(Q[4]),
        .Q(\r1_reg_n_0_[4] ),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(\r1[11]_i_1_n_0 ),
        .D(Q[5]),
        .Q(\r1_reg_n_0_[5] ),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(\r1[11]_i_1_n_0 ),
        .D(Q[6]),
        .Q(\r1_reg_n_0_[6] ),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(\r1[11]_i_1_n_0 ),
        .D(Q[7]),
        .Q(\r1_reg_n_0_[7] ),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(\r1[11]_i_1_n_0 ),
        .D(Q[8]),
        .Q(\r1_reg_n_0_[8] ),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r1_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(\r1[11]_i_1_n_0 ),
        .D(Q[9]),
        .Q(\r1_reg_n_0_[9] ),
        .R(SR[0]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \r2[0]_i_1 
       (.I0(Q[0]),
        .I1(Q[13]),
        .I2(alusg[0]),
        .O(res_alu[0]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \r2[10]_i_1 
       (.I0(Q[10]),
        .I1(Q[13]),
        .I2(alusg[10]),
        .O(res_alu[10]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \r2[11]_i_1 
       (.I0(Q[11]),
        .I1(Q[13]),
        .I2(alusg[11]),
        .O(res_alu[11]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \r2[12]_i_1 
       (.I0(alusg[12]),
        .I1(Q[13]),
        .O(res_alu[12]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \r2[13]_i_1 
       (.I0(alusg[13]),
        .I1(Q[13]),
        .O(res_alu[13]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \r2[14]_i_1 
       (.I0(alusg[14]),
        .I1(Q[13]),
        .O(res_alu[14]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \r2[15]_i_3 
       (.I0(alusg[15]),
        .I1(Q[13]),
        .O(res_alu[15]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \r2[1]_i_1 
       (.I0(Q[1]),
        .I1(Q[13]),
        .I2(alusg[1]),
        .O(res_alu[1]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \r2[2]_i_1 
       (.I0(Q[2]),
        .I1(Q[13]),
        .I2(alusg[2]),
        .O(res_alu[2]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \r2[3]_i_1 
       (.I0(Q[3]),
        .I1(Q[13]),
        .I2(alusg[3]),
        .O(res_alu[3]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \r2[4]_i_1 
       (.I0(Q[4]),
        .I1(Q[13]),
        .I2(alusg[4]),
        .O(res_alu[4]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \r2[5]_i_1 
       (.I0(Q[5]),
        .I1(Q[13]),
        .I2(alusg[5]),
        .O(res_alu[5]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \r2[6]_i_1 
       (.I0(Q[6]),
        .I1(Q[13]),
        .I2(alusg[6]),
        .O(res_alu[6]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \r2[7]_i_1 
       (.I0(Q[7]),
        .I1(Q[13]),
        .I2(alusg[7]),
        .O(res_alu[7]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \r2[8]_i_1 
       (.I0(Q[8]),
        .I1(Q[13]),
        .I2(alusg[8]),
        .O(res_alu[8]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \r2[9]_i_1 
       (.I0(Q[9]),
        .I1(Q[13]),
        .I2(alusg[9]),
        .O(res_alu[9]));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(res_alu[0]),
        .Q(res[0]),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(res_alu[10]),
        .Q(res[10]),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(res_alu[11]),
        .Q(res[11]),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(res_alu[12]),
        .Q(res[12]),
        .R(SR[1]));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(res_alu[13]),
        .Q(res[13]),
        .R(SR[1]));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(res_alu[14]),
        .Q(res[14]),
        .R(SR[1]));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(res_alu[15]),
        .Q(res[15]),
        .R(SR[1]));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(res_alu[1]),
        .Q(res[1]),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(res_alu[2]),
        .Q(res[2]),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(res_alu[3]),
        .Q(res[3]),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(res_alu[4]),
        .Q(res[4]),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(res_alu[5]),
        .Q(res[5]),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(res_alu[6]),
        .Q(res[6]),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(res_alu[7]),
        .Q(res[7]),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(res_alu[8]),
        .Q(res[8]),
        .R(SR[0]));
  FDRE #(
    .INIT(1'b0)) 
    \r2_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(\FSM_sequential_currstate_reg[1] ),
        .D(res_alu[9]),
        .Q(res[9]),
        .R(SR[0]));
  MUXF7 \seg_OBUF[0]_inst_i_1 
       (.I0(\seg_OBUF[0]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[0]_inst_i_3_n_0 ),
        .O(seg_OBUF[0]),
        .S(\ndisp_reg[1] [1]));
  LUT6 #(
    .INIT(64'h2094FFFF20940000)) 
    \seg_OBUF[0]_inst_i_2 
       (.I0(res[7]),
        .I1(res[6]),
        .I2(res[4]),
        .I3(res[5]),
        .I4(\ndisp_reg[1] [0]),
        .I5(\seg_OBUF[0]_inst_i_4_n_0 ),
        .O(\seg_OBUF[0]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h2094FFFF20940000)) 
    \seg_OBUF[0]_inst_i_3 
       (.I0(res[15]),
        .I1(res[14]),
        .I2(res[12]),
        .I3(res[13]),
        .I4(\ndisp_reg[1] [0]),
        .I5(\seg_OBUF[0]_inst_i_5_n_0 ),
        .O(\seg_OBUF[0]_inst_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h2094)) 
    \seg_OBUF[0]_inst_i_4 
       (.I0(res[3]),
        .I1(res[2]),
        .I2(res[0]),
        .I3(res[1]),
        .O(\seg_OBUF[0]_inst_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h2094)) 
    \seg_OBUF[0]_inst_i_5 
       (.I0(res[11]),
        .I1(res[10]),
        .I2(res[8]),
        .I3(res[9]),
        .O(\seg_OBUF[0]_inst_i_5_n_0 ));
  MUXF7 \seg_OBUF[1]_inst_i_1 
       (.I0(\seg_OBUF[1]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[1]_inst_i_3_n_0 ),
        .O(seg_OBUF[1]),
        .S(\ndisp_reg[1] [1]));
  LUT6 #(
    .INIT(64'hB680FFFFB6800000)) 
    \seg_OBUF[1]_inst_i_2 
       (.I0(res[7]),
        .I1(res[4]),
        .I2(res[5]),
        .I3(res[6]),
        .I4(\ndisp_reg[1] [0]),
        .I5(\seg_OBUF[1]_inst_i_4_n_0 ),
        .O(\seg_OBUF[1]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB680FFFFB6800000)) 
    \seg_OBUF[1]_inst_i_3 
       (.I0(res[15]),
        .I1(res[12]),
        .I2(res[13]),
        .I3(res[14]),
        .I4(\ndisp_reg[1] [0]),
        .I5(\seg_OBUF[1]_inst_i_5_n_0 ),
        .O(\seg_OBUF[1]_inst_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hB680)) 
    \seg_OBUF[1]_inst_i_4 
       (.I0(res[3]),
        .I1(res[0]),
        .I2(res[1]),
        .I3(res[2]),
        .O(\seg_OBUF[1]_inst_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hB680)) 
    \seg_OBUF[1]_inst_i_5 
       (.I0(res[11]),
        .I1(res[8]),
        .I2(res[9]),
        .I3(res[10]),
        .O(\seg_OBUF[1]_inst_i_5_n_0 ));
  MUXF7 \seg_OBUF[2]_inst_i_1 
       (.I0(\seg_OBUF[2]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[2]_inst_i_3_n_0 ),
        .O(seg_OBUF[2]),
        .S(\ndisp_reg[1] [1]));
  LUT6 #(
    .INIT(64'hA210FFFFA2100000)) 
    \seg_OBUF[2]_inst_i_2 
       (.I0(res[7]),
        .I1(res[4]),
        .I2(res[5]),
        .I3(res[6]),
        .I4(\ndisp_reg[1] [0]),
        .I5(\seg_OBUF[2]_inst_i_4_n_0 ),
        .O(\seg_OBUF[2]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hA210FFFFA2100000)) 
    \seg_OBUF[2]_inst_i_3 
       (.I0(res[15]),
        .I1(res[12]),
        .I2(res[13]),
        .I3(res[14]),
        .I4(\ndisp_reg[1] [0]),
        .I5(\seg_OBUF[2]_inst_i_5_n_0 ),
        .O(\seg_OBUF[2]_inst_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'hA210)) 
    \seg_OBUF[2]_inst_i_4 
       (.I0(res[3]),
        .I1(res[0]),
        .I2(res[1]),
        .I3(res[2]),
        .O(\seg_OBUF[2]_inst_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hA210)) 
    \seg_OBUF[2]_inst_i_5 
       (.I0(res[11]),
        .I1(res[8]),
        .I2(res[9]),
        .I3(res[10]),
        .O(\seg_OBUF[2]_inst_i_5_n_0 ));
  MUXF7 \seg_OBUF[3]_inst_i_1 
       (.I0(\seg_OBUF[3]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[3]_inst_i_3_n_0 ),
        .O(seg_OBUF[3]),
        .S(\ndisp_reg[1] [1]));
  LUT6 #(
    .INIT(64'hC214FFFFC2140000)) 
    \seg_OBUF[3]_inst_i_2 
       (.I0(res[7]),
        .I1(res[6]),
        .I2(res[4]),
        .I3(res[5]),
        .I4(\ndisp_reg[1] [0]),
        .I5(\seg_OBUF[3]_inst_i_4_n_0 ),
        .O(\seg_OBUF[3]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hC214FFFFC2140000)) 
    \seg_OBUF[3]_inst_i_3 
       (.I0(res[15]),
        .I1(res[14]),
        .I2(res[12]),
        .I3(res[13]),
        .I4(\ndisp_reg[1] [0]),
        .I5(\seg_OBUF[3]_inst_i_5_n_0 ),
        .O(\seg_OBUF[3]_inst_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hC214)) 
    \seg_OBUF[3]_inst_i_4 
       (.I0(res[3]),
        .I1(res[2]),
        .I2(res[0]),
        .I3(res[1]),
        .O(\seg_OBUF[3]_inst_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hC214)) 
    \seg_OBUF[3]_inst_i_5 
       (.I0(res[11]),
        .I1(res[10]),
        .I2(res[8]),
        .I3(res[9]),
        .O(\seg_OBUF[3]_inst_i_5_n_0 ));
  MUXF7 \seg_OBUF[4]_inst_i_1 
       (.I0(\seg_OBUF[4]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[4]_inst_i_3_n_0 ),
        .O(seg_OBUF[4]),
        .S(\ndisp_reg[1] [1]));
  LUT6 #(
    .INIT(64'h5710FFFF57100000)) 
    \seg_OBUF[4]_inst_i_2 
       (.I0(res[7]),
        .I1(res[5]),
        .I2(res[6]),
        .I3(res[4]),
        .I4(\ndisp_reg[1] [0]),
        .I5(\seg_OBUF[4]_inst_i_4_n_0 ),
        .O(\seg_OBUF[4]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5710FFFF57100000)) 
    \seg_OBUF[4]_inst_i_3 
       (.I0(res[15]),
        .I1(res[13]),
        .I2(res[14]),
        .I3(res[12]),
        .I4(\ndisp_reg[1] [0]),
        .I5(\seg_OBUF[4]_inst_i_5_n_0 ),
        .O(\seg_OBUF[4]_inst_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h5710)) 
    \seg_OBUF[4]_inst_i_4 
       (.I0(res[3]),
        .I1(res[1]),
        .I2(res[2]),
        .I3(res[0]),
        .O(\seg_OBUF[4]_inst_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h5710)) 
    \seg_OBUF[4]_inst_i_5 
       (.I0(res[11]),
        .I1(res[9]),
        .I2(res[10]),
        .I3(res[8]),
        .O(\seg_OBUF[4]_inst_i_5_n_0 ));
  MUXF7 \seg_OBUF[5]_inst_i_1 
       (.I0(\seg_OBUF[5]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[5]_inst_i_3_n_0 ),
        .O(seg_OBUF[5]),
        .S(\ndisp_reg[1] [1]));
  LUT6 #(
    .INIT(64'h5910FFFF59100000)) 
    \seg_OBUF[5]_inst_i_2 
       (.I0(res[7]),
        .I1(res[6]),
        .I2(res[5]),
        .I3(res[4]),
        .I4(\ndisp_reg[1] [0]),
        .I5(\seg_OBUF[5]_inst_i_4_n_0 ),
        .O(\seg_OBUF[5]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5910FFFF59100000)) 
    \seg_OBUF[5]_inst_i_3 
       (.I0(res[15]),
        .I1(res[14]),
        .I2(res[13]),
        .I3(res[12]),
        .I4(\ndisp_reg[1] [0]),
        .I5(\seg_OBUF[5]_inst_i_5_n_0 ),
        .O(\seg_OBUF[5]_inst_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h5910)) 
    \seg_OBUF[5]_inst_i_4 
       (.I0(res[3]),
        .I1(res[2]),
        .I2(res[1]),
        .I3(res[0]),
        .O(\seg_OBUF[5]_inst_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h5910)) 
    \seg_OBUF[5]_inst_i_5 
       (.I0(res[11]),
        .I1(res[10]),
        .I2(res[9]),
        .I3(res[8]),
        .O(\seg_OBUF[5]_inst_i_5_n_0 ));
  MUXF7 \seg_OBUF[6]_inst_i_1 
       (.I0(\seg_OBUF[6]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[6]_inst_i_3_n_0 ),
        .O(seg_OBUF[6]),
        .S(\ndisp_reg[1] [1]));
  LUT6 #(
    .INIT(64'h4025FFFF40250000)) 
    \seg_OBUF[6]_inst_i_2 
       (.I0(res[7]),
        .I1(res[4]),
        .I2(res[6]),
        .I3(res[5]),
        .I4(\ndisp_reg[1] [0]),
        .I5(\seg_OBUF[6]_inst_i_4_n_0 ),
        .O(\seg_OBUF[6]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h4025FFFF40250000)) 
    \seg_OBUF[6]_inst_i_3 
       (.I0(res[15]),
        .I1(res[12]),
        .I2(res[14]),
        .I3(res[13]),
        .I4(\ndisp_reg[1] [0]),
        .I5(\seg_OBUF[6]_inst_i_5_n_0 ),
        .O(\seg_OBUF[6]_inst_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h4025)) 
    \seg_OBUF[6]_inst_i_4 
       (.I0(res[3]),
        .I1(res[0]),
        .I2(res[2]),
        .I3(res[1]),
        .O(\seg_OBUF[6]_inst_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h4025)) 
    \seg_OBUF[6]_inst_i_5 
       (.I0(res[11]),
        .I1(res[8]),
        .I2(res[10]),
        .I3(res[9]),
        .O(\seg_OBUF[6]_inst_i_5_n_0 ));
endmodule

module clkdiv
   (clk10hz,
    CLK,
    clk);
  output clk10hz;
  output CLK;
  input clk;

  wire CLK;
  wire I;
  wire clear;
  wire clk;
  wire clk10hz;
  wire \cnt[0]_i_3_n_0 ;
  wire \cnt[0]_i_4_n_0 ;
  wire \cnt[0]_i_5_n_0 ;
  wire \cnt[0]_i_6_n_0 ;
  wire \cnt[0]_i_7_n_0 ;
  wire [22:0]cnt_reg;
  wire \cnt_reg[0]_i_2_n_0 ;
  wire \cnt_reg[0]_i_2_n_1 ;
  wire \cnt_reg[0]_i_2_n_2 ;
  wire \cnt_reg[0]_i_2_n_3 ;
  wire \cnt_reg[0]_i_2_n_4 ;
  wire \cnt_reg[0]_i_2_n_5 ;
  wire \cnt_reg[0]_i_2_n_6 ;
  wire \cnt_reg[0]_i_2_n_7 ;
  wire \cnt_reg[12]_i_1_n_0 ;
  wire \cnt_reg[12]_i_1_n_1 ;
  wire \cnt_reg[12]_i_1_n_2 ;
  wire \cnt_reg[12]_i_1_n_3 ;
  wire \cnt_reg[12]_i_1_n_4 ;
  wire \cnt_reg[12]_i_1_n_5 ;
  wire \cnt_reg[12]_i_1_n_6 ;
  wire \cnt_reg[12]_i_1_n_7 ;
  wire \cnt_reg[16]_i_1_n_0 ;
  wire \cnt_reg[16]_i_1_n_1 ;
  wire \cnt_reg[16]_i_1_n_2 ;
  wire \cnt_reg[16]_i_1_n_3 ;
  wire \cnt_reg[16]_i_1_n_4 ;
  wire \cnt_reg[16]_i_1_n_5 ;
  wire \cnt_reg[16]_i_1_n_6 ;
  wire \cnt_reg[16]_i_1_n_7 ;
  wire \cnt_reg[20]_i_1_n_1 ;
  wire \cnt_reg[20]_i_1_n_2 ;
  wire \cnt_reg[20]_i_1_n_3 ;
  wire \cnt_reg[20]_i_1_n_4 ;
  wire \cnt_reg[20]_i_1_n_5 ;
  wire \cnt_reg[20]_i_1_n_6 ;
  wire \cnt_reg[20]_i_1_n_7 ;
  wire \cnt_reg[4]_i_1_n_0 ;
  wire \cnt_reg[4]_i_1_n_1 ;
  wire \cnt_reg[4]_i_1_n_2 ;
  wire \cnt_reg[4]_i_1_n_3 ;
  wire \cnt_reg[4]_i_1_n_4 ;
  wire \cnt_reg[4]_i_1_n_5 ;
  wire \cnt_reg[4]_i_1_n_6 ;
  wire \cnt_reg[4]_i_1_n_7 ;
  wire \cnt_reg[8]_i_1_n_0 ;
  wire \cnt_reg[8]_i_1_n_1 ;
  wire \cnt_reg[8]_i_1_n_2 ;
  wire \cnt_reg[8]_i_1_n_3 ;
  wire \cnt_reg[8]_i_1_n_4 ;
  wire \cnt_reg[8]_i_1_n_5 ;
  wire \cnt_reg[8]_i_1_n_6 ;
  wire \cnt_reg[8]_i_1_n_7 ;
  wire \cnt_reg_n_0_[16] ;
  wire [3:3]\NLW_cnt_reg[20]_i_1_CO_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  BUFG BUFG_INST2
       (.I(I),
        .O(clk10hz));
  (* box_type = "PRIMITIVE" *) 
  BUFG BUFG_INST3
       (.I(\cnt_reg_n_0_[16] ),
        .O(CLK));
  LUT4 #(
    .INIT(16'h4000)) 
    \cnt[0]_i_1 
       (.I0(\cnt[0]_i_3_n_0 ),
        .I1(\cnt[0]_i_4_n_0 ),
        .I2(\cnt[0]_i_5_n_0 ),
        .I3(\cnt[0]_i_6_n_0 ),
        .O(clear));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFBF)) 
    \cnt[0]_i_3 
       (.I0(cnt_reg[7]),
        .I1(cnt_reg[20]),
        .I2(I),
        .I3(cnt_reg[13]),
        .I4(cnt_reg[8]),
        .I5(cnt_reg[11]),
        .O(\cnt[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \cnt[0]_i_4 
       (.I0(cnt_reg[22]),
        .I1(cnt_reg[18]),
        .I2(cnt_reg[21]),
        .I3(cnt_reg[14]),
        .I4(\cnt_reg_n_0_[16] ),
        .I5(cnt_reg[17]),
        .O(\cnt[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \cnt[0]_i_5 
       (.I0(cnt_reg[12]),
        .I1(cnt_reg[15]),
        .I2(cnt_reg[19]),
        .I3(cnt_reg[10]),
        .I4(cnt_reg[6]),
        .I5(cnt_reg[9]),
        .O(\cnt[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \cnt[0]_i_6 
       (.I0(cnt_reg[3]),
        .I1(cnt_reg[4]),
        .I2(cnt_reg[5]),
        .I3(cnt_reg[2]),
        .I4(cnt_reg[0]),
        .I5(cnt_reg[1]),
        .O(\cnt[0]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt[0]_i_7 
       (.I0(cnt_reg[0]),
        .O(\cnt[0]_i_7_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_7 ),
        .Q(cnt_reg[0]),
        .R(clear));
  CARRY4 \cnt_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\cnt_reg[0]_i_2_n_0 ,\cnt_reg[0]_i_2_n_1 ,\cnt_reg[0]_i_2_n_2 ,\cnt_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_reg[0]_i_2_n_4 ,\cnt_reg[0]_i_2_n_5 ,\cnt_reg[0]_i_2_n_6 ,\cnt_reg[0]_i_2_n_7 }),
        .S({cnt_reg[3:1],\cnt[0]_i_7_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_5 ),
        .Q(cnt_reg[10]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_4 ),
        .Q(cnt_reg[11]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_7 ),
        .Q(cnt_reg[12]),
        .R(clear));
  CARRY4 \cnt_reg[12]_i_1 
       (.CI(\cnt_reg[8]_i_1_n_0 ),
        .CO({\cnt_reg[12]_i_1_n_0 ,\cnt_reg[12]_i_1_n_1 ,\cnt_reg[12]_i_1_n_2 ,\cnt_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[12]_i_1_n_4 ,\cnt_reg[12]_i_1_n_5 ,\cnt_reg[12]_i_1_n_6 ,\cnt_reg[12]_i_1_n_7 }),
        .S(cnt_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_6 ),
        .Q(cnt_reg[13]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_5 ),
        .Q(cnt_reg[14]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_4 ),
        .Q(cnt_reg[15]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[16] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[16]_i_1_n_7 ),
        .Q(\cnt_reg_n_0_[16] ),
        .R(clear));
  CARRY4 \cnt_reg[16]_i_1 
       (.CI(\cnt_reg[12]_i_1_n_0 ),
        .CO({\cnt_reg[16]_i_1_n_0 ,\cnt_reg[16]_i_1_n_1 ,\cnt_reg[16]_i_1_n_2 ,\cnt_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[16]_i_1_n_4 ,\cnt_reg[16]_i_1_n_5 ,\cnt_reg[16]_i_1_n_6 ,\cnt_reg[16]_i_1_n_7 }),
        .S({cnt_reg[19:17],\cnt_reg_n_0_[16] }));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[17] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[16]_i_1_n_6 ),
        .Q(cnt_reg[17]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[18] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[16]_i_1_n_5 ),
        .Q(cnt_reg[18]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[19] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[16]_i_1_n_4 ),
        .Q(cnt_reg[19]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_6 ),
        .Q(cnt_reg[1]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[20] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[20]_i_1_n_7 ),
        .Q(cnt_reg[20]),
        .R(clear));
  CARRY4 \cnt_reg[20]_i_1 
       (.CI(\cnt_reg[16]_i_1_n_0 ),
        .CO({\NLW_cnt_reg[20]_i_1_CO_UNCONNECTED [3],\cnt_reg[20]_i_1_n_1 ,\cnt_reg[20]_i_1_n_2 ,\cnt_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[20]_i_1_n_4 ,\cnt_reg[20]_i_1_n_5 ,\cnt_reg[20]_i_1_n_6 ,\cnt_reg[20]_i_1_n_7 }),
        .S({I,cnt_reg[22:20]}));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[21] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[20]_i_1_n_6 ),
        .Q(cnt_reg[21]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[22] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[20]_i_1_n_5 ),
        .Q(cnt_reg[22]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[23] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[20]_i_1_n_4 ),
        .Q(I),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_5 ),
        .Q(cnt_reg[2]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_4 ),
        .Q(cnt_reg[3]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_7 ),
        .Q(cnt_reg[4]),
        .R(clear));
  CARRY4 \cnt_reg[4]_i_1 
       (.CI(\cnt_reg[0]_i_2_n_0 ),
        .CO({\cnt_reg[4]_i_1_n_0 ,\cnt_reg[4]_i_1_n_1 ,\cnt_reg[4]_i_1_n_2 ,\cnt_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[4]_i_1_n_4 ,\cnt_reg[4]_i_1_n_5 ,\cnt_reg[4]_i_1_n_6 ,\cnt_reg[4]_i_1_n_7 }),
        .S(cnt_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_6 ),
        .Q(cnt_reg[5]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_5 ),
        .Q(cnt_reg[6]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_4 ),
        .Q(cnt_reg[7]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_7 ),
        .Q(cnt_reg[8]),
        .R(clear));
  CARRY4 \cnt_reg[8]_i_1 
       (.CI(\cnt_reg[4]_i_1_n_0 ),
        .CO({\cnt_reg[8]_i_1_n_0 ,\cnt_reg[8]_i_1_n_1 ,\cnt_reg[8]_i_1_n_2 ,\cnt_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[8]_i_1_n_4 ,\cnt_reg[8]_i_1_n_5 ,\cnt_reg[8]_i_1_n_6 ,\cnt_reg[8]_i_1_n_7 }),
        .S(cnt_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_6 ),
        .Q(cnt_reg[9]),
        .R(clear));
endmodule

module disp7
   (Q,
    an_OBUF,
    dp_OBUF,
    oper,
    CLK);
  output [1:0]Q;
  output [3:0]an_OBUF;
  output dp_OBUF;
  input [3:0]oper;
  input CLK;

  wire CLK;
  wire [1:0]Q;
  wire [3:0]an_OBUF;
  wire dp_OBUF;
  wire [3:0]oper;
  wire [1:0]plusOp;

  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \an_OBUF[0]_inst_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(an_OBUF[0]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \an_OBUF[1]_inst_i_1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .O(an_OBUF[1]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \an_OBUF[2]_inst_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(an_OBUF[2]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \an_OBUF[3]_inst_i_1 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(an_OBUF[3]));
  LUT6 #(
    .INIT(64'h505F3030505F3F3F)) 
    dp_OBUF_inst_i_1
       (.I0(oper[2]),
        .I1(oper[0]),
        .I2(Q[1]),
        .I3(oper[1]),
        .I4(Q[0]),
        .I5(oper[3]),
        .O(dp_OBUF));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \ndisp[0]_i_1 
       (.I0(Q[0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \ndisp[1]_i_1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .O(plusOp[1]));
  FDRE #(
    .INIT(1'b0)) 
    \ndisp_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(plusOp[0]),
        .Q(Q[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ndisp_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(plusOp[1]),
        .Q(Q[1]),
        .R(1'b0));
endmodule

(* NotValidForBitStream *)
module fpga_basicIO
   (clk,
    btnC,
    btnU,
    btnL,
    btnR,
    btnD,
    sw,
    led,
    an,
    seg,
    dp);
  input clk;
  input btnC;
  input btnU;
  input btnL;
  input btnR;
  input btnD;
  input [15:0]sw;
  output [15:0]led;
  output [3:0]an;
  output [6:0]seg;
  output dp;

  wire [3:0]an;
  wire [3:0]an_OBUF;
  wire btnC;
  wire btnC_IBUF;
  wire btnCreg;
  wire btnD;
  wire btnD_IBUF;
  wire btnL;
  wire btnL_IBUF;
  wire btnR;
  wire btnR_IBUF;
  wire btnU;
  wire btnU_IBUF;
  wire clk;
  wire clk10hz;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire dclk;
  wire dp;
  wire dp_OBUF;
  wire [15:0]led;
  wire [15:0]led_OBUF;
  wire [1:0]ndisp;
  wire [5:2]oper;
  wire [6:0]seg;
  wire [6:0]seg_OBUF;
  wire [15:0]sw;
  wire [15:0]sw_IBUF;

  OBUF \an_OBUF[0]_inst 
       (.I(an_OBUF[0]),
        .O(an[0]));
  OBUF \an_OBUF[1]_inst 
       (.I(an_OBUF[1]),
        .O(an[1]));
  OBUF \an_OBUF[2]_inst 
       (.I(an_OBUF[2]),
        .O(an[2]));
  OBUF \an_OBUF[3]_inst 
       (.I(an_OBUF[3]),
        .O(an[3]));
  IBUF btnC_IBUF_inst
       (.I(btnC),
        .O(btnC_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    btnCreg_reg
       (.C(clk10hz),
        .CE(1'b1),
        .D(btnC_IBUF),
        .Q(btnCreg),
        .R(1'b0));
  IBUF btnD_IBUF_inst
       (.I(btnD),
        .O(btnD_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    btnDreg_reg
       (.C(clk10hz),
        .CE(1'b1),
        .D(btnD_IBUF),
        .Q(oper[2]),
        .R(1'b0));
  IBUF btnL_IBUF_inst
       (.I(btnL),
        .O(btnL_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    btnLreg_reg
       (.C(clk10hz),
        .CE(1'b1),
        .D(btnL_IBUF),
        .Q(oper[4]),
        .R(1'b0));
  IBUF btnR_IBUF_inst
       (.I(btnR),
        .O(btnR_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    btnRreg_reg
       (.C(clk10hz),
        .CE(1'b1),
        .D(btnR_IBUF),
        .Q(oper[3]),
        .R(1'b0));
  IBUF btnU_IBUF_inst
       (.I(btnU),
        .O(btnU_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    btnUreg_reg
       (.C(clk10hz),
        .CE(1'b1),
        .D(btnU_IBUF),
        .Q(oper[5]),
        .R(1'b0));
  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  OBUF dp_OBUF_inst
       (.I(dp_OBUF),
        .O(dp));
  alu inst_alu
       (.Q(led_OBUF),
        .btnCreg(btnCreg),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .\ndisp_reg[1] (ndisp),
        .oper(oper),
        .seg_OBUF(seg_OBUF));
  clkdiv inst_clkdiv
       (.CLK(dclk),
        .clk(clk_IBUF_BUFG),
        .clk10hz(clk10hz));
  disp7 inst_disp7
       (.CLK(dclk),
        .Q(ndisp),
        .an_OBUF(an_OBUF),
        .dp_OBUF(dp_OBUF),
        .oper(oper));
  OBUF \led_OBUF[0]_inst 
       (.I(led_OBUF[0]),
        .O(led[0]));
  OBUF \led_OBUF[10]_inst 
       (.I(led_OBUF[10]),
        .O(led[10]));
  OBUF \led_OBUF[11]_inst 
       (.I(led_OBUF[11]),
        .O(led[11]));
  OBUF \led_OBUF[12]_inst 
       (.I(led_OBUF[12]),
        .O(led[12]));
  OBUF \led_OBUF[13]_inst 
       (.I(led_OBUF[13]),
        .O(led[13]));
  OBUF \led_OBUF[14]_inst 
       (.I(led_OBUF[14]),
        .O(led[14]));
  OBUF \led_OBUF[15]_inst 
       (.I(led_OBUF[15]),
        .O(led[15]));
  OBUF \led_OBUF[1]_inst 
       (.I(led_OBUF[1]),
        .O(led[1]));
  OBUF \led_OBUF[2]_inst 
       (.I(led_OBUF[2]),
        .O(led[2]));
  OBUF \led_OBUF[3]_inst 
       (.I(led_OBUF[3]),
        .O(led[3]));
  OBUF \led_OBUF[4]_inst 
       (.I(led_OBUF[4]),
        .O(led[4]));
  OBUF \led_OBUF[5]_inst 
       (.I(led_OBUF[5]),
        .O(led[5]));
  OBUF \led_OBUF[6]_inst 
       (.I(led_OBUF[6]),
        .O(led[6]));
  OBUF \led_OBUF[7]_inst 
       (.I(led_OBUF[7]),
        .O(led[7]));
  OBUF \led_OBUF[8]_inst 
       (.I(led_OBUF[8]),
        .O(led[8]));
  OBUF \led_OBUF[9]_inst 
       (.I(led_OBUF[9]),
        .O(led[9]));
  OBUF \seg_OBUF[0]_inst 
       (.I(seg_OBUF[0]),
        .O(seg[0]));
  OBUF \seg_OBUF[1]_inst 
       (.I(seg_OBUF[1]),
        .O(seg[1]));
  OBUF \seg_OBUF[2]_inst 
       (.I(seg_OBUF[2]),
        .O(seg[2]));
  OBUF \seg_OBUF[3]_inst 
       (.I(seg_OBUF[3]),
        .O(seg[3]));
  OBUF \seg_OBUF[4]_inst 
       (.I(seg_OBUF[4]),
        .O(seg[4]));
  OBUF \seg_OBUF[5]_inst 
       (.I(seg_OBUF[5]),
        .O(seg[5]));
  OBUF \seg_OBUF[6]_inst 
       (.I(seg_OBUF[6]),
        .O(seg[6]));
  IBUF \sw_IBUF[0]_inst 
       (.I(sw[0]),
        .O(sw_IBUF[0]));
  IBUF \sw_IBUF[10]_inst 
       (.I(sw[10]),
        .O(sw_IBUF[10]));
  IBUF \sw_IBUF[11]_inst 
       (.I(sw[11]),
        .O(sw_IBUF[11]));
  IBUF \sw_IBUF[12]_inst 
       (.I(sw[12]),
        .O(sw_IBUF[12]));
  IBUF \sw_IBUF[13]_inst 
       (.I(sw[13]),
        .O(sw_IBUF[13]));
  IBUF \sw_IBUF[14]_inst 
       (.I(sw[14]),
        .O(sw_IBUF[14]));
  IBUF \sw_IBUF[15]_inst 
       (.I(sw[15]),
        .O(sw_IBUF[15]));
  IBUF \sw_IBUF[1]_inst 
       (.I(sw[1]),
        .O(sw_IBUF[1]));
  IBUF \sw_IBUF[2]_inst 
       (.I(sw[2]),
        .O(sw_IBUF[2]));
  IBUF \sw_IBUF[3]_inst 
       (.I(sw[3]),
        .O(sw_IBUF[3]));
  IBUF \sw_IBUF[4]_inst 
       (.I(sw[4]),
        .O(sw_IBUF[4]));
  IBUF \sw_IBUF[5]_inst 
       (.I(sw[5]),
        .O(sw_IBUF[5]));
  IBUF \sw_IBUF[6]_inst 
       (.I(sw[6]),
        .O(sw_IBUF[6]));
  IBUF \sw_IBUF[7]_inst 
       (.I(sw[7]),
        .O(sw_IBUF[7]));
  IBUF \sw_IBUF[8]_inst 
       (.I(sw[8]),
        .O(sw_IBUF[8]));
  IBUF \sw_IBUF[9]_inst 
       (.I(sw[9]),
        .O(sw_IBUF[9]));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[0] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[0]),
        .Q(led_OBUF[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[10] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[10]),
        .Q(led_OBUF[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[11] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[11]),
        .Q(led_OBUF[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[12] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[12]),
        .Q(led_OBUF[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[13] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[13]),
        .Q(led_OBUF[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[14] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[14]),
        .Q(led_OBUF[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[15] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[15]),
        .Q(led_OBUF[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[1] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[1]),
        .Q(led_OBUF[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[2] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[2]),
        .Q(led_OBUF[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[3] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[3]),
        .Q(led_OBUF[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[4] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[4]),
        .Q(led_OBUF[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[5] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[5]),
        .Q(led_OBUF[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[6] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[6]),
        .Q(led_OBUF[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[7] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[7]),
        .Q(led_OBUF[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[8] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[8]),
        .Q(led_OBUF[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[9] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[9]),
        .Q(led_OBUF[9]),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

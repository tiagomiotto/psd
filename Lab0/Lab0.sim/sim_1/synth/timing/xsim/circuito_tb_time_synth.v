// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Fri Sep 28 18:06:46 2018
// Host        : LAPTOP-TIAGO running 64-bit major release  (build 9200)
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               C:/Users/tiago/Desktop/Programing/PSD/Lab0/Lab0.sim/sim_1/synth/timing/xsim/circuito_tb_time_synth.v
// Design      : fpga_basicIO
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

module circuito
   (\seg[3] ,
    \seg[6] ,
    \seg[0] ,
    \seg[1] ,
    \seg[5] ,
    \seg[3]_0 ,
    \seg[6]_0 ,
    \seg[0]_0 ,
    \seg[1]_0 ,
    \seg[5]_0 ,
    \seg[2] ,
    \seg[4] ,
    \seg[4]_0 ,
    \seg[2]_0 ,
    Q,
    instr,
    btnCreg,
    clk_IBUF_BUFG);
  output \seg[3] ;
  output \seg[6] ;
  output \seg[0] ;
  output \seg[1] ;
  output \seg[5] ;
  output \seg[3]_0 ;
  output \seg[6]_0 ;
  output \seg[0]_0 ;
  output \seg[1]_0 ;
  output \seg[5]_0 ;
  output \seg[2] ;
  output \seg[4] ;
  output \seg[4]_0 ;
  output \seg[2]_0 ;
  input [7:0]Q;
  input [2:0]instr;
  input btnCreg;
  input clk_IBUF_BUFG;

  wire [7:0]Q;
  wire btnCreg;
  wire clk_IBUF_BUFG;
  wire [2:2]currstate;
  wire enable;
  wire inst_control_n_2;
  wire inst_control_n_3;
  wire inst_control_n_4;
  wire inst_control_n_5;
  wire inst_control_n_6;
  wire inst_control_n_7;
  wire inst_control_n_8;
  wire inst_control_n_9;
  wire inst_datapath_n_0;
  wire inst_datapath_n_1;
  wire inst_datapath_n_2;
  wire inst_datapath_n_3;
  wire inst_datapath_n_4;
  wire inst_datapath_n_5;
  wire inst_datapath_n_6;
  wire [2:0]instr;
  wire \seg[0] ;
  wire \seg[0]_0 ;
  wire \seg[1] ;
  wire \seg[1]_0 ;
  wire \seg[2] ;
  wire \seg[2]_0 ;
  wire \seg[3] ;
  wire \seg[3]_0 ;
  wire \seg[4] ;
  wire \seg[4]_0 ;
  wire \seg[5] ;
  wire \seg[5]_0 ;
  wire \seg[6] ;
  wire \seg[6]_0 ;

  control inst_control
       (.DI(inst_control_n_9),
        .E(enable),
        .Q(Q[7:1]),
        .S({inst_control_n_2,inst_control_n_3,inst_control_n_4,inst_control_n_5}),
        .\accum_reg[3] ({inst_control_n_6,inst_control_n_7,inst_control_n_8}),
        .\accum_reg[7] ({inst_datapath_n_0,inst_datapath_n_1,inst_datapath_n_2,inst_datapath_n_3,inst_datapath_n_4,inst_datapath_n_5,inst_datapath_n_6}),
        .btnCreg(btnCreg),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .\currstate_reg[2]_0 (currstate),
        .instr(instr));
  datapath inst_datapath
       (.DI(inst_control_n_9),
        .E(enable),
        .Q({inst_datapath_n_0,inst_datapath_n_1,inst_datapath_n_2,inst_datapath_n_3,inst_datapath_n_4,inst_datapath_n_5,inst_datapath_n_6}),
        .S({inst_control_n_2,inst_control_n_3,inst_control_n_4,inst_control_n_5}),
        .btnCreg(btnCreg),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .\currstate_reg[2] (currstate),
        .\seg[0] (\seg[0] ),
        .\seg[0]_0 (\seg[0]_0 ),
        .\seg[1] (\seg[1] ),
        .\seg[1]_0 (\seg[1]_0 ),
        .\seg[2] (\seg[2] ),
        .\seg[2]_0 (\seg[2]_0 ),
        .\seg[3] (\seg[3] ),
        .\seg[3]_0 (\seg[3]_0 ),
        .\seg[4] (\seg[4] ),
        .\seg[4]_0 (\seg[4]_0 ),
        .\seg[5] (\seg[5] ),
        .\seg[5]_0 (\seg[5]_0 ),
        .\seg[6] (\seg[6] ),
        .\seg[6]_0 (\seg[6]_0 ),
        .\sw_reg_reg[3] ({inst_control_n_6,inst_control_n_7,inst_control_n_8}),
        .\sw_reg_reg[7] (Q));
endmodule

module clkdiv
   (clk10hz,
    CLK,
    clk);
  output clk10hz;
  output CLK;
  input clk;

  wire CLK;
  wire I;
  wire clear;
  wire clk;
  wire clk10hz;
  wire \cnt[0]_i_3_n_0 ;
  wire \cnt[0]_i_4_n_0 ;
  wire \cnt[0]_i_5_n_0 ;
  wire \cnt[0]_i_6_n_0 ;
  wire \cnt[0]_i_7_n_0 ;
  wire [22:0]cnt_reg;
  wire \cnt_reg[0]_i_2_n_0 ;
  wire \cnt_reg[0]_i_2_n_1 ;
  wire \cnt_reg[0]_i_2_n_2 ;
  wire \cnt_reg[0]_i_2_n_3 ;
  wire \cnt_reg[0]_i_2_n_4 ;
  wire \cnt_reg[0]_i_2_n_5 ;
  wire \cnt_reg[0]_i_2_n_6 ;
  wire \cnt_reg[0]_i_2_n_7 ;
  wire \cnt_reg[12]_i_1_n_0 ;
  wire \cnt_reg[12]_i_1_n_1 ;
  wire \cnt_reg[12]_i_1_n_2 ;
  wire \cnt_reg[12]_i_1_n_3 ;
  wire \cnt_reg[12]_i_1_n_4 ;
  wire \cnt_reg[12]_i_1_n_5 ;
  wire \cnt_reg[12]_i_1_n_6 ;
  wire \cnt_reg[12]_i_1_n_7 ;
  wire \cnt_reg[16]_i_1_n_0 ;
  wire \cnt_reg[16]_i_1_n_1 ;
  wire \cnt_reg[16]_i_1_n_2 ;
  wire \cnt_reg[16]_i_1_n_3 ;
  wire \cnt_reg[16]_i_1_n_4 ;
  wire \cnt_reg[16]_i_1_n_5 ;
  wire \cnt_reg[16]_i_1_n_6 ;
  wire \cnt_reg[16]_i_1_n_7 ;
  wire \cnt_reg[20]_i_1_n_1 ;
  wire \cnt_reg[20]_i_1_n_2 ;
  wire \cnt_reg[20]_i_1_n_3 ;
  wire \cnt_reg[20]_i_1_n_4 ;
  wire \cnt_reg[20]_i_1_n_5 ;
  wire \cnt_reg[20]_i_1_n_6 ;
  wire \cnt_reg[20]_i_1_n_7 ;
  wire \cnt_reg[4]_i_1_n_0 ;
  wire \cnt_reg[4]_i_1_n_1 ;
  wire \cnt_reg[4]_i_1_n_2 ;
  wire \cnt_reg[4]_i_1_n_3 ;
  wire \cnt_reg[4]_i_1_n_4 ;
  wire \cnt_reg[4]_i_1_n_5 ;
  wire \cnt_reg[4]_i_1_n_6 ;
  wire \cnt_reg[4]_i_1_n_7 ;
  wire \cnt_reg[8]_i_1_n_0 ;
  wire \cnt_reg[8]_i_1_n_1 ;
  wire \cnt_reg[8]_i_1_n_2 ;
  wire \cnt_reg[8]_i_1_n_3 ;
  wire \cnt_reg[8]_i_1_n_4 ;
  wire \cnt_reg[8]_i_1_n_5 ;
  wire \cnt_reg[8]_i_1_n_6 ;
  wire \cnt_reg[8]_i_1_n_7 ;
  wire \cnt_reg_n_0_[16] ;
  wire [3:3]\NLW_cnt_reg[20]_i_1_CO_UNCONNECTED ;

  (* box_type = "PRIMITIVE" *) 
  BUFG BUFG_INST2
       (.I(I),
        .O(clk10hz));
  (* box_type = "PRIMITIVE" *) 
  BUFG BUFG_INST3
       (.I(\cnt_reg_n_0_[16] ),
        .O(CLK));
  LUT4 #(
    .INIT(16'h4000)) 
    \cnt[0]_i_1 
       (.I0(\cnt[0]_i_3_n_0 ),
        .I1(\cnt[0]_i_4_n_0 ),
        .I2(\cnt[0]_i_5_n_0 ),
        .I3(\cnt[0]_i_6_n_0 ),
        .O(clear));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFBF)) 
    \cnt[0]_i_3 
       (.I0(cnt_reg[7]),
        .I1(cnt_reg[20]),
        .I2(I),
        .I3(cnt_reg[13]),
        .I4(cnt_reg[8]),
        .I5(cnt_reg[11]),
        .O(\cnt[0]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \cnt[0]_i_4 
       (.I0(cnt_reg[22]),
        .I1(cnt_reg[18]),
        .I2(cnt_reg[21]),
        .I3(cnt_reg[14]),
        .I4(\cnt_reg_n_0_[16] ),
        .I5(cnt_reg[17]),
        .O(\cnt[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \cnt[0]_i_5 
       (.I0(cnt_reg[12]),
        .I1(cnt_reg[15]),
        .I2(cnt_reg[19]),
        .I3(cnt_reg[10]),
        .I4(cnt_reg[6]),
        .I5(cnt_reg[9]),
        .O(\cnt[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \cnt[0]_i_6 
       (.I0(cnt_reg[3]),
        .I1(cnt_reg[4]),
        .I2(cnt_reg[5]),
        .I3(cnt_reg[2]),
        .I4(cnt_reg[0]),
        .I5(cnt_reg[1]),
        .O(\cnt[0]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt[0]_i_7 
       (.I0(cnt_reg[0]),
        .O(\cnt[0]_i_7_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_7 ),
        .Q(cnt_reg[0]),
        .R(clear));
  CARRY4 \cnt_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\cnt_reg[0]_i_2_n_0 ,\cnt_reg[0]_i_2_n_1 ,\cnt_reg[0]_i_2_n_2 ,\cnt_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_reg[0]_i_2_n_4 ,\cnt_reg[0]_i_2_n_5 ,\cnt_reg[0]_i_2_n_6 ,\cnt_reg[0]_i_2_n_7 }),
        .S({cnt_reg[3:1],\cnt[0]_i_7_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[10] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_5 ),
        .Q(cnt_reg[10]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[11] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_4 ),
        .Q(cnt_reg[11]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[12] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_7 ),
        .Q(cnt_reg[12]),
        .R(clear));
  CARRY4 \cnt_reg[12]_i_1 
       (.CI(\cnt_reg[8]_i_1_n_0 ),
        .CO({\cnt_reg[12]_i_1_n_0 ,\cnt_reg[12]_i_1_n_1 ,\cnt_reg[12]_i_1_n_2 ,\cnt_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[12]_i_1_n_4 ,\cnt_reg[12]_i_1_n_5 ,\cnt_reg[12]_i_1_n_6 ,\cnt_reg[12]_i_1_n_7 }),
        .S(cnt_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[13] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_6 ),
        .Q(cnt_reg[13]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[14] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_5 ),
        .Q(cnt_reg[14]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[15] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_4 ),
        .Q(cnt_reg[15]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[16] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[16]_i_1_n_7 ),
        .Q(\cnt_reg_n_0_[16] ),
        .R(clear));
  CARRY4 \cnt_reg[16]_i_1 
       (.CI(\cnt_reg[12]_i_1_n_0 ),
        .CO({\cnt_reg[16]_i_1_n_0 ,\cnt_reg[16]_i_1_n_1 ,\cnt_reg[16]_i_1_n_2 ,\cnt_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[16]_i_1_n_4 ,\cnt_reg[16]_i_1_n_5 ,\cnt_reg[16]_i_1_n_6 ,\cnt_reg[16]_i_1_n_7 }),
        .S({cnt_reg[19:17],\cnt_reg_n_0_[16] }));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[17] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[16]_i_1_n_6 ),
        .Q(cnt_reg[17]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[18] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[16]_i_1_n_5 ),
        .Q(cnt_reg[18]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[19] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[16]_i_1_n_4 ),
        .Q(cnt_reg[19]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_6 ),
        .Q(cnt_reg[1]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[20] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[20]_i_1_n_7 ),
        .Q(cnt_reg[20]),
        .R(clear));
  CARRY4 \cnt_reg[20]_i_1 
       (.CI(\cnt_reg[16]_i_1_n_0 ),
        .CO({\NLW_cnt_reg[20]_i_1_CO_UNCONNECTED [3],\cnt_reg[20]_i_1_n_1 ,\cnt_reg[20]_i_1_n_2 ,\cnt_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[20]_i_1_n_4 ,\cnt_reg[20]_i_1_n_5 ,\cnt_reg[20]_i_1_n_6 ,\cnt_reg[20]_i_1_n_7 }),
        .S({I,cnt_reg[22:20]}));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[21] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[20]_i_1_n_6 ),
        .Q(cnt_reg[21]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[22] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[20]_i_1_n_5 ),
        .Q(cnt_reg[22]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[23] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[20]_i_1_n_4 ),
        .Q(I),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_5 ),
        .Q(cnt_reg[2]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_4 ),
        .Q(cnt_reg[3]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_7 ),
        .Q(cnt_reg[4]),
        .R(clear));
  CARRY4 \cnt_reg[4]_i_1 
       (.CI(\cnt_reg[0]_i_2_n_0 ),
        .CO({\cnt_reg[4]_i_1_n_0 ,\cnt_reg[4]_i_1_n_1 ,\cnt_reg[4]_i_1_n_2 ,\cnt_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[4]_i_1_n_4 ,\cnt_reg[4]_i_1_n_5 ,\cnt_reg[4]_i_1_n_6 ,\cnt_reg[4]_i_1_n_7 }),
        .S(cnt_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_6 ),
        .Q(cnt_reg[5]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[6] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_5 ),
        .Q(cnt_reg[6]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[7] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_4 ),
        .Q(cnt_reg[7]),
        .R(clear));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[8] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_7 ),
        .Q(cnt_reg[8]),
        .R(clear));
  CARRY4 \cnt_reg[8]_i_1 
       (.CI(\cnt_reg[4]_i_1_n_0 ),
        .CO({\cnt_reg[8]_i_1_n_0 ,\cnt_reg[8]_i_1_n_1 ,\cnt_reg[8]_i_1_n_2 ,\cnt_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[8]_i_1_n_4 ,\cnt_reg[8]_i_1_n_5 ,\cnt_reg[8]_i_1_n_6 ,\cnt_reg[8]_i_1_n_7 }),
        .S(cnt_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[9] 
       (.C(clk),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_6 ),
        .Q(cnt_reg[9]),
        .R(clear));
endmodule

module control
   (E,
    \currstate_reg[2]_0 ,
    S,
    \accum_reg[3] ,
    DI,
    Q,
    \accum_reg[7] ,
    instr,
    btnCreg,
    clk_IBUF_BUFG);
  output [0:0]E;
  output [0:0]\currstate_reg[2]_0 ;
  output [3:0]S;
  output [2:0]\accum_reg[3] ;
  output [0:0]DI;
  input [6:0]Q;
  input [6:0]\accum_reg[7] ;
  input [2:0]instr;
  input btnCreg;
  input clk_IBUF_BUFG;

  wire [0:0]DI;
  wire [0:0]E;
  wire [6:0]Q;
  wire [3:0]S;
  wire [2:0]\accum_reg[3] ;
  wire [6:0]\accum_reg[7] ;
  wire btnCreg;
  wire clk_IBUF_BUFG;
  wire [1:0]currstate;
  wire currstate0;
  wire \currstate[0]_i_1_n_0 ;
  wire \currstate[1]_i_1_n_0 ;
  wire \currstate[1]_i_2_n_0 ;
  wire \currstate[2]_i_1_n_0 ;
  wire [0:0]\currstate_reg[2]_0 ;
  wire [2:0]instr;

  LUT4 #(
    .INIT(16'h659A)) 
    __5_carry__0_i_1
       (.I0(Q[6]),
        .I1(currstate[0]),
        .I2(currstate[1]),
        .I3(\accum_reg[7] [6]),
        .O(S[3]));
  LUT4 #(
    .INIT(16'h59A6)) 
    __5_carry__0_i_2
       (.I0(Q[5]),
        .I1(currstate[1]),
        .I2(currstate[0]),
        .I3(\accum_reg[7] [5]),
        .O(S[2]));
  LUT4 #(
    .INIT(16'h59A6)) 
    __5_carry__0_i_3
       (.I0(Q[4]),
        .I1(currstate[1]),
        .I2(currstate[0]),
        .I3(\accum_reg[7] [4]),
        .O(S[1]));
  LUT4 #(
    .INIT(16'h59A6)) 
    __5_carry__0_i_4
       (.I0(Q[3]),
        .I1(currstate[1]),
        .I2(currstate[0]),
        .I3(\accum_reg[7] [3]),
        .O(S[0]));
  LUT2 #(
    .INIT(4'h2)) 
    __5_carry_i_1
       (.I0(currstate[1]),
        .I1(currstate[0]),
        .O(DI));
  LUT4 #(
    .INIT(16'h59A6)) 
    __5_carry_i_2
       (.I0(Q[2]),
        .I1(currstate[1]),
        .I2(currstate[0]),
        .I3(\accum_reg[7] [2]),
        .O(\accum_reg[3] [2]));
  LUT4 #(
    .INIT(16'h59A6)) 
    __5_carry_i_3
       (.I0(Q[1]),
        .I1(currstate[1]),
        .I2(currstate[0]),
        .I3(\accum_reg[7] [1]),
        .O(\accum_reg[3] [1]));
  LUT4 #(
    .INIT(16'h59A6)) 
    __5_carry_i_4
       (.I0(Q[0]),
        .I1(currstate[1]),
        .I2(currstate[0]),
        .I3(\accum_reg[7] [0]),
        .O(\accum_reg[3] [0]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \accum[7]_i_1 
       (.I0(currstate[1]),
        .I1(\currstate_reg[2]_0 ),
        .O(E));
  LUT6 #(
    .INIT(64'h00000000EEEEEEE2)) 
    \currstate[0]_i_1 
       (.I0(currstate[0]),
        .I1(currstate0),
        .I2(\currstate_reg[2]_0 ),
        .I3(instr[0]),
        .I4(currstate[1]),
        .I5(btnCreg),
        .O(\currstate[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \currstate[1]_i_1 
       (.I0(\currstate[1]_i_2_n_0 ),
        .I1(btnCreg),
        .O(\currstate[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000010100)) 
    \currstate[1]_i_2 
       (.I0(currstate[1]),
        .I1(currstate[0]),
        .I2(instr[2]),
        .I3(instr[1]),
        .I4(instr[0]),
        .I5(\currstate_reg[2]_0 ),
        .O(\currstate[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00002262)) 
    \currstate[2]_i_1 
       (.I0(\currstate_reg[2]_0 ),
        .I1(currstate0),
        .I2(instr[2]),
        .I3(currstate[1]),
        .I4(btnCreg),
        .O(\currstate[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFAAABABBE)) 
    \currstate[2]_i_2 
       (.I0(currstate[1]),
        .I1(currstate[0]),
        .I2(instr[2]),
        .I3(instr[1]),
        .I4(instr[0]),
        .I5(\currstate_reg[2]_0 ),
        .O(currstate0));
  FDRE #(
    .INIT(1'b0)) 
    \currstate_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\currstate[0]_i_1_n_0 ),
        .Q(currstate[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \currstate_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\currstate[1]_i_1_n_0 ),
        .Q(currstate[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \currstate_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\currstate[2]_i_1_n_0 ),
        .Q(\currstate_reg[2]_0 ),
        .R(1'b0));
endmodule

module datapath
   (Q,
    \seg[3] ,
    \seg[6] ,
    \seg[0] ,
    \seg[1] ,
    \seg[5] ,
    \seg[3]_0 ,
    \seg[6]_0 ,
    \seg[0]_0 ,
    \seg[1]_0 ,
    \seg[5]_0 ,
    \seg[2] ,
    \seg[4] ,
    \seg[4]_0 ,
    \seg[2]_0 ,
    DI,
    \sw_reg_reg[3] ,
    S,
    \sw_reg_reg[7] ,
    \currstate_reg[2] ,
    btnCreg,
    E,
    clk_IBUF_BUFG);
  output [6:0]Q;
  output \seg[3] ;
  output \seg[6] ;
  output \seg[0] ;
  output \seg[1] ;
  output \seg[5] ;
  output \seg[3]_0 ;
  output \seg[6]_0 ;
  output \seg[0]_0 ;
  output \seg[1]_0 ;
  output \seg[5]_0 ;
  output \seg[2] ;
  output \seg[4] ;
  output \seg[4]_0 ;
  output \seg[2]_0 ;
  input [0:0]DI;
  input [2:0]\sw_reg_reg[3] ;
  input [3:0]S;
  input [7:0]\sw_reg_reg[7] ;
  input [0:0]\currstate_reg[2] ;
  input btnCreg;
  input [0:0]E;
  input clk_IBUF_BUFG;

  wire [0:0]DI;
  wire [0:0]E;
  wire [6:0]Q;
  wire [3:0]S;
  wire __5_carry__0_n_1;
  wire __5_carry__0_n_2;
  wire __5_carry__0_n_3;
  wire __5_carry_i_5_n_0;
  wire __5_carry_n_0;
  wire __5_carry_n_1;
  wire __5_carry_n_2;
  wire __5_carry_n_3;
  wire \accum_reg_n_0_[0] ;
  wire [7:0]addsubsg;
  wire btnCreg;
  wire clk_IBUF_BUFG;
  wire [0:0]\currstate_reg[2] ;
  wire [7:0]res_alu;
  wire \seg[0] ;
  wire \seg[0]_0 ;
  wire \seg[1] ;
  wire \seg[1]_0 ;
  wire \seg[2] ;
  wire \seg[2]_0 ;
  wire \seg[3] ;
  wire \seg[3]_0 ;
  wire \seg[4] ;
  wire \seg[4]_0 ;
  wire \seg[5] ;
  wire \seg[5]_0 ;
  wire \seg[6] ;
  wire \seg[6]_0 ;
  wire [2:0]\sw_reg_reg[3] ;
  wire [7:0]\sw_reg_reg[7] ;
  wire [3:3]NLW___5_carry__0_CO_UNCONNECTED;

  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 __5_carry
       (.CI(1'b0),
        .CO({__5_carry_n_0,__5_carry_n_1,__5_carry_n_2,__5_carry_n_3}),
        .CYINIT(\accum_reg_n_0_[0] ),
        .DI({Q[2:0],DI}),
        .O(addsubsg[3:0]),
        .S({\sw_reg_reg[3] ,__5_carry_i_5_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 __5_carry__0
       (.CI(__5_carry_n_0),
        .CO({NLW___5_carry__0_CO_UNCONNECTED[3],__5_carry__0_n_1,__5_carry__0_n_2,__5_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,Q[5:3]}),
        .O(addsubsg[7:4]),
        .S(S));
  LUT1 #(
    .INIT(2'h2)) 
    __5_carry_i_5
       (.I0(\sw_reg_reg[7] [0]),
        .O(__5_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h8F80)) 
    \accum[0]_i_1 
       (.I0(\accum_reg_n_0_[0] ),
        .I1(\sw_reg_reg[7] [0]),
        .I2(\currstate_reg[2] ),
        .I3(addsubsg[0]),
        .O(res_alu[0]));
  LUT4 #(
    .INIT(16'h8F80)) 
    \accum[1]_i_1 
       (.I0(Q[0]),
        .I1(\sw_reg_reg[7] [1]),
        .I2(\currstate_reg[2] ),
        .I3(addsubsg[1]),
        .O(res_alu[1]));
  LUT4 #(
    .INIT(16'h8F80)) 
    \accum[2]_i_1 
       (.I0(Q[1]),
        .I1(\sw_reg_reg[7] [2]),
        .I2(\currstate_reg[2] ),
        .I3(addsubsg[2]),
        .O(res_alu[2]));
  LUT4 #(
    .INIT(16'h8F80)) 
    \accum[3]_i_1 
       (.I0(Q[2]),
        .I1(\sw_reg_reg[7] [3]),
        .I2(\currstate_reg[2] ),
        .I3(addsubsg[3]),
        .O(res_alu[3]));
  LUT4 #(
    .INIT(16'h8F80)) 
    \accum[4]_i_1 
       (.I0(Q[3]),
        .I1(\sw_reg_reg[7] [4]),
        .I2(\currstate_reg[2] ),
        .I3(addsubsg[4]),
        .O(res_alu[4]));
  LUT4 #(
    .INIT(16'h8F80)) 
    \accum[5]_i_1 
       (.I0(Q[4]),
        .I1(\sw_reg_reg[7] [5]),
        .I2(\currstate_reg[2] ),
        .I3(addsubsg[5]),
        .O(res_alu[5]));
  LUT4 #(
    .INIT(16'h8F80)) 
    \accum[6]_i_1 
       (.I0(Q[5]),
        .I1(\sw_reg_reg[7] [6]),
        .I2(\currstate_reg[2] ),
        .I3(addsubsg[6]),
        .O(res_alu[6]));
  LUT4 #(
    .INIT(16'h8F80)) 
    \accum[7]_i_2 
       (.I0(Q[6]),
        .I1(\sw_reg_reg[7] [7]),
        .I2(\currstate_reg[2] ),
        .I3(addsubsg[7]),
        .O(res_alu[7]));
  FDRE #(
    .INIT(1'b0)) 
    \accum_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .D(res_alu[0]),
        .Q(\accum_reg_n_0_[0] ),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \accum_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .D(res_alu[1]),
        .Q(Q[0]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \accum_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .D(res_alu[2]),
        .Q(Q[1]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \accum_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .D(res_alu[3]),
        .Q(Q[2]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \accum_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .D(res_alu[4]),
        .Q(Q[3]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \accum_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .D(res_alu[5]),
        .Q(Q[4]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \accum_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .D(res_alu[6]),
        .Q(Q[5]),
        .R(btnCreg));
  FDRE #(
    .INIT(1'b0)) 
    \accum_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(E),
        .D(res_alu[7]),
        .Q(Q[6]),
        .R(btnCreg));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h2094)) 
    \seg_OBUF[0]_inst_i_4 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(\accum_reg_n_0_[0] ),
        .I3(Q[0]),
        .O(\seg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h2094)) 
    \seg_OBUF[0]_inst_i_5 
       (.I0(Q[6]),
        .I1(Q[5]),
        .I2(Q[3]),
        .I3(Q[4]),
        .O(\seg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h9E80)) 
    \seg_OBUF[1]_inst_i_4 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(\accum_reg_n_0_[0] ),
        .I3(Q[1]),
        .O(\seg[1]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hB680)) 
    \seg_OBUF[1]_inst_i_5 
       (.I0(Q[6]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[5]),
        .O(\seg[1] ));
  LUT4 #(
    .INIT(16'hA210)) 
    \seg_OBUF[2]_inst_i_4 
       (.I0(Q[2]),
        .I1(\accum_reg_n_0_[0] ),
        .I2(Q[0]),
        .I3(Q[1]),
        .O(\seg[2]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hA210)) 
    \seg_OBUF[2]_inst_i_5 
       (.I0(Q[6]),
        .I1(Q[3]),
        .I2(Q[4]),
        .I3(Q[5]),
        .O(\seg[2] ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hC214)) 
    \seg_OBUF[3]_inst_i_4 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(\accum_reg_n_0_[0] ),
        .I3(Q[0]),
        .O(\seg[3]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hC214)) 
    \seg_OBUF[3]_inst_i_5 
       (.I0(Q[6]),
        .I1(Q[5]),
        .I2(Q[3]),
        .I3(Q[4]),
        .O(\seg[3] ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h5710)) 
    \seg_OBUF[4]_inst_i_4 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\accum_reg_n_0_[0] ),
        .O(\seg[4]_0 ));
  LUT4 #(
    .INIT(16'h5710)) 
    \seg_OBUF[4]_inst_i_5 
       (.I0(Q[6]),
        .I1(Q[4]),
        .I2(Q[5]),
        .I3(Q[3]),
        .O(\seg[4] ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h5190)) 
    \seg_OBUF[5]_inst_i_4 
       (.I0(Q[2]),
        .I1(Q[1]),
        .I2(\accum_reg_n_0_[0] ),
        .I3(Q[0]),
        .O(\seg[5]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h5910)) 
    \seg_OBUF[5]_inst_i_5 
       (.I0(Q[6]),
        .I1(Q[5]),
        .I2(Q[4]),
        .I3(Q[3]),
        .O(\seg[5] ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h4025)) 
    \seg_OBUF[6]_inst_i_4 
       (.I0(Q[2]),
        .I1(\accum_reg_n_0_[0] ),
        .I2(Q[1]),
        .I3(Q[0]),
        .O(\seg[6]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h4025)) 
    \seg_OBUF[6]_inst_i_5 
       (.I0(Q[6]),
        .I1(Q[3]),
        .I2(Q[5]),
        .I3(Q[4]),
        .O(\seg[6] ));
endmodule

module disp7
   (seg_OBUF,
    an_OBUF,
    dp_OBUF,
    Q,
    \accum_reg[3] ,
    \accum_reg[7] ,
    \accum_reg[7]_0 ,
    \accum_reg[7]_1 ,
    \accum_reg[7]_2 ,
    \accum_reg[7]_3 ,
    \accum_reg[3]_0 ,
    \accum_reg[3]_1 ,
    \accum_reg[3]_2 ,
    \accum_reg[3]_3 ,
    \accum_reg[7]_4 ,
    \accum_reg[7]_5 ,
    \accum_reg[3]_4 ,
    \accum_reg[3]_5 ,
    instr,
    btnDreg,
    CLK);
  output [6:0]seg_OBUF;
  output [3:0]an_OBUF;
  output dp_OBUF;
  input [7:0]Q;
  input \accum_reg[3] ;
  input \accum_reg[7] ;
  input \accum_reg[7]_0 ;
  input \accum_reg[7]_1 ;
  input \accum_reg[7]_2 ;
  input \accum_reg[7]_3 ;
  input \accum_reg[3]_0 ;
  input \accum_reg[3]_1 ;
  input \accum_reg[3]_2 ;
  input \accum_reg[3]_3 ;
  input \accum_reg[7]_4 ;
  input \accum_reg[7]_5 ;
  input \accum_reg[3]_4 ;
  input \accum_reg[3]_5 ;
  input [2:0]instr;
  input btnDreg;
  input CLK;

  wire CLK;
  wire [7:0]Q;
  wire \accum_reg[3] ;
  wire \accum_reg[3]_0 ;
  wire \accum_reg[3]_1 ;
  wire \accum_reg[3]_2 ;
  wire \accum_reg[3]_3 ;
  wire \accum_reg[3]_4 ;
  wire \accum_reg[3]_5 ;
  wire \accum_reg[7] ;
  wire \accum_reg[7]_0 ;
  wire \accum_reg[7]_1 ;
  wire \accum_reg[7]_2 ;
  wire \accum_reg[7]_3 ;
  wire \accum_reg[7]_4 ;
  wire \accum_reg[7]_5 ;
  wire [3:0]an_OBUF;
  wire btnDreg;
  wire dp_OBUF;
  wire [2:0]instr;
  wire [1:0]ndisp;
  wire [1:0]plusOp;
  wire [6:0]seg_OBUF;
  wire \seg_OBUF[0]_inst_i_2_n_0 ;
  wire \seg_OBUF[0]_inst_i_3_n_0 ;
  wire \seg_OBUF[1]_inst_i_2_n_0 ;
  wire \seg_OBUF[1]_inst_i_3_n_0 ;
  wire \seg_OBUF[2]_inst_i_2_n_0 ;
  wire \seg_OBUF[2]_inst_i_3_n_0 ;
  wire \seg_OBUF[3]_inst_i_2_n_0 ;
  wire \seg_OBUF[3]_inst_i_3_n_0 ;
  wire \seg_OBUF[4]_inst_i_2_n_0 ;
  wire \seg_OBUF[4]_inst_i_3_n_0 ;
  wire \seg_OBUF[5]_inst_i_2_n_0 ;
  wire \seg_OBUF[5]_inst_i_3_n_0 ;
  wire \seg_OBUF[6]_inst_i_2_n_0 ;
  wire \seg_OBUF[6]_inst_i_3_n_0 ;

  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \an_OBUF[0]_inst_i_1 
       (.I0(ndisp[1]),
        .I1(ndisp[0]),
        .O(an_OBUF[0]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \an_OBUF[1]_inst_i_1 
       (.I0(ndisp[1]),
        .I1(ndisp[0]),
        .O(an_OBUF[1]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \an_OBUF[2]_inst_i_1 
       (.I0(ndisp[0]),
        .I1(ndisp[1]),
        .O(an_OBUF[2]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \an_OBUF[3]_inst_i_1 
       (.I0(ndisp[1]),
        .I1(ndisp[0]),
        .O(an_OBUF[3]));
  LUT6 #(
    .INIT(64'h505F3030505F3F3F)) 
    dp_OBUF_inst_i_1
       (.I0(instr[1]),
        .I1(instr[0]),
        .I2(ndisp[0]),
        .I3(btnDreg),
        .I4(ndisp[1]),
        .I5(instr[2]),
        .O(dp_OBUF));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \ndisp[0]_i_1 
       (.I0(ndisp[0]),
        .O(plusOp[0]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \ndisp[1]_i_1 
       (.I0(ndisp[0]),
        .I1(ndisp[1]),
        .O(plusOp[1]));
  FDRE #(
    .INIT(1'b0)) 
    \ndisp_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(plusOp[0]),
        .Q(ndisp[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ndisp_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(plusOp[1]),
        .Q(ndisp[1]),
        .R(1'b0));
  MUXF7 \seg_OBUF[0]_inst_i_1 
       (.I0(\seg_OBUF[0]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[0]_inst_i_3_n_0 ),
        .O(seg_OBUF[0]),
        .S(ndisp[0]));
  LUT6 #(
    .INIT(64'h2094FFFF20940000)) 
    \seg_OBUF[0]_inst_i_2 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(ndisp[1]),
        .I5(\accum_reg[3]_1 ),
        .O(\seg_OBUF[0]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h2094FFFF20940000)) 
    \seg_OBUF[0]_inst_i_3 
       (.I0(Q[7]),
        .I1(Q[6]),
        .I2(Q[4]),
        .I3(Q[5]),
        .I4(ndisp[1]),
        .I5(\accum_reg[7]_1 ),
        .O(\seg_OBUF[0]_inst_i_3_n_0 ));
  MUXF7 \seg_OBUF[1]_inst_i_1 
       (.I0(\seg_OBUF[1]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[1]_inst_i_3_n_0 ),
        .O(seg_OBUF[1]),
        .S(ndisp[0]));
  LUT6 #(
    .INIT(64'hB680FFFFB6800000)) 
    \seg_OBUF[1]_inst_i_2 
       (.I0(Q[3]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(ndisp[1]),
        .I5(\accum_reg[3]_2 ),
        .O(\seg_OBUF[1]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hB680FFFFB6800000)) 
    \seg_OBUF[1]_inst_i_3 
       (.I0(Q[7]),
        .I1(Q[4]),
        .I2(Q[5]),
        .I3(Q[6]),
        .I4(ndisp[1]),
        .I5(\accum_reg[7]_2 ),
        .O(\seg_OBUF[1]_inst_i_3_n_0 ));
  MUXF7 \seg_OBUF[2]_inst_i_1 
       (.I0(\seg_OBUF[2]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[2]_inst_i_3_n_0 ),
        .O(seg_OBUF[2]),
        .S(ndisp[0]));
  LUT6 #(
    .INIT(64'hA210FFFFA2100000)) 
    \seg_OBUF[2]_inst_i_2 
       (.I0(Q[3]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(Q[2]),
        .I4(ndisp[1]),
        .I5(\accum_reg[3]_4 ),
        .O(\seg_OBUF[2]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hA210FFFFA2100000)) 
    \seg_OBUF[2]_inst_i_3 
       (.I0(Q[7]),
        .I1(Q[4]),
        .I2(Q[5]),
        .I3(Q[6]),
        .I4(ndisp[1]),
        .I5(\accum_reg[7]_4 ),
        .O(\seg_OBUF[2]_inst_i_3_n_0 ));
  MUXF7 \seg_OBUF[3]_inst_i_1 
       (.I0(\seg_OBUF[3]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[3]_inst_i_3_n_0 ),
        .O(seg_OBUF[3]),
        .S(ndisp[0]));
  LUT6 #(
    .INIT(64'hC214FFFFC2140000)) 
    \seg_OBUF[3]_inst_i_2 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(ndisp[1]),
        .I5(\accum_reg[3] ),
        .O(\seg_OBUF[3]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hC214FFFFC2140000)) 
    \seg_OBUF[3]_inst_i_3 
       (.I0(Q[7]),
        .I1(Q[6]),
        .I2(Q[4]),
        .I3(Q[5]),
        .I4(ndisp[1]),
        .I5(\accum_reg[7] ),
        .O(\seg_OBUF[3]_inst_i_3_n_0 ));
  MUXF7 \seg_OBUF[4]_inst_i_1 
       (.I0(\seg_OBUF[4]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[4]_inst_i_3_n_0 ),
        .O(seg_OBUF[4]),
        .S(ndisp[0]));
  LUT6 #(
    .INIT(64'h5710FFFF57100000)) 
    \seg_OBUF[4]_inst_i_2 
       (.I0(Q[3]),
        .I1(Q[1]),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(ndisp[1]),
        .I5(\accum_reg[3]_5 ),
        .O(\seg_OBUF[4]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5710FFFF57100000)) 
    \seg_OBUF[4]_inst_i_3 
       (.I0(Q[7]),
        .I1(Q[5]),
        .I2(Q[6]),
        .I3(Q[4]),
        .I4(ndisp[1]),
        .I5(\accum_reg[7]_5 ),
        .O(\seg_OBUF[4]_inst_i_3_n_0 ));
  MUXF7 \seg_OBUF[5]_inst_i_1 
       (.I0(\seg_OBUF[5]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[5]_inst_i_3_n_0 ),
        .O(seg_OBUF[5]),
        .S(ndisp[0]));
  LUT6 #(
    .INIT(64'h5910FFFF59100000)) 
    \seg_OBUF[5]_inst_i_2 
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(ndisp[1]),
        .I5(\accum_reg[3]_3 ),
        .O(\seg_OBUF[5]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5910FFFF59100000)) 
    \seg_OBUF[5]_inst_i_3 
       (.I0(Q[7]),
        .I1(Q[6]),
        .I2(Q[5]),
        .I3(Q[4]),
        .I4(ndisp[1]),
        .I5(\accum_reg[7]_3 ),
        .O(\seg_OBUF[5]_inst_i_3_n_0 ));
  MUXF7 \seg_OBUF[6]_inst_i_1 
       (.I0(\seg_OBUF[6]_inst_i_2_n_0 ),
        .I1(\seg_OBUF[6]_inst_i_3_n_0 ),
        .O(seg_OBUF[6]),
        .S(ndisp[0]));
  LUT6 #(
    .INIT(64'h4025FFFF40250000)) 
    \seg_OBUF[6]_inst_i_2 
       (.I0(Q[3]),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[1]),
        .I4(ndisp[1]),
        .I5(\accum_reg[3]_0 ),
        .O(\seg_OBUF[6]_inst_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h4025FFFF40250000)) 
    \seg_OBUF[6]_inst_i_3 
       (.I0(Q[7]),
        .I1(Q[4]),
        .I2(Q[6]),
        .I3(Q[5]),
        .I4(ndisp[1]),
        .I5(\accum_reg[7]_0 ),
        .O(\seg_OBUF[6]_inst_i_3_n_0 ));
endmodule

(* NotValidForBitStream *)
module fpga_basicIO
   (clk,
    btnC,
    btnU,
    btnL,
    btnR,
    btnD,
    sw,
    led,
    an,
    seg,
    dp);
  input clk;
  input btnC;
  input btnU;
  input btnL;
  input btnR;
  input btnD;
  input [15:0]sw;
  output [15:0]led;
  output [3:0]an;
  output [6:0]seg;
  output dp;

  wire [3:0]an;
  wire [3:0]an_OBUF;
  wire btnC;
  wire btnC_IBUF;
  wire btnCreg;
  wire btnD;
  wire btnD_IBUF;
  wire btnDreg;
  wire btnL;
  wire btnL_IBUF;
  wire btnR;
  wire btnR_IBUF;
  wire btnU;
  wire btnU_IBUF;
  wire clk;
  wire clk10hz;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire dclk;
  wire dp;
  wire dp_OBUF;
  wire inst_circuito_n_0;
  wire inst_circuito_n_1;
  wire inst_circuito_n_10;
  wire inst_circuito_n_11;
  wire inst_circuito_n_12;
  wire inst_circuito_n_13;
  wire inst_circuito_n_2;
  wire inst_circuito_n_3;
  wire inst_circuito_n_4;
  wire inst_circuito_n_5;
  wire inst_circuito_n_6;
  wire inst_circuito_n_7;
  wire inst_circuito_n_8;
  wire inst_circuito_n_9;
  wire [2:0]instr;
  wire [15:0]led;
  wire [15:0]led_OBUF;
  wire [6:0]seg;
  wire [6:0]seg_OBUF;
  wire [15:0]sw;
  wire [15:0]sw_IBUF;

initial begin
 $sdf_annotate("circuito_tb_time_synth.sdf",,,,"tool_control");
end
  OBUF \an_OBUF[0]_inst 
       (.I(an_OBUF[0]),
        .O(an[0]));
  OBUF \an_OBUF[1]_inst 
       (.I(an_OBUF[1]),
        .O(an[1]));
  OBUF \an_OBUF[2]_inst 
       (.I(an_OBUF[2]),
        .O(an[2]));
  OBUF \an_OBUF[3]_inst 
       (.I(an_OBUF[3]),
        .O(an[3]));
  IBUF btnC_IBUF_inst
       (.I(btnC),
        .O(btnC_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    btnCreg_reg
       (.C(clk10hz),
        .CE(1'b1),
        .D(btnC_IBUF),
        .Q(btnCreg),
        .R(1'b0));
  IBUF btnD_IBUF_inst
       (.I(btnD),
        .O(btnD_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    btnDreg_reg
       (.C(clk10hz),
        .CE(1'b1),
        .D(btnD_IBUF),
        .Q(btnDreg),
        .R(1'b0));
  IBUF btnL_IBUF_inst
       (.I(btnL),
        .O(btnL_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    btnLreg_reg
       (.C(clk10hz),
        .CE(1'b1),
        .D(btnL_IBUF),
        .Q(instr[1]),
        .R(1'b0));
  IBUF btnR_IBUF_inst
       (.I(btnR),
        .O(btnR_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    btnRreg_reg
       (.C(clk10hz),
        .CE(1'b1),
        .D(btnR_IBUF),
        .Q(instr[0]),
        .R(1'b0));
  IBUF btnU_IBUF_inst
       (.I(btnU),
        .O(btnU_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    btnUreg_reg
       (.C(clk10hz),
        .CE(1'b1),
        .D(btnU_IBUF),
        .Q(instr[2]),
        .R(1'b0));
  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  OBUF dp_OBUF_inst
       (.I(dp_OBUF),
        .O(dp));
  circuito inst_circuito
       (.Q(led_OBUF[7:0]),
        .btnCreg(btnCreg),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .instr(instr),
        .\seg[0] (inst_circuito_n_2),
        .\seg[0]_0 (inst_circuito_n_7),
        .\seg[1] (inst_circuito_n_3),
        .\seg[1]_0 (inst_circuito_n_8),
        .\seg[2] (inst_circuito_n_10),
        .\seg[2]_0 (inst_circuito_n_13),
        .\seg[3] (inst_circuito_n_0),
        .\seg[3]_0 (inst_circuito_n_5),
        .\seg[4] (inst_circuito_n_11),
        .\seg[4]_0 (inst_circuito_n_12),
        .\seg[5] (inst_circuito_n_4),
        .\seg[5]_0 (inst_circuito_n_9),
        .\seg[6] (inst_circuito_n_1),
        .\seg[6]_0 (inst_circuito_n_6));
  clkdiv inst_clkdiv
       (.CLK(dclk),
        .clk(clk_IBUF_BUFG),
        .clk10hz(clk10hz));
  disp7 inst_disp7
       (.CLK(dclk),
        .Q(led_OBUF[7:0]),
        .\accum_reg[3] (inst_circuito_n_5),
        .\accum_reg[3]_0 (inst_circuito_n_6),
        .\accum_reg[3]_1 (inst_circuito_n_7),
        .\accum_reg[3]_2 (inst_circuito_n_8),
        .\accum_reg[3]_3 (inst_circuito_n_9),
        .\accum_reg[3]_4 (inst_circuito_n_13),
        .\accum_reg[3]_5 (inst_circuito_n_12),
        .\accum_reg[7] (inst_circuito_n_0),
        .\accum_reg[7]_0 (inst_circuito_n_1),
        .\accum_reg[7]_1 (inst_circuito_n_2),
        .\accum_reg[7]_2 (inst_circuito_n_3),
        .\accum_reg[7]_3 (inst_circuito_n_4),
        .\accum_reg[7]_4 (inst_circuito_n_10),
        .\accum_reg[7]_5 (inst_circuito_n_11),
        .an_OBUF(an_OBUF),
        .btnDreg(btnDreg),
        .dp_OBUF(dp_OBUF),
        .instr(instr),
        .seg_OBUF(seg_OBUF));
  OBUF \led_OBUF[0]_inst 
       (.I(led_OBUF[0]),
        .O(led[0]));
  OBUF \led_OBUF[10]_inst 
       (.I(led_OBUF[10]),
        .O(led[10]));
  OBUF \led_OBUF[11]_inst 
       (.I(led_OBUF[11]),
        .O(led[11]));
  OBUF \led_OBUF[12]_inst 
       (.I(led_OBUF[12]),
        .O(led[12]));
  OBUF \led_OBUF[13]_inst 
       (.I(led_OBUF[13]),
        .O(led[13]));
  OBUF \led_OBUF[14]_inst 
       (.I(led_OBUF[14]),
        .O(led[14]));
  OBUF \led_OBUF[15]_inst 
       (.I(led_OBUF[15]),
        .O(led[15]));
  OBUF \led_OBUF[1]_inst 
       (.I(led_OBUF[1]),
        .O(led[1]));
  OBUF \led_OBUF[2]_inst 
       (.I(led_OBUF[2]),
        .O(led[2]));
  OBUF \led_OBUF[3]_inst 
       (.I(led_OBUF[3]),
        .O(led[3]));
  OBUF \led_OBUF[4]_inst 
       (.I(led_OBUF[4]),
        .O(led[4]));
  OBUF \led_OBUF[5]_inst 
       (.I(led_OBUF[5]),
        .O(led[5]));
  OBUF \led_OBUF[6]_inst 
       (.I(led_OBUF[6]),
        .O(led[6]));
  OBUF \led_OBUF[7]_inst 
       (.I(led_OBUF[7]),
        .O(led[7]));
  OBUF \led_OBUF[8]_inst 
       (.I(led_OBUF[8]),
        .O(led[8]));
  OBUF \led_OBUF[9]_inst 
       (.I(led_OBUF[9]),
        .O(led[9]));
  OBUF \seg_OBUF[0]_inst 
       (.I(seg_OBUF[0]),
        .O(seg[0]));
  OBUF \seg_OBUF[1]_inst 
       (.I(seg_OBUF[1]),
        .O(seg[1]));
  OBUF \seg_OBUF[2]_inst 
       (.I(seg_OBUF[2]),
        .O(seg[2]));
  OBUF \seg_OBUF[3]_inst 
       (.I(seg_OBUF[3]),
        .O(seg[3]));
  OBUF \seg_OBUF[4]_inst 
       (.I(seg_OBUF[4]),
        .O(seg[4]));
  OBUF \seg_OBUF[5]_inst 
       (.I(seg_OBUF[5]),
        .O(seg[5]));
  OBUF \seg_OBUF[6]_inst 
       (.I(seg_OBUF[6]),
        .O(seg[6]));
  IBUF \sw_IBUF[0]_inst 
       (.I(sw[0]),
        .O(sw_IBUF[0]));
  IBUF \sw_IBUF[10]_inst 
       (.I(sw[10]),
        .O(sw_IBUF[10]));
  IBUF \sw_IBUF[11]_inst 
       (.I(sw[11]),
        .O(sw_IBUF[11]));
  IBUF \sw_IBUF[12]_inst 
       (.I(sw[12]),
        .O(sw_IBUF[12]));
  IBUF \sw_IBUF[13]_inst 
       (.I(sw[13]),
        .O(sw_IBUF[13]));
  IBUF \sw_IBUF[14]_inst 
       (.I(sw[14]),
        .O(sw_IBUF[14]));
  IBUF \sw_IBUF[15]_inst 
       (.I(sw[15]),
        .O(sw_IBUF[15]));
  IBUF \sw_IBUF[1]_inst 
       (.I(sw[1]),
        .O(sw_IBUF[1]));
  IBUF \sw_IBUF[2]_inst 
       (.I(sw[2]),
        .O(sw_IBUF[2]));
  IBUF \sw_IBUF[3]_inst 
       (.I(sw[3]),
        .O(sw_IBUF[3]));
  IBUF \sw_IBUF[4]_inst 
       (.I(sw[4]),
        .O(sw_IBUF[4]));
  IBUF \sw_IBUF[5]_inst 
       (.I(sw[5]),
        .O(sw_IBUF[5]));
  IBUF \sw_IBUF[6]_inst 
       (.I(sw[6]),
        .O(sw_IBUF[6]));
  IBUF \sw_IBUF[7]_inst 
       (.I(sw[7]),
        .O(sw_IBUF[7]));
  IBUF \sw_IBUF[8]_inst 
       (.I(sw[8]),
        .O(sw_IBUF[8]));
  IBUF \sw_IBUF[9]_inst 
       (.I(sw[9]),
        .O(sw_IBUF[9]));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[0] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[0]),
        .Q(led_OBUF[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[10] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[10]),
        .Q(led_OBUF[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[11] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[11]),
        .Q(led_OBUF[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[12] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[12]),
        .Q(led_OBUF[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[13] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[13]),
        .Q(led_OBUF[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[14] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[14]),
        .Q(led_OBUF[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[15] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[15]),
        .Q(led_OBUF[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[1] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[1]),
        .Q(led_OBUF[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[2] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[2]),
        .Q(led_OBUF[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[3] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[3]),
        .Q(led_OBUF[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[4] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[4]),
        .Q(led_OBUF[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[5] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[5]),
        .Q(led_OBUF[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[6] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[6]),
        .Q(led_OBUF[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[7] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[7]),
        .Q(led_OBUF[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[8] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[8]),
        .Q(led_OBUF[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sw_reg_reg[9] 
       (.C(clk10hz),
        .CE(1'b1),
        .D(sw_IBUF[9]),
        .Q(led_OBUF[9]),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
